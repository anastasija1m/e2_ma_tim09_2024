package com.ma.eventplanner.fragments.employees;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentEmployeeEditWorkScheduleBinding;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EmployeeWorkHours;
import com.ma.eventplanner.model.EmployeeWorkSchedule;

import org.jetbrains.annotations.NotNull;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

public class EmployeeEditWorkScheduleFragment extends Fragment {

    private FragmentEmployeeEditWorkScheduleBinding binding;
    private Employee employee;

    public EmployeeEditWorkScheduleFragment() { }

    public static EmployeeEditWorkScheduleFragment newInstance() {
        return new EmployeeEditWorkScheduleFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeEditWorkScheduleBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindEmployeeDetails();
        binding.btnEditWorkSchedule.setOnClickListener(v -> updateWorkSchedule());
    }

    private void bindEmployeeDetails() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("employee")) {
            employee = bundle.getParcelable("employee");
            if (employee != null) {
                bindBasicDetails();
                bindSchedule();
            }
        }
    }

    private void bindBasicDetails() {
        binding.employeeName.setText(employee.getName());
        binding.employeeSurname.setText(employee.getSurname());
    }

    private void bindSchedule() {
        EmployeeWorkSchedule workSchedule = employee.getWorkSchedule();

        Date startDate = workSchedule.getScheduleStartDate();
        LocalDate startLocalDate = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        binding.scheduleStartDate.init(
                startLocalDate.getYear(),
                startLocalDate.getMonthValue() - 1,
                startLocalDate.getDayOfMonth(),
                null);
        binding.scheduleStartDate.setMinDate(System.currentTimeMillis() - 1000);

        Date endDate = workSchedule.getScheduleEndDate();
        LocalDate endLocalDate = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        binding.scheduleEndDate.init(
                endLocalDate.getYear(),
                endLocalDate.getMonthValue() - 1,
                endLocalDate.getDayOfMonth(),
                null);
        binding.scheduleEndDate.setMinDate(System.currentTimeMillis() - 1000);

        for (DayOfWeek day : DayOfWeek.values()) {
            String dayName = day.toString().toLowerCase();
            dayName = Character.toUpperCase(dayName.charAt(0)) + dayName.substring(1);
            bindWorkHours(day, employee.getWorkSchedule().getWorkHoursMap().get(dayName));
        }
    }

    private void bindWorkHours(DayOfWeek day, EmployeeWorkHours workHours) {
        if (workHours != null) {
            TimePicker startPicker = getStartPickerForDay(day);
            TimePicker endPicker = getEndPickerForDay(day);

            LocalTime startTime = LocalTime.parse(workHours.getStartTime());
            LocalTime endTime = LocalTime.parse(workHours.getEndTime());

            setWorkHours(startPicker, startTime, endPicker, endTime);
        }
    }

    private TimePicker getStartPickerForDay(DayOfWeek day) {
        switch (day) {
            case MONDAY: return binding.mondayStart;
            case TUESDAY: return binding.tuesdayStart;
            case WEDNESDAY: return binding.wednesdayStart;
            case THURSDAY: return binding.thursdayStart;
            case FRIDAY: return binding.fridayStart;
            case SATURDAY: return binding.saturdayStart;
            case SUNDAY: return binding.sundayStart;
            default: return null;
        }
    }

    private TimePicker getEndPickerForDay(DayOfWeek day) {
        switch (day) {
            case MONDAY: return binding.mondayEnd;
            case TUESDAY: return binding.tuesdayEnd;
            case WEDNESDAY: return binding.wednesdayEnd;
            case THURSDAY: return binding.thursdayEnd;
            case FRIDAY: return binding.fridayEnd;
            case SATURDAY: return binding.saturdayEnd;
            case SUNDAY: return binding.sundayEnd;
            default: return null;
        }
    }

    private void setWorkHours(TimePicker startPicker, LocalTime startTime, TimePicker endPicker, LocalTime endTime) {
        startPicker.setHour(startTime.getHour());
        startPicker.setMinute(startTime.getMinute());
        endPicker.setHour(endTime.getHour());
        endPicker.setMinute(endTime.getMinute());
    }

    private void updateWorkSchedule() {
        EmployeeWorkSchedule workSchedule = employee.getWorkSchedule();

        LocalDate startDate = getSelectedDate(binding.scheduleStartDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = getSelectedDate(binding.scheduleEndDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        if (endDate.isBefore(startDate)) {
            showToast("End date cannot be before start date");
            return;
        }

        workSchedule.setScheduleStartDate(java.sql.Date.valueOf(String.valueOf(startDate)));
        workSchedule.setScheduleEndDate(java.sql.Date.valueOf(String.valueOf(endDate)));

        for (DayOfWeek day : DayOfWeek.values()) {
            if (!validateWorkHoursForDay(day)) {
                return;
            }
        }

        for (DayOfWeek day : DayOfWeek.values()) {
            String dayName = day.toString().toLowerCase();
            dayName = Character.toUpperCase(dayName.charAt(0)) + dayName.substring(1);
            updateWorkHoursForDay(day, employee.getWorkSchedule().getWorkHoursMap().get(dayName));
        }

        employee.setWorkSchedule(workSchedule);

        UsersOperations usersOperations = new UsersOperations();
        usersOperations.updateEmployee(employee.getEmail(), employee, new UsersOperations.OnUpdateEmployeeListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Employee work schedule updated successfully");
                Navigation.findNavController(requireView()).navigateUp();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Failed to update employee work schedule: ", e);
            }
        });
    }

    private boolean validateWorkHoursForDay(DayOfWeek day) {
        EmployeeWorkHours workHours = employee.getWorkSchedule().getWorkHoursMap().get(day);
        if (workHours != null) {
            if (!validate(workHours.getStartTime(), workHours.getEndTime())) {
                showToast(day + ": Work hours are not valid");
                return false;
            }
        }
        return true;
    }

    private Boolean validate(String startTime, String endTime) {
        LocalTime start = LocalTime.parse(startTime);
        LocalTime end = LocalTime.parse(endTime);

        return !start.isAfter(end);
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void updateWorkHoursForDay(DayOfWeek day, EmployeeWorkHours workHours) {
        if (workHours != null) {
            updateWorkHours(getStartPickerForDay(day), getEndPickerForDay(day), workHours);
        }
    }

    private void updateWorkHours(TimePicker startPicker, TimePicker endPicker, EmployeeWorkHours workHours) {
        LocalTime startTime = LocalTime.of(startPicker.getHour(), startPicker.getMinute());
        LocalTime endTime = LocalTime.of(endPicker.getHour(), endPicker.getMinute());
        workHours.setStartTime(startTime.toString());
        workHours.setEndTime(endTime.toString());
    }

    private Date getSelectedDate(DatePicker datePicker) {
        int year = datePicker.getYear();
        int month = datePicker.getMonth();
        int day = datePicker.getDayOfMonth();

        LocalDate localDate = LocalDate.of(year, month + 1, day);

        return java.sql.Date.valueOf(String.valueOf(localDate));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
