package com.ma.eventplanner.fragments.employees;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.employee.WeeklyCalendarAdapter;
import com.ma.eventplanner.databinding.FragmentWeeklyCalendarBinding;
import com.ma.eventplanner.model.Employee;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class WeeklyCalendarFragment extends Fragment {

    private FragmentWeeklyCalendarBinding binding;
    private GridView mWeeklyGridView;
    private WeeklyCalendarAdapter mAdapter;
    private Calendar mCurrentDate;
    TextView monthYearTextView;
    private WeekViewModel weekViewModel;
    private Employee mEmployee;
    private String mEmployeeUid;

    public WeeklyCalendarFragment() { }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentWeeklyCalendarBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        weekViewModel = new ViewModelProvider(requireActivity()).get(WeekViewModel.class);

        mWeeklyGridView = view.findViewById(R.id.weeklyGridView);

        mCurrentDate = Calendar.getInstance();
        mAdapter = new WeeklyCalendarAdapter(requireContext(), generateWeekDates(), mEmployee, mEmployeeUid);
        mWeeklyGridView.setAdapter(mAdapter);

        monthYearTextView = view.findViewById(R.id.monthYearTextView);

        Button previousWeekButton = view.findViewById(R.id.previousWeekButton);
        Button nextWeekButton = view.findViewById(R.id.nextWeekButton);

        previousWeekButton.setOnClickListener(v -> {
            mCurrentDate.add(Calendar.WEEK_OF_YEAR, -1);
            mAdapter.setWeekDates(generateWeekDates());
            mAdapter.notifyDataSetChanged();
            updateMonthYearText();

            weekViewModel.setSelectedWeek(mCurrentDate);
        });

        nextWeekButton.setOnClickListener(v -> {
            mCurrentDate.add(Calendar.WEEK_OF_YEAR, 1);
            mAdapter.setWeekDates(generateWeekDates());
            mAdapter.notifyDataSetChanged();
            updateMonthYearText();

            weekViewModel.setSelectedWeek(mCurrentDate);
        });

        updateMonthYearText();
    }

    public void setEmployee(Employee employee, String employeeUid) {
        this.mEmployee = employee;
        this.mEmployeeUid = employeeUid;
    }

    private List<Date> generateWeekDates() {
        List<Date> weekDates = new ArrayList<>();
        Calendar calendar = (Calendar) mCurrentDate.clone();

        calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);

        for (int i = 0; i < 7; i++) {
            weekDates.add(calendar.getTime());
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());

        return weekDates;
    }

    private void updateMonthYearText() {
        Calendar firstDayOfWeek = (Calendar) mCurrentDate.clone();
        firstDayOfWeek.set(Calendar.DAY_OF_WEEK, firstDayOfWeek.getFirstDayOfWeek());

        Calendar lastDayOfWeek = (Calendar) mCurrentDate.clone();
        lastDayOfWeek.set(Calendar.DAY_OF_WEEK, lastDayOfWeek.getFirstDayOfWeek() + 6);

        SimpleDateFormat monthYearFormat = new SimpleDateFormat("MMMM yyyy", Locale.getDefault());

        String monthYearText;
        if (firstDayOfWeek.get(Calendar.MONTH) != lastDayOfWeek.get(Calendar.MONTH)) {
            String firstMonth = monthYearFormat.format(firstDayOfWeek.getTime());
            String lastMonth = monthYearFormat.format(lastDayOfWeek.getTime());
            monthYearText = firstMonth + " - " + lastMonth;
        } else {
            monthYearText = monthYearFormat.format(firstDayOfWeek.getTime());
        }

        monthYearTextView = requireView().findViewById(R.id.monthYearTextView);
        monthYearTextView.setText(monthYearText);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
