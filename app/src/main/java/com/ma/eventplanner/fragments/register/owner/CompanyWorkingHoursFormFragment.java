package com.ma.eventplanner.fragments.register.owner;

import static android.content.ContentValues.TAG;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.LoginActivity;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.RegistrationRequestsOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.RegistrationRequest;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.User;
import com.ma.eventplanner.model.WeeklyWorkingSchedule;
import com.ma.eventplanner.model.WorkDays;
import com.ma.eventplanner.model.WorkHours;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

public class CompanyWorkingHoursFormFragment extends Fragment {

    private CheckBox[] checkboxes;
    private Button[] startButtons, endButtons;
    private int hour, minute;
    private RegistrationRequestsOperations registrationRequestsOptions;
    private UsersOperations usersOperations;
    private NotificationsOperations notificationsOperations;


    public CompanyWorkingHoursFormFragment() {
    }

    @NonNull
    public static CompanyWorkingHoursFormFragment newInstance(String param1, String param2) {
        CompanyWorkingHoursFormFragment fragment = new CompanyWorkingHoursFormFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_working_hours_form, container, false);
        registrationRequestsOptions = new RegistrationRequestsOperations();
        usersOperations = new UsersOperations();
        notificationsOperations = new NotificationsOperations();
        initializeViews(rootView);
        setupListeners();
        return rootView;
    }

    private void initializeViews(@NonNull View rootView) {
        checkboxes = new CheckBox[]{
                rootView.findViewById(R.id.checkbox_monday),
                rootView.findViewById(R.id.checkbox_tuesday),
                rootView.findViewById(R.id.checkbox_wednesday),
                rootView.findViewById(R.id.checkbox_thursday),
                rootView.findViewById(R.id.checkbox_friday),
                rootView.findViewById(R.id.checkbox_saturday),
                rootView.findViewById(R.id.checkbox_sunday)
        };

        startButtons = new Button[]{
                rootView.findViewById(R.id.btn_monday_start),
                rootView.findViewById(R.id.btn_tuesday_start),
                rootView.findViewById(R.id.btn_wednesday_start),
                rootView.findViewById(R.id.btn_thursday_start),
                rootView.findViewById(R.id.btn_friday_start),
                rootView.findViewById(R.id.btn_saturday_start),
                rootView.findViewById(R.id.btn_sunday_start)
        };

        endButtons = new Button[]{
                rootView.findViewById(R.id.btn_monday_end),
                rootView.findViewById(R.id.btn_tuesday_end),
                rootView.findViewById(R.id.btn_wednesday_end),
                rootView.findViewById(R.id.btn_thursday_end),
                rootView.findViewById(R.id.btn_friday_end),
                rootView.findViewById(R.id.btn_saturday_end),
                rootView.findViewById(R.id.btn_sunday_end)
        };
    }

    private void setupListeners() {
        for (int i = 0; i < checkboxes.length; i++) {
            int finalI = i;
            checkboxes[i].setOnCheckedChangeListener((buttonView, isChecked) -> updateButtonState(finalI));
            updateButtonState(i);
            setupButtonClickListeners(finalI);
        }
    }

    private void setupButtonClickListeners(int index) {
        startButtons[index].setOnClickListener(v -> {
            if (checkboxes[index].isChecked()) {
                popTimePicker(startButtons[index]);
            }
        });

        endButtons[index].setOnClickListener(v -> {
            if (checkboxes[index].isChecked()) {
                popTimePicker(endButtons[index]);
            }
        });
    }

    private void updateButtonState(int index) {
        boolean isEnabled = checkboxes[index].isChecked();
        startButtons[index].setEnabled(isEnabled);
        endButtons[index].setEnabled(isEnabled);
    }

    private void popTimePicker(Button button) {
        TimePickerDialog.OnTimeSetListener onTimeSetListener = (view, selectedHour, selectedMinute) -> {
            hour = selectedHour;
            minute = selectedMinute;
            button.setText(String.format(Locale.getDefault(), "%02d:%02d", hour, minute));
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(
                getContext(), onTimeSetListener, hour, minute, true);
        timePickerDialog.setTitle("Select time");
        timePickerDialog.show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button sendRequestButton = view.findViewById(R.id.send_request_button);
        sendRequestButton.setOnClickListener(v -> createRegistrationRequest());
    }

    private void createRegistrationRequest() {
        Bundle currentArguments = getArguments();

        if (currentArguments == null) {
            return;
        }

        WeeklyWorkingSchedule weeklySchedule = createWeeklySchedule();
        Owner owner = currentArguments.getParcelable("owner");
        Company company = currentArguments.getParcelable("company");
        Intent profileImage = currentArguments.getParcelable("profileImage");
        Intent companyImage = currentArguments.getParcelable("companyImage");
        assert company != null;
        company.setWeeklyWorkingSchedule(weeklySchedule);

        RegistrationRequest registrationRequest = new RegistrationRequest("", owner, company, RequestStatus.ON_PENDING, new Date());

        registrationRequestsOptions.save(registrationRequest, profileImage, companyImage,  new RegistrationRequestsOperations.OnSaveSuccessListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Registration request saved with document");
                sendNotificationToAdmins(registrationRequest);
                Toast.makeText(getContext(), "Registration request has been sent", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error saving registration request", e);
                Toast.makeText(getContext(), "Failed to send registration request", Toast.LENGTH_SHORT).show();
            }
        });



    }

    private WeeklyWorkingSchedule createWeeklySchedule() {
        WeeklyWorkingSchedule weeklySchedule = new WeeklyWorkingSchedule();
        for (int i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i].isChecked()) {
                DayOfWeek dayOfWeek = getDayOfWeekFromIndex(i);
                String startTimeText = startButtons[i].getText().toString();
                String endTimeText = endButtons[i].getText().toString();

                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
                Date startTime = null;
                Date endTime = null;

                try {
                    startTime = formatter.parse(startTimeText);
                    endTime = formatter.parse(endTimeText);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                WorkHours workHours = new WorkHours(startTime, endTime);

                WorkDays workDay = new WorkDays(dayOfWeek, workHours);

                switch (Objects.requireNonNull(dayOfWeek)) {
                    case MONDAY:
                        weeklySchedule.setMonday(workDay);
                        break;
                    case TUESDAY:
                        weeklySchedule.setTuesday(workDay);
                        break;
                    case WEDNESDAY:
                        weeklySchedule.setWednesday(workDay);
                        break;
                    case THURSDAY:
                        weeklySchedule.setThursday(workDay);
                        break;
                    case FRIDAY:
                        weeklySchedule.setFriday(workDay);
                        break;
                    case SATURDAY:
                        weeklySchedule.setSaturday(workDay);
                        break;
                    case SUNDAY:
                        weeklySchedule.setSunday(workDay);
                        break;
                }
            }
        }
        return weeklySchedule;
    }

    private DayOfWeek getDayOfWeekFromIndex(int index) {
        switch (index) {
            case 0:
                return DayOfWeek.MONDAY;
            case 1:
                return DayOfWeek.TUESDAY;
            case 2:
                return DayOfWeek.WEDNESDAY;
            case 3:
                return DayOfWeek.THURSDAY;
            case 4:
                return DayOfWeek.FRIDAY;
            case 5:
                return DayOfWeek.SATURDAY;
            case 6:
                return DayOfWeek.SUNDAY;
            default:
                return null;
        }
    }

    private void sendNotificationToAdmins(RegistrationRequest registrationRequest) {
        usersOperations.getAllByRole(Role.ADMIN, new UsersOperations.OnGetAllByRoleListener() {
            @Override
            public void onSuccess(HashMap<String, User> userMap) {
                for (String adminId : userMap.keySet()) {
                    Notification notification = createNotificationForAdmin(adminId, registrationRequest);
                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Notification sent to admin: " + adminId);
                            startLoginActivity();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error sending notification to admin: " + adminId, e);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting admins", e);
            }
        });
    }

    private Notification createNotificationForAdmin(String adminId, RegistrationRequest registrationRequest) {
        String title = "Registration request";
        String content = String.format("%s %s wants to register his company %s.",
                registrationRequest.getOwner().getFirstName(),
                registrationRequest.getOwner().getLastName(),
                registrationRequest.getCompany().getName());

        return new Notification("", title, content, adminId);
    }

    private void startLoginActivity() {
        startActivity(new Intent(requireActivity(), LoginActivity.class));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
