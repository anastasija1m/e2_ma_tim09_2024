package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CompanyReportOperations;
import com.ma.eventplanner.model.CompanyReport;

import java.util.ArrayList;
import java.util.List;

public class ReportsFragment extends Fragment {

    private List<CompanyReport> reportList = new ArrayList<>();
    private CompanyReportOperations reportsOperations;
    private RecyclerViewAdapter adapter;

    public ReportsFragment() {
        // Required empty public constructor
    }

    public static ReportsFragment newInstance() {
        return new ReportsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reports, container, false);

        reportsOperations = new CompanyReportOperations();
        // Change the layout manager to LinearLayoutManager with vertical orientation

        // Fetch reports from database
        fetchReports(rootView);

        return rootView;
    }

    private void fetchReports(View rootView) {
        // Assuming you have a method to fetch reports from the database
        // Implement the method based on your database operations
        // For example:
        reportsOperations.getAll(new CompanyReportOperations.GetAllCompanyReportsListener<CompanyReport>() {
            @Override
            public void onSuccess(List<CompanyReport> result) {
                reportList = result;
                adapter = new RecyclerViewAdapter(reportList);
                RecyclerView recyclerView = rootView.findViewById(R.id.recyclerViewReports);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
                adapter = new RecyclerViewAdapter(reportList);
                RecyclerView recyclerView = rootView.findViewById(R.id.recyclerViewReports);
                recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                recyclerView.setAdapter(adapter);
            }
        });
    }


    // RecyclerViewAdapter class to bind data to RecyclerView
    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

        private List<CompanyReport> reportList;

        RecyclerViewAdapter(List<CompanyReport> reportList) {
            this.reportList = reportList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_report, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            CompanyReport report = reportList.get(position);
            holder.bind(report);
        }

        @Override
        public int getItemCount() {
            return reportList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {

            private TextView textViewReporterId;
            private TextView textViewReportedCompanyId;
            private TextView textViewReportDate;
            private TextView textViewReportReason;
            private TextView textViewStatus;

            ViewHolder(View itemView) {
                super(itemView);
                // Initialize your views here
                textViewReporterId = itemView.findViewById(R.id.textViewReporterId);
                textViewReportedCompanyId = itemView.findViewById(R.id.textViewReportedCompanyId);
                textViewReportDate = itemView.findViewById(R.id.textViewReportDate);
                textViewReportReason = itemView.findViewById(R.id.textViewReportReason);
                textViewStatus = itemView.findViewById(R.id.textViewStatus);
            }

            void bind(CompanyReport report) {
                // Bind report data to views
                textViewReporterId.setText("Reporter: " + report.getReporterId());
                textViewReportedCompanyId.setText("Reported Company ID: " + report.getReportedCompanyId());
                textViewReportDate.setText("Report Date: " + report.getReportDate().toString());
                textViewReportReason.setText("Report Reason: " + report.getReportReason());
                textViewStatus.setText("Status: " + report.getStatus().toString());
            }
        }
    }
}
