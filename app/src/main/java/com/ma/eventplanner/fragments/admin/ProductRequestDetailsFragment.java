package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.ProductRequestAdapter;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.ProductRequestsOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.ProductRequest;
import com.ma.eventplanner.model.RegistrationRequest;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductRequestDetailsFragment extends Fragment {
    private ProductRequest productRequest;
    private List<Subcategory> subcategories;
    private ProductRequestsOperations productRequestsOperations;
    private SubcategoriesOperations subcategoriesOperations;
    private NotificationsOperations notificationsOperations;
    private ProductsOperations productsOperations;
    private String productRequestId;
    private TextView productNameTextView;
    private TextView productCategoryTextView;
    private TextView productPriceTextView;
    private TextView productDiscountTextView;
    private TextView productDescriptionTextView;
    private TextView subcategoryNameEditText;
    private TextView descriptionEditText;
    private CheckBox pickExistingSubcategoryCheckBox;
    private Spinner existingSubcategorySpinner;

    public ProductRequestDetailsFragment() {
    }

    public static ProductRequestDetailsFragment newInstance() {
        ProductRequestDetailsFragment fragment = new ProductRequestDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle currentArguments = getArguments();

        if (currentArguments == null) {
            return;
        }


        productRequestId = currentArguments.getString("selected_request_id");
        productRequestsOperations = new ProductRequestsOperations();
        subcategoriesOperations = new SubcategoriesOperations();
        notificationsOperations = new NotificationsOperations();
        productsOperations = new ProductsOperations();
        subcategories = new ArrayList<>();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_request_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadProductRequest(new ProductRequestsOperations.GetProductRequestByIdListener<ProductRequest>() {
            @Override
            public void onSuccess(ProductRequest result) {
                productRequest = result;

                productNameTextView = view.findViewById(R.id.product_name_details);
                productCategoryTextView = view.findViewById(R.id.product_category_name_details);
                productPriceTextView = view.findViewById(R.id.product_price_details);
                productDiscountTextView = view.findViewById(R.id.product_discount_details);
                productDescriptionTextView = view.findViewById(R.id.product_description_details);
                subcategoryNameEditText = view.findViewById(R.id.product_subcategory_name);
                descriptionEditText = view.findViewById(R.id.product_description);
                //Spinner typeSpinner = view.findViewById(R.id.requested_subcategory_type_spinner);

                productNameTextView.setText(" " + productRequest.getProduct().getName());
                productCategoryTextView.setText(" " + productRequest.getProduct().getCategory().getName());
                productPriceTextView.setText(" " + productRequest.getProduct().getPrice());
                productDiscountTextView.setText(" " + productRequest.getProduct().getDiscount());
                productDescriptionTextView.setText(" " + productRequest.getProduct().getDescription());
                subcategoryNameEditText.setText(productRequest.getRequestedSubcategory().getName());
                descriptionEditText.setText(productRequest.getRequestedSubcategory().getDescription());

                /*
                if(productRequest.getRequestedSubcategory().getType() == SubcategoryType.PRODUCT) {
                    typeSpinner.setSelection(0);
                } else if (productRequest.getRequestedSubcategory().getType() == SubcategoryType.SERVICE) {
                    typeSpinner.setSelection(1);
                }
                 */
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        loadSubcategories(new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {

            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = result;
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        pickExistingSubcategoryCheckBox = view.findViewById(R.id.pick_existing_subcategory);

        subcategoryNameEditText = view.findViewById(R.id.product_subcategory_name);
        descriptionEditText = view.findViewById(R.id.product_description);
        Spinner existingSubcategorySpinner = view.findViewById(R.id.existing_subcategory_spinner);

        pickExistingSubcategoryCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    subcategoryNameEditText.setEnabled(false);
                    descriptionEditText.setEnabled(false);
                    existingSubcategorySpinner.setEnabled(true);
                } else {
                    subcategoryNameEditText.setEnabled(true);
                    descriptionEditText.setEnabled(true);
                    existingSubcategorySpinner.setEnabled(false);
                }
            }
        });

        Button rejectButton = view.findViewById(R.id.product_request_reject);

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productRequest.setStatus(RequestStatus.REJECTED);

                productRequestsOperations.update(productRequest, new ProductRequestsOperations.UpdateProductRequestListener() {
                    @Override
                    public void onSuccess() {
                        NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                        navController.navigate(R.id.nav_dummy);
                    }

                    @Override
                    public void onFailure(Exception e) {
                    }
                });
            }
        });

        Button acceptButton = view.findViewById(R.id.product_request_accept);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productRequest.setStatus(RequestStatus.ACCEPTED);

                productRequestsOperations.update(productRequest, new ProductRequestsOperations.UpdateProductRequestListener() {
                    @Override
                    public void onSuccess() {
                        NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                        navController.navigate(R.id.nav_dummy);
                    }

                    @Override
                    public void onFailure(Exception e) {
                    }
                });

                if (pickExistingSubcategoryCheckBox.isChecked()) {
                    String selectedSubcategory = (String) existingSubcategorySpinner.getSelectedItem();

                    Subcategory selectedCategory = null;
                    for (Subcategory subcategory : subcategories) {
                        if (subcategory.getName().equals(selectedSubcategory)) {
                            selectedCategory = subcategory;
                            break;
                        }
                    }

                    if (selectedCategory != null) {
                        Product product = productRequest.getProduct();
                        product.setSubcategory(selectedCategory);

                        productsOperations.save(product, new ProductsOperations.ProductSaveListener(){
                            @Override
                            public void onSuccess(String productId) {
                                Notification notification = createNotification(productRequest);

                                notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onFailure(Exception e) {

                                    }
                                });

                                NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                                navController.navigate(R.id.nav_dummy);
                            }

                            @Override
                            public void onFailure(Exception e) {

                            }
                        });
                    }
                } else {
                    Subcategory subcategory = new Subcategory("", subcategoryNameEditText.getText().toString(),
                            descriptionEditText.getText().toString(), productRequest.getRequestedSubcategory().getType(),
                            productRequest.getRequestedSubcategory().getCategoryId());
                    subcategoriesOperations.save(subcategory, new SubcategoriesOperations.SaveSubcategoryListener() {
                        @Override
                        public void onSuccess(String subcategoryId) {
                            subcategory.setId(subcategoryId);

                            Product product = productRequest.getProduct();
                            product.setSubcategory(subcategory);
                            productsOperations.save(product, new ProductsOperations.ProductSaveListener(){
                                @Override
                                public void onSuccess(String productId) {
                                    Notification notification = createNotification(productRequest);

                                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                        @Override
                                        public void onSuccess() {

                                        }

                                        @Override
                                        public void onFailure(Exception e) {

                                        }
                                    });

                                    NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                                    navController.navigate(R.id.nav_dummy);
                                }

                                @Override
                                public void onFailure(Exception e) {

                                }
                            });
                        }

                        @Override
                        public void onFailure(Exception e) {

                        }
                    });
                }
            }
        });

    }

    private Notification createNotification(ProductRequest productRequest) {
        String title = "Product request accepted";
        String content = String.format("Product request %s is accepted.",
                productRequest.getProduct().getName());

        return new Notification("", title, content, productRequest.getOwnerId());
    }

    private void loadProductRequest(ProductRequestsOperations.GetProductRequestByIdListener<ProductRequest> listener) {
        productRequestsOperations.getById(productRequestId, listener);
    }

    private void loadSubcategories(SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory> listener) {
        subcategoriesOperations.getAllByType(SubcategoryType.PRODUCT, new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = result;

                List<String> subcategoryNames = new ArrayList<>();
                for (Subcategory subcategory : subcategories) {
                    subcategoryNames.add(subcategory.getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryNames);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                existingSubcategorySpinner = requireView().findViewById(R.id.existing_subcategory_spinner);
                existingSubcategorySpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }
}