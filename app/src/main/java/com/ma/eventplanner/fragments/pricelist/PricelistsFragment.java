package com.ma.eventplanner.fragments.pricelist;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentPricelistsBinding;
import com.ma.eventplanner.fragments.pricelist.PricelistBundleFragment;
import com.ma.eventplanner.fragments.pricelist.PricelistProductFragment;
import com.ma.eventplanner.fragments.pricelist.PricelistServiceFragment;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class PricelistsFragment extends Fragment {

    private String loggedUserId;
    private List<Product> productList = new ArrayList<>();
    private List<Service> serviceList = new ArrayList<>();
    private List<com.ma.eventplanner.model.Bundle> bundleList = new ArrayList<>();
    private UsersOperations usersOperations;
    private ProductsOperations productsOperations;
    private BundlesOperations bundlesOperations;
    private ServicesOperations servicesOperations;
    private PricelistProductFragment pricelistProductFragment;
    private PricelistServiceFragment pricelistServiceFragment;
    private PricelistBundleFragment pricelistBundleFragment;
    private String loggedUserCompanyId;
    private Button generatePdfButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        FragmentPricelistsBinding binding = FragmentPricelistsBinding.inflate(inflater, container, false);
        View rootView = binding.getRoot();
        generatePdfButton = rootView.findViewById(R.id.btnGeneratePDF);
        generatePdfButton.setOnClickListener(v -> generatePdf());

        usersOperations = new UsersOperations();
        productsOperations = new ProductsOperations();
        servicesOperations = new ServicesOperations();
        bundlesOperations = new BundlesOperations();

        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");
        retrieveCompanyId();
        return rootView;
    }

    private void fetchData(){
        fetchProducts();
        fetchServices();
        fetchBundles();
    }

    private void retrieveCompanyId() {
        usersOperations.getEmployeeById(loggedUserId, new UsersOperations.OnGetEmployeeByIdListener() {
            @Override
            public void onSuccess(Employee employee) {
                loggedUserCompanyId = employee.getCompanyId();
                fetchData();
            }

            @Override
            public void onFailure(Exception e) {
                usersOperations.getOwnerById(loggedUserId, new UsersOperations.OnGetOwnerByIdListener() {
                    @Override
                    public void onSuccess(Owner owner) {
                        loggedUserCompanyId = owner.getCompanyId();
                        fetchData();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        // Handle failure (e.g., show an error message)
                    }
                });
            }
        });
    }

    private void fetchProducts() {
        productsOperations.getAllByCompanyId(loggedUserCompanyId, new ProductsOperations.GetAllProductsListener<Product>() {
            @Override
            public void onSuccess(ArrayList<Product> result) {
                productList.clear();
                productList.addAll(result);
                pricelistProductFragment = new PricelistProductFragment(productList);
                checkDataFetched();
            }

            @Override
            public void onFailure(Exception e) {
                // Log error
                e.printStackTrace();
            }
        });
    }

    private void fetchServices() {
        servicesOperations.getAllByCompanyId(loggedUserCompanyId, new ServicesOperations.GetAllServicesListener<Service>() {
            @Override
            public void onSuccess(ArrayList<Service> result) {
                serviceList.clear();
                serviceList.addAll(result);
                pricelistServiceFragment = new PricelistServiceFragment(serviceList);
                checkDataFetched();
            }

            @Override
            public void onFailure(Exception e) {
                // Log error
                e.printStackTrace();
            }
        });
    }

    private void fetchBundles() {
        bundlesOperations.getAllByCompanyId(loggedUserCompanyId, new BundlesOperations.GetAllBundlesListener<com.ma.eventplanner.model.Bundle>() {
            @Override
            public void onSuccess(ArrayList<com.ma.eventplanner.model.Bundle> result) {
                bundleList.clear();
                bundleList.addAll(result);
                pricelistBundleFragment = new PricelistBundleFragment(bundleList);
                checkDataFetched();
            }

            @Override
            public void onFailure(Exception e) {
                // Log error
                e.printStackTrace();
            }
        });
    }

    private void checkDataFetched() {
        if (productList.size() > 0 && serviceList.size() > 0 && bundleList.size() > 0) {
            setupFragments();
        }
    }

    private void setupFragments() {
        if (loggedUserCompanyId != null) {
            // Replace the containers with the fragments
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.products_fragment_container, pricelistProductFragment)
                    .replace(R.id.services_fragment_container, pricelistServiceFragment)
                    .replace(R.id.bundles_fragment_container, pricelistBundleFragment)
                    .commit();
        }
    }

    private void generatePdf() {
        PdfDocument pdfDocument = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(300, 600, 1).create();
        PdfDocument.Page page = pdfDocument.startPage(pageInfo);

        Canvas canvas = page.getCanvas();
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        canvas.drawText("Hello, this is a sample PDF!", 40, 50, paint);

        pdfDocument.finishPage(page);

        // Save the PDF
        File directory = new File(requireContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), "EventPlannerPDFs");
        if (!directory.exists() && !directory.mkdirs()) {
            Toast.makeText(getContext(), "Failed to create directory", Toast.LENGTH_SHORT).show();
            return;
        }
        File file = new File(directory, "sample.pdf");
        try {
            pdfDocument.writeTo(new FileOutputStream(file));
            Toast.makeText(getContext(), "PDF saved to " + file.getAbsolutePath(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getContext(), "Failed to save PDF", Toast.LENGTH_SHORT).show();
        }

        // Close the document
        pdfDocument.close();
    }
}
