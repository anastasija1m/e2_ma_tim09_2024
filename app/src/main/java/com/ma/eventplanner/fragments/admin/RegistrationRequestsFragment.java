package com.ma.eventplanner.fragments.admin;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.widget.SearchView;

import androidx.fragment.app.Fragment;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.CategoriesSpinnerAdapter;
import com.ma.eventplanner.adapters.EventTypeSpinnerAdapter;
import com.ma.eventplanner.adapters.RegistrationRequestsAdapter;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.RegistrationRequestsOperations;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.RegistrationRequest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RegistrationRequestsFragment extends Fragment {
    private List<RegistrationRequest> requests;
    private RegistrationRequestsOperations registrationRequestsOptions;
    private CategoriesOperations categoriesOperations;
    private EventTypesOperations eventTypesOperations;
    private RegistrationRequestsAdapter adapter;
    private List<Category> categories;
    private List<EventType> eventTypes;
    private SimpleDateFormat dateFormat;

    public RegistrationRequestsFragment() {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    }

    public static RegistrationRequestsFragment newInstance() {
        return new RegistrationRequestsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        registrationRequestsOptions = new RegistrationRequestsOperations();
        categoriesOperations = new CategoriesOperations();
        eventTypesOperations = new EventTypesOperations();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_requests, container, false);


        ImageButton filterButton = view.findViewById(R.id.filter_reg_req_button);
        filterButton.setOnClickListener(v -> showFilterDialog());

        SearchView searchView = view.findViewById(R.id.search_text);

        fetchRegistrationRequests();
        fetchCategories();
        fetchEventTypes();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchRequests(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchRequests(newText);
                return false;
            }
        });

        return view;
    }

    private void searchRequests(String query) {
        if (requests == null || adapter == null) {
            return;
        }

        if (query.isEmpty()) {
            adapter.updateData(new ArrayList<>(requests));
            return;
        }

        List<RegistrationRequest> filteredList = new ArrayList<>();
        for (RegistrationRequest request : requests) {
            Company company = request.getCompany();
            Owner owner = request.getOwner();

            if (company != null && owner != null) {
                if (company.getName().toLowerCase().contains(query.toLowerCase()) ||
                        owner.getFirstName().toLowerCase().contains(query.toLowerCase()) ||
                        owner.getLastName().toLowerCase().contains(query.toLowerCase()) ||
                        company.getEmail().toLowerCase().contains(query.toLowerCase()) ||
                        owner.getEmail().toLowerCase().contains(query.toLowerCase())) {
                    filteredList.add(request);
                }
            }
        }

        adapter.updateData(filteredList);
    }


    private void showFilterDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_filter_registration_requests, null);

        DatePicker startDatePicker = dialogView.findViewById(R.id.start_date_picker);
        DatePicker endDatePicker = dialogView.findViewById(R.id.end_date_picker);
        Spinner categorySpinner = dialogView.findViewById(R.id.category_spinner);
        Spinner eventTypeSpinner = dialogView.findViewById(R.id.event_type_spinner);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        startDatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);

        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        endDatePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);

        if (categories != null) {
            List<Category> categoriesWithAny = new ArrayList<>(categories);
            categoriesWithAny.add(0, null);
            CategoriesSpinnerAdapter categoryAdapter = new CategoriesSpinnerAdapter(getContext(), categoriesWithAny);
            categorySpinner.setAdapter(categoryAdapter);
        }

        if (eventTypes != null) {
            List<EventType> eventTypesWithAny = new ArrayList<>(eventTypes);
            eventTypesWithAny.add(0, null);
            EventTypeSpinnerAdapter eventTypeAdapter = new EventTypeSpinnerAdapter(getContext(), eventTypesWithAny);
            eventTypeSpinner.setAdapter(eventTypeAdapter);
        }

        new AlertDialog.Builder(getContext())
                .setTitle("Filter Requests")
                .setView(dialogView)
                .setPositiveButton("Apply", (dialog, which) -> {
                    int startYear = startDatePicker.getYear();
                    int startMonth = startDatePicker.getMonth();
                    int startDay = startDatePicker.getDayOfMonth();
                    String startDate = startYear + "-" + (startMonth + 1) + "-" + startDay;

                    int endYear = endDatePicker.getYear();
                    int endMonth = endDatePicker.getMonth();
                    int endDay = endDatePicker.getDayOfMonth();
                    String endDate = endYear + "-" + (endMonth + 1) + "-" + endDay;

                    Category selectedCategory = (Category) categorySpinner.getSelectedItem();
                    EventType selectedEventType = (EventType) eventTypeSpinner.getSelectedItem();

                    filterRequests(startDate, endDate,
                            selectedCategory != null ? selectedCategory.getId() : "",
                            selectedEventType != null ? selectedEventType.getId() : "");
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    private void filterRequests(String startDate, String endDate, String categoryId, String eventTypeId) {
        if (requests == null || adapter == null) {
            return;
        }

        List<RegistrationRequest> filteredList = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        boolean areAllFiltersEmpty = TextUtils.isEmpty(startDate) && TextUtils.isEmpty(endDate) && TextUtils.isEmpty(categoryId) && TextUtils.isEmpty(eventTypeId);

        if (areAllFiltersEmpty) {
            filteredList = new ArrayList<>(requests);
            adapter.updateData(filteredList);
            return;
        }

        Date start = null;
        Date end = null;
        try {
            if (!TextUtils.isEmpty(startDate)) {
                start = dateFormat.parse(startDate);
            }
            if (!TextUtils.isEmpty(endDate)) {
                end = dateFormat.parse(endDate);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        for (RegistrationRequest request : requests) {
            boolean matchesFilter = true;
            Company company = request.getCompany();

            if (start != null || end != null) {
                Date requestDate = request.getSent();
                if (requestDate != null) {
                    if (start != null && requestDate.before(start)) {
                        matchesFilter = false;
                    }
                    if (end != null && requestDate.after(end)) {
                        matchesFilter = false;
                    }
                }
            }

            if (!TextUtils.isEmpty(categoryId)) {
                if (company != null) {
                    List<String> categoryIds = company.getCategoryIds();
                    if (categoryIds == null || !categoryIds.contains(categoryId)) {
                        matchesFilter = false;
                    }
                } else {
                    matchesFilter = false;
                }
            }

            if (!TextUtils.isEmpty(eventTypeId)) {
                if (company != null) {
                    List<String> eventTypeIds = company.getEventTypeIds();
                    if (eventTypeIds == null || !eventTypeIds.contains(eventTypeId)) {
                        matchesFilter = false;
                    }
                } else {
                    matchesFilter = false;
                }
            }

            if (/*isAnyFilterActive && */matchesFilter) {
                filteredList.add(request);
            }
        }

        adapter.updateData(filteredList);
    }

    private void fetchRegistrationRequests() {
        registrationRequestsOptions.getAll(new RegistrationRequestsOperations.OnGetAllSuccessListener() {
            @Override
            public void onSuccess(List<RegistrationRequest> registrationRequests) {
                requests = registrationRequests;

                adapter = new RegistrationRequestsAdapter(getContext(), requests, getActivity());


                ListView listView = getView().findViewById(R.id.requests_list);
                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestsFragment", String.valueOf(e));
            }
        });
    }

    private void fetchCategories() {
        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categories = result;
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestsFragment", String.valueOf(e));
            }
        });
    }
    private void fetchEventTypes() {
        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                eventTypes = result;
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestsFragment", String.valueOf(e));
            }
        });
    }




    /*
    private void rejectRequest(RegistrationRequest registrationRequest) {
        registrationRequest.reject();

        RegistrationRequestsOptions registrationRequestsOptions = new RegistrationRequestsOptions();
        registrationRequestsOptions.update(registrationRequest, new RegistrationRequestsOptions.OnSaveSuccessListener() {
            @Override
            public void onSuccess() {
                adapter.refreshList(requests);
                Log.d(TAG, "Request updated successfully.");
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error updating request: " + e.getMessage());
            }
        });

    }
     */

}
