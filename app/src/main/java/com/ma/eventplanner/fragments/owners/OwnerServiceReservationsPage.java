package com.ma.eventplanner.fragments.owners;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.ServiceReservationsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentOwnerServiceReservationsPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.ServiceReservation;
import com.ma.eventplanner.model.ServiceReservationStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;

public class OwnerServiceReservationsPage extends Fragment {

    private final ArrayList<ServiceReservation> serviceReservations = new ArrayList<>();
    private OwnerReservationViewModel ownerReservationViewModel;
    private FragmentOwnerServiceReservationsPageBinding binding;

    public OwnerServiceReservationsPage() {

    }

    public static OwnerServiceReservationsPage newInstance() {
        return new OwnerServiceReservationsPage();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ownerReservationViewModel = new ViewModelProvider(this).get(OwnerReservationViewModel.class);
        binding = FragmentOwnerServiceReservationsPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        SearchView searchView = binding.searchText;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ownerReservationViewModel.setSearchText(newText);
                return true;
            }
        });
        ownerReservationViewModel.getText().observe(getViewLifecycleOwner(), this::filterReservations);

        retrieveOwnerReservations();

        Spinner statusSpinner = binding.statusSpinner;
        statusSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedStatus = parent.getItemAtPosition(position).toString();
                filterByStatus(selectedStatus);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return root;
    }

    private void retrieveOwnerReservations() {

        ServiceReservationsOperations reservationsOperations = new ServiceReservationsOperations();
        reservationsOperations.getAll(new ServiceReservationsOperations.GetAllServiceReservationsListener<ServiceReservation>() {
            @Override
            public void onSuccess(List<ServiceReservation> result) {
                serviceReservations.clear();
                serviceReservations.addAll(result);
                FragmentTransition.to(OwnerServiceReservationsList.newInstance(serviceReservations), requireActivity(), false, R.id.scroll_owner_reservations_list);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error retrieving employee reservations: ", e);
            }
        });
    }

    private void filterReservations(String query) {
        ArrayList<ServiceReservation> filteredReservations = new ArrayList<>();
        if (query == null || query.isEmpty()) {
            filteredReservations.addAll(serviceReservations);
            updateUI(filteredReservations);
        } else {
            int totalReservations = serviceReservations.size();
            AtomicInteger counter = new AtomicInteger(0);
            for (ServiceReservation reservation : serviceReservations) {
                getEventOrganizerForReservation(reservation, query, filteredReservations, counter, totalReservations);
                getServiceForReservation(reservation, query, filteredReservations, counter, totalReservations);
                getEmployeeForReservation(reservation, query, filteredReservations, counter, totalReservations);
            }
        }
    }

    private void getServiceForReservation(ServiceReservation reservation, String query, ArrayList<ServiceReservation> filteredReservations, AtomicInteger counter, int totalReservations) {
        ServicesOperations serviceOperations = new ServicesOperations();
        serviceOperations.getById(reservation.getServiceId(), new ServicesOperations.GetServiceByIdListener() {
            @Override
            public void onSuccess(Service service) {
                if (service.getName().toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault()))) {
                    filteredReservations.add(reservation);
                }
                if (counter.incrementAndGet() == totalReservations) {
                    updateUI(filteredReservations);
                }
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching service for reservation: ", e);
                if (counter.incrementAndGet() == totalReservations) {
                    updateUI(filteredReservations);
                }
            }
        });
    }

    private void getEventOrganizerForReservation(ServiceReservation reservation, String query, ArrayList<ServiceReservation> filteredReservations, AtomicInteger counter, int totalReservations) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEventOrganizerByEmail(reservation.getOrganizerEmail(), new UsersOperations.OnGetEventOrganizerByEmailListener() {
            @Override
            public void onSuccess(EventOrganizer eventOrganizer) {
                if (eventOrganizer.getFirstName().toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault()))
                        || eventOrganizer.getLastName().toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault()))) {
                    filteredReservations.add(reservation);
                }
                if (counter.incrementAndGet() == totalReservations) {
                    updateUI(filteredReservations);
                }
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching event organizer for reservation: ", e);
                if (counter.incrementAndGet() == totalReservations) {
                    updateUI(filteredReservations);
                }
            }
        });
    }

    private void getEmployeeForReservation(ServiceReservation reservation, String query, ArrayList<ServiceReservation> filteredReservations, AtomicInteger counter, int totalReservations) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEmployeeByEmail(reservation.getEmployeeEmail(), new UsersOperations.OnGetEmployeeByEmailListener() {
            @Override
            public void onSuccess(Employee employee) {
                if (employee.getName().toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault()))
                        || employee.getSurname().toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault()))) {
                    filteredReservations.add(reservation);
                }
                if (counter.incrementAndGet() == totalReservations) {
                    updateUI(filteredReservations);
                }
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching employee for reservation: ", e);
                if (counter.incrementAndGet() == totalReservations) {
                    updateUI(filteredReservations);
                }
            }
        });
    }

    private void filterByStatus(String selectedStatus) {
        ArrayList<ServiceReservation> filteredReservations = new ArrayList<>();
        for (ServiceReservation reservation : serviceReservations) {
            if (reservation.getServiceReservationStatus() == ServiceReservationStatus.valueOf(selectedStatus)) {
                filteredReservations.add(reservation);
            }
        }
        updateUI(filteredReservations);
    }

    private void updateUI(ArrayList<ServiceReservation> filteredReservations) {
        OwnerServiceReservationsList ownerServiceReservationsList = OwnerServiceReservationsList.newInstance(filteredReservations);
        FragmentTransition.to(ownerServiceReservationsList, requireActivity(), false, R.id.scroll_owner_reservations_list);
    }

    @Override
    public void onResume() {
        super.onResume();
        retrieveOwnerReservations();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}