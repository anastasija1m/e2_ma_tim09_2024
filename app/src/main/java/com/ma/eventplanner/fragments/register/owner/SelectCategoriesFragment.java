package com.ma.eventplanner.fragments.register.owner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SelectCategoriesFragment extends Fragment {
    private ListView categoryListView;
    private List<Category> categoryList;
    private Map<Category, Boolean> categorySelectionMap;

    @NonNull
    public static SelectCategoriesFragment newInstance() {
        SelectCategoriesFragment fragment = new SelectCategoriesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_select_categories, container, false);
        categoryListView = view.findViewById(R.id.category_list_view);
        CategoriesOperations categoriesOperations = new CategoriesOperations();

        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> categories) {
                categoryList = categories;

                categorySelectionMap = new HashMap<>();
                for (Category category : categoryList) {
                    categorySelectionMap.put(category, false);
                }

                setupCategoryListView();
                setupNextButton(view);
            }

            @Override
            public void onFailure(Exception e) {
            }
        });

        return view;
    }

    private void setupCategoryListView() {
        ArrayAdapter<Category> adapter = new ArrayAdapter<Category>(requireContext(),
                R.layout.list_item_category, R.id.text1, categoryList) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = view.findViewById(R.id.text1);
                CheckBox checkBox = view.findViewById(R.id.checkbox);

                Category category = getItem(position);
                if (category != null) {
                    textView.setText(category.getName());
                    checkBox.setChecked(Boolean.TRUE.equals(categorySelectionMap.get(category)));
                    checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        categorySelectionMap.put(category, isChecked);
                    });
                }

                return view;
            }
        };
        categoryListView.setAdapter(adapter);
    }

    private void setupNextButton(@NonNull View rootView) {
        Button nextButton = rootView.findViewById(R.id.next_button);
        nextButton.setOnClickListener(v -> navigateToSelectEventTypeFormFragment());
    }

    private void navigateToSelectEventTypeFormFragment() {
        List<Category> selectedCategories = getSelectedCategories();

        Company company = getCompanyFromArguments();
        if (company == null) return;

        company.setCategoryIds(getSelectedCategoryIds(selectedCategories));

        assert getArguments() != null;
        getArguments().putParcelable("company", company);

        Fragment selectEventTypesFragment = new SelectEventTypesFragment();
        selectEventTypesFragment.setArguments(getArguments());

        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, selectEventTypesFragment)
                .addToBackStack(null)
                .commit();
    }

    private List<Category> getSelectedCategories() {
        List<Category> selectedCategories = new ArrayList<>();
        for (Map.Entry<Category, Boolean> entry : categorySelectionMap.entrySet()) {
            if (entry.getValue()) {
                selectedCategories.add(entry.getKey());
            }
        }
        return selectedCategories;
    }

    private Company getCompanyFromArguments() {
        Bundle currentArguments = getArguments();
        if (currentArguments == null) return null;
        return currentArguments.getParcelable("company");
    }

    private List<String> getSelectedCategoryIds(List<Category> selectedCategories) {
        List<String> selectedCategoryIds = new ArrayList<>();
        for (Category sc : selectedCategories) {
            selectedCategoryIds.add(sc.getId());
        }
        return selectedCategoryIds;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}
