package com.ma.eventplanner.fragments.products;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.ProductListAdapter;
import com.ma.eventplanner.databinding.FragmentProductsListBinding;
import com.ma.eventplanner.model.Product;

import java.util.ArrayList;


public class ProductsListFragment extends ListFragment {

    private ProductListAdapter productListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Product> mProducts;
    private FragmentProductsListBinding binding;

    public ProductsListFragment() {

    }

    public static ProductsListFragment newInstance(ArrayList<Product> products) {
        ProductsListFragment productsListFragment = new ProductsListFragment();
        Bundle args = new Bundle();
        ArrayList<Product> filtratedList = new ArrayList<>();
        for (Product product:products
             ) {
            if(!product.isDeleted()){
                filtratedList.add(product);
            }
        }
        args.putParcelableArrayList(ARG_PARAM, filtratedList);
        productsListFragment.setArguments(args);
        return productsListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            mProducts = getArguments().getParcelableArrayList(ARG_PARAM);
            productListAdapter = new ProductListAdapter(getActivity(), mProducts);
            setListAdapter(productListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentProductsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}