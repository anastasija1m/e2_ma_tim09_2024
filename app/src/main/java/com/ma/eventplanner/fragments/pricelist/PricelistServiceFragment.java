package com.ma.eventplanner.fragments.pricelist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.model.Service;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class PricelistServiceFragment extends Fragment {

    private List<Service> serviceList = new ArrayList<>();
    private ServicesOperations servicesOperations;
    private RecyclerViewAdapter adapter;

    public PricelistServiceFragment() { }

    public PricelistServiceFragment(List<Service> services) {
        this.serviceList = services;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pricelist_service, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);

        servicesOperations = new ServicesOperations();
        adapter = new RecyclerViewAdapter(serviceList);

        // Change the layout manager to LinearLayoutManager with vertical orientation
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(adapter);

        return rootView;
    }

    // RecyclerViewAdapter class to bind data to RecyclerView
    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

        private List<Service> serviceList;

        RecyclerViewAdapter(List<Service> serviceList) {
            this.serviceList = serviceList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Service service = serviceList.get(position);
            holder.bind(service);
        }

        @Override
        public int getItemCount() {
            return serviceList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewSerial, textViewName, textViewPrice, textViewDiscount, textViewNewPrice;

            ViewHolder(@NonNull View itemView) {
                super(itemView);
                textViewSerial = itemView.findViewById(R.id.textViewSerial);
                textViewName = itemView.findViewById(R.id.textViewName);
                textViewPrice = itemView.findViewById(R.id.textViewPrice);
                textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
                textViewNewPrice = itemView.findViewById(R.id.textViewNewPrice);

                // Set click listener
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                // Get the selected service
                Service selectedService = serviceList.get(getAdapterPosition());

                NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
                Bundle bundle = new Bundle();
                bundle.putParcelable("service", selectedService);
                navController.navigate(R.id.nav_edit_service, bundle);
            }

            void bind(Service service) {
                textViewSerial.setText(String.valueOf(getAdapterPosition() + 1)); // Serial number
                textViewName.setText(service.getName());
                textViewPrice.setText(String.valueOf(service.getPrice()));
                textViewDiscount.setText(String.valueOf(service.getDiscount()));
                textViewNewPrice.setText(String.valueOf(getNewPrice(service)));
            }

            private double getNewPrice(Service service) {
                double newPrice = service.getPrice() * (1 - (double) service.getDiscount() / 100);
                DecimalFormat df = new DecimalFormat("#.##");
                return Double.parseDouble(df.format(newPrice));
            }

        }
    }
}
