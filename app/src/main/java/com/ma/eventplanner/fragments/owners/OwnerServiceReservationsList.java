package com.ma.eventplanner.fragments.owners;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.OwnerReservationListAdapter;
import com.ma.eventplanner.databinding.FragmentOwnerServiceReservationsListBinding;
import com.ma.eventplanner.model.ServiceReservation;

import java.util.ArrayList;

public class OwnerServiceReservationsList extends ListFragment {

    private OwnerReservationListAdapter ownerReservationListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<ServiceReservation> mServiceReservations;
    private FragmentOwnerServiceReservationsListBinding binding;

    public OwnerServiceReservationsList() {

    }

    public static OwnerServiceReservationsList newInstance(ArrayList<ServiceReservation> serviceReservations) {
        OwnerServiceReservationsList ownerServiceReservationsList = new OwnerServiceReservationsList();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, serviceReservations);
        ownerServiceReservationsList.setArguments(args);

        return ownerServiceReservationsList;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mServiceReservations = getArguments().getParcelableArrayList(ARG_PARAM);
            ownerReservationListAdapter = new OwnerReservationListAdapter(getActivity(), mServiceReservations);
            setListAdapter(ownerReservationListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentOwnerServiceReservationsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}