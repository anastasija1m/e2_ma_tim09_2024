package com.ma.eventplanner.fragments.events;

import android.content.Context;
import android.content.SharedPreferences;
import android.icu.util.Calendar;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.databinding.FragmentEventCreationPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventType;

import java.time.*;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventCreationPageFragment extends Fragment {

    private FragmentEventCreationPageBinding binding;
    private Spinner typeSpinner;
    private EditText nameEditText, descriptionEditText, maxParticipantsEditText, locationEditText, maxDistanceEditText;
    private DatePicker datePicker;
    private Boolean isPrivate = null;
    private EventsOperations eventsOperations;
    private EventTypesOperations eventTypesOperations;
    private String organizerId;
    private Map<String, String> typesId;
    public EventCreationPageFragment() { }

    public static EventCreationPageFragment newInstance() {
        return new EventCreationPageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        organizerId = sharedPreferences.getString("userId", "");

        binding = FragmentEventCreationPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        typesId = new HashMap<>();
        eventsOperations = new EventsOperations();
        eventTypesOperations = new EventTypesOperations();
        initializeViews();
        setupListeners();

        return root;
    }

    private void initializeViews() {
        getEventTypes();
        Calendar calendar = Calendar.getInstance();
        nameEditText = binding.eventName;
        descriptionEditText = binding.eventDescription;
        maxParticipantsEditText = binding.eventMaxParticipants;
        locationEditText = binding.eventLocation;
        maxDistanceEditText = binding.eventMaxDistance;
        datePicker = binding.eventDate;
        datePicker.setMinDate(calendar.getTimeInMillis());
        typeSpinner = binding.eventType;
    }

    private void setupListeners() {
        binding.eventCreateButton.setOnClickListener(v ->{
            if (validateFields()) {
                addNewEvent();
            }
        });
        binding.eventPrivacy.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_closed) {
                    isPrivate = true;
                } else if (checkedId == R.id.radio_open) {
                    isPrivate = false;
                } else {
                    isPrivate = null;
                }
            }
        });
    }

    private boolean validateFields() {
        EditText[] fields = {nameEditText, descriptionEditText, locationEditText, maxDistanceEditText, maxParticipantsEditText};

        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText().toString().trim())) {
                showToast("Please fill in all fields");
                return false;
            }
        }
        if (isPrivate == null) {
            showToast("Please select privacy");
            return false;
        }
        return true;
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void addNewEvent() {
        String selectedType = typeSpinner.getSelectedItem().toString();

        LocalDate date = getDateFromDatePicker(datePicker);
        LocalDateTime dateTime = date.atStartOfDay();
        Instant instant = dateTime.toInstant(ZoneOffset.UTC);
        Long timestamp = instant.toEpochMilli();

        String name = nameEditText.getText().toString().trim();
        String desc = descriptionEditText.getText().toString().trim();
        String locE = locationEditText.getText().toString().trim();
        int maxPart = Integer.parseInt(maxParticipantsEditText.getText().toString().trim());
        int maxDist = Integer.parseInt(maxDistanceEditText.getText().toString().trim());
        Event event;
        if(selectedType.equals("Other")) {
            event = new Event(null, name, desc, maxPart, isPrivate, locE, maxDist, timestamp, null, organizerId);
        }
        else {
            String typeId = typesId.get(selectedType);
            event = new Event(null, name, desc, maxPart, isPrivate, locE, maxDist, timestamp, typeId, organizerId);
        }

        eventsOperations.save(event, new EventsOperations.SaveEventListener() {
            @Override
            public void onSuccess(String eventId) {
                Log.e("EventSave", "Event saved successful: " + eventId);
                event.setId(eventId);
                EventsPageFragment.addEvent(event);
                showToast("Event created successfully");
                Navigation.findNavController(requireView()).navigateUp();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("EventSave", "Error saving event to database: " + e.getMessage());
            }
        });
    }

    private void getEventTypes() {
        if(!typesId.isEmpty()) {
            return;
        }
        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                Spinner spinner = binding.getRoot().findViewById(R.id.event_type);

                List<String> typesArray = new ArrayList<>();
                for (EventType eventType : result) {
                    typesArray.add(eventType.getTypeName());
                    typesId.put(eventType.getTypeName(), eventType.getId());
                }
                typesArray.add("Other");
                String[] typesArrayAsArray = typesArray.toArray(new String[0]);

                ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, typesArrayAsArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    private LocalDate getDateFromDatePicker(DatePicker datePicker) {
        int year = datePicker.getYear();
        int month = datePicker.getMonth() + 1;
        int day = datePicker.getDayOfMonth();
        return LocalDate.of(year, month, day);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}