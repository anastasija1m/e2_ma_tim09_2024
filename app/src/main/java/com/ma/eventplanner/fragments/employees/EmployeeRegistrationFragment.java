package com.ma.eventplanner.fragments.employees;

import static android.content.ContentValues.TAG;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.AuthenticationManager;
import com.ma.eventplanner.databinding.FragmentEmployeeRegistrationBinding;
import com.ma.eventplanner.model.Address;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.EmployeeWorkHours;
import com.ma.eventplanner.model.EmployeeWorkSchedule;

import org.jetbrains.annotations.NotNull;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class EmployeeRegistrationFragment extends Fragment {

    private FragmentEmployeeRegistrationBinding binding;
    private AuthenticationManager authenticationManager;
    private EditText nameEditText, surnameEditText, emailEditText, passwordEditText,
                     confirmPasswordEditText, phoneNumberEditText, countryText, cityText,
                     postalCodeText, streetText, streetNumberText;

    private Owner loggedInOwner;
    private ImageView employeePictureView;
    private Intent image;
    private Button uploadImageButton;
    private Switch timePickerSwitch, saturdayPickerSwitch, sundayPickerSwitch;
    private DatePicker startDatePicker, endDatePicker;

    public EmployeeRegistrationFragment() { }

    public static EmployeeRegistrationFragment newInstance() {
        return new EmployeeRegistrationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        authenticationManager = new AuthenticationManager();
        loggedInOwner = new Owner();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInOwner = homeActivity.getLoggedInOwner();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentEmployeeRegistrationBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        initializeViews();
        setupListeners();

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUploadImageButton();
    }

    private void initializeViews() {
        nameEditText = binding.editTextName;
        surnameEditText = binding.editTextSurname;
        emailEditText = binding.editTextEmail;
        passwordEditText = binding.editTextPassword;
        confirmPasswordEditText = binding.editTextConfirmPassword;
        phoneNumberEditText = binding.editTextPhoneNumber;
        employeePictureView = binding.employeePictureView;
        uploadImageButton = binding.btnUploadPicture;
        countryText = binding.editTextAddressCountry;
        cityText = binding.editTextAddressCity;
        postalCodeText = binding.editTextAddressPostalCode;
        streetText = binding.editTextAddressStreet;
        streetNumberText = binding.editTextAddressStreetNum;
    }

    private void setupListeners() {
        setupRegisterButton();
        setupTimePickerSwitch();
        setupDatePickers();
    }

    private void setupUploadImageButton() {
        uploadImageButton.setOnClickListener(v -> showUploadOptions());
    }

    private void setupRegisterButton() {
        binding.btnRegister.setOnClickListener(v -> {
            if (validateFields()) {
                registerEmployee();
            }
        });
    }

    private void registerEmployee() {
        Address address = setAddress();

        Employee employee = new Employee(
                getTextFromEditText(emailEditText),
                getTextFromEditText(passwordEditText),
                getTextFromEditText(confirmPasswordEditText),
                Role.EMPLOYEE,
                getTextFromEditText(nameEditText),
                getTextFromEditText(surnameEditText),
                "",
                address,
                getTextFromEditText(phoneNumberEditText),
                true,
                loggedInOwner.getCompanyId()
        );

        EmployeeWorkSchedule workSchedule = createWorkSchedule();
        LocalDate startDate = getDateFromDatePicker(startDatePicker);
        LocalDate endDate = getDateFromDatePicker(endDatePicker);

        assert workSchedule != null;
        workSchedule.setScheduleStartDate(java.sql.Date.valueOf(String.valueOf(startDate)));
        workSchedule.setScheduleEndDate(java.sql.Date.valueOf(String.valueOf(endDate)));
        employee.setWorkSchedule(workSchedule);

        authenticationManager.createUserWithEmailAndPassword(employee.getEmail(), employee.getPassword(), getActivity(), employee, image, new AuthenticationManager.OnUserCreationListener() {
            @Override
            public void onSuccess() {
                NavController navController = Navigation.findNavController(requireView());
                navController.navigate(R.id.nav_employees);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Failed to create user: " + e.getMessage(), e);
                Toast.makeText(requireContext(), "Failed to create user: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Address setAddress() {
        String country = getTextFromEditText(countryText);
        String city = getTextFromEditText(cityText);
        String postalCode = getTextFromEditText(postalCodeText);
        String street = getTextFromEditText(streetText);
        String streetNumber = getTextFromEditText(streetNumberText);

        return new Address(country, city, postalCode, street, streetNumber);
    }

    private String getTextFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    private void showUploadOptions() {
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent chooserIntent = Intent.createChooser(pickPhotoIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePictureIntent});
        imageChooserLauncher.launch(chooserIntent);
    }

    private final ActivityResultLauncher<Intent> imageChooserLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    image = result.getData();
                    if (image != null) {
                        Uri selectedImageUri = image.getData();
                        if (selectedImageUri != null) {
                            employeePictureView.setImageURI(selectedImageUri);
                        } else {
                            Bitmap bitmap = (Bitmap) Objects.requireNonNull(image.getExtras()).get("data");
                            employeePictureView.setImageBitmap(bitmap);
                        }
                    }
                }
            });

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void setupTimePickerSwitch() {
        timePickerSwitch = binding.switchTimePicker;
        saturdayPickerSwitch = binding.switchSaturday;
        sundayPickerSwitch = binding.switchSunday;

        timePickerSwitch.setOnCheckedChangeListener(this::setPickerEnabled);
        saturdayPickerSwitch.setOnCheckedChangeListener(this::setSaturdayPickerEnabled);
        sundayPickerSwitch.setOnCheckedChangeListener(this::setSundayPickerEnabled);
    }

    private void setPickerEnabled(CompoundButton buttonView, boolean isChecked) {
        String[] daysOfWeek = {"monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"};
        for (String day : daysOfWeek) {
            int startTimePickerId = getResources().getIdentifier(day + "_start", "id", requireActivity().getPackageName());
            int endTimePickerId = getResources().getIdentifier(day + "_end", "id", requireActivity().getPackageName());

            TimePicker startTimePicker = requireView().findViewById(startTimePickerId);
            TimePicker endTimePicker = requireView().findViewById(endTimePickerId);

            startTimePicker.setEnabled(isChecked);
            endTimePicker.setEnabled(isChecked);
        }
    }

    private void setSaturdayPickerEnabled(CompoundButton button, boolean isChecked) {
        int saturdayStartTimePickerId = getResources().getIdentifier("saturday_start", "id", requireActivity().getPackageName());
        int saturdayEndTimePickerId = getResources().getIdentifier("saturday_end", "id", requireActivity().getPackageName());

        TimePicker saturdayStartTimePicker = requireView().findViewById(saturdayStartTimePickerId);
        TimePicker saturdayEndTimePicker = requireView().findViewById(saturdayEndTimePickerId);

        saturdayStartTimePicker.setEnabled(isChecked);
        saturdayEndTimePicker.setEnabled(isChecked);
    }

    private void setSundayPickerEnabled(CompoundButton button, boolean isChecked) {
        int sundayStartTimePickerId = getResources().getIdentifier("sunday_start", "id", requireActivity().getPackageName());
        int sundayEndTimePickerId = getResources().getIdentifier("sunday_end", "id", requireActivity().getPackageName());

        TimePicker sundayStartTimePicker = requireView().findViewById(sundayStartTimePickerId);
        TimePicker sundayEndTimePicker = requireView().findViewById(sundayEndTimePickerId);

        sundayStartTimePicker.setEnabled(isChecked);
        sundayEndTimePicker.setEnabled(isChecked);
    }

    private void setupDatePickers() {
        startDatePicker = binding.scheduleStartDate;
        endDatePicker = binding.scheduleEndDate;

        startDatePicker.setMinDate(System.currentTimeMillis() - 1000);
        endDatePicker.setMinDate(System.currentTimeMillis() - 1000);
    }

    private LocalDate getDateFromDatePicker(DatePicker datePicker) {
        int year = datePicker.getYear();
        int month = datePicker.getMonth() + 1;
        int day = datePicker.getDayOfMonth();
        return LocalDate.of(year, month, day);
    }

    private EmployeeWorkSchedule createWorkSchedule() {
        EmployeeWorkSchedule workSchedule = new EmployeeWorkSchedule();
        if (timePickerSwitch.isChecked()) {
            Map<String, EmployeeWorkHours> workHoursMap = new HashMap<>();
            String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

            for (String day : daysOfWeek) {
                if (arePickersEnabled(day)) {
                    EmployeeWorkHours workHours = getWorkHoursFromBindings(day);
                    if (workHours != null) {
                        String validationError = workHours.validate();
                        if (validationError != null) {
                            showToast(day + ": " + validationError);
                            return null;
                        }
                        workHoursMap.put(day, workHours);
                    }
                }
            }
            workSchedule.setWorkHoursMap(workHoursMap);
        } else {
            workSchedule.initializeWorkHours();
        }
        return workSchedule;
    }

    private boolean arePickersEnabled(String day) {
        int startTimePickerId = getResources().getIdentifier(day.toLowerCase() + "_start", "id", requireActivity().getPackageName());
        int endTimePickerId = getResources().getIdentifier(day.toLowerCase() + "_end", "id", requireActivity().getPackageName());

        TimePicker startTimePicker = requireView().findViewById(startTimePickerId);
        TimePicker endTimePicker = requireView().findViewById(endTimePickerId);

        return startTimePicker.isEnabled() && endTimePicker.isEnabled();
    }

    private LocalTime getTimeFromTimePicker(TimePicker timePicker) {
        int hour = timePicker.getHour();
        int minute = timePicker.getMinute();
        return LocalTime.of(hour, minute);
    }

    private EmployeeWorkHours getWorkHoursFromBindings(String day) {
        try {
            TimePicker startTimePicker = (TimePicker) binding.getClass().getDeclaredField(day.toLowerCase() + "Start").get(binding);
            TimePicker endTimePicker = (TimePicker) binding.getClass().getDeclaredField(day.toLowerCase() + "End").get(binding);

            assert startTimePicker != null;
            LocalTime startTime = getTimeFromTimePicker(startTimePicker);
            assert endTimePicker != null;
            LocalTime endTime = getTimeFromTimePicker(endTimePicker);

            String startTimeString = startTime.toString();
            String endTimeString = endTime.toString();

            return new EmployeeWorkHours(startTimeString, endTimeString);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean validateFields() {
        EditText[] fields = { nameEditText, surnameEditText, emailEditText, passwordEditText, confirmPasswordEditText,
                countryText, cityText, postalCodeText, streetText, streetNumberText, phoneNumberEditText };

        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText().toString().trim())) {
                showToast("Please fill in all fields");
                return false;
            }
        }

        String password = passwordEditText.getText().toString().trim();
        String confirmPassword = confirmPasswordEditText.getText().toString().trim();

        if (!password.equals(confirmPassword)) {
            showToast("Passwords do not match");
            return false;
        }

        if (!isValidEmail(emailEditText.getText().toString().trim())) {
            showToast("Please enter a valid email address");
            return false;
        }

        LocalDate startDate = getDateFromDatePicker(startDatePicker);
        LocalDate endDate = getDateFromDatePicker(endDatePicker);

        if (endDate.isBefore(startDate)) {
            showToast("End date cannot be before start date");
            return false;
        }

        return validateWorkHours();
    }

    private boolean validateWorkHours() {
        if (timePickerSwitch.isChecked()) {
            String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

            for (String day : daysOfWeek) {
                EmployeeWorkHours workHours = getWorkHoursFromBindings(day);
                if (workHours != null) {
                    if (!validate(workHours.getStartTime(), workHours.getEndTime())) {
                        showToast(day + ": Work hours are not valid");
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private Boolean validate(String startTime, String endTime) {
        LocalTime start = LocalTime.parse(startTime);
        LocalTime end = LocalTime.parse(endTime);

        return !start.isAfter(end);
    }

    private boolean isValidEmail(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.matches(emailPattern);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}