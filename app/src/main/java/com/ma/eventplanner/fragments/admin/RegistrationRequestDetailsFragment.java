package com.ma.eventplanner.fragments.admin;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.LoginActivity;
import com.ma.eventplanner.database.AuthenticationManager;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.CompaniesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.ImagesOperations;
import com.ma.eventplanner.database.RegistrationRequestsOperations;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.RegistrationRequest;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.WeeklyWorkingSchedule;
import com.ma.eventplanner.model.WorkDays;
import com.ma.eventplanner.utils.EmailSender;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import android.util.Log;

public class RegistrationRequestDetailsFragment extends Fragment {
    private String registrationRequestId;
    private RegistrationRequest request;
    private List<Category> categories;
    private List<EventType> eventTypes;
    private RegistrationRequestsOperations registrationRequestsOperations;
    private CategoriesOperations categoriesOperations;
    private EventTypesOperations eventTypesOperations;
    private ImagesOperations imagesOperations;
    private AuthenticationManager authenticationManager;
    private Bitmap ownerProfilePicture;
    private Bitmap companyPicture;
    private ImageView companyLogoImageView;
    private ImageView ownerPhotoImageView;
    private Button acceptButton;
    private Button rejectButton;

    public RegistrationRequestDetailsFragment() {
    }

    public static RegistrationRequestDetailsFragment newInstance(String param1, String param2) {
        RegistrationRequestDetailsFragment fragment = new RegistrationRequestDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle currentArguments = getArguments();

        if (currentArguments == null) {
            return;
        }


        registrationRequestId = currentArguments.getString("selected_request_id");

        registrationRequestsOperations = new RegistrationRequestsOperations();
        categoriesOperations = new CategoriesOperations();
        eventTypesOperations = new EventTypesOperations();
        imagesOperations = new ImagesOperations();
        authenticationManager = new AuthenticationManager();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_registration_request_details, container, false);
        getRegistrationRequest();

        companyLogoImageView = view.findViewById(R.id.company_image);
        ownerPhotoImageView = view.findViewById(R.id.owner_profile_image);
        return view;
    }


    private void getRegistrationRequest() {
        registrationRequestsOperations.getById(registrationRequestId, new RegistrationRequestsOperations.GetRegistrationRequestByIdListener<RegistrationRequest>() {
            @Override
            public void onSuccess(RegistrationRequest result) {
                request = result;
                getCategories();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestDetailsFragment", String.valueOf(e));
            }
        });
    }

    private void getCategories() {
        categoriesOperations.getAllByIds(request.getCompany().getCategoryIds(), new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categories = result;
                getEventTypes();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestDetailsFragment", String.valueOf(e));
            }
        });
    }

    private void getEventTypes() {
        eventTypesOperations.getAllByIds(request.getCompany().getEventTypeIds(), new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                eventTypes = result;
                loadImages();
                displayRegistrationRequestDetails();
                displayWorkingHours();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestDetailsFragment", String.valueOf(e));
            }
        });
    }

    private void displayRegistrationRequestDetails() {
        TextView ownerFirstNameTextView = getView().findViewById(R.id.owner_first_name);
        TextView ownerLastNameTextView = getView().findViewById(R.id.owner_last_name);
        TextView ownerEmailTextView = getView().findViewById(R.id.owner_email);
        TextView ownerAddressTextView = getView().findViewById(R.id.owner_address);
        TextView ownerPhoneTextView = getView().findViewById(R.id.owner_phone);

        ownerFirstNameTextView.setText(request.getOwner().getFirstName());
        ownerLastNameTextView.setText(request.getOwner().getLastName());
        ownerEmailTextView.setText(request.getOwner().getEmail());
        ownerAddressTextView.setText(request.getOwner().getAddress().toString());
        ownerPhoneTextView.setText(request.getOwner().getPhoneNumber());

        TextView companyNameTextView = getView().findViewById(R.id.company_name);
        TextView companyEmailTextView = getView().findViewById(R.id.company_email);
        TextView companyAddressTextView = getView().findViewById(R.id.company_address);
        TextView companyPhoneTextView = getView().findViewById(R.id.company_phone);
        TextView companyDescriptionTextView = getView().findViewById(R.id.company_description);

        companyNameTextView.setText(request.getCompany().getName());
        companyEmailTextView.setText(request.getCompany().getEmail());
        companyAddressTextView.setText(request.getCompany().getAddress().toString());
        companyPhoneTextView.setText(request.getCompany().getPhoneNumber());
        companyDescriptionTextView.setText(request.getCompany().getDescription());

        ListView companyCategoriesListView = getView().findViewById(R.id.company_categories);
        ListView companyEventTypesListView = getView().findViewById(R.id.company_event_types);

        List<String> categoryNames = new ArrayList<>();
        for (Category category : categories) {
            categoryNames.add(category.getName());
        }

        List<String> eventTypeNames = new ArrayList<>();
        for (EventType eventType : eventTypes) {
            eventTypeNames.add(eventType.getTypeName());
        }

        ArrayAdapter<String> categoriesAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, categoryNames);
        companyCategoriesListView.setAdapter(categoriesAdapter);

        ArrayAdapter<String> eventTypesAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, eventTypeNames);
        companyEventTypesListView.setAdapter(eventTypesAdapter);


        acceptButton = getView().findViewById(R.id.accept_reg_req);
        rejectButton = getView().findViewById(R.id.reject_reg_req);

        setButtonStates();

        acceptButton.setOnClickListener(v -> acceptRequest());
        rejectButton.setOnClickListener(v -> rejectRequest());

    }

    private void setButtonStates() {
        if (request.getStatus() == RequestStatus.ON_PENDING) {
            acceptButton.setEnabled(true);
            rejectButton.setEnabled(true);

            acceptButton.setOnClickListener(v -> acceptRequest());
            rejectButton.setOnClickListener(v -> rejectRequest());
        } else {
            acceptButton.setEnabled(false);
            rejectButton.setEnabled(false);
        }
    }

    private void acceptRequest() {
        request.accept();

        RegistrationRequestsOperations registrationRequestsOptions = new RegistrationRequestsOperations();
        registrationRequestsOptions.update(request, new RegistrationRequestsOperations.OnSaveSuccessListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Request updated successfully.");
                registerCompany(request);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error updating request: " + e.getMessage());
            }
        });
    }

    private void registerCompany(RegistrationRequest registrationRequest) {
        String getPath = "/requests/company-images/";
        String savePath = "/company-images/";
        Company company = registrationRequest.getCompany();
        String imageName = company.getImageName();
        ImagesOperations imagesOperations = new ImagesOperations();

        imagesOperations.get(getPath, imageName)
                .addOnSuccessListener(imageBytes -> {
                    ImagesOperations saveImagesOperations = new ImagesOperations();
                    saveImagesOperations.save(savePath, imageName, imageBytes)
                            .addOnSuccessListener(imageUrl -> {
                                saveCompany(company, registrationRequest.getOwner());
                            })
                            .addOnFailureListener(exception -> {
                                Log.e(TAG, "Error: " + exception.getMessage());
                            });
                })
                .addOnFailureListener(exception -> {
                    Log.e(TAG, "Error: " + exception.getMessage());
                });
    }

    private void saveCompany(Company company, Owner owner) {
        CompaniesOperations companiesOperations = new CompaniesOperations();
        companiesOperations.save(company, new CompaniesOperations.CompanySaveListener() {
            @Override
            public void onSuccess(String companyId) {
                registerUser(owner, companyId);
                Log.d(TAG, "Company is saved. CompanyId: " + companyId);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error: " + e.getMessage());
            }
        });
    }

    private void registerUser(Owner owner, String companyId) {
        String path = "/requests/profile-images/";

        owner.setCompanyId(companyId);
        String imageName = owner.getImageName();
        ImagesOperations imagesOperations = new ImagesOperations();

        imagesOperations.get(path, imageName)
                .addOnSuccessListener(imageBytes -> {

                    Intent intent = new Intent();
                    intent.putExtra("user_image", imageBytes);

                    authenticationManager.createUserWithEmailAndPassword(owner.getEmail(), owner.getPassword(), getActivity(), owner, intent, new AuthenticationManager.OnUserCreationListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(requireContext(), "User is create successful", Toast.LENGTH_SHORT).show();
                            navigateToDummyFragment();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Failed to create user: " + e.getMessage(), e);
                            Toast.makeText(requireContext(), "Failed to create user: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });

                })
                .addOnFailureListener(exception -> {
                    Log.e(TAG, "Error: " + exception.getMessage());
                });
    }

    private void loadImages() {
        String companyImagePath = "/requests/company-images/";
        String ownerImagePath = "/requests/profile-images/";

        String companyImageName = request.getCompany().getImageName();
        String ownerImageName = request.getCompany().getImageName();


        imagesOperations.get(companyImagePath, companyImageName).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                companyPicture = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                companyLogoImageView.setImageBitmap(companyPicture);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestDetailsFragment", "Error loading company logo", e);
            }
        });

        imagesOperations.get(ownerImagePath, ownerImageName).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                ownerProfilePicture = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                ownerPhotoImageView.setImageBitmap(ownerProfilePicture);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Log.e("RegistrationRequestDetailsFragment", "Error loading owner photo", e);
            }
        });
    }

    private void displayWorkingHours() {
        TextView mondayWorkingHoursTextView = getView().findViewById(R.id.monday_working_hours);
        TextView tuesdayWorkingHoursTextView = getView().findViewById(R.id.tuesday_working_hours);
        TextView wednesdayWorkingHoursTextView = getView().findViewById(R.id.wednesday_working_hours);
        TextView thursdayWorkingHoursTextView = getView().findViewById(R.id.thursday_working_hours);
        TextView fridayWorkingHoursTextView = getView().findViewById(R.id.friday_working_hours);
        TextView saturdayWorkingHoursTextView = getView().findViewById(R.id.saturday_working_hours);
        TextView sundayWorkingHoursTextView = getView().findViewById(R.id.sunday_working_hours);

        WeeklyWorkingSchedule schedule = request.getCompany().getWeeklyWorkingSchedule();
        setDayWorkingHours(mondayWorkingHoursTextView, schedule.getMonday());
        setDayWorkingHours(tuesdayWorkingHoursTextView, schedule.getTuesday());
        setDayWorkingHours(wednesdayWorkingHoursTextView, schedule.getWednesday());
        setDayWorkingHours(thursdayWorkingHoursTextView, schedule.getThursday());
        setDayWorkingHours(fridayWorkingHoursTextView, schedule.getFriday());
        setDayWorkingHours(saturdayWorkingHoursTextView, schedule.getSaturday());
        setDayWorkingHours(sundayWorkingHoursTextView, schedule.getSunday());
    }

    private void setDayWorkingHours(TextView textView, WorkDays workDays) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");

        if (workDays != null && workDays.getWorkHours() != null) {
            String startTime = workDays.getWorkHours().getStartTime() != null ? timeFormat.format(workDays.getWorkHours().getStartTime()) : "N/A";
            String endTime = workDays.getWorkHours().getEndTime() != null ? timeFormat.format(workDays.getWorkHours().getEndTime()) : "N/A";
            textView.setText(startTime + " - " + endTime);
        } else {
            textView.setText("N/A");
        }
    }

    private void rejectRequest() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_reject_reason, null);
        EditText rejectReasonEditText = dialogView.findViewById(R.id.reject_reason);

        builder.setTitle("Reject Request")
                .setView(dialogView)
                .setPositiveButton("Apply", (dialog, which) -> {
                    String rejectReason = rejectReasonEditText.getText().toString();
                    request.reject();
                    registrationRequestsOperations.update(request, new RegistrationRequestsOperations.OnSaveSuccessListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(getContext(), "Request rejected successfully", Toast.LENGTH_SHORT).show();
                            sendRejectionEmail(RegistrationRequestDetailsFragment.this.request.getOwner(), rejectReason);
                            navigateToDummyFragment();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error rejecting request: " + e.getMessage(), e);
                        }
                    });
                })
                .setNegativeButton("Cancel", null)
                .show();
    }

    private void sendRejectionEmail(Owner owner, String rejectReason) {
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("eventplannerteam8@gmail.com", "iyhj pyod lxir xvhc");
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("eventplannerteam8@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(owner.getEmail()));
            message.setSubject("Your registration request has been rejected");
            String body = "Dear " + owner.getFirstName() + " " + owner.getLastName() + ",\n\n"
                    + "We regret to inform you that your registration request has been rejected.\n\n"
                    + "Reason for rejection: " + rejectReason + "\n\n"
                    + "Please feel free to contact us if you have any questions or concerns.\n\n"
                    + "Best regards,\n"
                    + "Event Planner Team";
            message.setText(body);

            // Slanje email-a
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Transport.send(message);
                        Log.i(TAG, "Email sent successfully.");
                    } catch (MessagingException e) {
                        Log.e(TAG, "Error sending email: " + e.getMessage(), e);
                    }
                }
            }).start();

        } catch (MessagingException e) {
            Log.e(TAG, "Error creating email message: " + e.getMessage(), e);
        }
    }
    private void navigateToDummyFragment() {
        NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
        navController.navigate(R.id.nav_dummy);
    }
}