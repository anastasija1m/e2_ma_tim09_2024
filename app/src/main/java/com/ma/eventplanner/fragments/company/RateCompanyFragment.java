package com.ma.eventplanner.fragments.company;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.CompanyRatingOperations;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.databinding.FragmentRateCompanyBinding;
import com.ma.eventplanner.model.CompanyRating;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Notification;

import java.util.Date;

public class RateCompanyFragment extends DialogFragment {

    private FragmentRateCompanyBinding binding;
    private EventOrganizer loggedInEventOrganizer;
    private EditText editComment;
    private RadioGroup radioGroupRatings;
    private Button buttonSubmit;
    private Button buttonCancel;
    private String companyId;
    private String mOwnerId;

    public RateCompanyFragment() {

    }

    public static RateCompanyFragment newInstance() {
        return new RateCompanyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loggedInEventOrganizer = new EventOrganizer();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInEventOrganizer = homeActivity.getLoggedInEventOrganizer();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentRateCompanyBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        editComment = root.findViewById(R.id.edit_comment);
        radioGroupRatings = root.findViewById(R.id.radio_group_ratings);
        buttonSubmit = root.findViewById(R.id.button_submit);
        buttonCancel = root.findViewById(R.id.button_cancel);

        buttonSubmit.setOnClickListener(v -> {
            String comment = editComment.getText().toString();
            int selectedRatingId = radioGroupRatings.getCheckedRadioButtonId();

            if (selectedRatingId == -1) {
                Toast.makeText(getContext(), "Please select a rating", Toast.LENGTH_SHORT).show();
                return;
            }

            RadioButton selectedRadioButton = root.findViewById(selectedRatingId);
            Long rating = Long.parseLong(selectedRadioButton.getText().toString());
            Date currentDate = new Date();

            CompanyRating companyRating = new CompanyRating(comment, rating, currentDate, loggedInEventOrganizer.getEmail(), companyId);

            Toast.makeText(getContext(), "Rating submitted: " + rating + "\nComment: " + comment, Toast.LENGTH_SHORT).show();
            CompanyRatingOperations ratingOperations = new CompanyRatingOperations();

            ratingOperations.save(companyRating, new CompanyRatingOperations.SaveCompanyRatingListener() {
                @Override
                public void onSuccess(String companyRatingId) {
                    Toast.makeText(getContext(), "Rating submitted successfully", Toast.LENGTH_SHORT).show();
                    Notification notification = createNotification();
                    NotificationsOperations notificationsOperations = new NotificationsOperations();
                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Notification sent");
                            Navigation.findNavController(requireView()).navigateUp();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error sending notification", e);
                        }
                    });
                }

                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(getContext(), "Failed to submit rating", Toast.LENGTH_SHORT).show();
                    Navigation.findNavController(requireView()).navigateUp();
                }
            });
        });

        buttonCancel.setOnClickListener(v ->
                Navigation.findNavController(requireView()).navigateUp());

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("companyId") && bundle.containsKey("ownerId")) {
            companyId = (String) bundle.getSerializable("companyId");
            mOwnerId = (String) bundle.getSerializable("ownerId");
        }
    }

    private Notification createNotification() {
        String title = "New review has been added!";
        String content = String.format("%s has left a review for your company!", loggedInEventOrganizer.getEmail());
        return new Notification("", title, content, mOwnerId);
    }

}