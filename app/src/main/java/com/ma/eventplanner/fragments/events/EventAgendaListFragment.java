package com.ma.eventplanner.fragments.events;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.EventAgendaListAdapter;
import com.ma.eventplanner.adapters.EventsListAdapter;
import com.ma.eventplanner.databinding.FragmentEventAgendaBinding;
import com.ma.eventplanner.databinding.FragmentEventAgendaListBinding;
import com.ma.eventplanner.databinding.FragmentEventsListBinding;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventActivity;

import java.util.ArrayList;

public class EventAgendaListFragment extends ListFragment {

    private EventAgendaListAdapter eventAgendaListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<EventActivity> mActivities;
    private FragmentEventAgendaListBinding binding;
    public EventAgendaListFragment() {}
    public static EventAgendaListFragment newInstance(ArrayList<EventActivity> activities) {
        EventAgendaListFragment fragment = new EventAgendaListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, activities);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mActivities = getArguments().getParcelableArrayList(ARG_PARAM);
            eventAgendaListAdapter = new EventAgendaListAdapter(getActivity(), mActivities);
            setListAdapter(eventAgendaListAdapter);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventAgendaListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}