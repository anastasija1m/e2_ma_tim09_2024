package com.ma.eventplanner.fragments.employees;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.adapters.employee.EmployeeReservationsListAdapter;
import com.ma.eventplanner.databinding.FragmentEmployeeReservationListBinding;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.ServiceReservation;

import java.util.ArrayList;

public class EmployeeReservationListFragment extends ListFragment {

    private EmployeeReservationsListAdapter employeeReservationsListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<ServiceReservation> mServiceReservations;
    private FragmentEmployeeReservationListBinding binding;
    private Employee loggedInEmployee;

    public EmployeeReservationListFragment() {
    }

    public static EmployeeReservationListFragment newInstance(ArrayList<ServiceReservation> serviceReservations) {
        EmployeeReservationListFragment employeeReservationListFragment = new EmployeeReservationListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, serviceReservations);
        employeeReservationListFragment.setArguments(args);

        return employeeReservationListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loggedInEmployee = new Employee();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInEmployee = homeActivity.getLoggedInEmployee();
        if (getArguments() != null) {
            mServiceReservations = getArguments().getParcelableArrayList(ARG_PARAM);
            employeeReservationsListAdapter = new EmployeeReservationsListAdapter(getActivity(), mServiceReservations, loggedInEmployee);
            setListAdapter(employeeReservationsListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeeReservationListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}