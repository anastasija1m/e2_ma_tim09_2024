package com.ma.eventplanner.fragments.products;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.CategoryListAdapter;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.BottomSheetProductFilterBinding;
import com.ma.eventplanner.databinding.FragmentProductsPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.fragments.employees.EmployeesListFragment;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventStatus;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProductsPageFragment extends Fragment implements View.OnClickListener {

    private static ArrayList<Product> products = new ArrayList<>();
    private ProductsPageViewModel productsViewModel;
    private FragmentProductsPageBinding binding;
    private BottomSheetProductFilterBinding filterBinding;
    private String loggedUserId;
    private String loggedUserCompanyId;
    private static ProductsOperations productsOperations;
    private UsersOperations usersOperations;
    private RadioGroup categoryRadioGroup, subcategoryRadioGroup, eventtypeRadioGroup;
    private CheckBox availableCheckbox;
    private EditText descriptionEditText;
    private CategoriesOperations categoriesOperations;
    private SubcategoriesOperations subcategoriesOperations;
    private EventTypesOperations eventTypesOperations;
    private View filterRoot;
    private BottomSheetDialog bottomSheetDialog;
    private List<Category> selectedCategories = new ArrayList<>();
    private List<Subcategory> selectedSubcategories = new ArrayList<>();
    private int selectedPrice;
    private boolean isAvailable;
    private String descriptionQuery;
    private List<EventType> selectedEventTypes = new ArrayList<>();
    private List<Category> categories = new ArrayList<>();
    private List<Subcategory> subcategories = new ArrayList<>();
    private List<EventType> eventTypes = new ArrayList<>();
    public ProductsPageFragment() {}
    public static ProductsPageFragment newInstance() { return new ProductsPageFragment();}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        productsViewModel = new ViewModelProvider(this).get(ProductsPageViewModel.class);
        binding = FragmentProductsPageBinding.inflate(inflater, container, false);
        filterBinding = BottomSheetProductFilterBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        filterRoot = filterBinding.getRoot();
        usersOperations = new UsersOperations();
        productsOperations = new ProductsOperations();
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");
        final Role[] role = new Role[1];
        usersOperations.getEmployeeById(loggedUserId, new UsersOperations.OnGetEmployeeByIdListener() {
            @Override
            public void onSuccess(Employee employee) {
                loggedUserCompanyId = employee.getCompanyId();
                role[0] = employee.getRole();
            }

            @Override
            public void onFailure(Exception e) {
                usersOperations.getOwnerById(loggedUserId, new UsersOperations.OnGetOwnerByIdListener() {
                    @Override
                    public void onSuccess(Owner owner) {
                        loggedUserCompanyId = owner.getCompanyId();
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
            }
        });

        // Promeniti u loggedUserCompany
        productsOperations.getAllByCompanyId("1L", new ProductsOperations.GetAllProductsListener<Product>() {
            @Override
            public void onSuccess(ArrayList<Product> result) {
                products = result;
                ProductsListFragment productsListFragment = ProductsListFragment.newInstance(products);
                FragmentTransition.to(productsListFragment, getActivity(), false, R.id.scroll_products_list);
            }

            @Override
            public void onFailure(Exception e) {
            }
        });


        SearchView searchView = binding.searchText;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                productsViewModel.setSearchText(newText);
                return true;
            }
        });

        productsViewModel.getText().observe(getViewLifecycleOwner(), this::filterProducts);

        AppCompatImageButton registerButton = root.findViewById(R.id.new_product_button);
        registerButton.setOnClickListener(this);

        if(role[0] == Role.EMPLOYEE){
            registerButton.setVisibility(View.GONE);
        }

        categoryRadioGroup = filterRoot.findViewById(R.id.category_radio_group);
        categoriesOperations = new CategoriesOperations();

        subcategoryRadioGroup = filterRoot.findViewById(R.id.subcategory_radio_group);
        subcategoriesOperations = new SubcategoriesOperations();

        eventtypeRadioGroup = filterRoot.findViewById(R.id.eventtype_radio_group);
        eventTypesOperations = new EventTypesOperations();

        SeekBar priceSeekBar = filterRoot.findViewById(R.id.price_range_seekbar);
        TextView minPriceTextView = filterRoot.findViewById(R.id.min_price_textview);

        availableCheckbox = filterRoot.findViewById(R.id.available_checkbox);
        descriptionEditText = filterRoot.findViewById(R.id.description);
        priceSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Update the text of the TextViews as per the SeekBar progress
                minPriceTextView.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Not needed for this implementation
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Not needed for this implementation
            }
        });


        AppCompatButton filterButton = root.findViewById(R.id.productFilters);
        filterButton.setOnClickListener(v -> {

            if (bottomSheetDialog == null || !bottomSheetDialog.isShowing()) {
                bottomSheetDialog = new BottomSheetDialog(requireContext(), R.style.FullScreenBottomSheetDialog);
                retrieveCategoriesFromDatabase();
                retrieveSubcategoriesFromDatabase();
                retrieveEventsFromDatabase();

            } else {
                bottomSheetDialog.dismiss();
            }

        });

        AppCompatButton applyFilterButton = filterRoot.findViewById(R.id.apply_filter_button);
        applyFilterButton.setOnClickListener(v -> {
            // Get selected categories
            selectedCategories.clear();
            for (int i = 0; i < categoryRadioGroup.getChildCount(); i++) {
                RadioButton radioButton = (RadioButton) categoryRadioGroup.getChildAt(i);
                if (radioButton.isChecked()) {
                    // Add the selected category to the list
                    Category selectedCategory = categories.get(i); // Assuming 'categories' is the list retrieved from the database
                    selectedCategories.add(selectedCategory);
                }
            }

            // Get selected subcategories
            selectedSubcategories.clear();
            for (int i = 0; i < subcategoryRadioGroup.getChildCount(); i++) {
                RadioButton radioButton = (RadioButton) subcategoryRadioGroup.getChildAt(i);
                if (radioButton.isChecked()) {
                    // Add the selected subcategory to the list
                    Subcategory selectedSubcategory = subcategories.get(i); // Assuming 'subcategories' is the list retrieved from the database
                    selectedSubcategories.add(selectedSubcategory);
                }
            }

            // Get selected price
            selectedPrice = priceSeekBar.getProgress();

            // Get availability status
            isAvailable = availableCheckbox.isChecked();

            // Get description query
            if(descriptionEditText==null){
                descriptionQuery = "";
            } else {
            descriptionQuery = descriptionEditText.getText().toString();}

            // Get selected event types
            selectedEventTypes.clear();
            for (int i = 0; i < eventtypeRadioGroup.getChildCount(); i++) {
                RadioButton radioButton = (RadioButton) eventtypeRadioGroup.getChildAt(i);
                if (radioButton.isChecked()) {
                    // Add the selected event type to the list
                    EventType selectedEventType = eventTypes.get(i); // Assuming 'eventTypes' is the list retrieved from the database
                    selectedEventTypes.add(selectedEventType);
                }
            }

            // Perform filtering based on the selected parameters
            ArrayList<Product> filteredProducts = filterProducts(products, selectedCategories, selectedSubcategories, selectedPrice, isAvailable, descriptionQuery, selectedEventTypes);
            ProductsListFragment productsListFragment = ProductsListFragment.newInstance(filteredProducts);
            FragmentTransition.to(productsListFragment, getActivity(), false, R.id.scroll_products_list);
        });


        return root;
    }

    private ArrayList<Product> filterProducts(List<Product> products, List<Category> selectedCategories, List<Subcategory> selectedSubcategories, int selectedPrice, boolean isAvailable, String descriptionQuery, List<EventType> selectedEventTypes) {
        ArrayList<Product> filteredProducts = new ArrayList<>();

        for (Product product : products) {
            boolean meetsCriteria = true;

            // Check if the product belongs to any of the selected categories
            if (!selectedCategories.isEmpty()) {
                boolean categoryMatch = false;
                for (Category selectedCategory : selectedCategories) {
                    if (selectedCategory.getId().equals(product.getCategory().getId())) {
                        categoryMatch = true;
                        break;
                    }
                }
                if (!categoryMatch) {
                    meetsCriteria = false;
                }
            }

            // Check if the product belongs to any of the selected subcategories
            if (!selectedSubcategories.isEmpty()) {
                boolean subcategoryMatch = false;
                for (Subcategory selectedSubcategory : selectedSubcategories) {
                    if (selectedSubcategory.getId().equals(product.getSubcategory().getId())) {
                        subcategoryMatch = true;
                        break;
                    }
                }
                if (!subcategoryMatch) {
                    meetsCriteria = false;
                }
            }

            // Check if the product price is within the selected price range
            if (selectedPrice != 0 && product.getPrice() > selectedPrice) {
                meetsCriteria = false;
            }

            // Check if the product is available
            if (isAvailable && !product.isAvailable()) {
                meetsCriteria = false;
            }

            // Check if the product description contains the query string
            if (!descriptionQuery.isEmpty() && !product.getDescription().toLowerCase().contains(descriptionQuery.toLowerCase())) {
                meetsCriteria = false;
            }

            // Check if the product belongs to any of the selected event types
            if (!selectedEventTypes.isEmpty()) {
                List<EventType> productEvents = product.getEvents();
                boolean hasMatchingEventType = false;
                for (EventType eventType : productEvents) {
                    if (selectedEventTypes.contains(eventType)) {
                        hasMatchingEventType = true;
                        break;
                    }
                }
                if (!hasMatchingEventType) {
                    meetsCriteria = false;
                }
            }

            if (meetsCriteria) {
                filteredProducts.add(product);
            }
        }

        return filteredProducts;
    }



    private void retrieveCategoriesFromDatabase() {
        // Assuming you have a method to retrieve categories from the database
        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                // Populate radio buttons here after successfully retrieving categories
                populateRadioButtons(result);
                categories = result;
                bottomSheetDialog.setContentView(filterRoot); // Use the root view of BottomSheetProductFilterBinding
                bottomSheetDialog.show();
            }

            @Override
            public void onFailure(Exception e) {
                // Handle failure case
            }
        });
    }


    private void populateRadioButtons(List<Category> categories) {
        // Clear existing radio buttons
        categoryRadioGroup.removeAllViews();

        // Loop through the list of categories
        int i = 1;
        for (Category category : categories) {
            // Create a new radio button
            RadioButton radioButton = new RadioButton(requireContext());
            radioButton.setText(category.getName());
            radioButton.setId(i); // Set a unique ID for the radio button
            i++;

            // Add the radio button to the radio group
            categoryRadioGroup.addView(radioButton);
        }
    }

    private void retrieveSubcategoriesFromDatabase() {
        // Assuming you have a method to retrieve categories from the database
        subcategoriesOperations.getAll(new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                // Populate radio buttons here after successfully retrieving categories

                populateSubRadioButtons(result);
                subcategories = result;
                bottomSheetDialog.setContentView(filterRoot); // Use the root view of BottomSheetProductFilterBinding
                bottomSheetDialog.show();
            }

            @Override
            public void onFailure(Exception e) {
                // Handle failure case
            }
        });
    }


    private void populateSubRadioButtons(List<Subcategory> categories) {
        // Clear existing radio buttons
        subcategoryRadioGroup.removeAllViews();

        // Loop through the list of categories
        int i = 1;
        for (Subcategory category : categories) {
            // Create a new radio button
            RadioButton radioButton = new RadioButton(requireContext());
            radioButton.setText(category.getName());
            radioButton.setId(i); // Set a unique ID for the radio button
            i++;

            // Add the radio button to the radio group
            subcategoryRadioGroup.addView(radioButton);
        }
    }

    private void retrieveEventsFromDatabase() {
        // Assuming you have a method to retrieve categories from the database
        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                // Populate radio buttons here after successfully retrieving categories
                populateRadioEventButtons(result);
                eventTypes = result;
                bottomSheetDialog.setContentView(filterRoot); // Use the root view of BottomSheetProductFilterBinding
                bottomSheetDialog.show();
            }

            @Override
            public void onFailure(Exception e) {
                // Handle failure case
            }
        });
    }


    private void populateRadioEventButtons(List<EventType> categories) {
        // Clear existing radio buttons
        eventtypeRadioGroup.removeAllViews();

        // Loop through the list of categories
        int i = 1;
        for (EventType category : categories) {
            // Create a new radio button
            RadioButton radioButton = new RadioButton(requireContext());
            radioButton.setText(category.getTypeName());
            radioButton.setId(i); // Set a unique ID for the radio button
            i++;

            // Add the radio button to the radio group
           eventtypeRadioGroup.addView(radioButton);
        }
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }


    private void filterProducts(String query) {
        ArrayList<Product> filteredList = new ArrayList<>();
        String lowerCaseQuery = query.toLowerCase();
        for (Product product : products) {
            if (product.getName().toLowerCase().contains(lowerCaseQuery)) {
                filteredList.add(product);
            }
        }
        ProductsListFragment productsListFragment = ProductsListFragment.newInstance(filteredList);
        FragmentTransition.to(productsListFragment, getActivity(), false, R.id.scroll_products_list);
    }


    private void navigateToNewProduct() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
        Bundle bundle = new Bundle();
        navController.navigate(R.id.nav_new_product, bundle);
    }

    @Override
    public void onClick(View v) {
        navigateToNewProduct();
    }

}