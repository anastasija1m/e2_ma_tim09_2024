package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.RatingReportsAdapter;
import com.ma.eventplanner.databinding.FragmentRatingReportsListBinding;
import com.ma.eventplanner.model.RatingReport;

import java.util.ArrayList;

public class RatingReportsListFragment extends ListFragment {

    private RatingReportsAdapter ratingReportsAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<RatingReport> mRatingReports;
    private FragmentRatingReportsListBinding binding;

    public RatingReportsListFragment() {

    }

    public static RatingReportsListFragment newInstance(ArrayList<RatingReport> ratingReports) {
        RatingReportsListFragment ratingReportsListFragment = new RatingReportsListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, ratingReports);
        ratingReportsListFragment.setArguments(args);

        return ratingReportsListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mRatingReports = getArguments().getParcelableArrayList(ARG_PARAM);
            ratingReportsAdapter = new RatingReportsAdapter(getActivity(), mRatingReports);
            setListAdapter(ratingReportsAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentRatingReportsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}