package com.ma.eventplanner.fragments.services;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.ProductListAdapter;
import com.ma.eventplanner.adapters.ServiceListAdapter;
import com.ma.eventplanner.databinding.FragmentProductsListBinding;
import com.ma.eventplanner.databinding.FragmentServicesListBinding;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Service;

import java.util.ArrayList;


public class ServicesListFragment extends ListFragment {

    private ServiceListAdapter serviceListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Service> mServices;
    private FragmentServicesListBinding binding;

    public ServicesListFragment() {

    }

    public static ServicesListFragment newInstance(ArrayList<Service> services) {
        ServicesListFragment servicesListFragment = new ServicesListFragment();
        Bundle args = new Bundle();
        ArrayList<Service> filtratedList = new ArrayList<>();
        for (Service service:services
        ) {
            if(!service.isDeleted()){
                filtratedList.add(service);
            }
        }
        args.putParcelableArrayList(ARG_PARAM, filtratedList);
        servicesListFragment.setArguments(args);
        return servicesListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            mServices = getArguments().getParcelableArrayList(ARG_PARAM);
            serviceListAdapter = new ServiceListAdapter(getActivity(), mServices);
            setListAdapter(serviceListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentServicesListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}