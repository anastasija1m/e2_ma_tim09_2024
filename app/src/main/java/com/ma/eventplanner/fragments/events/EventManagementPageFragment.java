package com.ma.eventplanner.fragments.events;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.tabs.TabLayout;
import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.EventManagementViewPagerAdapter;
import com.ma.eventplanner.databinding.FragmentEventManagementPageBinding;
import com.ma.eventplanner.model.Event;

public class EventManagementPageFragment extends Fragment {

    FragmentEventManagementPageBinding binding;
    private Event event;
    TabLayout tabLayout;
    ViewPager2 viewPager2;
    Button button;
    EventManagementViewPagerAdapter eventManagementViewPagerAdapter;

    public EventManagementPageFragment() { }

    public static EventManagementPageFragment newInstance() {
        return new EventManagementPageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventManagementPageBinding.inflate(inflater, container, false);
        tabLayout = binding.getRoot().findViewById(R.id.tab_layout_event);
        viewPager2 = binding.getRoot().findViewById(R.id.view_pager_event);

        Bundle args = getArguments();
        if (args != null && args.containsKey("event")) {
            event = args.getParcelable("event");
        }
        eventManagementViewPagerAdapter = new EventManagementViewPagerAdapter(requireActivity(), event);
        viewPager2.setAdapter(eventManagementViewPagerAdapter);

        button = binding.buttonSaveEvent;
        button.setOnClickListener(v ->{
            Navigation.findNavController(requireView()).navigateUp();
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager2.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                tabLayout.selectTab(tabLayout.getTabAt(position));
            }
        });
        return binding.getRoot();
    }

    public Event getEvent() {
        return event;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}