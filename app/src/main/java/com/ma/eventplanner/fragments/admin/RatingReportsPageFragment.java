package com.ma.eventplanner.fragments.admin;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.RatingReportOperations;
import com.ma.eventplanner.database.ServiceReservationsOperations;
import com.ma.eventplanner.databinding.FragmentOwnerServiceReservationsPageBinding;
import com.ma.eventplanner.databinding.FragmentRatingReportsPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.fragments.owners.OwnerReservationViewModel;
import com.ma.eventplanner.fragments.owners.OwnerServiceReservationsList;
import com.ma.eventplanner.fragments.owners.OwnerServiceReservationsPage;
import com.ma.eventplanner.model.RatingReport;
import com.ma.eventplanner.model.ServiceReservation;

import java.util.ArrayList;
import java.util.List;

public class RatingReportsPageFragment extends Fragment {

    private final ArrayList<RatingReport> ratingReports = new ArrayList<>();
    private FragmentRatingReportsPageBinding binding;

    public RatingReportsPageFragment() {

    }

    public static RatingReportsPageFragment newInstance() {
        return new RatingReportsPageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentRatingReportsPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        retrieveRatingReports();

        return root;
    }

    private void retrieveRatingReports() {

        RatingReportOperations ratingReportOperations = new RatingReportOperations();
        ratingReportOperations.getAll(new RatingReportOperations.GetAllRatingReportsListener<RatingReport>() {
            @Override
            public void onSuccess(List<RatingReport> result) {
                ratingReports.clear();
                ratingReports.addAll(result);
                FragmentTransition.to(RatingReportsListFragment.newInstance(ratingReports), requireActivity(), false, R.id.scroll_rating_reports_list);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error retrieving rating reports: ", e);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        retrieveRatingReports();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}