package com.ma.eventplanner.fragments.employees;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.employee.EmployeeListAdapter;
import com.ma.eventplanner.databinding.FragmentEmployeesListBinding;
import com.ma.eventplanner.model.Employee;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class EmployeesListFragment extends ListFragment {

    private EmployeeListAdapter employeeListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Employee> mEmployees;
    private HashMap<String, Employee> mEmployeeMap;
    private FragmentEmployeesListBinding binding;

    public EmployeesListFragment() { }

    public static EmployeesListFragment newInstance(HashMap<String, Employee> employees) {
        EmployeesListFragment employeesListFragment = new EmployeesListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, employees);
        employeesListFragment.setArguments(args);
        return employeesListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Serializable serializable = getArguments().getSerializable(ARG_PARAM);
            if (serializable instanceof HashMap) {
                mEmployeeMap = (HashMap<String, Employee>) serializable;
                mEmployees = new ArrayList<>(mEmployeeMap.values());
                employeeListAdapter = new EmployeeListAdapter(getActivity(), mEmployeeMap);
                setListAdapter(employeeListAdapter);
            }
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeesListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}