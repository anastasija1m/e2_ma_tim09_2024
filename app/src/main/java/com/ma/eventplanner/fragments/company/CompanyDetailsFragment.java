package com.ma.eventplanner.fragments.company;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.CompanyReportOperations;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentCompanyDetailsBinding;
import com.ma.eventplanner.model.Administrator;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.ReportStatus;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.User;
import com.ma.eventplanner.model.CompanyReport;

import java.util.Date;
import java.util.HashMap;

public class CompanyDetailsFragment extends Fragment {

    private FragmentCompanyDetailsBinding binding;
    private Company company;
    private User loggedInUser;
    private EventOrganizer organizer;
    private Administrator admin;
    private Button reportCompanyButton;

    public CompanyDetailsFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentCompanyDetailsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        if (getArguments() != null) {
            company = getArguments().getParcelable("company");
        }

        loggedInUser = new User();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInUser = homeActivity.getLoggedInUser();
        organizer = homeActivity.getLoggedInEventOrganizer();

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView companyNameTextView = view.findViewById(R.id.company_name);
        reportCompanyButton = view.findViewById(R.id.report_button);

        reportCompanyButton.setOnClickListener(this::onReportButtonClick);

        if (company != null) {
            companyNameTextView.setText(company.getName());
        } else {
            Toast.makeText(requireContext(), "Company object is null", Toast.LENGTH_SHORT).show();
        }
    }

    private void onReportButtonClick(View v) {
        if (company != null && loggedInUser != null) {
            showReportDialog();
        } else {
            Toast.makeText(requireContext(), "User or company is null", Toast.LENGTH_SHORT).show();
        }
    }

    private void showReportDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        View dialogView = LayoutInflater.from(requireContext()).inflate(R.layout.company_report_dialog, null);
        EditText reportReasonEditText = dialogView.findViewById(R.id.report_reason_edittext);
        Button submitReportButton = dialogView.findViewById(R.id.submit_report_button);

        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();

        submitReportButton.setOnClickListener(v -> {
            String reportReason = reportReasonEditText.getText().toString().trim();
            if (!reportReason.isEmpty()) {
                submitReport(reportReason);
                alertDialog.dismiss();
            } else {
                reportReasonEditText.setError(getString(R.string.error_empty_reason));
            }
        });

        alertDialog.show();
    }


    private void submitReport(String reportReason) {
        CompanyReport companyReport = new CompanyReport();
        companyReport.setReporterId(organizer.getEmail());
        companyReport.setReportedCompanyId(company.getId());
        companyReport.setReportDate(new Date());
        companyReport.setReportReason(reportReason);
        companyReport.setStatus(ReportStatus.REPORTED);

        CompanyReportOperations companyReportOperations = new CompanyReportOperations();
        companyReportOperations.save(companyReport, new CompanyReportOperations.CompanyReportSaveListener() {
            @Override
            public void onSuccess(String reportId) {
                notifyAdministrator();
                Toast.makeText(requireContext(), "Report submitted successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(requireContext(), "Failed to submit report", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void notifyAdministrator() {
        Notification notification = new Notification();
        notification.setTitle("Company Reported");
        notification.setContent("A company has been reported.");
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getAllByRole(Role.ADMIN, new UsersOperations.OnGetAllByRoleListener() {
            @Override
            public void onSuccess(HashMap<String, User> userMap) {
                NotificationsOperations notificationsOperations = new NotificationsOperations();
                for (String userId : userMap.keySet()) {
                    // For each admin user ID, create a notification and save it
                    Notification adminNotification = new Notification();
                    adminNotification.setTitle(notification.getTitle());
                    adminNotification.setContent(notification.getContent());
                    adminNotification.setRecipientId(userId);

                    notificationsOperations.save(adminNotification, new NotificationsOperations.OnSaveSuccessListener() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(requireContext(), "Administrator notified", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Toast.makeText(requireContext(), "Failed to notify administrator", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Exception e) {
                // Handle failure to fetch admins
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
