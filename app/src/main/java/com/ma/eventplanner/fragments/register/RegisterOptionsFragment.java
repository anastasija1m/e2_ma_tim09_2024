package com.ma.eventplanner.fragments.register;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.fragments.register.owner.PersonalDataFormFragment;

import androidx.annotation.Nullable;

public class RegisterOptionsFragment extends Fragment {

    public RegisterOptionsFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static RegisterOptionsFragment newInstance(String param1, String param2) {
        RegisterOptionsFragment fragment = new RegisterOptionsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register_options, container, false);

        setupOrganizerButton(view);
        setupOwnerButton(view);

        return view;
    }

    private void setupOrganizerButton(@NonNull View view) {
        view.findViewById(R.id.register_organizer_button).setOnClickListener(this::onOrganizerButtonClick);
    }

    private void setupOwnerButton(@NonNull View view) {
        view.findViewById(R.id.register_owner_button).setOnClickListener(this::onOwnerButtonClick);
    }

    private void onOrganizerButtonClick(View v) {
        replaceFragment(new com.ma.eventplanner.fragments.register.organizer.PersonalDataFormFragment());
    }

    private void onOwnerButtonClick(View v) {
        replaceFragment(new PersonalDataFormFragment());
    }

    private void replaceFragment(Fragment fragment) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
