package com.ma.eventplanner.fragments.register.owner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.model.Address;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Role;

import java.util.Objects;

public class CompanyDataFormFragment extends Fragment {

    private TextInputEditText emailInput, nameInput, countryInput,
            cityInput, postalCodeInput, streetInput,
            numberInput, phoneNumberInput, descriptionInput;

    private ImageView imageView;
    private Intent image;
    private Button uploadImageButton;

    public CompanyDataFormFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static CompanyDataFormFragment newInstance() {
        CompanyDataFormFragment fragment = new CompanyDataFormFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_company_data_form, container, false);
        initializeViews(view);
        setupNextButton(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUploadImageButton();
    }

    private void initializeViews(@NonNull View rootView) {
        emailInput = rootView.findViewById(R.id.company_email);
        nameInput = rootView.findViewById(R.id.company_name);

        countryInput = rootView.findViewById(R.id.company_country);
        cityInput = rootView.findViewById(R.id.company_city);
        postalCodeInput = rootView.findViewById(R.id.company_postal_code);
        streetInput = rootView.findViewById(R.id.company_street);
        numberInput = rootView.findViewById(R.id.company_house_number);

        phoneNumberInput = rootView.findViewById(R.id.company_phone_number);
        descriptionInput = rootView.findViewById(R.id.company_description);

        uploadImageButton = rootView.findViewById(R.id.company_upload_button);

        imageView = rootView.findViewById(R.id.company_picture_view);

    }

    @NonNull
    private Company setCompanyData() {
        String email = Objects.requireNonNull(emailInput.getText()).toString();
        String name = Objects.requireNonNull(nameInput.getText()).toString();
        String description = Objects.requireNonNull(descriptionInput.getText()).toString();
        String phoneNumber = Objects.requireNonNull(phoneNumberInput.getText()).toString();

        Address address = setAddress();

        return new Company("", email, name, address, phoneNumber, description, "");
    }

    @NonNull
    private Address setAddress() {
        String country = Objects.requireNonNull(countryInput.getText()).toString();
        String city = Objects.requireNonNull(cityInput.getText()).toString();
        String postalCode = Objects.requireNonNull(postalCodeInput.getText()).toString();
        String street = Objects.requireNonNull(streetInput.getText()).toString();
        String number = Objects.requireNonNull(numberInput.getText()).toString();

        return new Address(country, city, postalCode, street, number);
    }

    private void setupNextButton(@NonNull View view) {
        Button nextButton = view.findViewById(R.id.company_next_button);
        nextButton.setOnClickListener(this::onNextButtonClick);
    }

    private void onNextButtonClick(View v) {
        Bundle currentArguments = getArguments();

        if (currentArguments == null) {
            return;
        }

        Company company = setCompanyData();

        currentArguments.putParcelable("company", company);
        currentArguments.putParcelable("companyImage", image);

        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SelectCategoriesFragment selectCategoriesFragment = new SelectCategoriesFragment();
        selectCategoriesFragment.setArguments(currentArguments);
        fragmentTransaction.replace(R.id.fragment_container, selectCategoriesFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void setupUploadImageButton() {
        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUploadOptions();
            }
        });
    }

    private final ActivityResultLauncher<Intent> imageChooserLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    image = result.getData();
                    if (image != null) {
                        Uri selectedImageUri = image.getData();
                        if (selectedImageUri != null) {
                            imageView.setImageURI(selectedImageUri);
                        } else {
                            Bitmap bitmap = (Bitmap) Objects.requireNonNull(image.getExtras()).get("data");
                            imageView.setImageBitmap(bitmap);
                        }
                    }
                }
            });

    private void showUploadOptions() {
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent chooserIntent = Intent.createChooser(pickPhotoIntent, "Select Image");
        imageChooserLauncher.launch(chooserIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
