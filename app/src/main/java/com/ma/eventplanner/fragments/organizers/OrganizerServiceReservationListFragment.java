package com.ma.eventplanner.fragments.organizers;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.adapters.OrganizerReservationListAdapter;
import com.ma.eventplanner.databinding.FragmentOrganizerServiceReservationListBinding;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.ServiceReservation;

import java.util.ArrayList;

public class OrganizerServiceReservationListFragment extends ListFragment {

    private OrganizerReservationListAdapter organizerReservationListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<ServiceReservation> mServiceReservations;
    private FragmentOrganizerServiceReservationListBinding binding;
    private EventOrganizer loggedInEventOrganizer;

    public OrganizerServiceReservationListFragment() {

    }

    public static OrganizerServiceReservationListFragment newInstance(ArrayList<ServiceReservation> serviceReservations) {
        OrganizerServiceReservationListFragment organizerServiceReservationListFragment = new OrganizerServiceReservationListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, serviceReservations);
        organizerServiceReservationListFragment.setArguments(args);

        return organizerServiceReservationListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loggedInEventOrganizer = new EventOrganizer();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInEventOrganizer = homeActivity.getLoggedInEventOrganizer();
        if (getArguments() != null) {
            mServiceReservations = getArguments().getParcelableArrayList(ARG_PARAM);
            organizerReservationListAdapter = new OrganizerReservationListAdapter(getActivity(), mServiceReservations, loggedInEventOrganizer);
            setListAdapter(organizerReservationListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentOrganizerServiceReservationListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}