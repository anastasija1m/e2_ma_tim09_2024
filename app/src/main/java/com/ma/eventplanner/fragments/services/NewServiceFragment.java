package com.ma.eventplanner.fragments.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.ProductRequestsOperations;
import com.ma.eventplanner.database.ServiceRequestsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentNewServiceBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.AcceptanceType;
import com.ma.eventplanner.model.Address;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.ProductRequest;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.ServiceRequest;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class NewServiceFragment extends Fragment {

    private FragmentNewServiceBinding binding;
    private EditText nameEditText, durationEditText, descriptionEditText, priceEditText, discountEditText, specificitiesEditText, reservationDeadlineEditText, cancellationDeadlineEditText, requestSubcategoryEditText;
    private Spinner categorySpinner, subcategorySpinner, eventTypeSpinner, employeeSpinner, acceptanceCriteriaSpinner;
    private CheckBox availableCheckBox, visibleCheckBox;
    private RecyclerView selectedEmployeesRecyclerView, selectedEventsRecyclerView;
    private List<Category> categories;
    private List<Subcategory> subcategories;
    private List<EventType> eventTypes;
    private List<Employee> employees;
    private List<AcceptanceType> acceptanceCriteria;
    private List<Employee> selectedEmployees = new ArrayList<>();
    private List<EventType> selectedEventTypes = new ArrayList<>();
    private ServicesOperations servicesOperations;
    private EventTypesOperations eventTypesOperations;
    private UsersOperations usersOperations;
    String loggedUserId;
    String loggedUserCompanyId;


    public NewServiceFragment() {
    }

    public static NewServiceFragment newInstance() {
        return new NewServiceFragment();
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentNewServiceBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        servicesOperations = new ServicesOperations();
        eventTypesOperations = new EventTypesOperations();
        usersOperations = new UsersOperations();

        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");
        usersOperations.getEmployeeById(loggedUserId, new UsersOperations.OnGetEmployeeByIdListener() {
            @Override
            public void onSuccess(Employee employee) {
                loggedUserCompanyId = employee.getCompanyId();
            }

            @Override
            public void onFailure(Exception e) {
                usersOperations.getOwnerById(loggedUserId, new UsersOperations.OnGetOwnerByIdListener() {
                    @Override
                    public void onSuccess(Owner owner) {
                        loggedUserCompanyId = owner.getCompanyId();
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
            }
        });

        initializeViews(root);
        setupListeners();
        setupRecyclerView(root);
        setupEventRecyclerView(root);

        return root;
    }

    private void initializeViews(View root) {
        // Assuming you have predefined lists of categories, subcategories, and event types
        CategoriesOperations categoriesOperations = new CategoriesOperations();
        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categories = result;
                categories.add(0, new Category("-1L", "Select Category", "", ""));
                List<String> categoryNames = getFieldList(categories, "name");
                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categoryNames);
                categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categorySpinner.setAdapter(categoryAdapter);

            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                eventTypes = result;
                eventTypes.add(0, new EventType("-1L", "Select Event Type", "", new ArrayList<>(), false));
                List<String> eventTypeNames = getFieldList(eventTypes, "typeName");
                ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeNames);
                eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventTypeSpinner.setAdapter(eventTypeAdapter);

            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        usersOperations.getAllEmployeesByCompanyId(loggedUserCompanyId, new UsersOperations.OnGetAllEmployeessListener() {
            @Override
            public void onSuccess(List<Employee> result) {
                employees = result;
                Employee placeholderEmployee = new Employee(
                        "",                          // Email
                        "",                          // Password
                        "",
                        Role.EMPLOYEE,
                        "Select Employee",           // Name (or any placeholder value)
                        "",                          // Surname
                        "",                          // Image name
                        new Address(),                          // Address (assuming it's a String)
                        "",                           // Phone number (assuming it's an int or long)
                        false,                        // isAccountActive (assuming it's a boolean)
                        "1L"
                );

                employees.add(0, placeholderEmployee);
                List<String> employeeNames = getFieldList(employees, "name");
                ArrayAdapter<String> employeeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, employeeNames);
                employeeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                employeeSpinner.setAdapter(employeeAdapter);

            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        acceptanceCriteria = getAcceptanceCriteria();

        categorySpinner = root.findViewById(R.id.spinner_category);
        subcategorySpinner = root.findViewById(R.id.spinner_subcategory);
        eventTypeSpinner = root.findViewById(R.id.spinner_event_type);
        employeeSpinner = root.findViewById(R.id.spinner_employees);
        acceptanceCriteriaSpinner = root.findViewById(R.id.spinner_acceptance_type);

        selectedEmployeesRecyclerView = root.findViewById(R.id.recycler_selected_employees);
        selectedEmployeesRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        selectedEventsRecyclerView = root.findViewById(R.id.recycler_selected_events);
        selectedEventsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        // Extract only the names from the objects
        List<String> acceptanceCriteriaNames = new ArrayList<>();
        for (AcceptanceType type : acceptanceCriteria) {
            acceptanceCriteriaNames.add(type.toString());
        }

        // Create adapters for spinners
        ArrayAdapter<String> acceptanceCriteriaAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, acceptanceCriteriaNames);

        // Set layout resource for adapters
        acceptanceCriteriaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set adapters for spinners
        acceptanceCriteriaSpinner.setAdapter(acceptanceCriteriaAdapter);

        nameEditText = binding.editTextName;
        requestSubcategoryEditText = binding.requestSubcategory;
        durationEditText = binding.editTextDuration;
        descriptionEditText = binding.editTextDescription;
        priceEditText = binding.editTextPrice;
        discountEditText = binding.editTextDiscount;
        specificitiesEditText = binding.editTextSpecificities;
        reservationDeadlineEditText = binding.editTextReservationDeadline;
        cancellationDeadlineEditText = binding.editTextCancellationDeadline;
        availableCheckBox = binding.checkboxAvailable;
        visibleCheckBox = binding.checkboxVisible;

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid category is selected (excluding the placeholder)
                if (position > 0) {
                    String selectedCategoryName = categorySpinner.getSelectedItem().toString();
                    Category selectedCategory = findCategoryByName(selectedCategoryName);
                    getSubcategories(selectedCategory);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no category is selected
            }
        });
        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    Employee selectedEmployee = employees.get(position);

                    // Add the selected employee to the list
                    selectedEmployees.add(selectedEmployee);

                    // Update the RecyclerView to display the list of selected employees
                    updateSelectedEmployeesRecyclerView();

                    // Remove the selected employee from the Spinner's list of available employees
                    employees.remove(selectedEmployee);
                    updateEmployeeSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no employee is selected
            }
        });

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    EventType eventType = eventTypes.get(position);

                    // Add the selected employee to the list
                    selectedEventTypes.add(eventType);

                    // Update the RecyclerView to display the list of selected employees
                    updateSelectedEventsRecyclerView();

                    // Remove the selected employee from the Spinner's list of available employees
                    eventTypes.remove(eventType);
                    updateEventsSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no employee is selected
            }
        });

    }

    private void setupRecyclerView(View root) {
        selectedEmployeesRecyclerView = root.findViewById(R.id.recycler_selected_employees);
        selectedEmployeesRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEmployeesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Employee employee = selectedEmployees.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(employee.getName() + " " + employee.getSurname());
            }

            @Override
            public int getItemCount() {
                return selectedEmployees.size();
            }
        };

// Set the adapter to the RecyclerView
        selectedEmployeesRecyclerView.setAdapter(selectedEmployeesAdapter);



    }

    private void updateSelectedEmployeesRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEmployeesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Employee employee = selectedEmployees.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(employee.getName() + " " + employee.getSurname());
            }

            @Override
            public int getItemCount() {
                return selectedEmployees.size();
            }
        };

// Set the adapter to the RecyclerView
        selectedEmployeesRecyclerView.setAdapter(selectedEmployeesAdapter);

    }

    private void setupEventRecyclerView(View root) {
        selectedEventsRecyclerView = root.findViewById(R.id.recycler_selected_events);
        selectedEventsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEmployeesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                EventType eventType = selectedEventTypes.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventType.getTypeName());
            }

            @Override
            public int getItemCount() {
                return selectedEventTypes.size();
            }
        };

// Set the adapter to the RecyclerView
        selectedEventsRecyclerView.setAdapter(selectedEmployeesAdapter);

    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateEmployeeSpinnerAdapter() {
        // Create a new list of strings to store employee names and surnames
        List<String> employeeNames = getEmployeeNames(employees);

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> employeeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, employeeNames);

        // Set the layout resource for the adapter
        employeeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        employeeSpinner.setAdapter(employeeAdapter);
    }

    private void updateSelectedEventsRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEventAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                EventType eventType = selectedEventTypes.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventType.getTypeName());
            }

            @Override
            public int getItemCount() {
                return selectedEventTypes.size();
            }
        };

        // Set the adapter to the RecyclerView
        selectedEventsRecyclerView.setAdapter(selectedEventAdapter);

    }

    private void updateSubcategorySpinner(List<Subcategory> subcategories) {
        subcategories.add(0, new Subcategory("-1L", "Select Category", "", SubcategoryType.PRODUCT,""));

        // Extract names from the list of subcategories
        List<String> subcategoryNames = getFieldList(subcategories, "name");

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryNames);

        // Set the layout resource for the adapter
        subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        subcategorySpinner.setAdapter(subcategoryAdapter);
    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateEventsSpinnerAdapter() {

        List<String> eventTypeNames = new ArrayList<>();
        for (EventType event : eventTypes) {
            // Concatenate first name and last name
            String fullName = event.getTypeName();
            eventTypeNames.add(fullName);
        }
        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeNames);

        // Set the layout resource for the adapter
        eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        eventTypeSpinner.setAdapter(eventTypeAdapter);
    }



    private List<String> getEmployeeNames(List<Employee> employees) {
        List<String> employeeNames = new ArrayList<>();
        for (Employee employee : employees) {
            // Concatenate first name and last name
            String fullName = employee.getName() + " " + employee.getSurname();
            employeeNames.add(fullName);
        }
        return employeeNames;
    }



    private List<String> getFieldList(List<?> list, String... fieldNames) {
        List<String> fieldList = new ArrayList<>();
        try {
            for (Object obj : list) {
                StringBuilder valueBuilder = new StringBuilder();
                for (String fieldName : fieldNames) {
                    Field field = obj.getClass().getDeclaredField(fieldName);
                    field.setAccessible(true);
                    String value = (String) field.get(obj);
                    valueBuilder.append(value).append(" ");
                }
                fieldList.add(valueBuilder.toString().trim());
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldList;
    }


    private List<Category> getCategories() {
        List<Category> categories = new ArrayList<>();

        // Sample category data
        Category category1 = new Category("1L", "Electronics", "Products related to electronics", "");
        Category category2 = new Category("2L", "Clothing", "Apparel and clothing items", "");

        // Adding categories to the list
        categories.add(category1);
        categories.add(category2);

        return categories;
    }

    private void getSubcategories(Category selectedCategory) {
        SubcategoriesOperations subcategoriesOperations = new SubcategoriesOperations();
        subcategoriesOperations.getAllByCategoryId(selectedCategory.getId(), new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = result;
                updateSubcategorySpinner(result);
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    // Method to populate the list of acceptance criteria
    private List<AcceptanceType> getAcceptanceCriteria() {
        List<AcceptanceType> acceptanceCriteria = new ArrayList<>();

        // Add acceptance criteria options
        acceptanceCriteria.add(AcceptanceType.AUTOMATIC);
        acceptanceCriteria.add(AcceptanceType.MANUAL);

        return acceptanceCriteria;
    }

    private void setupListeners() {
        setupImagePicker();
        setupRegisterButton();
    }

    private void setupImagePicker() {
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                this::handleImagePickerResult);

        binding.btnUploadPicture.setOnClickListener(v -> mGetContent.launch("image/*"));
    }

    private void handleImagePickerResult(Uri result) {
        if (result != null) {
            binding.servicePictureView.setImageURI(result);
        }
    }

    private void setupRegisterButton() {
        binding.btnSaveService.setOnClickListener(v -> {
            if (validateFields()) {
                addNewService();
            }
        });
    }

    private boolean validateFields() {
        EditText[] fields = {nameEditText, durationEditText, descriptionEditText, priceEditText, discountEditText, specificitiesEditText, reservationDeadlineEditText, cancellationDeadlineEditText};
        Spinner[] spinners = {categorySpinner, subcategorySpinner, eventTypeSpinner, acceptanceCriteriaSpinner};

        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText().toString().trim())) {
                showToast("Please fill in all fields");
                return false;
            }
        }

        // Check if subcategory spinner is not selected and requestSubcategoryEditText is empty
        if (subcategorySpinner.getSelectedItemPosition() == 0 && TextUtils.isEmpty(requestSubcategoryEditText.getText().toString().trim())) {
            showToast("Please select a subcategory or enter a value in the 'Request Subcategory' field");
            return false;
        } else if (subcategorySpinner.getSelectedItemPosition() == 0 && !TextUtils.isEmpty(requestSubcategoryEditText.getText().toString().trim())) {
            // Subcategory spinner is not selected but requestSubcategoryEditText is filled
            makeServiceRequest(requestSubcategoryEditText.getText().toString());
            return false; // Return false as the request is made
        }

        for (Spinner spinner : spinners) {
            if (spinner.getSelectedItem() == null) {
                showToast("Please select a value from all spinners");
                return false;
            }
        }

        return true;
    }

    private void makeServiceRequest(String requestedSubcategory) {
        ServiceRequest serviceRequest = new ServiceRequest();
        Service service = createServiceFromInput();
        serviceRequest.setService(service);
        Subcategory request = new Subcategory();
        request.setName(requestedSubcategory);
        request.setType(SubcategoryType.SERVICE);
        request.setDescription("opis");
        request.setCategoryId(service.getCategory().getId());
        serviceRequest.setRequestedSubcategory(request);
        serviceRequest.setOwnerId(loggedUserId);
        serviceRequest.setStatus(RequestStatus.ON_PENDING);

        ServiceRequestsOperations serviceRequestsOperations = new ServiceRequestsOperations();

        serviceRequestsOperations.save(serviceRequest, new ServiceRequestsOperations.SaveServiceRequestListener() {
            @Override
            public void onSuccess(String serviceRequestId) {
                Log.e("ServiceSave", "Service request sent successful: " + serviceRequestId);

            }

            @Override
            public void onFailure(Exception e) {
                Log.e("ServiceSave", "Error sending service request: " + e.getMessage());

            }
        });

        Navigation.findNavController(requireView()).navigateUp();

    }



    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void addNewService() {
        Service newService = createServiceFromInput();

        servicesOperations.save(newService, new ServicesOperations.ServiceSaveListener() {
            @Override
            public void onSuccess(String serviceId) {
                Log.e("ServiceSave", "Service saved successful: " + serviceId);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("ServiceSave", "Error saving service to database: " + e.getMessage());
            }
        });

        Navigation.findNavController(requireView()).navigateUp();
    }

    private Service createServiceFromInput() {
        Service newService = new Service();
        newService.setId(String.valueOf(System.currentTimeMillis()));
        newService.setCompanyId("1L");
        newService.setName(getTextFromEditText(nameEditText));
        newService.setDescription(getTextFromEditText(descriptionEditText));
        newService.setSpecificities(getTextFromEditText(specificitiesEditText));
        String selectedCategoryName = categorySpinner.getSelectedItem().toString();
        Category selectedCategory = findCategoryByName(selectedCategoryName);
        newService.setCategory(selectedCategory);

        String selectedSubcategoryName = subcategorySpinner.getSelectedItem().toString();
        Subcategory selectedSubcategory = findSubcategoryByName(selectedSubcategoryName);
        newService.setSubcategory(selectedSubcategory);

        newService.setEvents(selectedEventTypes);

        String priceText = getTextFromEditText(priceEditText);
        double price = Double.parseDouble(priceText);
        newService.setPrice(price);

        String discountText = getTextFromEditText(discountEditText);
        int discount = Integer.parseInt(discountText);
        newService.setDiscount(discount);

        // Retrieve additional fields
        String reservationDeadlineText = getTextFromEditText(reservationDeadlineEditText);
        int reservationDeadline = Integer.parseInt(reservationDeadlineText);
        newService.setReservationDeadline(reservationDeadline);

        String durationText = getTextFromEditText(durationEditText);
        int duration = Integer.parseInt(durationText);
        newService.setDuration(duration);

        String cancellationDeadlineText = getTextFromEditText(cancellationDeadlineEditText);
        int cancellationDeadline = Integer.parseInt(cancellationDeadlineText);
        newService.setCancellationDeadline(cancellationDeadline);

        selectedEmployees.add(new Employee(
                "",                          // Email
                "",                          // Password
                "",
                Role.EMPLOYEE,
                "Select Employee",           // Name (or any placeholder value)
                "",                          // Surname
                "",                          // Image name
                new Address(),                          // Address (assuming it's a String)
                "",                           // Phone number (assuming it's an int or long)
                false,                        // isAccountActive (assuming it's a boolean)
                "1L"
        ));
        newService.setEmployees(selectedEmployees);

        // Handle acceptance criteria selection if applicable
        AcceptanceType selectedAcceptanceType = (acceptanceCriteriaSpinner.getSelectedItem() instanceof AcceptanceType) ? (AcceptanceType) acceptanceCriteriaSpinner.getSelectedItem() : null;
        newService.setAcceptanceType(selectedAcceptanceType);

        newService.setVisible(visibleCheckBox.isChecked());
        newService.setAvailable(availableCheckBox.isChecked());
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.ic_service);
        newService.setImages(images);
        newService.setDeleted(false);

        return newService;
    }



    private Category findCategoryByName(String categoryName) {
        for (Category category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }
        return null; // Return null if not found
    }

    private Subcategory findSubcategoryByName(String subcategoryName) {
        for (Subcategory subcategory : subcategories) {
            if (subcategory.getName().equals(subcategoryName)) {
                return subcategory;
            }
        }
        return null; // Return null if not found
    }

    private EventType findEventTypeByName(String eventTypeName) {
        for (EventType eventType : eventTypes) {
            if (eventType.getTypeName().equals(eventTypeName)) {
                return eventType;
            }
        }
        return null; // Return null if not found
    }


    private String getTextFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}