package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.ProductRequestAdapter;
import com.ma.eventplanner.adapters.ServiceRequestAdapter;
import com.ma.eventplanner.database.ProductRequestsOperations;
import com.ma.eventplanner.database.ServiceRequestsOperations;
import com.ma.eventplanner.model.ProductRequest;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.ServiceRequest;
import com.ma.eventplanner.model.Subcategory;

import java.util.ArrayList;
import java.util.List;

public class ServiceRequestsFragment extends Fragment {

    private List<ServiceRequest> serviceRequests;
    private ServiceRequestsOperations serviceRequestsOperations;
    public ServiceRequestsFragment() {
    }

    public static ServiceRequestsFragment newInstance() {
        ServiceRequestsFragment fragment = new ServiceRequestsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        serviceRequestsOperations = new ServiceRequestsOperations();
        serviceRequests = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_requests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView serviceRequestsListView = view.findViewById(R.id.service_requests_list);
        serviceRequests = new ArrayList<>();

        loadServiceRequests(new ServiceRequestsOperations.GetAllServiceRequestsListener<ServiceRequest>() {
            @Override
            public void onSuccess(List<ServiceRequest> result) {
                serviceRequests.clear();
                serviceRequests.addAll(result);

                ServiceRequestAdapter adapter = new ServiceRequestAdapter(requireContext(), serviceRequests, getActivity());
                serviceRequestsListView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void loadServiceRequests(ServiceRequestsOperations.GetAllServiceRequestsListener<ServiceRequest> listener) {
        serviceRequestsOperations.getAllByStatus(RequestStatus.ON_PENDING, listener);
    }
}