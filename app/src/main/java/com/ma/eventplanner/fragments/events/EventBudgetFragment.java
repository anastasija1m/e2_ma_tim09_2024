package com.ma.eventplanner.fragments.events;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.databinding.FragmentEventBudgetBinding;
import com.ma.eventplanner.model.BudgetSubcategory;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Item;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;

public class EventBudgetFragment extends Fragment {

    private static ArrayList<BudgetSubcategory> budgetSubcategories;
    private FragmentEventBudgetBinding binding;
    private Event event;
    private EventsOperations eventsOperations;
    private EventTypesOperations eventTypesOperations;
    private CategoriesOperations categoriesOperations;
    private SubcategoriesOperations subcategoriesOperations;
    private Map<String, String> catId;
    private Map<String, String> subcatId;
    public EventBudgetFragment(Event event) {
        this.event = event;
    }
    public static EventBudgetFragment newInstance(Event event) {
        EventBudgetFragment fragment = new EventBudgetFragment(event);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = FragmentEventBudgetBinding.inflate(inflater, container, false);
        Button addButton = binding.addButton;

        catId = new HashMap<>();
        subcatId = new HashMap<>();
        categoriesOperations = new CategoriesOperations();
        subcategoriesOperations = new SubcategoriesOperations();
        eventTypesOperations = new EventTypesOperations();
        eventsOperations = new EventsOperations();
        budgetSubcategories = new ArrayList<>();
        getSubcategories();

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = binding.getRoot().findViewById(R.id.numberEditText);
                String inputText = editText.getText().toString().trim();  // Trim whitespace characters

                if (!inputText.isEmpty()) {
                    int price = Integer.parseInt(inputText);

                    Spinner spinner = binding.getRoot().findViewById(R.id.secondSpinner);
                    String selectedText = spinner.getSelectedItem().toString();
                    String id = subcatId.get(selectedText);

                    final Subcategory[] s1 = new Subcategory[1];
                    subcategoriesOperations.getById(id, new SubcategoriesOperations.GetSubcategoryByIdListener<Subcategory>() {
                        @Override
                        public void onSuccess(Subcategory result) {
                            s1[0] = result;
                            categoriesOperations.getById(result.getCategoryId(), new CategoriesOperations.GetCategoryByIdListener<Category>() {
                                @Override
                                public void onSuccess(Category result) {
                                    List<Item> itemList = new ArrayList<>();
                                    budgetSubcategories.add(new BudgetSubcategory(result, s1[0], price, itemList));
                                    event.setBudgetSubcategories(budgetSubcategories);

                                    TableFragment tableFragment = new TableFragment();
                                    tableFragment.setBudgetSubcategories(budgetSubcategories);
                                    tableFragment.setParent(EventBudgetFragment.this);
                                    getChildFragmentManager().beginTransaction()
                                            .replace(R.id.tableContainer, tableFragment)
                                            .commit();

                                    Spinner spinner = binding.getRoot().findViewById(R.id.secondSpinner);
                                    LinearLayout linearLayout = binding.getRoot().findViewById(R.id.llayout);
                                    if(spinner.getAdapter().getCount() == 1)
                                        linearLayout.setVisibility(View.INVISIBLE);

                                    editText.setText("");
                                    updateEvent();
                                    getSubcategories();
                                }
                                @Override
                                public void onFailure(Exception e) {
                                }
                            });
                        }
                        @Override
                        public void onFailure(Exception e) {
                        }
                    });
                }
                else {
                    Toast.makeText(requireContext(), "Fill price field", Toast.LENGTH_SHORT).show();
                }
            }
        });

        prepareBudgetList(budgetSubcategories);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        budgetSubcategories = (ArrayList<BudgetSubcategory>) event.getBudgetSubcategories();
        addTableFragment(view);
    }

    public void addTableFragment(View view) {
        TableFragment tableFragment = new TableFragment();
        tableFragment.setBudgetSubcategories(budgetSubcategories);
        tableFragment.setParent(this);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.tableContainer, tableFragment)
                .commit();
    }

    public void remove(BudgetSubcategory budgetSubcategory) {
        budgetSubcategories.remove(budgetSubcategory);
        event.setBudgetSubcategories(budgetSubcategories);
        updateEvent();
        getSubcategories();
    }

    public void changePrice(int price, int index) {
        budgetSubcategories.get(index).setPrice(price);
        event.setBudgetSubcategories(budgetSubcategories);
        updateEvent();
    }

    private void prepareBudgetList(ArrayList<BudgetSubcategory> budgetSubcategories) {
        if(!budgetSubcategories.isEmpty()) return;
        eventsOperations.getById(event.getId(), new EventsOperations.GetEventByIdListener<Event>() {
            @Override
            public void onSuccess(Event result) {
                event = result;
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void updateEvent() {
        eventsOperations.update(event, new EventsOperations.UpdateEventListener() {
            @Override
            public void onSuccess() {
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void getSubcategories() {
        subcatId.clear();
        if(event.getTypeId() != null) {
            eventTypesOperations.getById(event.getTypeId(), new EventTypesOperations.GetEventTypeByIdListener<EventType>() {
                @Override
                public void onSuccess(EventType result) {
                    List<String> subcatNames = new ArrayList<>();
                    List<String> subcatNames2 = new ArrayList<>();
                    final List<String> kategNames = new ArrayList<>();

                    CompletableFuture<Void> allCategoryFutures = new CompletableFuture<>();
                    AtomicInteger counter = new AtomicInteger(0);

                    for (String s : result.getSuggestedSubcategoryIds()) {
                        subcategoriesOperations.getById(s, new SubcategoriesOperations.GetSubcategoryByIdListener<Subcategory>() {
                            @Override
                            public void onSuccess(Subcategory result3) {
                                subcatNames2.add(result3.getName());
                                categoriesOperations.getById(result3.getCategoryId(), new CategoriesOperations.GetCategoryByIdListener<Category>() {
                                    @Override
                                    public void onSuccess(Category result4) {
                                        kategNames.add(result4.getName());

                                        counter.incrementAndGet();
                                        if (counter.get() == result.getSuggestedSubcategoryIds().size()) {
                                            allCategoryFutures.complete(null);
                                        }
                                    }
                                    @Override
                                    public void onFailure(Exception e) {
                                    }
                                });
                            }
                            @Override
                            public void onFailure(Exception e) {
                            }
                        });
                    }
                    allCategoryFutures.thenRun(() -> {

                        int i = 0;
                        if (!kategNames.isEmpty()) {
                            for (String s : result.getSuggestedSubcategoryIds()) {
                                boolean exist = false;
                                for(BudgetSubcategory b: event.getBudgetSubcategories()) {
                                    if (Objects.equals(b.getSubcategory().getName(), subcatNames2.get(i))) {
                                        exist = true;
                                        break;
                                    }
                                }
                                if(!exist) {
                                    subcatNames.add(kategNames.get(i) + ", " + subcatNames2.get(i));
                                    subcatId.put(kategNames.get(i) + ", " + subcatNames2.get(i), s);
                                }
                                i++;
                            }
                        }
                        if(subcatNames.isEmpty()) {
                            LinearLayout linearLayout = binding.getRoot().findViewById(R.id.llayout);
                            linearLayout.setVisibility(View.INVISIBLE);
                        }
                        else {
                            LinearLayout linearLayout = binding.getRoot().findViewById(R.id.llayout);
                            linearLayout.setVisibility(View.VISIBLE);
                        }

                        Spinner spinner = binding.getRoot().findViewById(R.id.secondSpinner);
                        String[] typesArrayAsArray = subcatNames.toArray(new String[0]);

                        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, typesArrayAsArray);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner.setAdapter(adapter);
                    });}
                @Override
                public void onFailure(Exception e) {
                }
            });
        }
        else {
            subcategoriesOperations.getAll(new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
                @Override
                public void onSuccess(List<Subcategory> result) {
                    List<String> subcatNames = new ArrayList<>();
                    final List<String> kategNames = new ArrayList<>();

                    CompletableFuture<Void> allCategoryFutures = new CompletableFuture<>();
                    AtomicInteger counter = new AtomicInteger(0);

                    for (Subcategory s : result) {
                        categoriesOperations.getById(s.getCategoryId(), new CategoriesOperations.GetCategoryByIdListener<Category>() {
                            @Override
                            public void onSuccess(Category result3) {
                                kategNames.add(result3.getName());

                                counter.incrementAndGet();
                                if (counter.get() == result.size()) {
                                    allCategoryFutures.complete(null);
                                }
                            }

                            @Override
                            public void onFailure(Exception e) {
                            }
                        });
                    }
                    allCategoryFutures.thenRun(() -> {

                    int i = 0;
                    if (!kategNames.isEmpty()) {
                        for (Subcategory s : result) {
                            boolean exist = false;
                            for(BudgetSubcategory b: event.getBudgetSubcategories()) {
                                if (Objects.equals(b.getSubcategory().getName(), s.getName())) {
                                    exist = true;
                                    break;
                                }
                            }
                            if(!exist) {
                                subcatNames.add(kategNames.get(i) + ", " + s.getName());
                                subcatId.put(kategNames.get(i) + ", " + s.getName(), s.getId());
                            }
                            i++;
                        }
                    }
                    if(subcatNames.isEmpty()) {
                        LinearLayout linearLayout = binding.getRoot().findViewById(R.id.llayout);
                        linearLayout.setVisibility(View.INVISIBLE);
                    }
                    else {
                        LinearLayout linearLayout = binding.getRoot().findViewById(R.id.llayout);
                        linearLayout.setVisibility(View.VISIBLE);
                    }

                    Spinner spinner = binding.getRoot().findViewById(R.id.secondSpinner);
                    String[] typesArrayAsArray = subcatNames.toArray(new String[0]);
                    Toast.makeText(requireContext(), "PROSAo", Toast.LENGTH_SHORT).show();

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, typesArrayAsArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                });}
                @Override
                public void onFailure(Exception e) {
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}