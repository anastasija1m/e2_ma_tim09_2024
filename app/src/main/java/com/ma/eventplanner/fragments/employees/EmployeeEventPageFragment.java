package com.ma.eventplanner.fragments.employees;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.databinding.FragmentEmployeeEventPageBinding;
import com.ma.eventplanner.model.EmployeeEvent;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class EmployeeEventPageFragment extends Fragment {

    private static final String ARG_EMPLOYEE_EVENTS = "employee_events";

    private FragmentEmployeeEventPageBinding binding;
    private WeekViewModel weekViewModel;
    private EmployeeEventsListFragment employeeEventsListFragment;
    private ArrayList<EmployeeEvent> originalEmployeeEvents;
    private ArrayList<EmployeeEvent> filteredEmployeeEvents;

    public EmployeeEventPageFragment() { }

    public static EmployeeEventPageFragment newInstance(ArrayList<EmployeeEvent> employeeEvents) {
        EmployeeEventPageFragment fragment = new EmployeeEventPageFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_EMPLOYEE_EVENTS, employeeEvents);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            originalEmployeeEvents = getArguments().getParcelableArrayList(ARG_EMPLOYEE_EVENTS);
            filteredEmployeeEvents = new ArrayList<>(originalEmployeeEvents);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeEventPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        weekViewModel = new ViewModelProvider(requireActivity()).get(WeekViewModel.class);

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        weekViewModel.getSelectedWeek().observe(getViewLifecycleOwner(), this::filterEventsForSelectedWeek);

        employeeEventsListFragment = EmployeeEventsListFragment.newInstance(filteredEmployeeEvents);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.scroll_events_list, employeeEventsListFragment);
        transaction.commit();
    }

    private void filterEventsForSelectedWeek(Calendar selectedWeek) {
        ArrayList<EmployeeEvent> tempFilteredEvents = new ArrayList<>();

        for (EmployeeEvent event : originalEmployeeEvents) {
            if (isDateInSelectedWeek(event.getDate(), selectedWeek) && !tempFilteredEvents.contains(event)) {
                tempFilteredEvents.add(event);
            }
        }

        filteredEmployeeEvents.clear();
        filteredEmployeeEvents.addAll(tempFilteredEvents);

        if (employeeEventsListFragment != null) {
            employeeEventsListFragment.updateEventList(filteredEmployeeEvents);
        }
    }

    private boolean isDateInSelectedWeek(Date eventDate, Calendar selectedWeek) {
        LocalDate eventLocalDate = eventDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        LocalDate startOfWeek = LocalDate.of(selectedWeek.get(Calendar.YEAR),
                selectedWeek.get(Calendar.MONTH) + 1,
                selectedWeek.get(Calendar.DAY_OF_MONTH)).minusDays(selectedWeek.get(Calendar.DAY_OF_WEEK) - 1);
        LocalDate endOfWeek = startOfWeek.plusDays(6);

        return !eventLocalDate.isBefore(startOfWeek) && !eventLocalDate.isAfter(endOfWeek);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}