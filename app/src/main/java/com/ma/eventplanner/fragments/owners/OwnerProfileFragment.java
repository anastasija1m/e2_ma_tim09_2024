package com.ma.eventplanner.fragments.owners;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.CompanyRatingOperations;
import com.ma.eventplanner.databinding.FragmentOwnerProfileBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.fragments.company.CompanyRatingsListFragment;
import com.ma.eventplanner.model.CompanyRating;
import com.ma.eventplanner.model.Owner;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class OwnerProfileFragment extends Fragment {

    private Owner owner;
    private FragmentOwnerProfileBinding binding;
    private final ArrayList<CompanyRating> mCompanyRatings = new ArrayList<>();
    private String selectedRating;
    private Calendar selectedDate;

    public OwnerProfileFragment() {
    }

    public static OwnerProfileFragment newInstance() {
        return new OwnerProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        owner = new Owner();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        owner = homeActivity.getLoggedInOwner();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentOwnerProfileBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (owner != null) {
            bindOwnerDetails();
            retrieveCompanyRatings(owner.getCompanyId());

            binding.spinnerRating.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectedRating = parent.getItemAtPosition(position).toString();
                    filterCompanyRatings();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            binding.filterByDate.init(Calendar.getInstance().get(Calendar.YEAR),
                    Calendar.getInstance().get(Calendar.MONTH),
                    Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                    (view1, year, monthOfYear, dayOfMonth) -> {
                        selectedDate = Calendar.getInstance();
                        selectedDate.set(year, monthOfYear, dayOfMonth);
                        filterCompanyRatings();
                    });
        }
    }

    private void bindOwnerDetails() {
        binding.ownerName.setText(owner.getFirstName());
        binding.ownerSurname.setText(owner.getLastName());
        binding.ownerEmail.setText(owner.getEmail());
    }

    private void retrieveCompanyRatings(String companyId) {
        CompanyRatingOperations companyRatingOperations = new CompanyRatingOperations();
        companyRatingOperations.getAllByCompanyId(companyId, new CompanyRatingOperations.GetAllCompanyRatingsListener<CompanyRating>() {
            @Override
            public void onSuccess(List<CompanyRating> companyRatings) {
                mCompanyRatings.clear();
                mCompanyRatings.addAll(companyRatings);
                filterCompanyRatings();  // Filter initially to apply any default selections
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching company reservations: ", e);
            }
        });
    }

    private void filterCompanyRatings() {
        List<CompanyRating> filteredRatings = new ArrayList<>();
        for (CompanyRating rating : mCompanyRatings) {
            boolean matchesRating = (selectedRating == null || String.valueOf(rating.getRating()).equals(selectedRating));
            boolean matchesDate = (selectedDate == null || isSameDay(rating.getRatingDate(), selectedDate));
            if (matchesRating && matchesDate) {
                filteredRatings.add(rating);
            }
        }
        FragmentTransition.to(CompanyRatingsListFragment.newInstance((ArrayList<CompanyRating>) filteredRatings), requireActivity(), false, R.id.scroll_owner_company_ratings);
    }

    private boolean isSameDay(java.util.Date date, Calendar calendar) {
        Calendar ratingDate = Calendar.getInstance();
        ratingDate.setTime(date);
        return ratingDate.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) &&
                ratingDate.get(Calendar.MONTH) == calendar.get(Calendar.MONTH) &&
                ratingDate.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
