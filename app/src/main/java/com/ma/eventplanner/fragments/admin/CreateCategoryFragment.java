package com.ma.eventplanner.fragments.admin;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.CategoryListAdapter;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.model.Category;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CreateCategoryFragment extends Fragment {

    private TextInputEditText categoryNameEditText;
    private TextInputEditText descriptionEditText;
    private Button addButton;
    private ListView listView;
    private CategoryListAdapter adapter;
    private List<Category> categoryList;
    private CategoriesOperations categoriesOperations;
    private String loggedUserId;

    public CreateCategoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_category, container, false);
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");

        categoriesOperations = new CategoriesOperations();
        categoryList = new ArrayList<>();
        initializeViews(rootView);
        setupListView();
        setupAddButton();


        return rootView;
    }

    private void initializeViews(View rootView) {
        categoryNameEditText = rootView.findViewById(R.id.category_name);
        descriptionEditText = rootView.findViewById(R.id.description);
        addButton = rootView.findViewById(R.id.add_button);
        listView = rootView.findViewById(android.R.id.list);
    }

    private void setupListView() {
        adapter = new CategoryListAdapter(requireContext(), R.layout.list_item_category, categoryList, requireActivity(), categoriesOperations);
        listView.setAdapter(adapter);

        categoriesOperations.getAllByOwnerId(loggedUserId, new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categoryList.addAll(result);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
            }
        });

    }

    private void setupAddButton() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewCategory();
            }
        });
    }



    private void addNewCategory() {
        String categoryName = Objects.requireNonNull(categoryNameEditText.getText()).toString();
        String description = Objects.requireNonNull(descriptionEditText.getText()).toString();
        Category category = new Category(null, categoryName, description, loggedUserId);

        categoriesOperations.save(category, new CategoriesOperations.SaveCategoryListener() {
            @Override
            public void onSuccess(String categoryId) {
                Log.e("CategorySave", "Category saved successful: " + categoryId);
                categoryList.add(category);
                adapter.notifyDataSetChanged();

                categoryNameEditText.setText("");
                descriptionEditText.setText("");
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("CategorySave", "Error saving category to database: " + e.getMessage());
            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface Callback<T> {
        void onSuccess(T result);
        void onError(String errorMessage);
    }
}