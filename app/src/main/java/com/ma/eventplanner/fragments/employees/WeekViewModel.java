package com.ma.eventplanner.fragments.employees;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.Calendar;

public class WeekViewModel extends ViewModel {

    private MutableLiveData<Calendar> selectedWeek = new MutableLiveData<>();

    public void setSelectedWeek(Calendar week) {
        selectedWeek.setValue(week);
    }

    public MutableLiveData<Calendar> getSelectedWeek() {
        return selectedWeek;
    }
}