package com.ma.eventplanner.fragments;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

public class FragmentTransition {

    public static void to(Fragment newFragment, FragmentActivity fragmentActivity, boolean addToBackstack, int layoutViewId) {
        FragmentTransaction transaction = fragmentActivity
                .getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(layoutViewId, newFragment);
        if(addToBackstack) transaction.addToBackStack(null);

        transaction.commit();
    }
}
