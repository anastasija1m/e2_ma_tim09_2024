package com.ma.eventplanner.fragments.owners;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.RatingReportOperations;
import com.ma.eventplanner.databinding.FragmentReportRatingBinding;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.RatingReport;
import com.ma.eventplanner.model.RatingReportStatus;
import com.ma.eventplanner.model.User;

import java.util.Date;

public class ReportRatingFragment extends Fragment {

    private FragmentReportRatingBinding binding;
    private User loggedInUser;
    private String ratingId;
    private EditText textComment;
    private Button buttonSubmit;
    private Button buttonCancel;

    public ReportRatingFragment() {

    }

    public static ReportRatingFragment newInstance() {
        return new ReportRatingFragment();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentReportRatingBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        textComment = root.findViewById(R.id.comment_text);
        buttonSubmit = root.findViewById(R.id.send_report_button);
        buttonCancel = root.findViewById(R.id.cancel_button);

        buttonSubmit.setOnClickListener(v -> {
            String comment = textComment.getText().toString();

            Date currentDate = new Date();

            RatingReport ratingReport = new RatingReport(loggedInUser.getEmail(), currentDate, comment, RatingReportStatus.REPORTED, ratingId);

            Toast.makeText(getContext(), "Rating report submitted: " + ratingReport + "\nComment: " + comment, Toast.LENGTH_SHORT).show();
            RatingReportOperations ratingReportOperations = new RatingReportOperations();

            ratingReportOperations.save(ratingReport, new RatingReportOperations.SaveRatingReportListener() {
                @Override
                public void onSuccess(String ratingReportId) {
                    Toast.makeText(getContext(), "Rating report submitted successfully", Toast.LENGTH_SHORT).show();
                    Notification notification = createNotification();
                    NotificationsOperations notificationsOperations = new NotificationsOperations();
                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Notification sent");
                            Navigation.findNavController(requireView()).navigateUp();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error sending notification", e);
                        }
                    });
                }

                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(getContext(), "Failed to submit report", Toast.LENGTH_SHORT).show();
                    Navigation.findNavController(requireView()).navigateUp();
                }
            });
        });


        buttonCancel.setOnClickListener(v ->
                Navigation.findNavController(requireView()).navigateUp());

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("loggedInUser") && bundle.containsKey("ratingId")) {
            loggedInUser = bundle.getParcelable("loggedInUser");
            ratingId = (String) bundle.getSerializable("ratingId");
        }
    }

    private Notification createNotification() {
        String title = "New report has been added.";
        String content = String.format("%s has left a report. Check it out!", loggedInUser.getEmail());
        return new Notification("", title, content, "92wwxTav9aYoHj0PIA5e8QWH2a82");
    }
}