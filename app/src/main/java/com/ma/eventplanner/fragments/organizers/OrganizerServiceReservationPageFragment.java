package com.ma.eventplanner.fragments.organizers;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.ServiceReservationsOperations;
import com.ma.eventplanner.databinding.FragmentOrganizerServiceReservationPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.fragments.owners.OwnerServiceReservationsList;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.ServiceReservation;

import java.util.ArrayList;
import java.util.List;

public class OrganizerServiceReservationPageFragment extends Fragment {

    private final ArrayList<ServiceReservation> serviceReservations = new ArrayList<>();
    private FragmentOrganizerServiceReservationPageBinding binding;
    private EventOrganizer loggedInEventOrganizer;

    public OrganizerServiceReservationPageFragment() {

    }

    public static OrganizerServiceReservationPageFragment newInstance() {
        return new OrganizerServiceReservationPageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentOrganizerServiceReservationPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        loggedInEventOrganizer = new EventOrganizer();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInEventOrganizer = homeActivity.getLoggedInEventOrganizer();

        retrieveOrganizerReservations();
        return root;
    }

    private void retrieveOrganizerReservations() {

        ServiceReservationsOperations reservationsOperations = new ServiceReservationsOperations();
        reservationsOperations.getAllByOrganizerEmail(loggedInEventOrganizer.getEmail(), new ServiceReservationsOperations.GetAllServiceReservationsListener<ServiceReservation>() {
            @Override
            public void onSuccess(List<ServiceReservation> result) {
                serviceReservations.clear();
                serviceReservations.addAll(result);
                FragmentTransition.to(OrganizerServiceReservationListFragment.newInstance(serviceReservations), requireActivity(), false, R.id.scroll_organizer_reservations_list);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error retrieving organizer reservations: ", e);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        retrieveOrganizerReservations();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}