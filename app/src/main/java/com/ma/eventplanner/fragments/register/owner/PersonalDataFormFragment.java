package com.ma.eventplanner.fragments.register.owner;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.model.Address;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Role;

import java.util.Objects;

public class PersonalDataFormFragment extends Fragment {

    private TextInputEditText emailInput, passwordInput, passwordConfirmInput, firstNameInput,
            lastNameInput, countryInput, cityInput, postalCodeInput,
            streetInput, numberInput, phoneNumberInput;

    private ImageView profilePictureView;
    private Intent image;
    private Button uploadImageButton;

    public PersonalDataFormFragment() {
        // Required empty public constructor
    }

    @NonNull
    public static PersonalDataFormFragment newInstance(String param1, String param2) {
        PersonalDataFormFragment fragment = new PersonalDataFormFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_owner_personal_data_form, container, false);
        initializeViews(view);
        setupNextButton(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUploadImageButton();
    }

    private void initializeViews(@NonNull View rootView) {
        emailInput = rootView.findViewById(R.id.email);
        passwordInput = rootView.findViewById(R.id.password);
        passwordConfirmInput = rootView.findViewById(R.id.confirm_password);
        firstNameInput = rootView.findViewById(R.id.first_name);
        lastNameInput = rootView.findViewById(R.id.last_name);

        countryInput = rootView.findViewById(R.id.country);
        cityInput = rootView.findViewById(R.id.city);
        postalCodeInput = rootView.findViewById(R.id.postal_code);
        streetInput = rootView.findViewById(R.id.street);
        numberInput = rootView.findViewById(R.id.house_number);
        phoneNumberInput = rootView.findViewById(R.id.phone_number);

        uploadImageButton = rootView.findViewById(R.id.upload_button_owner);

        profilePictureView = rootView.findViewById(R.id.profile_picture_view_owner);

    }

    private void setupUploadImageButton() {
        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUploadOptions();
            }
        });
    }

    @NonNull
    private Owner setUserData() {
        String email = Objects.requireNonNull(emailInput.getText()).toString();
        String password = Objects.requireNonNull(passwordInput.getText()).toString();
        String passwordConfirm = Objects.requireNonNull(passwordConfirmInput.getText()).toString();
        String firstName = Objects.requireNonNull(firstNameInput.getText()).toString();
        String lastName = Objects.requireNonNull(lastNameInput.getText()).toString();
        String phoneNumber = Objects.requireNonNull(phoneNumberInput.getText()).toString();

        Address address = setAddress();

        return new Owner(email, password, passwordConfirm, Role.OWNER,
                firstName, lastName, "",
                address, phoneNumber, "");
    }

    @NonNull
    private Address setAddress() {
        String country = Objects.requireNonNull(countryInput.getText()).toString();
        String city = Objects.requireNonNull(cityInput.getText()).toString();
        String postalCode = Objects.requireNonNull(postalCodeInput.getText()).toString();
        String street = Objects.requireNonNull(streetInput.getText()).toString();
        String number = Objects.requireNonNull(numberInput.getText()).toString();

        return new Address(country, city, postalCode, street, number);
    }

    private void setupNextButton(@NonNull View view) {
        Button nextButton = view.findViewById(R.id.next_button);
        nextButton.setOnClickListener(this::onNextButtonClick);
    }

    private void onNextButtonClick(View v) {
        CompanyDataFormFragment fragment = new CompanyDataFormFragment();
        Owner owner = setUserData();
        Bundle arguments = new Bundle();

        arguments.putParcelable("owner", owner);
        arguments.putParcelable("profileImage", image);

        fragment.setArguments(arguments);
        replaceFragment(fragment);
    }

    private void replaceFragment(Fragment fragment) {
        getParentFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private final ActivityResultLauncher<Intent> imageChooserLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    image = result.getData();
                    if (image != null) {
                        Uri selectedImageUri = image.getData();
                        if (selectedImageUri != null) {
                            profilePictureView.setImageURI(selectedImageUri);
                        } else {
                            Bitmap bitmap = (Bitmap) Objects.requireNonNull(image.getExtras()).get("data");
                            profilePictureView.setImageBitmap(bitmap);
                        }
                    }
                }
            });

    private void showUploadOptions() {
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent chooserIntent = Intent.createChooser(pickPhotoIntent, "Select Image");
        imageChooserLauncher.launch(chooserIntent);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
