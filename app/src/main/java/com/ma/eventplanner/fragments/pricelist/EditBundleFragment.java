package com.ma.eventplanner.fragments.pricelist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;

import java.util.ArrayList;
import java.util.List;

public class EditBundleFragment extends Fragment {

    private static com.ma.eventplanner.model.Bundle bundle;
    private com.ma.eventplanner.model.Bundle editedBundle, notEditedBundle;
    private BundlesOperations bundlesOperations;
    private TextView textViewName;
    private TextView textViewPrice;
    private EditText editTextDiscount;
    private Button btnSave;

    public EditBundleFragment() {
        // Required empty public constructor
    }

    public static EditBundleFragment newInstance(Bundle bundle) {
        EditBundleFragment fragment = new EditBundleFragment();
        Bundle args = new Bundle();
        args.putParcelable("bundle", bundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        bundlesOperations = new BundlesOperations();
        return inflater.inflate(R.layout.fragment_edit_bundle, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize views
        textViewName = view.findViewById(R.id.textViewName);
        textViewPrice = view.findViewById(R.id.textViewPrice);
        editTextDiscount = view.findViewById(R.id.editTextDiscount);
        btnSave = view.findViewById(R.id.btn_save);

        // Set onClickListener for the Save button
        btnSave.setOnClickListener(v -> saveChanges());

        // Check if bundle data is passed as argument
        if (getArguments() != null && getArguments().containsKey("bundle")) {
            bundle = getArguments().getParcelable("bundle");
            notEditedBundle = getArguments().getParcelable("bundle");
            editedBundle = getArguments().getParcelable("bundle");
            // Set initial values
            textViewName.setText(bundle.getName());
            textViewPrice.setText(String.valueOf(bundle.getPrice()));
            editTextDiscount.setText(String.valueOf(bundle.getDiscount()));
        }
    }

    private void saveChanges() {
        notEditedBundle.setChanged(true);
        bundlesOperations.update(notEditedBundle, new BundlesOperations.UpdateBundleListener() {
            @Override
            public void onSuccess() {
                saveChangedBundle();
                editedBundle.setChanged(false);
                bundlesOperations.save(editedBundle, new BundlesOperations.BundleSaveListener() {
                    @Override
                    public void onSuccess(String bundleId) {
                        Toast.makeText(getContext(), "Bundle: " + editedBundle.getName() + " is edited.", Toast.LENGTH_SHORT).show();
                        Navigation.findNavController(requireView()).navigateUp();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Toast.makeText(getContext(), "Bundle: " + editedBundle.getName() + " is not edited.", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Bundle: " + editedBundle.getName() + " is not edited.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void saveChangedBundle() {
        int newDiscount = Integer.parseInt(editTextDiscount.getText().toString());
        bundle.setDiscount(newDiscount);
        updateBundle();
    }

    private void updateBundle() {
        if (bundle == null) {
            return;
        }
        editedBundle.setName(bundle.getName());
        editedBundle.setPrice(bundle.getPrice());
        editedBundle.setDiscount(bundle.getDiscount());
        editedBundle.setProducts(bundle.getProducts());
        editedBundle.setChanged(false);
    }
}
