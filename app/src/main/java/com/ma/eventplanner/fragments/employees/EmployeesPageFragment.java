package com.ma.eventplanner.fragments.employees;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentEmployeesPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Owner;

import java.util.HashMap;

public class EmployeesPageFragment extends Fragment implements View.OnClickListener {

    HashMap<String, Employee> mEmployeeMap = new HashMap<>();
    private EmployeesPageViewModel employeesViewModel;
    private FragmentEmployeesPageBinding binding;
    private UsersOperations usersOperations;
    private Owner loggedInOwner;

    public EmployeesPageFragment() {}

    public static EmployeesPageFragment newInstance() {
        return new EmployeesPageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        employeesViewModel = new ViewModelProvider(this).get(EmployeesPageViewModel.class);
        binding = FragmentEmployeesPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        usersOperations = new UsersOperations();

        SearchView searchView = binding.searchText;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                employeesViewModel.setSearchText(newText);
                return true;
            }
        });

        employeesViewModel.getText().observe(getViewLifecycleOwner(), this::filterEmployees);

        AppCompatImageButton registerButton = root.findViewById(R.id.employee_registration_button);
        registerButton.setOnClickListener(this);

        loggedInOwner = new Owner();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInOwner = homeActivity.getLoggedInOwner();

        retrieveAllEmployees();

        return root;
    }

    @Override
    public void onClick(View v) {
        navigateToRegistration();
    }

    private void filterEmployees(String query) {
        HashMap<String, Employee> filteredMap = new HashMap<>();
        String lowerCaseQuery = query.toLowerCase();
        for (HashMap.Entry<String, Employee> entry : mEmployeeMap.entrySet()) {
            Employee employee = entry.getValue();
            if (employee.getName().toLowerCase().contains(lowerCaseQuery) ||
                    employee.getEmail().toLowerCase().contains(lowerCaseQuery) ||
                    employee.getSurname().toLowerCase().contains(lowerCaseQuery)) {
                filteredMap.put(entry.getKey(), employee);
            }
        }
        EmployeesListFragment employeesListFragment = EmployeesListFragment.newInstance(filteredMap);
        FragmentTransition.to(employeesListFragment, requireActivity(), false, R.id.scroll_employees_list);
    }

    private void retrieveAllEmployees() {
        usersOperations.getAllEmployeesByCompany(loggedInOwner.getCompanyId(), new UsersOperations.OnGetAllEmployeesListener() {
            @Override
            public void onSuccess(HashMap<String, Employee> employeeMap) {
                mEmployeeMap = employeeMap;
                FragmentTransition.to(EmployeesListFragment.newInstance(mEmployeeMap), requireActivity(), false, R.id.scroll_employees_list);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error retrieving employees: ", e);
            }
        });
    }

    private void navigateToRegistration() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
        Bundle bundle = new Bundle();
        navController.navigate(R.id.nav_employee_registration, bundle);
    }

    @Override
    public void onResume() {
        super.onResume();
        retrieveAllEmployees();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}