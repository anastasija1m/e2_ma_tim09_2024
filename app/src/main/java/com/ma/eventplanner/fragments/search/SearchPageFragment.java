package com.ma.eventplanner.fragments.search;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.icu.util.Calendar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.CompaniesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentSearchPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.BudgetSubcategory;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Item;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

public class SearchPageFragment extends Fragment {

    private static ArrayList<Item> items = new ArrayList<>();
    private static ArrayList<Item> itemsCurrent = new ArrayList<>();
    private static ArrayList<Item> itemsFinal = new ArrayList<>();
    private FragmentSearchPageBinding binding;
    private TextView textViewEventDateStart;
    private TextView textViewEventDateEnd;
    private Calendar calendar;
    private ProductsOperations productsOperations;
    private ServicesOperations servicesOperations;
    private BundlesOperations bundlesOperations;
    private EventTypesOperations eventTypesOperations;
    private EventsOperations eventsOperations;
    private CategoriesOperations categoriesOperations;
    private SubcategoriesOperations subcategoriesOperations;
    private CompaniesOperations companiesOperations;
    private UsersOperations usersOperations;
    private Map<String, String> eventsIds;
    private Map<String, String> eventTypesIds;
    private Map<String, String> subcatIds;
    private List<Event> events;
    private List<EventType> eventTs;
    private String organizerId;
    private boolean isEventSelected = false;

    public SearchPageFragment() { }

    public static SearchPageFragment newInstance(String param1, String param2) {
        return new SearchPageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        organizerId = sharedPreferences.getString("userId", "");

        binding = FragmentSearchPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        productsOperations = new ProductsOperations();
        servicesOperations = new ServicesOperations();
        bundlesOperations = new BundlesOperations();
        eventTypesOperations = new EventTypesOperations();
        eventsOperations = new EventsOperations();
        subcategoriesOperations = new SubcategoriesOperations();
        categoriesOperations = new CategoriesOperations();
        companiesOperations = new CompaniesOperations();
        usersOperations = new UsersOperations();
        subcatIds = new HashMap<>();
        eventsIds = new HashMap<>();
        eventTypesIds = new HashMap<>();
        events = new ArrayList<>();
        eventTs = new ArrayList<>();

        textViewEventDateStart = root.findViewById(R.id.edittext_search_start);
        textViewEventDateEnd = root.findViewById(R.id.edittext_search_end);
        calendar = Calendar.getInstance();

        getEventTypes();
        getEvents();
        getCategories();

        Button button = binding.getRoot().findViewById(R.id.button_search_search);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search();
            }
        });

        Spinner spinner = binding.getRoot().findViewById(R.id.spinner_search_event);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                isEventSelected = !selectedItem.equals("Custom");
                prep();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        textViewEventDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                String selectedDate = dayOfMonth + "." + (month + 1) + "." + year + ".";
                                textViewEventDateStart.setText(selectedDate);
                            }
                        }, year, month, dayOfMonth);

                datePickerDialog.show();
            }
        });
        textViewEventDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(requireContext(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                String selectedDate = dayOfMonth + "." + (month + 1) + "." + year + ".";
                                textViewEventDateEnd.setText(selectedDate);
                            }
                        }, year, month, dayOfMonth);

                datePickerDialog.show();
            }
        });
        return root;
    }
    private void getCategories() {
        subcatIds.clear();
        List<String> subcatNames = new ArrayList<>();
        subcatNames.add("Any");
        subcategoriesOperations.getAll(new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                CompletableFuture<Void> allCategoryFutures = new CompletableFuture<>();
                AtomicInteger counter = new AtomicInteger(0);
                for(Subcategory s: result) {
                    categoriesOperations.getById(s.getCategoryId(), new CategoriesOperations.GetCategoryByIdListener<Category>() {
                        @Override
                        public void onSuccess(Category result1) {
                            subcatNames.add(result1.getName()+", "+s.getName());
                            subcatIds.put(result1.getName()+", "+s.getName(), s.getId());

                            counter.incrementAndGet();
                            if (counter.get() == result.size()) {
                                allCategoryFutures.complete(null);
                            }
                        }
                        @Override
                        public void onFailure(Exception e) {
                        }
                    });
                }
                allCategoryFutures.thenRun(() -> {
                    Spinner spinner = binding.getRoot().findViewById(R.id.spinner_search_category);
                    String[] typesArrayAsArray = subcatNames.toArray(new String[0]);

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, typesArrayAsArray);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(adapter);
                });
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void getEvents() {
        eventsIds.clear();
        eventsOperations.getAllByOrganizerId(organizerId, new EventsOperations.GetAllEventsListener<Event>() {
            @Override
            public void onSuccess(List<Event> result) {
                events.addAll(result);
                List<String> evNames = new ArrayList<>();
                evNames.add("Custom");
                for(Event et : result) {
                    evNames.add(et.getName());
                    eventsIds.put(et.getName(), et.getId());
                }
                Spinner spinner = binding.getRoot().findViewById(R.id.spinner_search_event);
                String[] typesArrayAsArray = evNames.toArray(new String[0]);

                ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, typesArrayAsArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void getEventTypes() {
        eventTypesIds.clear();
        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                List<String> etNames = new ArrayList<>();
                eventTs.addAll(result);
                etNames.add("Any");
                for(EventType et : result) {
                    etNames.add(et.getTypeName());
                    eventTypesIds.put(et.getTypeName(), et.getId());
                }
                Spinner spinner = binding.getRoot().findViewById(R.id.spinner_search_type);
                String[] typesArrayAsArray = etNames.toArray(new String[0]);

                ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, typesArrayAsArray);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void prepareItemsList() {
        items.clear();
        productsOperations.getAllProducts(new ProductsOperations.GetAllProductsListener<Product>() {
            @Override
            public void onSuccess(ArrayList<Product> result) {
                items.addAll(result);
                servicesOperations.getAllServices(new ServicesOperations.GetAllServicesListener<Service>() {
                    @Override
                    public void onSuccess(ArrayList<Service> result) {
                        items.addAll(result);
                        bundlesOperations.getAllBundles(new BundlesOperations.GetAllBundlesListener<com.ma.eventplanner.model.Bundle>() {
                            @Override
                            public void onSuccess(ArrayList<com.ma.eventplanner.model.Bundle> result) {
                                items.addAll(result);
                                usersOperations.getById(organizerId, new UsersOperations.OnGetByIdListener() {
                                    @Override
                                    public void onSuccess(User user) {
                                        if(user.getRole()== Role.EVENT_ORGANIZER) {
                                            List<Item> temp = new ArrayList<>();
                                            for(Item i : items) {
                                                if(i.isVisible()) temp.add(i);
                                            }
                                            items.clear();
                                            items.addAll(temp);
                                        }
                                        search();
                                    }
                                    @Override
                                    public void onFailure(Exception e) {
                                    }
                                });
                            }
                            @Override
                            public void onFailure(Exception e) {
                            }
                        });
                    }
                    @Override
                    public void onFailure(Exception e) {
                    }
                });
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }
    Event selected = null;
    private void prep() {
        Spinner eve = binding.getRoot().findViewById(R.id.spinner_search_event);
        String eves = eve.getSelectedItem().toString();
        String eveid = eventsIds.get(eves);
        EditText editText1 = binding.getRoot().findViewById(R.id.edittext_search_name);
        EditText editText2 = binding.getRoot().findViewById(R.id.edittext_search_provider);
        EditText editText3 = binding.getRoot().findViewById(R.id.edittext_search_min);
        EditText editText4 = binding.getRoot().findViewById(R.id.edittext_search_max);
        EditText editText = binding.getRoot().findViewById(R.id.edittext_search_location);
        Spinner sss = binding.getRoot().findViewById(R.id.spinner_search_type);
        Spinner ss1s = binding.getRoot().findViewById(R.id.spinner_search_category);
        sss.setSelection(0);
        ss1s.setSelection(0);
        editText.setText("");
        editText1.setText("");
        editText2.setText("");
        editText3.setText("");
        editText4.setText("");
        selected = null;
        if(eveid != null) {
            for(Event e : events){
                if(Objects.equals(e.getId(), eveid))
                    selected= e;
            }
            if(selected != null) {
                editText.setText(selected.getLocation());

                int index = 1;
                for(EventType evt: eventTs){
                    if(Objects.equals(selected.getTypeId(), evt.getId())) break;
                    index++;
                }
                if(selected.getTypeId()==null)sss.setSelection(0);
                else sss.setSelection(index);
            }
        }
        search();
    }
    private void search() {
        itemsCurrent.clear();
        itemsFinal.clear();
        CheckBox cbp = binding.getRoot().findViewById(R.id.checkbox_product);
        boolean cbps = cbp.isChecked();
        CheckBox cbs = binding.getRoot().findViewById(R.id.checkbox_service);
        boolean cbss = cbs.isChecked();
        CheckBox cbb = binding.getRoot().findViewById(R.id.checkbox_bundle);
        boolean cbbs = cbb.isChecked();
        CheckBox isav = binding.getRoot().findViewById(R.id.checkbox_available);
        boolean cisav = isav.isChecked();
        for(Item i : items) {
            if (!cisav || i.isAvailable()) {
                if (i instanceof Product && cbps) itemsCurrent.add(i);
                if (i instanceof Service && cbss) itemsCurrent.add(i);
                if (i instanceof com.ma.eventplanner.model.Bundle && cbbs) itemsCurrent.add(i);
            }
        }

        EditText minp = binding.getRoot().findViewById(R.id.edittext_search_min);
        EditText maxp = binding.getRoot().findViewById(R.id.edittext_search_max);
        String minText = minp.getText().toString().trim();
        String maxText = maxp.getText().toString().trim();
        float minValue = 0.0f;
        float maxValue = 10000000f;
        if (!minText.isEmpty()) {
            try {
                minValue = Float.parseFloat(minText);
            } catch (NumberFormatException e) {
                minValue = 0.0f;
            }
        }
        if (!maxText.isEmpty()) {
            try {
                maxValue = Float.parseFloat(maxText);
            } catch (NumberFormatException e) {
                maxValue = 10000000f;
            }
        }
        for(Item i: itemsCurrent) {
            if (i.getPrice() <= maxValue && i.getPrice() >= minValue) {
                itemsFinal.add(i);
            }
        }
        itemsCurrent.clear();
        itemsCurrent.addAll(itemsFinal);
        itemsFinal.clear();

        EditText name = binding.getRoot().findViewById(R.id.edittext_search_name);
        String nameText = name.getText().toString().toLowerCase().trim();
        for(Item i: itemsCurrent) {
            if (nameText.isEmpty() || i.getName().toLowerCase().contains(nameText)) {
                itemsFinal.add(i);
            }
        }

        Spinner ets = binding.getRoot().findViewById(R.id.spinner_search_type);
        String etss = ets.getSelectedItem().toString();
        String etid = eventTypesIds.get(etss);
        if(etid != null) {
            itemsCurrent.clear();
            itemsCurrent.addAll(itemsFinal);
            itemsFinal.clear();
            for(Item i: itemsCurrent) {
                List<EventType> listet = i.getEvents();
                for(EventType et: listet) {
                    if (Objects.equals(et.getId(), etid)) {
                        itemsFinal.add(i);
                        break;
                    }
                }
            }
        }

        if(!isEventSelected && selected == null) {
            Spinner cs = binding.getRoot().findViewById(R.id.spinner_search_category);
            String css;
            String cid = null;
            if(cs.getSelectedItem() != null) {
                css = cs.getSelectedItem().toString();
                cid = subcatIds.get(css);
            }
            if(cid != null) {
                itemsCurrent.clear();
                itemsCurrent.addAll(itemsFinal);
                itemsFinal.clear();
                for(Item i: itemsCurrent) {
                    if (i instanceof Product) {
                        if(Objects.equals(((Product) i).getSubcategory().getId(), cid))
                            itemsFinal.add(i);
                    }
                    if (i instanceof Service) {
                        if(Objects.equals(((Service) i).getSubcategory().getId(), cid))
                            itemsFinal.add(i);
                    }
                    if (i instanceof com.ma.eventplanner.model.Bundle) {
                        List<Subcategory> listasubc = ((com.ma.eventplanner.model.Bundle) i).getSubcategories();
                        for(Subcategory sc : listasubc) {
                            if(Objects.equals(sc.getId(), cid)) {
                                itemsFinal.add(i);
                                break;
                            }
                        }
                    }
                }
            }
        }
        else {
            itemsCurrent.clear();
            itemsCurrent.addAll(itemsFinal);
            itemsFinal.clear();
            for(BudgetSubcategory cid: selected.getBudgetSubcategories()){
                for(Item i: itemsCurrent) {
                    if (i instanceof Product) {
                        if(Objects.equals(((Product) i).getSubcategory().getId(), cid.getSubcategory().getId()))
                            itemsFinal.add(i);
                    }
                    if (i instanceof Service) {
                        if(Objects.equals(((Service) i).getSubcategory().getId(), cid.getSubcategory().getId()))
                            itemsFinal.add(i);
                    }
                    if (i instanceof com.ma.eventplanner.model.Bundle) {
                        List<Subcategory> listasubc = ((com.ma.eventplanner.model.Bundle) i).getSubcategories();
                        for(Subcategory sc : listasubc) {
                            if(Objects.equals(sc.getId(), cid.getSubcategory().getId())) {
                                itemsFinal.add(i);
                                break;
                            }
                        }
                    }
                }
            }
        }

        EditText loca = binding.getRoot().findViewById(R.id.edittext_search_location);
        String locaText = loca.getText().toString().toLowerCase().trim();
        EditText prov = binding.getRoot().findViewById(R.id.edittext_search_provider);
        String provText = prov.getText().toString().toLowerCase().trim();

        if(!locaText.isEmpty() || !provText.isEmpty()) {
            itemsCurrent.clear();
            itemsCurrent.addAll(itemsFinal);
            itemsFinal.clear();

            CompletableFuture<Void> allCategoryFutures = new CompletableFuture<>();
            AtomicInteger counter = new AtomicInteger(0);


            for(Item i: itemsCurrent) {
                companiesOperations.getById(i.getCompanyId(), new CompaniesOperations.GetCompanyByIdListener<Company>() {
                    @Override
                    public void onSuccess(Company result) {
                        if(provText.isEmpty() || result.getName().toLowerCase().contains(provText.toLowerCase())) {
                            if(locaText.isEmpty() || result.getAddress().getCity().toLowerCase().contains(locaText.toLowerCase())){
                                itemsFinal.add(i);
                            }
                        }
                        counter.incrementAndGet();
                        if (counter.get() == itemsCurrent.size()) {
                            allCategoryFutures.complete(null);
                        }
                    }
                    @Override
                    public void onFailure(Exception e) {
                    }
                });
            }
            allCategoryFutures.thenRun(() -> {
                FragmentTransition.to(SearchListFragment.newInstance(itemsFinal), getActivity(),
                        false, R.id.list_view);
            });
        }
            FragmentTransition.to(SearchListFragment.newInstance(itemsFinal), getActivity(),
                false, R.id.list_view);
    }
    public void onResume() {
        super.onResume();
        prepareItemsList();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}