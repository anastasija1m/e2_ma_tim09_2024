package com.ma.eventplanner.fragments.company;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.adapters.CompanyRatingsAdapter;
import com.ma.eventplanner.databinding.FragmentCompanyRatingsListBinding;
import com.ma.eventplanner.model.CompanyRating;
import com.ma.eventplanner.model.User;

import java.util.ArrayList;

public class CompanyRatingsListFragment extends ListFragment {


    private CompanyRatingsAdapter companyRatingsAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<CompanyRating> mCompanyRatings;
    private FragmentCompanyRatingsListBinding binding;
    private User loggedInUser;

    public CompanyRatingsListFragment() {

    }

    public static CompanyRatingsListFragment newInstance(ArrayList<CompanyRating> companyRatings) {
        CompanyRatingsListFragment companyRatingsListFragment = new CompanyRatingsListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, companyRatings);
        companyRatingsListFragment.setArguments(args);
        return companyRatingsListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loggedInUser = new User();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInUser = homeActivity.getLoggedInUser();
        if (getArguments() != null) {
            mCompanyRatings = getArguments().getParcelableArrayList(ARG_PARAM);
            companyRatingsAdapter = new CompanyRatingsAdapter(getActivity(), mCompanyRatings, loggedInUser);
            setListAdapter(companyRatingsAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCompanyRatingsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}