package com.ma.eventplanner.fragments.employees;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.employee.EmployeeEventListAdapter;
import com.ma.eventplanner.databinding.FragmentEmployeeEventsListBinding;
import com.ma.eventplanner.model.EmployeeEvent;

import java.util.ArrayList;

public class EmployeeEventsListFragment extends ListFragment {

    private EmployeeEventListAdapter employeeEventListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<EmployeeEvent> mEmployeeEvents;
    private FragmentEmployeeEventsListBinding binding;

    public EmployeeEventsListFragment() { }

    public static EmployeeEventsListFragment newInstance(ArrayList<EmployeeEvent> employeeEvents) {
        EmployeeEventsListFragment employeeEventsListFragment = new EmployeeEventsListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, employeeEvents);
        employeeEventsListFragment.setArguments(args);
        return employeeEventsListFragment;
    }

    public void updateEventList(ArrayList<EmployeeEvent> employeeEvents) {
        if (mEmployeeEvents != null) {
            for (EmployeeEvent event : employeeEvents) {
                if (!mEmployeeEvents.contains(event)) {
                    mEmployeeEvents.add(event);
                }
            }
            if (employeeEventListAdapter != null) {
                employeeEventListAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            mEmployeeEvents = getArguments().getParcelableArrayList(ARG_PARAM);
            employeeEventListAdapter = new EmployeeEventListAdapter(getActivity(), mEmployeeEvents);
            setListAdapter(employeeEventListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentEmployeeEventsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}