package com.ma.eventplanner.fragments.employees;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.AuthenticationManager;
import com.ma.eventplanner.database.ImagesOperations;
import com.ma.eventplanner.databinding.FragmentEmployeeDetailsBinding;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EmployeeEvent;
import com.ma.eventplanner.model.EmployeeWorkHours;
import com.ma.eventplanner.model.EmployeeWorkSchedule;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class EmployeeDetailsFragment extends Fragment {

    private FragmentEmployeeDetailsBinding binding;
    private Employee employee;
    private ImagesOperations imagesOperations;
    private String employeeUid;

    public EmployeeDetailsFragment() { }

    public static EmployeeDetailsFragment newInstance() {
        return new EmployeeDetailsFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imagesOperations = new ImagesOperations();
        bindEmployeeDetails();
        addWeeklyCalendarFragment(view);
        addEmployeeEventPageFragment(view);
    }

    private void bindEmployeeDetails() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("employee") && bundle.containsKey("employeeUid")) {
            employee = bundle.getParcelable("employee");
            employeeUid = bundle.getString("employeeUid");
            if (employee != null && employeeUid != null) {
                bindBasicDetails();
                bindWorkScheduleDetails();
                bindAccountStatusDetails();
                setupDeactivateButton();
                setupEditWorkingHoursButton();
            }
        }
    }

    private void bindBasicDetails() {
        String imagePath = "profile-images/";
        String imageName = employee.getImageName();
        imagesOperations.get(imagePath, imageName)
                .addOnSuccessListener(imageBytes -> {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                    binding.employeeImage.setImageBitmap(bitmap);
                })
                .addOnFailureListener(exception -> Log.e(TAG, "Error loading image: " + exception.getMessage()));
        binding.employeeName.setText(employee.getName());
        binding.employeeSurname.setText(employee.getSurname());
        binding.employeeEmail.setText(employee.getEmail());
        binding.employeePhoneNumber.setText(employee.getPhoneNumber());
        String address = getString(R.string.employee_address_format,
                employee.getAddress().getStreet(),
                employee.getAddress().getNumber(),
                employee.getAddress().getCity(),
                employee.getAddress().getCountry());
        binding.employeeAddress.setText(address);
    }

    private void bindWorkScheduleDetails() {
        populateWorkHoursTextViews(employee.getWorkSchedule());

        Date startDate = employee.getWorkSchedule().getScheduleStartDate();
        String formattedStartDate = formatDate(startDate);
        binding.scheduleStartDate.setText(formattedStartDate);

        Date endDate = employee.getWorkSchedule().getScheduleEndDate();
        String formattedEndDate = formatDate(endDate);
        binding.scheduleEndDate.setText(formattedEndDate);
    }

    private void populateWorkHoursTextViews(EmployeeWorkSchedule workSchedule) {
        Map<String, EmployeeWorkHours> workHoursMap = workSchedule.getWorkHoursMap();
        TextView[] workHourTextViews = {
                binding.mondayWorkHours, binding.tuesdayWorkHours, binding.wednesdayWorkHours,
                binding.thursdayWorkHours, binding.fridayWorkHours, binding.saturdayWorkHours,
                binding.sundayWorkHours};

        String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        for (int i = 0; i < daysOfWeek.length; i++) {
            EmployeeWorkHours workHours = workHoursMap.get(daysOfWeek[i]);

            populateWorkHoursTextView(workHourTextViews[i], workHours);
        }
    }

    private void populateWorkHoursTextView(TextView textView, EmployeeWorkHours workHours) {
        if (workHours != null) {
            String workHoursText = formatWorkHours(workHours);
            textView.setText(workHoursText);
        } else {
            textView.setText("Not available");
        }
    }

    private String formatWorkHours(EmployeeWorkHours workHours) {
        return workHours.getStartTime() + " - " + workHours.getEndTime();
    }

    private void addWeeklyCalendarFragment(View view) {
        FrameLayout weeklyCalendarContainer = view.findViewById(R.id.weeklyCalendarContainer);
        WeeklyCalendarFragment weeklyCalendarFragment = new WeeklyCalendarFragment();
        weeklyCalendarFragment.setEmployee(employee, employeeUid);
        getChildFragmentManager().beginTransaction()
                .replace(weeklyCalendarContainer.getId(), weeklyCalendarFragment)
                .commit();
    }

    private void addEmployeeEventPageFragment(View view) {
        List<EmployeeEvent> employeeEvents = employee.getEvents();
        FrameLayout employeeEventPageContainer = view.findViewById(R.id.employeeEventPageContainer);
        EmployeeEventPageFragment employeeEventPageFragment = EmployeeEventPageFragment.newInstance((ArrayList<EmployeeEvent>) employeeEvents);
        getChildFragmentManager().beginTransaction()
                .add(employeeEventPageContainer.getId(), employeeEventPageFragment)
                .commit();
    }

    private String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return dateFormat.format(date);
    }

    private void bindAccountStatusDetails() {
        String accountStatus = employee.getIsAccountActive() ? "Account is active" : "Account is deactivated";
        binding.isEmployeeAccountActive.setText(accountStatus);
    }

    private void setupDeactivateButton() {
        Button deactivateButton = requireView().findViewById(R.id.deactivate_account_button);
        deactivateButton.setText(employee.getIsAccountActive() ? "Deactivate Account" : "Activate Account");
        deactivateButton.setOnClickListener(v -> showConfirmationDialog());
    }

    private void setupEditWorkingHoursButton() {
        Button editWorkingHoursButton = requireView().findViewById(R.id.edit_work_schedule);
        editWorkingHoursButton.setOnClickListener(v -> navigateToEditWorkingHoursFragment());
    }

    private void navigateToEditWorkingHoursFragment() {
        NavController navController = Navigation.findNavController((HomeActivity) requireContext(), R.id.fragment_nav_content_main);
        Bundle bundle = new Bundle();
        bundle.putParcelable("employee", employee);
        navController.navigate(R.id.nav_employee_edit_work_schedule, bundle);
    }

    private void showConfirmationDialog() {
        String status = employee.getIsAccountActive() ? "deactivate" : "activate";
        new AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to " + status + " this account?")
                .setTitle("Confirmation")
                .setPositiveButton("Yes", (dialog, which) -> toggleAccountStatus())
                .setNegativeButton("No", null)
                .show();
    }

    private void toggleAccountStatus() {
        employee.setIsAccountActive(!employee.getIsAccountActive());
        String newAccountStatus = employee.getIsAccountActive() ? "Account is active" : "Account is deactivated";
        binding.isEmployeeAccountActive.setText(newAccountStatus);
        Button deactivateButton = requireView().findViewById(R.id.deactivate_account_button);
        deactivateButton.setText(employee.getIsAccountActive() ? "Deactivate Account" : "Activate Account");

        if (employee.getIsAccountActive()) {
            AuthenticationManager.getInstance().enableEmployeeAccount(employeeUid, employee.getEmail(), employee.getPassword(), requireActivity(), new AuthenticationManager.OnUserCreationListener() {
                @Override
                public void onSuccess() {
                    Toast.makeText(requireContext(), "Employee account enabled successfully", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(requireContext(), "Failed to enable employee account: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            AuthenticationManager.getInstance().disableEmployeeAccount(employeeUid, new AuthenticationManager.OnUserDeletionListener() {
                @Override
                public void onSuccess() {
                    Toast.makeText(requireContext(), "Employee account disabled successfully", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(requireContext(), "Failed to disable employee account: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
