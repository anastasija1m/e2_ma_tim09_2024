package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.SubcategoryListAdapter;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CreateSubcategoryFragment extends Fragment {

    private TextInputEditText subcategoryNameEditText;
    private TextInputEditText descriptionEditText;
    private Button addButton;
    private ListView listView;
    private SubcategoryListAdapter adapter;
    private List<Subcategory> subcategoryList;
    private View rootView;
    private SubcategoriesOperations subcategoriesOperations;
    private String categoryId;
    private Spinner typeSpinner;

    public CreateSubcategoryFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_create_subcategory, container, false);
        subcategoriesOperations = new SubcategoriesOperations();
        subcategoryList = new ArrayList<>();
        initializeViews();
        setupCategoryNameTextView();
        setupSpinner();
        setupListView();
        setupAddButton();



        return rootView;
    }

    private void initializeViews() {
        subcategoryNameEditText = rootView.findViewById(R.id.subcategory_name);
        descriptionEditText = rootView.findViewById(R.id.description);
        addButton = rootView.findViewById(R.id.add_button);
        typeSpinner = rootView.findViewById(R.id.subcategory_type_spinner);
        listView = rootView.findViewById(android.R.id.list);
    }

    private void setupCategoryNameTextView() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            String selectedCategoryName = bundle.getString("selected_category_name");
            categoryId = bundle.getString("selected_category_id");
            TextView categoryNameTextView = rootView.findViewById(R.id.category_name);
            String labelText = getString(R.string.category_label_with_name, getString(R.string.category_label), selectedCategoryName);
            categoryNameTextView.setText(labelText);
        }
    }

    private void setupSpinner() {
        Spinner subcategorySpinner = rootView.findViewById(R.id.subcategory_type_spinner);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(requireContext(),
                R.array.subcategory_options, android.R.layout.simple_spinner_item);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        subcategorySpinner.setAdapter(spinnerAdapter);
    }

    private void setupListView() {
        adapter = new SubcategoryListAdapter(requireContext(), R.layout.create_list_item_subcategory, subcategoryList,
                requireActivity(), subcategoriesOperations, categoryId, typeSpinner);
        listView.setAdapter(adapter);


        subcategoriesOperations.getAllByCategoryId(categoryId, new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategoryList.addAll(result);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void setupAddButton() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewSubcategory();
            }
        });
    }

    private void addNewSubcategory() {
        String subcategoryName = Objects.requireNonNull(subcategoryNameEditText.getText()).toString();
        String description = Objects.requireNonNull(descriptionEditText.getText()).toString();
        String selectedType = typeSpinner.getSelectedItem().toString();
        Subcategory subcategory = new Subcategory(null, subcategoryName, description, SubcategoryType.valueOf(selectedType), categoryId);

        subcategoriesOperations.save(subcategory, new SubcategoriesOperations.SaveSubcategoryListener() {
            @Override
            public void onSuccess(String subcategoryId) {
                Log.e("SubcategorySave", "Subcategory saved successful: " + subcategoryId);
                subcategoryList.add(subcategory);
                adapter.notifyDataSetChanged();

                subcategoryNameEditText.setText("");
                descriptionEditText.setText("");
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("SubcategorySave", "Error saving subcategory to database: " + e.getMessage());
            }
        });


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
