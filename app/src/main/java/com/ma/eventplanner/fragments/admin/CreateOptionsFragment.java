package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ma.eventplanner.R;

public class CreateOptionsFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public CreateOptionsFragment() {
        // Required empty public constructor
    }

    public static CreateOptionsFragment newInstance(String param1, String param2) {
        CreateOptionsFragment fragment = new CreateOptionsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_options, container, false);
        setupCreateCategorySubcategoryButton(view);
        setupCreateEventTypeButton(view);
        return view;
    }

    private void setupCreateCategorySubcategoryButton(View view) {
        view.findViewById(R.id.create_category_subcategory_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToCreateCategoryEvent();
            }
        });
    }

    private void setupCreateEventTypeButton(View view) {
        view.findViewById(R.id.create_event_type_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToCreateEventType();
            }
        });
    }

    private void navigateToCreateCategoryEvent() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
        navController.navigate(R.id.nav_create_category_event);
    }

    private void navigateToCreateEventType() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
        navController.navigate(R.id.nav_create_event_type);
    }

    public void onDestroyView() {
        super.onDestroyView();
    }
}
