package com.ma.eventplanner.fragments.employees;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentEmployeeAddEventBinding;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EmployeeEvent;
import com.ma.eventplanner.model.EmployeeWorkHours;
import com.ma.eventplanner.model.EventStatus;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

public class EmployeeAddEventFragment extends Fragment {

    private FragmentEmployeeAddEventBinding binding;
    private Employee employee;
    private User loggedInUser;
    private String employeeUid;
    private NotificationsOperations notificationsOperations;
    private String mOwnerId;

    public EmployeeAddEventFragment() { }

    public static EmployeeAddEventFragment newInstance() {
        return new EmployeeAddEventFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loggedInUser = new User();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInUser = homeActivity.getLoggedInUser();
        this.notificationsOperations = new NotificationsOperations();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeAddEventBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        setupSaveButton();
        return root;
    }

    private void setupSaveButton() {
        binding.saveButton.setOnClickListener(v -> saveEvent());
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();

        if (bundle != null && bundle.containsKey("selectedDate") && bundle.containsKey("employee") && bundle.containsKey("employeeUid")) {
            Date selectedDate = (Date) bundle.getSerializable("selectedDate");
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String formattedDate = dateFormat.format(selectedDate);
            employeeUid = (String) bundle.getSerializable("employeeUid");

            TextView selectedDateTextView = view.findViewById(R.id.selectedDateTextView);
            selectedDateTextView.setText(formattedDate);
            selectedDateTextView.setVisibility(View.VISIBLE);

            employee = bundle.getParcelable("employee");
            getOwnerId();
        }
    }

    private void saveEvent() {
        String eventName = binding.eventNameEditText.getText().toString();
        String startTime = String.format(Locale.getDefault(), "%02d:%02d", binding.startTimePicker.getHour(), binding.startTimePicker.getMinute());
        String endTime = String.format(Locale.getDefault(), "%02d:%02d", binding.endTimePicker.getHour(), binding.endTimePicker.getMinute());
        LocalDate date = parseDate(binding.selectedDateTextView.getText().toString());
        Notification notification = new Notification();
        if (date != null && employee != null) {
            if (isWithinWorkingHours(employee, date, startTime, endTime)) {
                if (!isEventOverlap(employee, date, startTime, endTime)) {
                    EmployeeEvent employeeEvent;

                    if (loggedInUser.getRole().equals(Role.EMPLOYEE)) {
                        employeeEvent = new EmployeeEvent(eventName, EventStatus.RESERVED, java.sql.Date.valueOf(String.valueOf(date)), startTime, endTime);
                        Log.i("Owner", mOwnerId);
                        notification = createNotification(mOwnerId);
                    } else {
                        employeeEvent = new EmployeeEvent(eventName, EventStatus.OCCUPIED, java.sql.Date.valueOf(String.valueOf(date)), startTime, endTime);
                        notification = createNotification(employeeUid);
                    }
                    employee.getEvents().add(employeeEvent);

                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Notification sent");
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error sending notification", e);
                        }
                    });

                    UsersOperations usersOperations = new UsersOperations();
                    usersOperations.updateEmployee(employee.getEmail(), employee, new UsersOperations.OnUpdateEmployeeListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Employee event list updated successfully");
                            Navigation.findNavController(requireView()).navigateUp();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Failed to update employee event list: ", e);
                        }
                    });
                } else {
                    Toast.makeText(requireContext(), "Event overlaps with existing events.", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(requireContext(), "Event is not within working hours for that day.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private Notification createNotification(String userId) {
        String title = "New event has been added";
        String content = String.format("New event for %s %s has been added.", employee.getName(), employee.getSurname());

        return new Notification("", title, content, userId);
    }

    private boolean isEventOverlap(Employee employee, LocalDate date, String startTime, String endTime) {
        for (EmployeeEvent event : employee.getEvents()) {
            if (event.getDate().equals(java.sql.Date.valueOf(String.valueOf(date)))) {
                if (isTimeOverlap(startTime, endTime, event.getStartTime(), event.getEndTime())) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isTimeOverlap(String startTime1, String endTime1, String startTime2, String endTime2) {
        int start1 = timeToMinutes(startTime1);
        int end1 = timeToMinutes(endTime1);
        int start2 = timeToMinutes(startTime2);
        int end2 = timeToMinutes(endTime2);

        return !(end1 <= start2 || start1 >= end2);
    }

    private int timeToMinutes(String time) {
        String[] parts = time.split(":");
        int hours = Integer.parseInt(parts[0]);
        int minutes = Integer.parseInt(parts[1]);
        return hours * 60 + minutes;
    }

    private LocalDate parseDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
        try {
            Date parsedDate = dateFormat.parse(dateString);
            return parsedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean isWithinWorkingHours(Employee employee, LocalDate date, String startTime, String endTime) {
        String dayOfWeek = date.getDayOfWeek().name();
        dayOfWeek = dayOfWeek.charAt(0) + dayOfWeek.substring(1).toLowerCase();
        EmployeeWorkHours workHours = employee.getWorkSchedule().getWorkHoursForDay(dayOfWeek);

        if (workHours != null) {
            LocalTime eventStartTime = LocalTime.parse(startTime);
            LocalTime eventEndTime = LocalTime.parse(endTime);
            LocalTime workStartTime = LocalTime.parse(workHours.getStartTime());
            LocalTime workEndTime = LocalTime.parse(workHours.getEndTime());

            return !(eventStartTime.isBefore(workStartTime) || eventEndTime.isAfter(workEndTime));
        }

        return false;
    }

    private void getOwnerId() {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getOwnerIdByCompanyId(employee.getCompanyId(), new UsersOperations.OnGetOwnerIdListener() {
            @Override
            public void onSuccess(String ownerId) {
                Log.i("Owner", ownerId);
                mOwnerId = ownerId;
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting owner ID: ", e);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}