package com.ma.eventplanner.fragments.company;

import static android.content.ContentValues.TAG;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CompaniesOperations;
import com.ma.eventplanner.database.ServiceReservationsOperations;
import com.ma.eventplanner.databinding.FragmentCompaniesPageBinding;
import com.ma.eventplanner.databinding.FragmentOwnerServiceReservationsPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.fragments.owners.OwnerReservationViewModel;
import com.ma.eventplanner.fragments.owners.OwnerServiceReservationsList;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.ServiceReservation;

import java.util.ArrayList;
import java.util.List;


public class CompaniesPageFragment extends Fragment {

    private final ArrayList<Company> companies = new ArrayList<>();
    private FragmentCompaniesPageBinding binding;

    public CompaniesPageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentCompaniesPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        retrieveCompanies();

        return root;
    }

    private void retrieveCompanies() {
        CompaniesOperations companiesOperations = new CompaniesOperations();
        companiesOperations.getAll(new CompaniesOperations.GetAllCompaniesListener<Company>() {
            @Override
            public void onSuccess(List<Company> result) {
                companies.clear();
                companies.addAll(result);
                FragmentTransition.to(CompaniesListFragment.newInstance(companies), requireActivity(), false, R.id.scroll_companies_list);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error retrieving employee reservations: ", e);
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        retrieveCompanies();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}