package com.ma.eventplanner.fragments.pricelist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.model.Product;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class PricelistProductFragment extends Fragment {

    private List<Product> productList = new ArrayList<>();
    private ProductsOperations productsOperations;
    private RecyclerViewAdapter adapter;

    public PricelistProductFragment() { }

    public PricelistProductFragment(List<Product> products) {
        this.productList = products;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pricelist_product, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);

        productsOperations = new ProductsOperations();
        adapter = new RecyclerViewAdapter(productList);

        // Change the layout manager to LinearLayoutManager with vertical orientation
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(adapter);

        return rootView;
    }

    // RecyclerViewAdapter class to bind data to RecyclerView
    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

        private List<Product> productList;

        RecyclerViewAdapter(List<Product> productList) {
            this.productList = productList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            Product product = productList.get(position);
            holder.bind(product);
        }

        @Override
        public int getItemCount() {
            return productList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewSerial, textViewName, textViewPrice, textViewDiscount, textViewNewPrice;

            ViewHolder(@NonNull View itemView) {
                super(itemView);
                textViewSerial = itemView.findViewById(R.id.textViewSerial);
                textViewName = itemView.findViewById(R.id.textViewName);
                textViewPrice = itemView.findViewById(R.id.textViewPrice);
                textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
                textViewNewPrice = itemView.findViewById(R.id.textViewNewPrice);

                // Set click listener
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                // Get the selected product
                Product selectedProduct = productList.get(getAdapterPosition());

                NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
                Bundle bundle = new Bundle();
                bundle.putParcelable("product", selectedProduct);
                navController.navigate(R.id.nav_edit_product, bundle);
            }

            void bind(Product product) {
                textViewSerial.setText(String.valueOf(getAdapterPosition() + 1)); // Serial number
                textViewName.setText(product.getName());
                textViewPrice.setText(String.valueOf(product.getPrice()));
                textViewDiscount.setText(String.valueOf(product.getDiscount()));
                textViewNewPrice.setText(String.valueOf(getNewPrice(product)));
            }

            private double getNewPrice(Product product) {
                double newPrice = product.getPrice() * (1 - (double) product.getDiscount() / 100);
                DecimalFormat df = new DecimalFormat("#.##");
                return Double.parseDouble(df.format(newPrice));
            }

        }
    }
}
