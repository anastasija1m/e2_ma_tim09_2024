package com.ma.eventplanner.fragments.events;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.EventsListAdapter;
import com.ma.eventplanner.databinding.FragmentEventsListBinding;
import com.ma.eventplanner.model.Event;

import java.util.ArrayList;

public class EventsListFragment extends ListFragment {

    private EventsListAdapter eventsListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Event> mEvents;
    private FragmentEventsListBinding binding;

    public EventsListFragment() {}

    public static EventsListFragment newInstance(ArrayList<Event> events) {
        EventsListFragment fragment = new EventsListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, events);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mEvents = getArguments().getParcelableArrayList(ARG_PARAM);
            eventsListAdapter = new EventsListAdapter(getActivity(), mEvents);
            setListAdapter(eventsListAdapter);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}