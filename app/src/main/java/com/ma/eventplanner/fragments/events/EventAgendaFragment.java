package com.ma.eventplanner.fragments.events;

import android.Manifest;
import android.app.Dialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventAgendaOperations;
import com.ma.eventplanner.databinding.FragmentEventAgendaBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EventAgendaFragment extends Fragment {

    private static final ArrayList<EventActivity> activities = new ArrayList<>();
    private FragmentEventAgendaBinding binding;
    EditText et1, et2, et3;
    Button add, cancel;
    Dialog dialog;
    private Event event;
    private String eventId;
    final static int CODE = 1232;
    private EventAgendaOperations eventAgendaOperations;

    public EventAgendaFragment(Event event) {
        this.event = event;
    }

    public static EventAgendaFragment newInstance(Event event) {
        EventAgendaFragment fragment = new EventAgendaFragment(event);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = new Dialog(requireContext());
        askPerm();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        eventId = event.getId();
        eventAgendaOperations = new EventAgendaOperations();
        binding = FragmentEventAgendaBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        prepareActivitiesList();
        Button createActivityButton = root.findViewById(R.id.button_create_agenda);
        createActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        Button generatePDFbutton = root.findViewById(R.id.button_pdf_agenda);
        generatePDFbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePDF();
            }
        });
        FragmentTransition.to(EventAgendaListFragment.newInstance(activities), getActivity(), false, R.id.scroll_agendass_list);
        return root;
    }
    public void askPerm() {
        ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE);
    }

    public void generatePDF() {
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(1080, 1920, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(92);

        int xMargin = 50;
        int yMargin = 150;
        int lineSpacing = 60;

        canvas.drawText(event.getName() + " - Agenda", xMargin, yMargin, paint);
        paint.setTextSize(42);
        canvas.drawText(event.getDescription(), xMargin, yMargin + lineSpacing, paint);
        canvas.drawText(event.getLocation() + ", " + getDate(event.getDate()), xMargin, yMargin + 2 * lineSpacing, paint);

        paint.setStrokeWidth(1);
        canvas.drawLine(xMargin, yMargin + 3 * lineSpacing, 1030, yMargin + 3 * lineSpacing, paint);

        paint.setTextSize(36);
        int yPosition = yMargin + 5 * lineSpacing;
        for (EventActivity activity : activities) {
            canvas.drawText("Activity: " + activity.getName(), xMargin, yPosition, paint);
            yPosition += lineSpacing;
            canvas.drawText("Time: " + getTime(activity.getStartTime()) + " - " + getTime(activity.getEndTime()) + "   Location: " + activity.getLocation(), xMargin, yPosition, paint);
            yPosition += lineSpacing;
            canvas.drawText("Description: " + activity.getDescription(), xMargin, yPosition, paint);
            yPosition += lineSpacing;

            paint.setStrokeWidth(1);
            canvas.drawLine(xMargin, yPosition, 1030, yPosition, paint);
            yPosition += lineSpacing;
        }

        document.finishPage(page);

        File ddir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String fileName = event.getName() + "-agenda.pdf";
        File file = new File(ddir, fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            document.writeTo(fos);
            document.close();
            showToast("PDF Generated");
        } catch (IOException e) {
            e.printStackTrace();
            showToast("Error generating PDF");
        }
    }

    private String getDate(Long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        return android.text.format.DateFormat.format("dd.MM.yyyy.", calendar).toString();
    }

    private String getTime(Long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return android.text.format.DateFormat.format("HH:mm", calendar).toString();
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(requireContext());
        LayoutInflater lf = this.getLayoutInflater();
        View customdiag = lf.inflate(R.layout.fragment_event_agenda_creation, null);

        et1 = customdiag.findViewById(R.id.createagenda1);
        et2 = customdiag.findViewById(R.id.createagenda2);
        et3 = customdiag.findViewById(R.id.createagenda6);
        add = customdiag.findViewById(R.id.button_create_agenda);
        cancel = customdiag.findViewById(R.id.button_cancel_agenda);

        TimePicker startPicker = customdiag.findViewById(R.id.agenda_start);
        TimePicker endPicker = customdiag.findViewById(R.id.agenda_end);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    String name = et1.getText().toString().trim();
                    String description = et2.getText().toString().trim();

                    int startHour = startPicker.getCurrentHour();
                    int startMinute = startPicker.getCurrentMinute();
                    int endHour = endPicker.getCurrentHour();
                    int endMinute = endPicker.getCurrentMinute();

                    Calendar startCal = Calendar.getInstance();
                    startCal.set(Calendar.HOUR_OF_DAY, startHour);
                    startCal.set(Calendar.MINUTE, startMinute);
                    startCal.set(Calendar.SECOND, 0);
                    startCal.set(Calendar.MILLISECOND, 0);
                    Long stime = startCal.getTimeInMillis();

                    Calendar endCal = Calendar.getInstance();
                    endCal.set(Calendar.HOUR_OF_DAY, endHour);
                    endCal.set(Calendar.MINUTE, endMinute);
                    endCal.set(Calendar.SECOND, 0);
                    endCal.set(Calendar.MILLISECOND, 0);
                    Long etime = endCal.getTimeInMillis();

                    String location = et3.getText().toString().trim();

                    if (name.isEmpty() || description.isEmpty() || location.isEmpty()) {
                        showToast("Name or description or location cannot be empty");
                        return;
                    }

                    EventActivity ea = new EventActivity(null, eventId, name, description, stime, etime, location);

                    eventAgendaOperations.save(ea, new EventAgendaOperations.SaveAgendaListener() {
                        @Override
                        public void onSuccess(String eventId1) {
                            Log.e("EventActivitySave", "EventActivity saved successful: " + eventId);
                            ea.setId(eventId1);
                            addActivity(ea);
                            showToast("Event created successfully");
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e("EventActivitySave", "Error saving eventActivity to database: " + e.getMessage());
                        }
                    });
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setContentView(customdiag);
        dialog.setTitle("Add Activity");
        dialog.show();
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    public void addActivity(EventActivity activity) {
        activities.add(activity);
        sortActivitiesByStartTime();
        FragmentTransition.to(EventAgendaListFragment.newInstance(activities), getActivity(), false, R.id.scroll_agendass_list);
    }

    private void sortActivitiesByStartTime() {
        Collections.sort(activities, new Comparator<EventActivity>() {
            @Override
            public int compare(EventActivity a1, EventActivity a2) {
                return Long.compare(a1.getStartTime(), a2.getStartTime());
            }
        });
    }

    private void prepareActivitiesList() {
        activities.clear();
        eventAgendaOperations.getAllByEventId(eventId, new EventAgendaOperations.GetAllAgendaListener<EventActivity>() {
            @Override
            public void onSuccess(List<EventActivity> result) {
                activities.addAll(result);
                sortActivitiesByStartTime();
                FragmentTransition.to(EventAgendaListFragment.newInstance(activities), getActivity(), false, R.id.scroll_agendass_list);
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}