package com.ma.eventplanner.fragments.employees;

import static android.content.ContentValues.TAG;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.AuthenticationManager;
import com.ma.eventplanner.database.ImagesOperations;
import com.ma.eventplanner.databinding.FragmentEmployeeProfileBinding;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EmployeeEvent;
import com.ma.eventplanner.model.EmployeeWorkHours;
import com.ma.eventplanner.model.EmployeeWorkSchedule;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class EmployeeProfileFragment extends Fragment {

    private FragmentEmployeeProfileBinding binding;
    private Employee employee;
    private ImagesOperations imagesOperations;

    public EmployeeProfileFragment() { }

    public static EmployeeProfileFragment newInstance() {
        return new EmployeeProfileFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        employee = new Employee();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        employee = homeActivity.getLoggedInEmployee();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEmployeeProfileBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imagesOperations = new ImagesOperations();
        bindEmployeeDetails();
        addWeeklyCalendarFragment(view);
        addEmployeeEventPageFragment(view);
    }

    private void bindEmployeeDetails() {
        if (employee != null) {
            bindBasicDetails();
            bindWorkScheduleDetails();
        }
    }

    private void bindBasicDetails() {
        String imagePath = "profile-images/";
        String imageName = employee.getImageName();
        imagesOperations.get(imagePath, imageName)
                .addOnSuccessListener(imageBytes -> {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                    binding.employeeImage.setImageBitmap(bitmap);
                })
                .addOnFailureListener(exception -> Log.e(TAG, "Error loading image: " + exception.getMessage()));
        binding.employeeName.setText(employee.getName());
        binding.employeeSurname.setText(employee.getSurname());
        binding.employeeEmail.setText(employee.getEmail());
    }

    private void bindWorkScheduleDetails() {
        populateWorkHoursTextViews(employee.getWorkSchedule());

        Date startDate = employee.getWorkSchedule().getScheduleStartDate();
        String formattedStartDate = formatDate(startDate);
        binding.scheduleStartDate.setText(formattedStartDate);

        Date endDate = employee.getWorkSchedule().getScheduleEndDate();
        String formattedEndDate = formatDate(endDate);
        binding.scheduleEndDate.setText(formattedEndDate);
    }

    private void populateWorkHoursTextViews(EmployeeWorkSchedule workSchedule) {
        Map<String, EmployeeWorkHours> workHoursMap = workSchedule.getWorkHoursMap();
        TextView[] workHourTextViews = {
                binding.mondayWorkHours, binding.tuesdayWorkHours, binding.wednesdayWorkHours,
                binding.thursdayWorkHours, binding.fridayWorkHours, binding.saturdayWorkHours,
                binding.sundayWorkHours};

        String[] daysOfWeek = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

        for (int i = 0; i < daysOfWeek.length; i++) {
            EmployeeWorkHours workHours = workHoursMap.get(daysOfWeek[i]);

            populateWorkHoursTextView(workHourTextViews[i], workHours);
        }
    }

    private void populateWorkHoursTextView(TextView textView, EmployeeWorkHours workHours) {
        if (workHours != null) {
            String workHoursText = formatWorkHours(workHours);
            textView.setText(workHoursText);
        } else {
            textView.setText("Not available");
        }
    }

    private String formatWorkHours(EmployeeWorkHours workHours) {
        return workHours.getStartTime() + " - " + workHours.getEndTime();
    }

    private String formatDate(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        return dateFormat.format(date);
    }

    private void addWeeklyCalendarFragment(View view) {
        FrameLayout weeklyCalendarContainer = view.findViewById(R.id.weeklyCalendarContainer);
        WeeklyCalendarFragment weeklyCalendarFragment = new WeeklyCalendarFragment();
        weeklyCalendarFragment.setEmployee(employee, AuthenticationManager.currentUserDetails().getId());
        getChildFragmentManager().beginTransaction()
                .replace(weeklyCalendarContainer.getId(), weeklyCalendarFragment)
                .commit();
    }

    private void addEmployeeEventPageFragment(View view) {
        List<EmployeeEvent> employeeEvents = employee.getEvents();
        FrameLayout employeeEventPageContainer = view.findViewById(R.id.employeeEventPageContainer);
        EmployeeEventPageFragment employeeEventPageFragment = EmployeeEventPageFragment.newInstance((ArrayList<EmployeeEvent>) employeeEvents);
        getChildFragmentManager().beginTransaction()
                .add(employeeEventPageContainer.getId(), employeeEventPageFragment)
                .commit();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}