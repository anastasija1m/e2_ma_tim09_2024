package com.ma.eventplanner.fragments.owners;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class OwnerReservationViewModel extends ViewModel {

    private final MutableLiveData<String> searchText;

    public OwnerReservationViewModel() {
        searchText = new MutableLiveData<>();
        searchText.setValue("");
    }

    public LiveData<String> getText() {
        return searchText;
    }

    public void setSearchText(String query) {
        searchText.setValue(query);
    }
}
