package com.ma.eventplanner.fragments.events;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.EventGuestListAdapter;
import com.ma.eventplanner.databinding.FragmentEventGuestsListBinding;
import com.ma.eventplanner.model.EventGuest;

import java.util.ArrayList;

public class EventGuestsListFragment extends ListFragment {

    private EventGuestListAdapter eventGuestListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<EventGuest> mGuests;
    private FragmentEventGuestsListBinding binding;
    private EventGuestsFragment parentFragment;
    public EventGuestsListFragment() {}

    public static EventGuestsListFragment newInstance(ArrayList<EventGuest> guests, EventGuestsFragment parentFragment) {
        EventGuestsListFragment fragment = new EventGuestsListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, guests);
        fragment.setArguments(args);
        fragment.setParentFragment(parentFragment);
        return fragment;
    }

    public void setParentFragment(EventGuestsFragment parentFragment) {
        this.parentFragment = parentFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mGuests = getArguments().getParcelableArrayList(ARG_PARAM);
            eventGuestListAdapter = new EventGuestListAdapter(parentFragment, getActivity(), mGuests);
            setListAdapter(eventGuestListAdapter);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentEventGuestsListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}