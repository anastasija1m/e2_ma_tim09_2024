package com.ma.eventplanner.fragments.bundles;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.BundleListAdapter;
import com.ma.eventplanner.databinding.FragmentBundlesListBinding;
import com.ma.eventplanner.model.Bundle;

import java.util.ArrayList;


public class BundlesListFragment extends ListFragment {

    private BundleListAdapter bundleListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Bundle> mBundles;
    private FragmentBundlesListBinding binding;

    public BundlesListFragment() {

    }

    public static BundlesListFragment newInstance(ArrayList<Bundle> bundles) {
        BundlesListFragment bundlesListFragment = new BundlesListFragment();
        android.os.Bundle args = new android.os.Bundle();
        ArrayList<Bundle> filtratedList = new ArrayList<>();
        for (Bundle bundle:bundles
        ) {
            if(!bundle.isDeleted()){
                filtratedList.add(bundle);
            }
        }
        args.putParcelableArrayList(ARG_PARAM, filtratedList);
        bundlesListFragment.setArguments(args);
        return bundlesListFragment;
    }

    @Override
    public void onCreate(@Nullable android.os.Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            mBundles = getArguments().getParcelableArrayList(ARG_PARAM);
            bundleListAdapter = new BundleListAdapter(getActivity(), mBundles);
            setListAdapter(bundleListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable android.os.Bundle savedInstanceState) {
        binding = FragmentBundlesListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}