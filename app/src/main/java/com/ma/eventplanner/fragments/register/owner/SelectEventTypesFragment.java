package com.ma.eventplanner.fragments.register.owner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.EventType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class SelectEventTypesFragment extends Fragment {

    private ListView eventTypesListView;
    private List<EventType> eventTypesList;
    private Map<EventType, Boolean> eventTypeSelectionMap;

    @NonNull
    public static SelectEventTypesFragment newInstance() {
        SelectEventTypesFragment fragment = new SelectEventTypesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_select_event_types, container, false);

        eventTypesListView = rootView.findViewById(R.id.event_type_list);
        EventTypesOperations eventTypesOperations = new EventTypesOperations();


        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> eventTypes) {
                eventTypesList = eventTypes;

                eventTypeSelectionMap = new HashMap<>();
                for (EventType eventType : eventTypesList) {
                    eventTypeSelectionMap.put(eventType, false);
                }

                setupEventTypeListView(rootView);
                setupNextButton(rootView);
            }

            @Override
            public void onFailure(Exception e) {
            }
        });


        return rootView;
    }

    private void setupEventTypeListView(@NonNull View rootView) {
        ArrayAdapter<EventType> eventTypeAdapter = new ArrayAdapter<EventType>(requireContext(),
                R.layout.list_item_event_type, R.id.text1, eventTypesList) {
            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView textView = view.findViewById(R.id.text1);
                CheckBox checkBox = view.findViewById(R.id.checkbox);

                EventType eventType = getItem(position);
                if (eventType != null) {
                    textView.setText(eventType.getTypeName());
                    checkBox.setChecked(Boolean.TRUE.equals(eventTypeSelectionMap.get(eventType)));
                    checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                        eventTypeSelectionMap.put(eventType, isChecked);
                    });
                }

                return view;
            }
        };
        eventTypesListView.setAdapter(eventTypeAdapter);
    }

    private void setupNextButton(@NonNull View rootView) {
        Button nextButton = rootView.findViewById(R.id.next_button);
        nextButton.setOnClickListener(v -> navigateToCompanyWorkingHoursFormFragment());
    }

    private void navigateToCompanyWorkingHoursFormFragment() {
        List<EventType> selectedEventTypes = getSelectedEventTypes();

        Company company = getCompanyFromArguments();
        if (company == null) return;

        company.setEventTypeIds(getSelectedEventTypeIds(selectedEventTypes));

        assert getArguments() != null;
        getArguments().putParcelable("company", company);

        Fragment workingHoursFormFragment = new CompanyWorkingHoursFormFragment();
        workingHoursFormFragment.setArguments(getArguments());

        FragmentManager fragmentManager = requireActivity().getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, workingHoursFormFragment)
                .addToBackStack(null)
                .commit();
    }

    private List<EventType> getSelectedEventTypes() {
        List<EventType> selectedEventTypes = new ArrayList<>();
        for (Map.Entry<EventType, Boolean> entry : eventTypeSelectionMap.entrySet()) {
            if (entry.getValue()) {
                selectedEventTypes.add(entry.getKey());
            }
        }
        return selectedEventTypes;
    }

    private List<String> getSelectedEventTypeIds(List<EventType> selectedEventTypes) {
        List<String> selectedEventTypeIds = new ArrayList<>();
        for (EventType eventType : selectedEventTypes) {
            selectedEventTypeIds.add(eventType.getId());
        }
        return selectedEventTypeIds;
    }

    private Company getCompanyFromArguments() {
        Bundle currentArguments = getArguments();
        if (currentArguments == null) return null;
        return currentArguments.getParcelable("company");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface Callback<T> {
        void onSuccess(T result);
        void onError(String errorMessage);
    }
}
