package com.ma.eventplanner.fragments.pricelist;

import static com.ma.eventplanner.database.BundlesOperations.*;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.model.Product;

import java.util.ArrayList;
import java.util.List;

public class EditProductFragment extends Fragment {

    private static Product product;
    private Product editedProduct, notEditedProduct;
    private ProductsOperations productsOperations;
    private BundlesOperations bundlesOperations;
    private TextView textViewName;
    private EditText editTextPrice;
    private EditText editTextDiscount;
    private Button btnSave;

    public EditProductFragment() {
        // Required empty public constructor
    }

    public static EditProductFragment newInstance(Product product) {
        EditProductFragment fragment = new EditProductFragment();
        Bundle args = new Bundle();
        args.putParcelable("product", product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        productsOperations = new ProductsOperations();
        bundlesOperations = new BundlesOperations();
        return inflater.inflate(R.layout.fragment_edit_product, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize views
        textViewName = view.findViewById(R.id.textViewName);
        editTextPrice = view.findViewById(R.id.editTextPrice);
        editTextDiscount = view.findViewById(R.id.editTextDiscount);
        btnSave = view.findViewById(R.id.btn_save);

        // Set onClickListener for the Save button
        btnSave.setOnClickListener(v -> saveChanges());

        // Check if product data is passed as argument
        if (getArguments() != null && getArguments().containsKey("product")) {
            product = getArguments().getParcelable("product");
            notEditedProduct = getArguments().getParcelable("product");
            editedProduct = getArguments().getParcelable("product");
            // Set initial values
            textViewName.setText(product.getName());
            editTextPrice.setText(String.valueOf(product.getPrice()));
            editTextDiscount.setText(String.valueOf(product.getDiscount()));
        }
    }

    private void saveChanges() {
        notEditedProduct.setChanged(true);
        productsOperations.update(notEditedProduct, new ProductsOperations.UpdateProductListener() {
            @Override
            public void onSuccess() {
                saveChangedProduct();
                editedProduct.setChanged(false);
                productsOperations.save(editedProduct, new ProductsOperations.ProductSaveListener() {
                    @Override
                    public void onSuccess(String productId) {
                        updateBundle();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Toast.makeText(getContext(), "Product: " + editedProduct.getName() + " is not edited." , Toast.LENGTH_SHORT).show();

                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Product: " + editedProduct.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateBundle() {
        bundlesOperations.getAllBundles(new BundlesOperations.GetAllBundlesListener<com.ma.eventplanner.model.Bundle>() {
            @Override
            public void onSuccess(ArrayList<com.ma.eventplanner.model.Bundle> result) {
                for (com.ma.eventplanner.model.Bundle bundle : result) {
                    for (Product product1 : bundle.getProducts()) {
                        List<Product> newList = bundle.getProducts();
                        if (product1.getId().equals(product.getId())) {
                            newList.remove(product);
                            newList.add(editedProduct);
                            bundle.setChanged(true);
                            bundlesOperations.update(bundle, new BundlesOperations.UpdateBundleListener() {
                                @Override
                                public void onSuccess() {
                                    bundle.setChanged(false);
                                    bundle.setProducts(newList);
                                    bundlesOperations.save(bundle, new BundlesOperations.BundleSaveListener() {
                                        @Override
                                        public void onSuccess(String productId) {
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Toast.makeText(getContext(), "Product: " + editedProduct.getName() + " is not edited." , Toast.LENGTH_SHORT).show();

                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    Toast.makeText(getContext(), "Product: " + editedProduct.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
                                }
                            });

                            break;
                        }
                    }

                }
                Toast.makeText(getContext(), "Product: " + editedProduct.getName() + " is edited." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }

            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    private void saveChangedProduct(){
        double newPrice = Double.parseDouble(editTextPrice.getText().toString());
        int newDiscount = Integer.parseInt(editTextDiscount.getText().toString());
        product.setPrice(newPrice);
        product.setDiscount(newDiscount);
        updateProduct();
    }

    private void updateProduct() {
        if (product == null)
            return;

        Product editedProduct = new Product();
        editedProduct.setName(product.getName());
        editedProduct.setDescription(product.getDescription());
        editedProduct.setPrice(product.getPrice());
        editedProduct.setDiscount(product.getDiscount());
        editedProduct.setCategory(product.getCategory());
        editedProduct.setSubcategory(product.getSubcategory());
        editedProduct.setEvents(product.getEvents());
        editedProduct.setAvailable(product.isAvailable());
        editedProduct.setVisible(product.isVisible());
        editedProduct.setCompanyId(product.getCompanyId());
        editedProduct.setImages(product.getImages());
        editedProduct.setDeleted(false);
        editedProduct.setChanged(false);

    }
}
