package com.ma.eventplanner.fragments.events;

import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.InputType;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.databinding.FragmentTableBinding;
import com.ma.eventplanner.model.BudgetSubcategory;

import java.util.ArrayList;
import java.util.List;

public class TableFragment extends Fragment {

    private FragmentTableBinding binding;
    private static List<BudgetSubcategory> budgetSubcategories = new ArrayList<>();
    private EventBudgetFragment eventBudgetFragment;
    public TableFragment() { }

    public static TableFragment newInstance() {
        TableFragment fragment = new TableFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void setBudgetSubcategories(ArrayList<BudgetSubcategory> budgetSubcategories) {
        TableFragment.budgetSubcategories = budgetSubcategories;
    }
    public void setParent(EventBudgetFragment eventBudgetFragment) {
        this.eventBudgetFragment = eventBudgetFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = FragmentTableBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        ScrollView scrollView = root.findViewById(R.id.scroll_layout);
        TableLayout mainTableLayout = new TableLayout(requireContext());
        mainTableLayout.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT
        ));

        TableRow headerRow = new TableRow(requireContext());
        headerRow.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT
        ));

        String[] headers = {"N", "Planned", "Spent"};
        for (int i = 0; i < headers.length; i++) {
            TextView headerTextView = new TextView(requireContext());
            headerTextView.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    i == 0 ? 1f : 4f
            ));
            headerTextView.setText(headers[i]);
            headerTextView.setGravity(Gravity.CENTER);
            headerTextView.setBackgroundColor(Color.parseColor("#AFC8AD"));
            headerRow.addView(headerTextView);
        }
        mainTableLayout.addView(headerRow);
        TableRow headerRow2 = new TableRow(requireContext());
        headerRow2.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT
        ));
        String[] headers2 = {"", "Subcategory","Price", "Item" ,"Price"};
        for (int i = 0; i < headers2.length; i++) {
            TextView headerTextView = new TextView(requireContext());
            headerTextView.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    i == 0 ? 1f : (i == 1 || i == 3) ? 3f : 1.5f
            ));
            headerTextView.setText(headers2[i]);
            headerTextView.setBackgroundColor(Color.parseColor("#AFC8AD"));
            headerRow2.addView(headerTextView);
        }
        mainTableLayout.addView(headerRow2);
        int ii = 1;
        int sumPlanned = 0;
        int sumSpent = 0;
        for (BudgetSubcategory rowData : budgetSubcategories) {
            TableRow tableRow = new TableRow(requireContext());
            tableRow.setLayoutParams(new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT
            ));

            // Add serial number
            TextView serialNumberTextView = new TextView(requireContext());
            TableRow.LayoutParams params = new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1f
            );
            params.setMargins(16, 0, 0, 0);
            serialNumberTextView.setLayoutParams(params);
            serialNumberTextView.setText(String.valueOf(ii));
            serialNumberTextView.setOnClickListener(view1 -> {
                if(!rowData.getItems().isEmpty()) {
                    Toast.makeText(requireContext(), "You have purchased items in this subcategory!", Toast.LENGTH_SHORT).show();
                }
                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                    builder.setTitle("Confirmation");
                    builder.setMessage("Are you sure you want to remove this item?");
                    builder.setPositiveButton("Yes", (dialog, which) -> {
                        budgetSubcategories.remove(rowData);
                        eventBudgetFragment.remove(rowData);
                        eventBudgetFragment.addTableFragment(requireView());
                    });
                    builder.setNegativeButton("No", (dialog, which) -> {
                    });
                    builder.show();
                }
            });
            tableRow.addView(serialNumberTextView);

            // Add category name
            TextView categoryNameTextView = new TextView(requireContext());
            categoryNameTextView.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    3f
            ));
            categoryNameTextView.setText(rowData.getSubcategory().getName() +",\n"+rowData.getCategory().getName());
            tableRow.addView(categoryNameTextView);

            // Add new column
            TextView newColumnTextView = new TextView(requireContext());
            newColumnTextView.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1.5f
            ));
            newColumnTextView.setText(rowData.getPrice().toString());
            sumPlanned+= rowData.getPrice();
            int finalIi = ii;
            newColumnTextView.setOnClickListener(view1 -> {
                AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
                builder.setTitle("Enter Price");

                // Set up the input
                final EditText input = new EditText(requireContext());
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", (dialog, which) -> {
                    String enteredValue = input.getText().toString();
                    if (!TextUtils.isEmpty(enteredValue)) {
                        int price = Integer.parseInt(enteredValue);
                        rowData.setPrice(price);
                        eventBudgetFragment.changePrice(price, finalIi -1);
                        newColumnTextView.setText(rowData.getPrice().toString());
                        eventBudgetFragment.addTableFragment(requireView());
                    }
                });
                builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

                builder.show();
            });
            tableRow.addView(newColumnTextView);

            // Create a nested TableLayout for items
            TableLayout itemTableLayout = new TableLayout(requireContext());
            itemTableLayout.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    4.5f
            ));

            // Add items to the nested TableLayout
            for (int i = 0; i < rowData.getItems().size(); i++) {
                final int index = i;
                TableRow itemRow = new TableRow(requireContext());
                itemRow.setLayoutParams(new TableLayout.LayoutParams(
                        TableLayout.LayoutParams.WRAP_CONTENT,
                        TableLayout.LayoutParams.WRAP_CONTENT
                ));

                TextView itemTextView1 = new TextView(requireContext());
                itemTextView1.setLayoutParams(new TableRow.LayoutParams(
                        0,
                        TableRow.LayoutParams.WRAP_CONTENT,
                        3f
                ));
                itemTextView1.setText(rowData.getItems().get(index).getName());
                itemTextView1.setOnClickListener(view1 -> {
                    Toast.makeText(requireContext(), "Item Content: " + rowData.getItems().get(index).getName(), Toast.LENGTH_SHORT).show();
                    //det
                });
                itemRow.addView(itemTextView1);

                TextView itemTextView2 = new TextView(requireContext()); // Second TextView
                itemTextView2.setLayoutParams(new TableRow.LayoutParams(
                        0,
                        TableRow.LayoutParams.WRAP_CONTENT,
                        1.5f
                ));
                itemTextView2.setText(String.valueOf(rowData.getItems().get(index).getPrice()));
                sumSpent+= (int)rowData.getItems().get(index).getPrice();
                itemRow.addView(itemTextView2);

                itemTableLayout.addView(itemRow);
            }
            tableRow.addView(itemTableLayout);
            mainTableLayout.addView(tableRow);
            ii++;
        }
        TableRow headerRow3 = new TableRow(requireContext());
        headerRow3.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT
        ));
        String[] headers3 = {"", "Total Planned:", String.valueOf(sumPlanned), "Total Spent:", String.valueOf(sumSpent)};
        for (int i = 0; i < headers3.length; i++) {
            TextView headerTextView = new TextView(requireContext());
            headerTextView.setLayoutParams(new TableRow.LayoutParams(
                    0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    i == 0 ? 1f : (i == 1 || i == 3) ? 3f : 1.5f
            ));
            headerTextView.setText(headers3[i]);
            headerTextView.setBackgroundColor(Color.parseColor("#AFC8AD"));
            headerRow3.addView(headerTextView);
        }
        mainTableLayout.addView(headerRow3);

        scrollView.addView(mainTableLayout);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}