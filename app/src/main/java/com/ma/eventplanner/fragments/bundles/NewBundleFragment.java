package com.ma.eventplanner.fragments.bundles;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentNewBundleBinding;
import com.ma.eventplanner.databinding.FragmentNewServiceBinding;
import com.ma.eventplanner.model.AcceptanceType;
import com.ma.eventplanner.model.Address;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Item;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;
import com.ma.eventplanner.model.Bundle;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class NewBundleFragment extends Fragment {

    private FragmentNewBundleBinding binding;
    private EditText nameEditText, descriptionEditText, discountEditText;
    private Spinner categorySpinner, productsSpinner, servicesSpinner, employeeSpinner, acceptanceCriteriaSpinner;
    private CheckBox availableCheckBox, visibleCheckBox;
    private RecyclerView selectedEmployeesRecyclerView, selectedProductsRecyclerView, selectedServicesRecyclerView;
    private List<Category> categories;
    private List<Subcategory> subcategories;
    private List<EventType> eventTypes;
    private List<Employee> employees;
    private List<Product> products;
    private List<Service> services;
    private List<AcceptanceType> acceptanceCriteria;
    private List<Employee> selectedEmployees = new ArrayList<>();
    private List<Subcategory> selectedSubcategories = new ArrayList<>();
    private List<Product> selectedProducts = new ArrayList<>();
    private List<Service> selectedServices = new ArrayList<>();
    private List<EventType> selectedEventTypes = new ArrayList<>();
    private BundlesOperations bundlesOperations;
    private EventTypesOperations eventTypesOperations;
    private ProductsOperations productsOperations;
    private ServicesOperations servicesOperations;
    private String loggedUserId;
    private String loggedUserCompanyId;
    private UsersOperations usersOperations;

    public NewBundleFragment() {
    }

    public static NewBundleFragment newInstance() {
        return new NewBundleFragment();
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, android.os.Bundle savedInstanceState) {
        binding = FragmentNewBundleBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        bundlesOperations = new BundlesOperations();
        productsOperations = new ProductsOperations();
        servicesOperations = new ServicesOperations();
        usersOperations = new UsersOperations();
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");
        usersOperations.getEmployeeById(loggedUserId, new UsersOperations.OnGetEmployeeByIdListener() {
            @Override
            public void onSuccess(Employee employee) {
                loggedUserCompanyId = employee.getCompanyId();
            }

            @Override
            public void onFailure(Exception e) {
                usersOperations.getOwnerById(loggedUserId, new UsersOperations.OnGetOwnerByIdListener() {
                    @Override
                    public void onSuccess(Owner owner) {
                        loggedUserCompanyId = owner.getCompanyId();
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
            }
        });
        initializeViews(root);
        setupListeners();
        setupProductsRecyclerView(root);
        setupServicesRecyclerView(root);

        return root;
    }

    private void initializeViews(View root) {
        CategoriesOperations categoriesOperations = new CategoriesOperations();
        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categories = result;
                categories.add(0, new Category("-1L", "Select Category", "", ""));
                List<String> categoryNames = getFieldList(categories, "name");
                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categoryNames);
                categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categorySpinner.setAdapter(categoryAdapter);

            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        eventTypes = getEventTypes();
        employees = getEmployees();
        acceptanceCriteria = getAcceptanceCriteria();
        List<Integer> images = new ArrayList<>();

        eventTypes.add(0, new EventType("1L", "Select Event Type", "", new ArrayList<>(), false));
        categorySpinner = root.findViewById(R.id.spinner_category);
        productsSpinner = root.findViewById(R.id.spinner_products);
        servicesSpinner = root.findViewById(R.id.spinner_services);
        acceptanceCriteriaSpinner = root.findViewById(R.id.spinner_acceptance_type);

        selectedProductsRecyclerView = root.findViewById(R.id.recycler_selected_products);
        selectedProductsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        selectedServicesRecyclerView = root.findViewById(R.id.recycler_selected_services);
        selectedServicesRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));


        List<String> acceptanceCriteriaNames = new ArrayList<>();
        for (AcceptanceType type : acceptanceCriteria) {
            acceptanceCriteriaNames.add(type.toString());
        }

        // Create adapters for spinners
        ArrayAdapter<String> acceptanceCriteriaAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, acceptanceCriteriaNames);
        acceptanceCriteriaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        acceptanceCriteriaSpinner.setAdapter(acceptanceCriteriaAdapter);

        nameEditText = binding.editTextName;
        discountEditText = binding.editTextDiscount;
        descriptionEditText = binding.editTextDescription;
        availableCheckBox = binding.checkboxAvailable;
        visibleCheckBox = binding.checkboxVisible;

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid category is selected (excluding the placeholder)
                if (position > 0) {
                    String selectedCategoryName = categorySpinner.getSelectedItem().toString();
                    Category selectedCategory = findCategoryByName(selectedCategoryName);
                    getProducts(selectedCategory);
                    getServices(selectedCategory);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no category is selected
            }
        });

        productsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    Product selectedProduct = products.get(position);

                    // Add the selected employee to the list
                    selectedProducts.add(selectedProduct);

                    // Update the RecyclerView to display the list of selected employees
                    updateSelectedProductsRecyclerView();

                    // Remove the selected employee from the Spinner's list of available employees
                    products.remove(selectedProduct);
                    updateProductsSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no employee is selected
            }
        });

        servicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    Service selectedService = services.get(position);

                    // Add the selected employee to the list
                    selectedServices.add(selectedService);

                    // Update the RecyclerView to display the list of selected employees
                    updateSelectedServicesRecyclerView();

                    // Remove the selected employee from the Spinner's list of available employees
                    services.remove(selectedService);
                    updateServicesSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no employee is selected
            }
        });
    }


    private void setupProductsRecyclerView(View root) {
        selectedProductsRecyclerView = root.findViewById(R.id.recycler_selected_products);
        selectedProductsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedProductsAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Product product = selectedProducts.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(product.getName());
            }

            @Override
            public int getItemCount() {
                return selectedProducts.size();
            }
        };

        selectedProductsRecyclerView.setAdapter(selectedProductsAdapter);
    }

    private void updateSelectedProductsRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedProductsAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Product product = selectedProducts.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(product.getName());
            }

            @Override
            public int getItemCount() {
                return selectedProducts.size();
            }
        };

        selectedProductsRecyclerView.setAdapter(selectedProductsAdapter);
    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateProductsSpinnerAdapter() {
        // Create a new list of strings to store employee names and surnames
        List<String> productsNames = getNameFieldList(products, "name");

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> productAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, productsNames);

        // Set the layout resource for the adapter
        productAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        productsSpinner.setAdapter(productAdapter);
    }

    private void setupServicesRecyclerView(View root) {
        selectedServicesRecyclerView = root.findViewById(R.id.recycler_selected_services);
        selectedServicesRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedServicesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Service service = selectedServices.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(service.getName());
            }

            @Override
            public int getItemCount() {
                return selectedEmployees.size();
            }
        };

        selectedServicesRecyclerView.setAdapter(selectedServicesAdapter);
    }

    private void updateSelectedServicesRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedServicesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {
                };
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Service service = selectedServices.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(service.getName());
            }

            @Override
            public int getItemCount() {
                return selectedServices.size();
            }
        };

        selectedServicesRecyclerView.setAdapter(selectedServicesAdapter);
    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateServicesSpinnerAdapter() {
        // Create a new list of strings to store employee names and surnames
        List<String> serviceNames = getNameFieldList(services, "name");

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> serviceAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, serviceNames);

        // Set the layout resource for the adapter
        serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        servicesSpinner.setAdapter(serviceAdapter);
    }

    private List<String> getFieldList(List<?> list, String... fieldNames) {
        List<String> fieldList = new ArrayList<>();
        try {
            for (Object obj : list) {
                StringBuilder valueBuilder = new StringBuilder();
                for (String fieldName : fieldNames) {
                    Field field = obj.getClass().getDeclaredField(fieldName);
                    field.setAccessible(true);
                    String value = (String) field.get(obj);
                    valueBuilder.append(value).append(" ");
                }
                fieldList.add(valueBuilder.toString().trim());
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldList;
    }

    private List<String> getNameFieldList(List<?> list, String fieldName) {
        List<String> fieldList = new ArrayList<>();
        try {
            for (Object obj : list) {
                // Get the field from the superclass (Item) if it's not declared in the subclass
                Field field = Item.class.getDeclaredField(fieldName);
                field.setAccessible(true); // Ensure that the field is accessible
                Object value = field.get(obj); // Get the value of the field
                fieldList.add(value.toString());
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldList;
    }


    private void getProducts(Category selectedCategory) {

        productsOperations.getAllByCategoryId(loggedUserCompanyId, selectedCategory, new ProductsOperations.GetAllProductsListener<Product>() {
            @Override
            public void onSuccess(ArrayList<Product> result) {
                products = result;
                updateProductsSpinner(result);
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
    }



    private void updateProductsSpinner(List<Product> subcategories) {
        // Extract names from the list of subcategories
        subcategories.add(0, new Product("1L", "1L", null, "Select Product", "", 0.0, 0, new ArrayList<>(), null, false, false, false, null));

        List<String> subcategoryNames = extractProductNames(subcategories);

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryNames);

        // Set the layout resource for the adapter
        subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        productsSpinner.setAdapter(subcategoryAdapter);
    }


    public  List<String> extractProductNames(List<Product> products) {
        List<String> productNames = new ArrayList<>();
        for (Product product : products) {
            productNames.add(product.getName());
        }
        return productNames;
    }


    private void getServices(Category selectedCategory) {

        servicesOperations.getAllByCategoryId(loggedUserCompanyId, selectedCategory, new ServicesOperations.GetAllServicesListener<Service>() {
            @Override
            public void onSuccess(ArrayList<Service> result) {
                services = result;
                updateServicesSpinner(result);
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
    }



    private void updateServicesSpinner(List<Service> subcategories) {
        // Extract names from the list of subcategories
        subcategories.add(0, new Service("1L", "1L", null, "Select Service", "", 0.0, 0, new ArrayList<>(), null, false, false, false, null, "", new ArrayList<>(), 0, 0, 0, null));

        List<String> subcategoryNames = extractServiceNames(subcategories);

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryNames);

        // Set the layout resource for the adapter
        subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        servicesSpinner.setAdapter(subcategoryAdapter);
    }

    public  List<String> extractServiceNames(List<Service> products) {
        List<String> productNames = new ArrayList<>();
        for (Service product : products) {
            productNames.add(product.getName());
        }
        return productNames;
    }

    private List<EventType> getEventTypes() {
        List<EventType> eventTypes = new ArrayList<>();

        // Sample event type data
        EventType eventType1 = new EventType("1L", "Type A", "Description for Type A", new ArrayList<>(), true);
        EventType eventType2 = new EventType("2L", "Type B", "Description for Type B", new ArrayList<>(), false);

        // Adding event types to the list
        eventTypes.add(eventType1);
        eventTypes.add(eventType2);

        return eventTypes;
    }


    // Method to populate the list of employees
    private List<Employee> getEmployees() {
        List<Employee> employees = new ArrayList<>();

        // Sample employee data
        //employees.add(new Employee("1L", "john@example.com", "password", "John", "Smith", "Address 1", "1234567890", R.drawable.avatar, true));
        //employees.add(new Employee(2L, "jane@example.com", "password", "Jane", "Doe", "Address 2", "9876543210", R.drawable.avatar, false));


        return employees;
    }

    // Method to populate the list of acceptance criteria
    private List<AcceptanceType> getAcceptanceCriteria() {
        List<AcceptanceType> acceptanceCriteria = new ArrayList<>();

        // Add acceptance criteria options
        acceptanceCriteria.add(AcceptanceType.AUTOMATIC);
        acceptanceCriteria.add(AcceptanceType.MANUAL);

        return acceptanceCriteria;
    }

    private void setupListeners() {
        setupRegisterButton();
    }


    private void setupRegisterButton() {
        binding.btnSaveService.setOnClickListener(v -> {
            if (validateFields()) {
                addNewService();
            }
        });
    }

    private boolean validateFields() {
        EditText[] fields = {nameEditText, descriptionEditText, discountEditText};
        Spinner[] spinners = {categorySpinner, acceptanceCriteriaSpinner};

        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText().toString().trim())) {
                showToast("Please fill in all fields");
                return false;
            }
        }

        for (Spinner spinner : spinners) {
            if (spinner.getSelectedItem() == null) {
                showToast("Please select a value from all spinners");
                return false;
            }
        }

        return true;
    }



    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void addNewService() {
        Bundle newBundle = createBundleFromInput();

        bundlesOperations.save(newBundle, new BundlesOperations.BundleSaveListener() {
            @Override
            public void onSuccess(String bundleId) {
                Log.e("BundleSave", "Bundle saved successful: " + bundleId);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("BundleSave", "Error saving bundle to database: " + e.getMessage());
            }
        });
        Navigation.findNavController(requireView()).navigateUp();
    }

    private Bundle createBundleFromInput() {
        Bundle newBundle = new Bundle();
        newBundle.setName(getTextFromEditText(nameEditText));
        newBundle.setDescription(getTextFromEditText(descriptionEditText));
        newBundle.setCompanyId(loggedUserCompanyId);
        String selectedCategoryName = categorySpinner.getSelectedItem().toString();
        Category selectedCategory = findCategoryByName(selectedCategoryName);
        newBundle.setCategory(selectedCategory);
        subcategories = getAllSubcategories(selectedProducts, selectedServices);
        newBundle.setSubcategories(subcategories);
        newBundle.setEvents(getAllEventTypes(selectedProducts, selectedServices));
        newBundle.setPrice(getPrice(selectedProducts, selectedServices));
        String discountText = getTextFromEditText(discountEditText);
        int discount =  Integer.parseInt(discountText);
        newBundle.setDiscount(discount);
        newBundle.setReservationDeadline(getReservationDeadline(selectedServices));
        newBundle.setCancellationDeadline(getCancellationDeadline(selectedServices));
        newBundle.setEmployees(selectedEmployees);
        newBundle.setProducts(selectedProducts);
        newBundle.setServices(selectedServices);
        AcceptanceType selectedAcceptanceType = (acceptanceCriteriaSpinner.getSelectedItem() instanceof AcceptanceType) ? (AcceptanceType) acceptanceCriteriaSpinner.getSelectedItem() : null;

        boolean hasManualAcceptance = false;
        for (Service service : selectedServices) {
            if (service.getAcceptanceType() == AcceptanceType.MANUAL) {
                hasManualAcceptance = true;
                break;
            }
        }

        if (hasManualAcceptance) {
            selectedAcceptanceType = AcceptanceType.MANUAL;
        }

        newBundle.setAcceptanceType(selectedAcceptanceType);
        newBundle.setVisible(visibleCheckBox.isChecked());
        newBundle.setAvailable(availableCheckBox.isChecked());
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.ic_bundle);
        newBundle.setImages(images);
        newBundle.setDeleted(false);

        return newBundle;
    }

    public int getReservationDeadline(List<Service> selectedServices) {
        // If no services are selected, return 0
        if (selectedServices.isEmpty()) {
            return 0;
        }

        // Initialize with the first service's reservation deadline
        int minDeadline = selectedServices.get(0).getReservationDeadline();

        // Iterate through the remaining services to find the smallest deadline
        for (int i = 1; i < selectedServices.size(); i++) {
            int currentDeadline = selectedServices.get(i).getReservationDeadline();
            if (currentDeadline < minDeadline) {
                minDeadline = currentDeadline;
            }
        }

        return minDeadline;
    }

    public int getCancellationDeadline(List<Service> selectedServices) {
        // If no services are selected, return 0
        if (selectedServices.isEmpty()) {
            return 0;
        }

        // Initialize with the first service's cancellation deadline
        int minDeadline = selectedServices.get(0).getCancellationDeadline();

        // Iterate through the remaining services to find the smallest cancellation deadline
        for (int i = 1; i < selectedServices.size(); i++) {
            int currentDeadline = selectedServices.get(i).getCancellationDeadline();
            if (currentDeadline < minDeadline) {
                minDeadline = currentDeadline;
            }
        }

        return minDeadline;
    }



    public double getPrice(List<Product> selectedProducts, List<Service> selectedServices) {
        double totalPrice = 0.0;

        // Add prices of selected products
        for (Product product : selectedProducts) {
            totalPrice += product.getPrice();
        }

        // Add prices of selected services
        for (Service service : selectedServices) {
            totalPrice += service.getPrice();
        }

        return totalPrice;
    }


    public static List<Subcategory> getAllSubcategories(List<Product> products, List<Service> services) {
        List<Subcategory> subcategories = new ArrayList<>();

        // Extract subcategories from products
        for (Product product : products) {
            Subcategory subcategory = product.getSubcategory();
            if (subcategory != null && !containsSubcategory(subcategories, subcategory)) {
                subcategories.add(subcategory);
            }
        }

        // Extract subcategories from services
        for (Service service : services) {
            Subcategory subcategory = service.getSubcategory();
            if (subcategory != null && !containsSubcategory(subcategories, subcategory)) {
                subcategories.add(subcategory);
            }
        }

        return subcategories;
    }

    private static boolean containsSubcategory(List<Subcategory> subcategories, Subcategory subcategory) {
        for (Subcategory existingSubcategory : subcategories) {
            if (existingSubcategory.getId().equals(subcategory.getId())) {
                return true;
            }
        }
        return false;
    }

    public static List<EventType> getAllEventTypes(List<Product> products, List<Service> services) {
        List<EventType> eventTypes = new ArrayList<>();

        // Extract event types from products
        for (Product product : products) {
            List<EventType> productEventTypes = product.getEvents();
            for (EventType eventType : productEventTypes) {
                if (!containsEventType(eventTypes, eventType)) {
                    eventTypes.add(eventType);
                }
            }
        }

        // Extract event types from services
        for (Service service : services) {
            List<EventType> serviceEventTypes = service.getEvents();
            for (EventType eventType : serviceEventTypes) {
                if (!containsEventType(eventTypes, eventType)) {
                    eventTypes.add(eventType);
                }
            }
        }

        return eventTypes;
    }

    private static boolean containsEventType(List<EventType> eventTypes, EventType eventType) {
        for (EventType existingEventType : eventTypes) {
            if (existingEventType.getId().equals(eventType.getId())) {
                return true;
            }
        }
        return false;
    }



    private Category findCategoryByName(String categoryName) {
        for (Category category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }
        return null; // Return null if not found
    }

    private Subcategory findSubcategoryByName(String subcategoryName) {
        for (Subcategory subcategory : subcategories) {
            if (subcategory.getName().equals(subcategoryName)) {
                return subcategory;
            }
        }
        return null; // Return null if not found
    }

    private EventType findEventTypeByName(String eventTypeName) {
        for (EventType eventType : eventTypes) {
            if (eventType.getTypeName().equals(eventTypeName)) {
                return eventType;
            }
        }
        return null; // Return null if not found
    }


    private String getTextFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}