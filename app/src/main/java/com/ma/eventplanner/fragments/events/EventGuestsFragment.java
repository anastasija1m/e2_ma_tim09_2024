package com.ma.eventplanner.fragments.events;

import android.Manifest;
import android.app.Dialog;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventAgendaOperations;
import com.ma.eventplanner.database.EventGuestsOperations;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.databinding.FragmentEventGuestsBinding;
import com.ma.eventplanner.databinding.FragmentEventsPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventGuest;
import com.ma.eventplanner.model.EventType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

public class EventGuestsFragment extends Fragment {
    private static final ArrayList<EventGuest> guests = new ArrayList<>();
    private FragmentEventGuestsBinding binding;
    EditText et1, et2, et3;
    private Event event;
    Button add, cancel;
    final static int CODE = 1232;
    //Dialog dialog;
    private String eventId;
    private EventGuestsOperations eventGuestsOperations;
    public EventGuestsFragment(Event event) {
        this.event = event;
    }

    public static EventGuestsFragment newInstance(Event event) {
        EventGuestsFragment fragment = new EventGuestsFragment(event);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        askPerm();
        //dialog = new Dialog(requireContext());
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        eventId = event.getId();
        eventGuestsOperations = new EventGuestsOperations();
        binding = FragmentEventGuestsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        prepareGuestsList();
        Button createGuestButton = root.findViewById(R.id.button_create_guest);
        createGuestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        Button generatePDFbutton = root.findViewById(R.id.button_pdf_guests);
        generatePDFbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generatePDF();
            }
        });
        FragmentTransition.to(EventGuestsListFragment.newInstance(guests, this), getActivity(), false, R.id.scroll_guestss_list);
        return root;
    }

    public void askPerm() {
        ActivityCompat.requestPermissions(requireActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, CODE);
    }

    public void generatePDF() {
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(1080, 1920, 1).create();
        PdfDocument.Page page = document.startPage(pageInfo);

        Canvas canvas = page.getCanvas();

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(92);

        int xMargin = 50;
        int yMargin = 150;
        int lineSpacing = 60;

        canvas.drawText(event.getName() + " - Guest List", xMargin, yMargin, paint);
        paint.setTextSize(42);
        canvas.drawText(event.getDescription(), xMargin, yMargin + lineSpacing, paint);
        canvas.drawText(event.getLocation() + ", " + getDate(event.getDate()), xMargin, yMargin + 2 * lineSpacing, paint);

        paint.setStrokeWidth(1);
        canvas.drawLine(xMargin, yMargin + 3 * lineSpacing, 1030, yMargin + 3 * lineSpacing, paint);

        paint.setTextSize(36);
        int yPosition = yMargin + 5 * lineSpacing;
        for (EventGuest guest : guests) {
            canvas.drawText("Name: " + guest.getName() + " " + guest.getSurname(), xMargin, yPosition, paint);
            yPosition += lineSpacing;
            canvas.drawText("Age: " + guest.getAgeRange() + "   Notes: " + guest.getAdditionalNotes(), xMargin, yPosition, paint);
            yPosition += lineSpacing;

            paint.setStrokeWidth(1);
            canvas.drawLine(xMargin, yPosition, 1030, yPosition, paint);
            yPosition += lineSpacing;
        }

        document.finishPage(page);

        File ddir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String fileName = event.getName() + "-guestList.pdf";
        File file = new File(ddir, fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            document.writeTo(fos);
            document.close();
            showToast("PDF Generated");
        } catch (IOException e) {
            e.printStackTrace();
            showToast("Error generating PDF");
        }
    }

    // Helper method to format date
    private String getDate(Long date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        return android.text.format.DateFormat.format("dd.MM.yyyy.", calendar).toString();
    }

    // Helper method to format time
    private String getTime(Long time) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        return android.text.format.DateFormat.format("HH:mm", calendar).toString();
    }

    public void addGuest(EventGuest guest) {
        guests.add(guest);
        FragmentTransition.to(EventGuestsListFragment.newInstance(guests, this), getActivity(), false, R.id.scroll_guestss_list);
    }

    public void openDialog() {
        final Dialog dialog = new Dialog(requireContext());
        LayoutInflater lf = this.getLayoutInflater();
        View customdiag = lf.inflate(R.layout.fragment_event_guest_creation, null);

        et1 = customdiag.findViewById(R.id.createguest1);
        et2 = customdiag.findViewById(R.id.createguest2);
        et3 = customdiag.findViewById(R.id.createguest6);
        add = customdiag.findViewById(R.id.button_create_guest);
        cancel = customdiag.findViewById(R.id.button_cancel_guest);
        Spinner age3 = customdiag.findViewById(R.id.age_range);

        CheckBox isInvitedCheckBox = customdiag.findViewById(R.id.checkbox1guest);
        CheckBox hasAcceptedCheckBox = customdiag.findViewById(R.id.checkbox2guest);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    String name = et1.getText().toString().trim();
                    String surname = et2.getText().toString().trim();

                    if (name.isEmpty() || surname.isEmpty()) {
                        showToast("Name and surname cannot be empty");
                        return;
                    }

                    String age = age3.getSelectedItem().toString();
                    boolean isInvited = isInvitedCheckBox.isChecked();
                    boolean hasAccepted = hasAcceptedCheckBox.isChecked();
                    String info = et3.getText().toString().trim();

                    if (!isInvited && hasAccepted) {
                        showToast("'Has Accepted' can only be true if 'Is Invited' is true");
                        return;
                    }

                    EventGuest eg = new EventGuest(null, eventId, name, surname, age, isInvited, hasAccepted, info);

                    eventGuestsOperations.save(eg, new EventGuestsOperations.SaveEventGuestListener() {
                        @Override
                        public void onSuccess(String eventIda) {
                            Log.e("EventGuestSave", "EventGuest saved successful: " + eventIda);
                            eg.setId(eventIda);
                            addGuest(eg);
                            showToast("EventGuest created successfully");
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e("EventGuestSave", "Error saving eventGuest to database: " + e.getMessage());
                        }
                    });
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setContentView(customdiag);
        dialog.setTitle("Add Guest");
        dialog.show();
    }

    public void openDialog2(EventGuest guest) {
        final Dialog dialog = new Dialog(requireContext());
        LayoutInflater lf = this.getLayoutInflater();
        View customdiag = lf.inflate(R.layout.fragment_event_guest_creation, null);

        et1 = customdiag.findViewById(R.id.createguest1);
        et2 = customdiag.findViewById(R.id.createguest2);
        et3 = customdiag.findViewById(R.id.createguest6);
        add = customdiag.findViewById(R.id.button_create_guest);
        add.setText("Update");
        cancel = customdiag.findViewById(R.id.button_cancel_guest);
        Spinner age3 = customdiag.findViewById(R.id.age_range);

        CheckBox isInvitedCheckBox = customdiag.findViewById(R.id.checkbox1guest);
        CheckBox hasAcceptedCheckBox = customdiag.findViewById(R.id.checkbox2guest);

        if (guest != null) {
            et1.setText(guest.getName());
            et2.setText(guest.getSurname());
            et3.setText(guest.getAdditionalNotes());
            String tex = guest.getAgeRange();
            int position = -1;
            if(Objects.equals(tex, "0–3")) position = 0;
            else if(Objects.equals(tex, "3–10")) position = 1;
            else if(Objects.equals(tex, "10–18")) position = 2;
            else if(Objects.equals(tex, "18–30")) position = 3;
            else if(Objects.equals(tex, "30–50")) position = 4;
            else if(Objects.equals(tex, "50–70")) position = 5;
            else if(Objects.equals(tex, "70+")) position = 6;
            age3.setSelection(position);

            isInvitedCheckBox.setChecked(guest.getInvited());
            hasAcceptedCheckBox.setChecked(guest.getHasAccepted());
        }

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = et1.getText().toString().trim();
                String surname = et2.getText().toString().trim();

                if (name.isEmpty() || surname.isEmpty()) {
                    showToast("Name and surname cannot be empty");
                    return;
                }

                String age = age3.getSelectedItem().toString();
                boolean isInvited = isInvitedCheckBox.isChecked();
                boolean hasAccepted = hasAcceptedCheckBox.isChecked();
                String info = et3.getText().toString().trim();

                if (!isInvited && hasAccepted) {
                    showToast("'Has Accepted' can only be true if 'Is Invited' is true");
                    return;
                }

                EventGuest eg = guest != null ? guest : new EventGuest(null, eventId, name, surname, age, isInvited, hasAccepted, info);

                eg.setName(name);
                eg.setSurname(surname);
                eg.setAgeRange(age);
                eg.setInvited(isInvited);
                eg.setHasAccepted(hasAccepted);
                eg.setAdditionalNotes(info);

                eventGuestsOperations.update(eg, new EventGuestsOperations.UpdateEventGuestListener() {
                    @Override
                    public void onSuccess() {
                        Log.e("EventGuestUpdate", "EventGuest updated successfully");
                        showToast("EventGuest updated successfully");
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.e("EventGuestUpdate", "Error updating eventGuest to database: " + e.getMessage());
                    }
                });
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.setContentView(customdiag);
        dialog.setTitle(guest == null ? "Add Guest" : "Edit Guest");
        dialog.show();
    }

    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void prepareGuestsList() {
        guests.clear();
        eventGuestsOperations.getAllByEventId(eventId, new EventGuestsOperations.GetAllEventGuestsListener<EventGuest>() {
            @Override
            public void onSuccess(List<EventGuest> result) {
                guests.addAll(result);
                FragmentTransition.to(EventGuestsListFragment.newInstance(guests, EventGuestsFragment.this), getActivity(), false, R.id.scroll_guestss_list);
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}