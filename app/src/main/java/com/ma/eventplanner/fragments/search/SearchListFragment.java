package com.ma.eventplanner.fragments.search;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.adapters.SearchListAdapter;
import com.ma.eventplanner.databinding.FragmentSearchListBinding;
import com.ma.eventplanner.model.Item;

import java.util.ArrayList;

public class SearchListFragment extends ListFragment {

    private SearchListAdapter itemListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Item> mItems;
    private FragmentSearchListBinding binding;
    public SearchListFragment() { }

    public static SearchListFragment newInstance(ArrayList<Item> items) {
        SearchListFragment searchListFragment = new SearchListFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_PARAM, items);
        searchListFragment.setArguments(args);
        return searchListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            mItems = getArguments().getParcelableArrayList(ARG_PARAM);
            itemListAdapter = new SearchListAdapter(getActivity(), mItems);
            setListAdapter(itemListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSearchListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}