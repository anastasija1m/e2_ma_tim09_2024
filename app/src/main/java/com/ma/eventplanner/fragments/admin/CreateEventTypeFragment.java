package com.ma.eventplanner.fragments.admin;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;

import androidx.fragment.app.Fragment;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.EventTypeListAdapter;
import com.ma.eventplanner.adapters.PickSubcategoryAdapter;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Subcategory;

import java.util.ArrayList;
import java.util.List;

public class CreateEventTypeFragment extends Fragment {

    private EditText typeNameEditText;
    private EditText descriptionEditText;
    private Switch isActiveSwitch;
    private Button addButton;
    private ListView listView;
    private EventTypeListAdapter adapter;
    private List<EventType> eventTypeList;
    private List<Subcategory> subcategories;
    private SubcategoriesOperations subcategoriesOperations;
    private EventTypesOperations eventTypesOperations;
    private PickSubcategoryAdapter subcategoryAdapter;

    public CreateEventTypeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_event_type, container, false);
        eventTypeList = new ArrayList<>();
        subcategoriesOperations = new SubcategoriesOperations();
        eventTypesOperations = new EventTypesOperations();

        subcategoriesOperations.getAll(new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = new ArrayList<>();
                subcategories.addAll(result);

                subcategoryAdapter = new PickSubcategoryAdapter(requireContext(), subcategories);
                ListView subcategoryListView = rootView.findViewById(R.id.suggested_subcategory_list);
                subcategoryListView.setAdapter(subcategoryAdapter);

                subcategoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting subcategories from Firebase", e);
            }
        });

        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                eventTypeList = result;
                setupListView();
                Log.d(TAG, "Get all event types is successful.");
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error event types from Firebase", e);
            }
        });




        initializeViews(rootView);
        //setupListView();
        setupAddButton();
        return rootView;
    }



    private void initializeViews(View rootView) {
        typeNameEditText = rootView.findViewById(R.id.type_name);
        descriptionEditText = rootView.findViewById(R.id.description);
        isActiveSwitch = rootView.findViewById(R.id.is_event_type_active);
        addButton = rootView.findViewById(R.id.add_button);
        listView = rootView.findViewById(android.R.id.list);
    }

    private void setupListView() {
        adapter = new EventTypeListAdapter(requireContext(), R.layout.create_list_item_event_type,
                eventTypeList, requireActivity(), eventTypesOperations, subcategoryAdapter);
        listView.setAdapter(adapter);
    }

    private void setupAddButton() {
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewEventType();
            }
        });
    }

    private void addNewEventType() {
        String typeName = typeNameEditText.getText().toString();
        String description = descriptionEditText.getText().toString();
        boolean isActive = isActiveSwitch.isChecked();

        List<String> selectedSubcategoriesIds = subcategoryAdapter.getSelectedSubcategoriesIds();
        EventType eventType = new EventType(null, typeName, description, selectedSubcategoriesIds, isActive);


        eventTypesOperations.save(eventType, new EventTypesOperations.SaveEventTypeListener() {
            @Override
            public void onSuccess(String categoryId) {
                Log.e("EventTypeSave", "Event type saved successful: " + categoryId);
                eventTypeList.add(eventType);
                adapter.notifyDataSetChanged();

                typeNameEditText.setText("");
                descriptionEditText.setText("");
                isActiveSwitch.setChecked(false);

                subcategoryAdapter.deselectAllCheckboxes();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("EventTypeSave", "Error saving event type to database: " + e.getMessage());
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
