package com.ma.eventplanner.fragments.notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.NotificationListAdapter;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.model.Notification;

import java.util.List;

public class NotificationFragment extends Fragment {

    private NotificationsOperations notificationsOperations;
    private List<Notification> notifications;
    private NotificationListAdapter adapter;

    public NotificationFragment() {
    }

    public static NotificationFragment newInstance() {
        return new NotificationFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        notificationsOperations = new NotificationsOperations();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notification, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "");

        notificationsOperations.getAllByRecipientId(userId, new NotificationsOperations.OnQueryCompleteListener() {
            @Override
            public void onSuccess(List<Notification> fetchedNotifications) {
                notifications = fetchedNotifications;
                ListView listView = view.findViewById(R.id.notifications_list);
                adapter = new NotificationListAdapter(requireContext(), notifications);

                adapter.setMarkAsReadClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Notification clickedNotification = (Notification) v.getTag();
                        markAsReadNotification(clickedNotification);
                    }
                });

                listView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(requireContext(), "Error fetching notifications: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void markAsReadNotification(Notification notification) {
        notification.markAsRead();

        notificationsOperations.update(notification, new NotificationsOperations.OnUpdateSuccessListener() {
            @Override
            public void onSuccess() {
                if (notifications != null) {
                    notifications.remove(notification);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(requireContext(), "Error updating notification: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}