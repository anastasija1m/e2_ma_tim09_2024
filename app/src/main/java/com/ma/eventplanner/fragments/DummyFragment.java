package com.ma.eventplanner.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.databinding.FragmentDummyBinding;

import org.jetbrains.annotations.NotNull;

public class DummyFragment extends Fragment {

    private FragmentDummyBinding binding;

    public DummyFragment() {

    }

    public static DummyFragment newInstance(String param1, String param2) {
        return new DummyFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDummyBinding.inflate(inflater, container, false);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}