package com.ma.eventplanner.fragments.company;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.CompanyListAdapter;
import com.ma.eventplanner.adapters.OwnerReservationListAdapter;
import com.ma.eventplanner.adapters.employee.EmployeeListAdapter;
import com.ma.eventplanner.databinding.FragmentCompaniesListBinding;
import com.ma.eventplanner.databinding.FragmentEmployeesListBinding;
import com.ma.eventplanner.databinding.FragmentOwnerServiceReservationsListBinding;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.Employee;

import org.checkerframework.checker.units.qual.C;

import java.util.ArrayList;
import java.util.HashMap;

public class CompaniesListFragment extends ListFragment {

    private CompanyListAdapter companyListAdapter;
    private static final String ARG_PARAM = "param";
    private ArrayList<Company> mCompanies;
    private FragmentCompaniesListBinding binding;

    public CompaniesListFragment() {

    }

    public static CompaniesListFragment newInstance(ArrayList<Company> companies) {
        CompaniesListFragment companiesListFragment = new CompaniesListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM, companies);
        companiesListFragment.setArguments(args);

        return companiesListFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mCompanies = getArguments().getParcelableArrayList(ARG_PARAM);
            companyListAdapter = new CompanyListAdapter(getActivity(), mCompanies);
            setListAdapter(companyListAdapter);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentCompaniesListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();;
        binding = null;
    }
}