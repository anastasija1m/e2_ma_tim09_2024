package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.adapters.ProductRequestAdapter;
import com.ma.eventplanner.database.ProductRequestsOperations;
import com.ma.eventplanner.model.ProductRequest;
import com.ma.eventplanner.model.RequestStatus;

import java.util.ArrayList;
import java.util.List;


public class ProductRequestsFragment extends Fragment {
    private List<ProductRequest> productRequests;
    private ProductRequestsOperations productRequestsOperations;
    public ProductRequestsFragment() {
    }

    public static ProductRequestsFragment newInstance() {
        ProductRequestsFragment fragment = new ProductRequestsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        productRequestsOperations = new ProductRequestsOperations();
        productRequests = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_requests, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ListView productRequestsListView = view.findViewById(R.id.product_requests_list);
        productRequests = new ArrayList<>();

        loadProductRequests(new ProductRequestsOperations.GetAllProductRequestsListener<ProductRequest>() {
            @Override
            public void onSuccess(List<ProductRequest> result) {
                productRequests.clear();
                productRequests.addAll(result);

                ProductRequestAdapter adapter = new ProductRequestAdapter(requireContext(), productRequests, getActivity());
                productRequestsListView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void loadProductRequests(ProductRequestsOperations.GetAllProductRequestsListener<ProductRequest> listener) {
        productRequestsOperations.getAllByStatus(RequestStatus.ON_PENDING, listener);
    }
}