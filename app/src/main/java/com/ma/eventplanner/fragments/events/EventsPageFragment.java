package com.ma.eventplanner.fragments.events;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.databinding.FragmentEventsPageBinding;
import com.ma.eventplanner.fragments.FragmentTransition;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventType;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class EventsPageFragment extends Fragment {

    private static final ArrayList<Event> events = new ArrayList<>();
    private FragmentEventsPageBinding binding;
    private EventsOperations eventsOperations;
    private String organizerId;
    public EventsPageFragment() {}

    public static EventsPageFragment newInstance() {
        return new EventsPageFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        organizerId = sharedPreferences.getString("userId", "");
        eventsOperations = new EventsOperations();
        prepareEventsList();

        binding = FragmentEventsPageBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Button createEventButton = root.findViewById(R.id.button_create);
        createEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
                Bundle bundle = new Bundle();
                navController.navigate(R.id.nav_event_creation, bundle);
            }
        });
        return root;
    }
    public static void addEvent(Event event) {
        events.add(event);
    }

    private void prepareEventsList() {
        if(!events.isEmpty()) {
            FragmentTransition.to(EventsListFragment.newInstance(events), getActivity(), false, R.id.scroll_eventss_list);
            return;
        }
        eventsOperations.getAllByOrganizerId(organizerId, new EventsOperations.GetAllEventsListener<Event>() {
            @Override
            public void onSuccess(List<Event> result) {
                events.addAll(result);
                FragmentTransition.to(EventsListFragment.newInstance(events), getActivity(), false, R.id.scroll_eventss_list);
            }
            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}