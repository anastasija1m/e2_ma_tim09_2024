package com.ma.eventplanner.fragments.admin;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.ProductRequestsOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.ServiceRequestsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.ProductRequest;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.ServiceRequest;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;

import java.util.ArrayList;
import java.util.List;

public class ServiceRequestDetailsFragment extends Fragment {
    private ServiceRequest serviceRequest;
    private List<Subcategory> subcategories;
    private ServiceRequestsOperations serviceRequestsOperations;
    private SubcategoriesOperations subcategoriesOperations;
    private NotificationsOperations notificationsOperations;
    private ServicesOperations servicesOperations;


    private String serviceRequestId;
    private TextView serviceNameTextView;
    private TextView serviceCategoryTextView;
    private TextView servicePriceTextView;
    private TextView serviceDiscountTextView;
    private TextView serviceDescriptionTextView;
    private TextView subcategoryNameEditText;
    private TextView descriptionEditText;
    private CheckBox pickExistingSubcategoryCheckBox;
    private Spinner existingSubcategorySpinner;
    public ServiceRequestDetailsFragment() {
    }

    public static ServiceRequestDetailsFragment newInstance() {
        ServiceRequestDetailsFragment fragment = new ServiceRequestDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle currentArguments = getArguments();

        if (currentArguments == null) {
            return;
        }


        serviceRequestId = currentArguments.getString("selected_request_id");
        serviceRequestsOperations = new ServiceRequestsOperations();
        subcategoriesOperations = new SubcategoriesOperations();
        notificationsOperations = new NotificationsOperations();
        servicesOperations = new ServicesOperations();
        subcategories = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_service_request_details, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadServiceRequest(new ServiceRequestsOperations.GetServiceRequestByIdListener<ServiceRequest>() {
            @Override
            public void onSuccess(ServiceRequest result) {
                serviceRequest = result;

                serviceNameTextView = view.findViewById(R.id.service_name_details);
                serviceCategoryTextView = view.findViewById(R.id.service_category_name_details);
                servicePriceTextView = view.findViewById(R.id.service_price_details);
                serviceDiscountTextView = view.findViewById(R.id.service_discount_details);
                serviceDescriptionTextView = view.findViewById(R.id.service_description_details);
                subcategoryNameEditText = view.findViewById(R.id.service_subcategory_name);
                descriptionEditText = view.findViewById(R.id.service_description);
                //Spinner typeSpinner = view.findViewById(R.id.requested_subcategory_type_spinner);

                serviceNameTextView.setText(" " + serviceRequest.getService().getName());
                serviceCategoryTextView.setText(" " + serviceRequest.getService().getCategory().getName());
                servicePriceTextView.setText(" " + serviceRequest.getService().getPrice());
                serviceDiscountTextView.setText(" " + serviceRequest.getService().getDiscount());
                serviceDescriptionTextView.setText(" " + serviceRequest.getService().getDescription());
                subcategoryNameEditText.setText(serviceRequest.getRequestedSubcategory().getName());
                descriptionEditText.setText(serviceRequest.getRequestedSubcategory().getDescription());

            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        loadSubcategories(new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {

            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = result;
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        pickExistingSubcategoryCheckBox = view.findViewById(R.id.service_pick_existing_subcategory);

        subcategoryNameEditText = view.findViewById(R.id.service_subcategory_name);
        descriptionEditText = view.findViewById(R.id.service_description);
        Spinner existingSubcategorySpinner = view.findViewById(R.id.service_subcategory_spinner);

        pickExistingSubcategoryCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    subcategoryNameEditText.setEnabled(false);
                    descriptionEditText.setEnabled(false);
                    existingSubcategorySpinner.setEnabled(true);
                } else {
                    subcategoryNameEditText.setEnabled(true);
                    descriptionEditText.setEnabled(true);
                    existingSubcategorySpinner.setEnabled(false);
                }
            }
        });

        Button rejectButton = view.findViewById(R.id.service_request_reject);

        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceRequest.setStatus(RequestStatus.REJECTED);

                serviceRequestsOperations.update(serviceRequest, new ServiceRequestsOperations.UpdateServiceRequestListener() {
                    @Override
                    public void onSuccess() {
                        NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                        navController.navigate(R.id.nav_dummy);
                    }

                    @Override
                    public void onFailure(Exception e) {
                    }
                });
            }
        });

        Button acceptButton = view.findViewById(R.id.service_request_accept);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceRequest.setStatus(RequestStatus.ACCEPTED);

                serviceRequestsOperations.update(serviceRequest, new ServiceRequestsOperations.UpdateServiceRequestListener() {
                    @Override
                    public void onSuccess() {
                        NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                        navController.navigate(R.id.nav_dummy);
                    }

                    @Override
                    public void onFailure(Exception e) {
                    }
                });

                if (pickExistingSubcategoryCheckBox.isChecked()) {
                    String selectedSubcategory = (String) existingSubcategorySpinner.getSelectedItem();

                    Subcategory selectedCategory = null;
                    for (Subcategory subcategory : subcategories) {
                        if (subcategory.getName().equals(selectedSubcategory)) {
                            selectedCategory = subcategory;
                            break;
                        }
                    }

                    if (selectedCategory != null) {
                        Service service = serviceRequest.getService();
                        service.setSubcategory(selectedCategory);

                        servicesOperations.save(service, new ServicesOperations.ServiceSaveListener(){
                            @Override
                            public void onSuccess(String productId) {
                                Notification notification = createNotification(serviceRequest);

                                notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onFailure(Exception e) {

                                    }
                                });

                                NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                                navController.navigate(R.id.nav_dummy);
                            }

                            @Override
                            public void onFailure(Exception e) {

                            }
                        });
                    }
                } else {
                    Subcategory subcategory = new Subcategory("", subcategoryNameEditText.getText().toString(),
                            descriptionEditText.getText().toString(), serviceRequest.getRequestedSubcategory().getType(),
                            serviceRequest.getRequestedSubcategory().getCategoryId());
                    subcategoriesOperations.save(subcategory, new SubcategoriesOperations.SaveSubcategoryListener() {
                        @Override
                        public void onSuccess(String subcategoryId) {
                            subcategory.setId(subcategoryId);

                            Service product = serviceRequest.getService();
                            product.setSubcategory(subcategory);
                            servicesOperations.save(product, new ServicesOperations.ServiceSaveListener(){
                                @Override
                                public void onSuccess(String productId) {
                                    Notification notification = createNotification(serviceRequest);

                                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                        @Override
                                        public void onSuccess() {

                                        }

                                        @Override
                                        public void onFailure(Exception e) {

                                        }
                                    });

                                    NavController navController = Navigation.findNavController(getActivity(), R.id.fragment_nav_content_main);
                                    navController.navigate(R.id.nav_dummy);
                                }

                                @Override
                                public void onFailure(Exception e) {

                                }
                            });
                        }

                        @Override
                        public void onFailure(Exception e) {

                        }
                    });
                }
            }
        });
    }

    private Notification createNotification(ServiceRequest serviceRequest) {
        String title = "Service request accepted";
        String content = String.format("Service request %s is accepted.",
                serviceRequest.getService().getName());

        return new Notification("", title, content, serviceRequest.getOwnerId());
    }

    private void loadServiceRequest(ServiceRequestsOperations.GetServiceRequestByIdListener<ServiceRequest> listener) {
        serviceRequestsOperations.getById(serviceRequestId, listener);
    }

    private void loadSubcategories(SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory> listener) {
        subcategoriesOperations.getAllByType(SubcategoryType.SERVICE, new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = result;

                List<String> subcategoryNames = new ArrayList<>();
                for (Subcategory subcategory : subcategories) {
                    subcategoryNames.add(subcategory.getName());
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryNames);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                existingSubcategorySpinner = requireView().findViewById(R.id.service_subcategory_spinner);
                existingSubcategorySpinner.setAdapter(adapter);
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }
}