package com.ma.eventplanner.fragments.register.organizer;

import static android.content.ContentValues.TAG;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.LoginActivity;
import com.ma.eventplanner.database.AuthenticationManager;
import com.ma.eventplanner.model.Address;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.User;

import java.util.Objects;

public class PersonalDataFormFragment extends Fragment {

    private AuthenticationManager authenticationManager;
    private Button registerButton;
    private TextInputEditText emailInput, passwordInput, passwordConfirmInput, firstNameInput,
            lastNameInput, countryInput, cityInput, postalCodeInput,
            streetInput, numberInput, phoneNumberInput;

    private ImageView profilePictureView;
    private Intent image;
    private Button uploadImageButton;

    public PersonalDataFormFragment() {
    }

    @NonNull
    public static PersonalDataFormFragment newInstance() {
        PersonalDataFormFragment fragment = new PersonalDataFormFragment();

        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        authenticationManager = new AuthenticationManager();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_organizer_personal_data_form, container, false);
        initializeViews(rootView);
        setupRegisterButton();

        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupUploadImageButton();
    }

    private void initializeViews(@NonNull View rootView) {
        emailInput = rootView.findViewById(R.id.email);
        passwordInput = rootView.findViewById(R.id.password);
        passwordConfirmInput = rootView.findViewById(R.id.confirm_password);
        firstNameInput = rootView.findViewById(R.id.first_name);

        lastNameInput = rootView.findViewById(R.id.last_name);
        countryInput = rootView.findViewById(R.id.country);
        cityInput = rootView.findViewById(R.id.city);
        postalCodeInput = rootView.findViewById(R.id.postal_code);
        streetInput = rootView.findViewById(R.id.street);
        numberInput = rootView.findViewById(R.id.house_number);
        phoneNumberInput = rootView.findViewById(R.id.phone_number);

        uploadImageButton = rootView.findViewById(R.id.upload_button);
        registerButton = rootView.findViewById(R.id.register_organizer_button);

        profilePictureView = rootView.findViewById(R.id.profile_picture_view);

    }

    private void setupRegisterButton() {
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setUserData();
                //startLoginActivity();
            }
        });
    }

    private void setupUploadImageButton() {
        uploadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showUploadOptions();
            }
        });
    }

    private void setUserData() {
        String email = Objects.requireNonNull(emailInput.getText()).toString();
        String password = Objects.requireNonNull(passwordInput.getText()).toString();
        String passwordConfirm = Objects.requireNonNull(passwordConfirmInput.getText()).toString();
        String firstName = Objects.requireNonNull(firstNameInput.getText()).toString();
        String lastName = Objects.requireNonNull(lastNameInput.getText()).toString();
        String phoneNumber = Objects.requireNonNull(phoneNumberInput.getText()).toString();

        Address address = setAddress();

        EventOrganizer eventOrganizer = new EventOrganizer(email, password, passwordConfirm, Role.EVENT_ORGANIZER,
                firstName, lastName, "",
                address, phoneNumber);

        //User newUser = eventOrganizer;

        //authenticationManager.createUserWithEmailAndPasswordLuka(email, password, getActivity(), newUser, image);




        authenticationManager.createUserWithEmailAndPassword(email, password, getActivity(), eventOrganizer, image, new AuthenticationManager.OnUserCreationListener() {
            @Override
            public void onSuccess() {
                // TODO 1: handle successful user creation
                startLoginActivity();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Failed to create user: " + e.getMessage(), e);
                Toast.makeText(requireContext(), "Failed to create user: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Address setAddress() {
        String country = Objects.requireNonNull(countryInput.getText()).toString();
        String city = Objects.requireNonNull(cityInput.getText()).toString();
        String postalCode = Objects.requireNonNull(postalCodeInput.getText()).toString();
        String street = Objects.requireNonNull(streetInput.getText()).toString();
        String number = Objects.requireNonNull(numberInput.getText()).toString();

        return new Address(country, city, postalCode, street, number);
    }

    private final ActivityResultLauncher<Intent> imageChooserLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    image = result.getData();
                    if (image != null) {
                        Uri selectedImageUri = image.getData();
                        if (selectedImageUri != null) {
                            profilePictureView.setImageURI(selectedImageUri);
                        } else {
                            Bitmap bitmap = (Bitmap) Objects.requireNonNull(image.getExtras()).get("data");
                            profilePictureView.setImageBitmap(bitmap);
                        }
                    }
                }
            });

    private void startLoginActivity() {
        startActivity(new Intent(requireActivity(), LoginActivity.class));
    }

    private void showUploadOptions() {
        Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Intent chooserIntent = Intent.createChooser(pickPhotoIntent, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{takePictureIntent});
        imageChooserLauncher.launch(chooserIntent);
    }

}