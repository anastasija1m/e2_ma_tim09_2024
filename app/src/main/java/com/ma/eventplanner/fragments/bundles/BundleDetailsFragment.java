package com.ma.eventplanner.fragments.bundles;

import android.app.AlertDialog;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.databinding.FragmentBundleDetailsBinding;
import com.ma.eventplanner.databinding.FragmentServiceDetailsBinding;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Bundle;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.Subcategory;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class BundleDetailsFragment extends Fragment {

    private FragmentBundleDetailsBinding binding;
    private Bundle bundle;
    private BundlesOperations bundlesOperations;

    public BundleDetailsFragment() { }

    public static BundleDetailsFragment newInstance() {
        return new BundleDetailsFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             android.os.Bundle savedInstanceState) {
        bundlesOperations = new BundlesOperations();
        binding = FragmentBundleDetailsBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, android.os.Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindBundleDetails();
    }


    private void bindBundleDetails() {
        android.os.Bundle bundleOs = getArguments();
        if (bundleOs != null && bundleOs.containsKey("bundle")) {
            bundle = bundleOs.getParcelable("bundle");
            if (bundle != null) {
                bindBasicDetails();
                setupDeleteButton();
                setupEditButton();
            }
        }
    }

    private void bindBasicDetails() {
        binding.bundleImage.setImageResource(bundle.getImages().get(0));
        binding.bundleName.setText(bundle.getName());
        binding.bundleDescription.setText(bundle.getDescription());
        binding.bundleCategory.setText(bundle.getCategory().getName());

        List<Employee> employees = bundle.getEmployees();
        List<String> employeeNames = new ArrayList<>();

        for (Employee employee : employees) {
            String fullName = employee.getName() + " " + employee.getSurname();
            employeeNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> adapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(employeeNames.get(position));
            }

            @Override
            public int getItemCount() {
                return employeeNames.size();
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.employeesRecyclerView.setLayoutManager(layoutManager);

        List<EventType> eventTypes = bundle.getEvents();
        List<String> eventNames = new ArrayList<>();

        for (EventType eventType : eventTypes) {
            String fullName = eventType.getTypeName();
            eventNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> eventAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventNames.get(position));
            }

            @Override
            public int getItemCount() {
                return eventNames.size();
            }
        };

        LinearLayoutManager layoutEventManager = new LinearLayoutManager(getContext());
        binding.eventsRecyclerView.setLayoutManager(layoutEventManager);

        List<Subcategory> subcategories = bundle.getSubcategories();
        List<String> subcategoryNames = new ArrayList<>();
        for (Subcategory subcategory : subcategories) {
            String fullName = subcategory.getName();
            subcategoryNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> subcategoryAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(subcategoryNames.get(position));
            }

            @Override
            public int getItemCount() {
                return subcategoryNames.size();
            }
        };

        LinearLayoutManager layoutSubcategoryManager = new LinearLayoutManager(getContext());
        binding.subcategoriesRecyclerView.setLayoutManager(layoutSubcategoryManager);

        List<Product> products = bundle.getProducts();
        List<String> productNames = new ArrayList<>();
        for (Product product : products) {
            String fullName = product.getName();
            productNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> productsAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(productNames.get(position));
            }

            @Override
            public int getItemCount() {
                return productNames.size();
            }
        };

        LinearLayoutManager layoutProductManager = new LinearLayoutManager(getContext());
        binding.productRecyclerView.setLayoutManager(layoutProductManager);

        List<Service> services = bundle.getServices();
        List<String> serviceNames = new ArrayList<>();
        for (Service service : services) {
            String fullName = service.getName();
            serviceNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> serviceAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(serviceNames.get(position));
            }

            @Override
            public int getItemCount() {
                return serviceNames.size();
            }
        };

        LinearLayoutManager layoutServiceManager = new LinearLayoutManager(getContext());
        binding.servicesRecyclerView.setLayoutManager(layoutServiceManager);

        binding.subcategoriesRecyclerView.setAdapter(subcategoryAdapter);
        binding.productRecyclerView.setAdapter(productsAdapter);
        binding.eventsRecyclerView.setAdapter(eventAdapter);
        binding.servicesRecyclerView.setAdapter(serviceAdapter);
        binding.employeesRecyclerView.setAdapter(adapter);
        binding.bundlePrice.setText(String.valueOf(bundle.getPrice()));
        binding.bundleDiscount.setText(String.valueOf(bundle.getDiscount()));
        binding.bundleReservationDeadline.setText(String.valueOf(bundle.getReservationDeadline()));
        binding.bundleCancellationDeadline.setText(String.valueOf(bundle.getCancellationDeadline()));
    }

    private void setupDeleteButton() {
        Button deleteButton = requireView().findViewById(R.id.delete_bundle_button);
        deleteButton.setOnClickListener(v -> showConfirmationDialog());
    }

    private void setupEditButton() {
        Button editButton = requireView().findViewById(R.id.edit_bundle_button);
        editButton.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
            android.os.Bundle bundleOs = new android.os.Bundle();
            bundleOs.putParcelable("bundle", bundle);
            navController.navigate(R.id.nav_bundle_edit, bundleOs);
        });
    }


    private void showConfirmationDialog() {
        new AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to delete this bundle?")
                .setTitle("Confirmation")
                .setPositiveButton("Yes", (dialog, which) -> toggleProductStatus())
                .setNegativeButton("No", null)
                .show();
    }

    private void toggleProductStatus() {
        bundle.setDeleted(true);
        bundlesOperations.update(bundle, new BundlesOperations.UpdateBundleListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getContext(), "Bundle: " + bundle.getName() + " is deleted." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Bundle: " + bundle.getName() + " is not deleted." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}