package com.ma.eventplanner.fragments.products;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.adapters.EventSpinnerAdapter;
import com.ma.eventplanner.database.CompaniesOperations;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.databinding.FragmentProductDetailsBinding;
import com.ma.eventplanner.model.BudgetSubcategory;
import com.ma.eventplanner.model.Company;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Item;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Subcategory;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


public class ProductDetailsFragment extends Fragment {

    private FragmentProductDetailsBinding binding;
    private Product product;
    private ProductsOperations productsOperations;
    private EventsOperations eventsOperations;
    private CompaniesOperations companiesOperations;
    private List<Event> events;
    private Company company;

    public ProductDetailsFragment() { }

    public static ProductDetailsFragment newInstance() {
        return new ProductDetailsFragment();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        productsOperations = new ProductsOperations();
        eventsOperations = new EventsOperations();
        companiesOperations = new CompaniesOperations();
        binding = FragmentProductDetailsBinding.inflate(inflater, container, false);

        Activity activity = getActivity();

        isEventOrganizer(activity);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindProductDetails();
        setupBuyButton();
    }

    private void setupBuyButton() {
        Button buyButton = requireView().findViewById(R.id.buy_product_button);
        buyButton.setOnClickListener(v -> showBuyDialog());
    }

    private void showBuyDialog() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_buy_product, null);

        Spinner eventSpinner = dialogView.findViewById(R.id.spinner_event_for_product);
        EventSpinnerAdapter eventAdapter = new EventSpinnerAdapter(requireContext(), events);
        eventSpinner.setAdapter(eventAdapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setView(dialogView)
                .setTitle("Buy product")
                .setPositiveButton("Confirm", (dialog, which) -> {
                    Event selectedEvent = (Event) eventSpinner.getSelectedItem();
                    addToBudget(selectedEvent);
                })
                .setNegativeButton("Cancel", (dialog, which) -> {
                    dialog.dismiss();
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void addToBudget(Event selectedEvent) {
        Subcategory productSubcategory = product.getSubcategory();

        BudgetSubcategory matchingBudgetSubcategory = selectedEvent.getBudgetSubcategories().stream()
                .filter(budgetSubcategory -> budgetSubcategory.getSubcategory().equals(productSubcategory))
                .findFirst()
                .orElse(null);

        if (matchingBudgetSubcategory == null) {
            List<Item> items = new ArrayList<>();
            items.add(product);
            BudgetSubcategory newBudgetSubcategory = new BudgetSubcategory(product.getCategory(), productSubcategory, 0, items);

            List<BudgetSubcategory> eventSubcategoryBudget = selectedEvent.getBudgetSubcategories();
            eventSubcategoryBudget.add(newBudgetSubcategory);

            selectedEvent.setBudgetSubcategories(eventSubcategoryBudget);

            eventsOperations.update(selectedEvent, new EventsOperations.UpdateEventListener() {
                @Override
                public void onSuccess() {
                    Toast.makeText(requireContext(), "The purchase is successful", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(requireContext(), "Purchase failed", Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            List<BudgetSubcategory> eventSubcategoryBudgets = selectedEvent.getBudgetSubcategories();
            eventSubcategoryBudgets.remove(matchingBudgetSubcategory);

            List<Item> items = matchingBudgetSubcategory.getItems();
            items.add(product);

            matchingBudgetSubcategory.setItems(items);

            eventSubcategoryBudgets.add(matchingBudgetSubcategory);

            selectedEvent.setBudgetSubcategories(eventSubcategoryBudgets);
            eventsOperations.update(selectedEvent, new EventsOperations.UpdateEventListener() {
                @Override
                public void onSuccess() {
                    Toast.makeText(requireContext(), "The purchase is successful", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Exception e) {
                    Toast.makeText(requireContext(), "Purchase failed", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }


    private void isEventOrganizer(Activity activity) {
        if (!(activity instanceof HomeActivity)) {
            return;
        }

        HomeActivity homeActivity = (HomeActivity) activity;

        if (homeActivity.getLoggedInUser().getRole() != Role.EVENT_ORGANIZER) {
            binding.linearLayoutBuyProduct.setVisibility(View.GONE);
        } else {
            binding.deleteProductButton.setVisibility(View.GONE);
            binding.editProductButton.setVisibility(View.GONE);
            events = new ArrayList<>();
            fetchEvents();
        }
    }


    private void bindProductDetails() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("product")) {
            product = bundle.getParcelable("product");
            if (product != null) {
                companiesOperations.getById(product.getCompanyId(), new CompaniesOperations.GetCompanyByIdListener<Company>() {
                    @Override
                    public void onSuccess(Company result) {
                        company = result;
                        bindBasicDetails();
                        setupDeleteButton();
                        setupEditButton();
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
            }
        }
    }

    private void bindBasicDetails() {
        binding.productImage.setImageResource(product.getImages().get(0));
        binding.productName.setText(product.getName());
        binding.productDescription.setText(product.getDescription());
        binding.productCategory.setText(product.getCategory().getName());
        binding.productSubcategory.setText(product.getSubcategory().getName());
        binding.productCompany.setText(company.getName());

        List<EventType> eventTypes = product.getEvents();
        List<String> eventTypeNames = new ArrayList<>();

        for (EventType eventType : eventTypes) {
            String fullName = eventType.getTypeName();
            eventTypeNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> eventadapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventTypeNames.get(position));
            }

            @Override
            public int getItemCount() {
                return eventTypeNames.size();
            }
        };

        LinearLayoutManager layoutEventManager = new LinearLayoutManager(getContext());
        binding.eventsRecyclerView.setLayoutManager(layoutEventManager);

        binding.eventsRecyclerView.setAdapter(eventadapter);
    }

    private void setupDeleteButton() {
        Button deleteButton = requireView().findViewById(R.id.delete_product_button);
        deleteButton.setOnClickListener(v -> showConfirmationDialog());
    }

    private void setupEditButton() {
        Button editButton = requireView().findViewById(R.id.edit_product_button);
        editButton.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
            Bundle bundle = new Bundle();
            bundle.putParcelable("product", product);
            navController.navigate(R.id.nav_product_edit, bundle);
        });
    }


    private void showConfirmationDialog() {
        new AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to delete this product?")
                .setTitle("Confirmation")
                .setPositiveButton("Yes", (dialog, which) -> toggleProductStatus())
                .setNegativeButton("No", null)
                .show();
    }

    private void toggleProductStatus() {
        product.setDeleted(true);
        productsOperations.update(product, new ProductsOperations.UpdateProductListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getContext(), "Product: " + product.getName() + " is deleted." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Product: " + product.getName() + " is not deleted." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }
        });
    }

    private void fetchEvents() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String organizerId = sharedPreferences.getString("userId", "");
        eventsOperations.getAllByOrganizerId(organizerId, new EventsOperations.GetAllEventsListener<Event>() {
            @Override
            public void onSuccess(List<Event> result) {
                events = result;
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("ServiceDetailsFragment", String.valueOf(e));
            }
        });
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}