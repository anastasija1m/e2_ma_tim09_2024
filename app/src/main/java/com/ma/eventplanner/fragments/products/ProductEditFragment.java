package com.ma.eventplanner.fragments.products;

import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.databinding.FragmentEmployeeRegistrationBinding;
import com.ma.eventplanner.databinding.FragmentNewProductBinding;
import com.ma.eventplanner.databinding.FragmentProductEditBinding;
import com.ma.eventplanner.fragments.employees.EmployeeRegistrationFragment;
import com.ma.eventplanner.fragments.employees.EmployeesPageFragment;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;
import com.ma.eventplanner.model.WorkSchedule;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ProductEditFragment extends Fragment {

    private FragmentProductEditBinding binding;
    private EditText nameEditText, descriptionEditText, priceEditText, discountEditText;
    private Spinner categorySpinner, subcategorySpinner, eventTypeSpinner;
    private CheckBox availableCheckBox, visibleCheckBox;
    private RecyclerView  selectedEventsRecyclerView;
    private List<Category> categories;
    private List<Subcategory> subcategories;
    private List<EventType> eventTypes;
    private Product productToEdit, notEditedProduct;
    private List<EventType> selectedEventTypes = new ArrayList<>();
    private ProductsOperations productsOperations;
    private CategoriesOperations categoriesOperations;
    private EventTypesOperations eventTypesOperations;

    public ProductEditFragment() {
    }

    public static ProductEditFragment newInstance(Product product) {
        ProductEditFragment fragment = new ProductEditFragment();
        Bundle args = new Bundle();
        args.putParcelable("product", product);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentProductEditBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        productsOperations = new ProductsOperations();
        categoriesOperations = new CategoriesOperations();
        eventTypesOperations = new EventTypesOperations();
        setupListeners();
        setupEventRecyclerView(root);

        // Check if product data is passed as argument
        if (getArguments() != null && getArguments().containsKey("product")) {
            productToEdit = getArguments().getParcelable("product");
            notEditedProduct = getArguments().getParcelable("product");
            if (productToEdit != null) {
                initializeViews(root);
                fillFieldsWithProductData();
                // Disable the spinner after filling it with data
                categorySpinner.setEnabled(false);
            }
        }

        return root;
    }


    private void fillFieldsWithProductData() {
        nameEditText.setText(productToEdit.getName());
        descriptionEditText.setText(productToEdit.getDescription());
        priceEditText.setText(String.valueOf(productToEdit.getPrice()));
        discountEditText.setText(String.valueOf(productToEdit.getDiscount()));

        availableCheckBox.setChecked(productToEdit.isAvailable());
        visibleCheckBox.setChecked(productToEdit.isVisible());

        selectedEventTypes.addAll(productToEdit.getEvents());
        updateSelectedEventsRecyclerView();
    }

    private void updateSelectedEventsRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEventAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                EventType eventType = selectedEventTypes.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventType.getTypeName());
            }

            @Override
            public int getItemCount() {
                return selectedEventTypes.size();
            }
        };

        // Set the adapter to the RecyclerView
        selectedEventsRecyclerView.setAdapter(selectedEventAdapter);

    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateEventsSpinnerAdapter() {

        List<String> eventTypeNames = new ArrayList<>();
        for (EventType event : eventTypes) {
            // Concatenate first name and last name
            String fullName = event.getTypeName();
            eventTypeNames.add(fullName);
        }
        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeNames);

        // Set the layout resource for the adapter
        eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        eventTypeSpinner.setAdapter(eventTypeAdapter);
    }

    private void setupEventRecyclerView(View root) {
        selectedEventsRecyclerView = root.findViewById(R.id.recycler_selected_events);
        selectedEventsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEmployeesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                EventType eventType = selectedEventTypes.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventType.getTypeName());
            }

            @Override
            public int getItemCount() {
                return selectedEventTypes.size();
            }
        };

// Set the adapter to the RecyclerView
        selectedEventsRecyclerView.setAdapter(selectedEmployeesAdapter);

    }

    private int getCategoryPositionByName(String categoryName) {
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getName().equals(categoryName)) {
                return i;
            }
        }
        return -1;
    }

    private int getSubcategoryPositionByName(String subcategoryName) {
        for (int i = 0; i < subcategories.size(); i++) {
            if (subcategories.get(i).getName().equals(subcategoryName)) {
                return i;
            }
        }
        return -1;
    }

    private int getEventTypePositionByName(String eventTypeName) {
        for (int i = 0; i < eventTypes.size(); i++) {
            if (eventTypes.get(i).getTypeName().equals(eventTypeName)) {
                return i;
            }
        }
        return -1;
    }


    private void initializeViews(View root) {
        // Assuming you have predefined lists of categories, subcategories, and event types
        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categories = result;
                List<String> categoryNames = getFieldList(categories, "name");
                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categoryNames);
                categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categorySpinner.setAdapter(categoryAdapter);
                // Select the category in the spinner
                if (productToEdit.getCategory() != null) {
                    int categoryPosition = getCategoryPositionByName(productToEdit.getCategory().getName());
                    if (categoryPosition != -1) {
                        categorySpinner.setSelection(categoryPosition);
                    }
                }
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
        SubcategoriesOperations subcategoriesOperations = new SubcategoriesOperations();
        subcategoriesOperations.getAllByCategoryId(notEditedProduct.getCategory().getId(), new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = result;
                List<String> subcategoryNames = getFieldList(subcategories, "name");
                ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryNames);
                subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subcategorySpinner.setAdapter(subcategoryAdapter);
                // Select the subcategory in the spinner
                if (productToEdit.getSubcategory() != null) {
                    int subcategoryPosition = getSubcategoryPositionByName(productToEdit.getSubcategory().getName());
                    if (subcategoryPosition != -1) {
                        subcategorySpinner.setSelection(subcategoryPosition);
                    }
                }
            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                eventTypes = result;
                List<String> eventTypeNames = getFieldList(eventTypes, "typeName");
                ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeNames);
                eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventTypeSpinner.setAdapter(eventTypeAdapter);

            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        categorySpinner = root.findViewById(R.id.spinner_category);
        subcategorySpinner = root.findViewById(R.id.spinner_subcategory);
        eventTypeSpinner = root.findViewById(R.id.spinner_event_type);

        nameEditText = binding.editTextName;
        descriptionEditText = binding.editTextDescription;
        priceEditText = binding.editTextPrice;
        discountEditText = binding.editTextDiscount;
        availableCheckBox = binding.checkboxAvailable;
        visibleCheckBox = binding.checkboxVisible;

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    EventType eventType = eventTypes.get(position);

                    // Add the selected event type to the list of selected event types
                    selectedEventTypes.add(eventType);

                    // Update the RecyclerView to display the list of selected event types
                    updateSelectedEventsRecyclerView();

                    // Remove the selected event type from the Spinner's list of available event types
                    eventTypes.remove(eventType);
                    updateEventsSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no event type is selected
            }

        });
    }

    private List<String> getFieldList(List<?> list, String fieldName) {
        List<String> fieldList = new ArrayList<>();
        try {
            for (Object obj : list) {
                Field field = obj.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);
                String value = (String) field.get(obj);
                fieldList.add(value);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldList;
    }

    private void setupListeners() {
        setupImagePicker();
        setupRegisterButton();
    }

    private void setupImagePicker() {
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                this::handleImagePickerResult);

        binding.btnUploadPicture.setOnClickListener(v -> mGetContent.launch("image/*"));
    }

    private void handleImagePickerResult(Uri result) {
        if (result != null) {
            binding.productPictureView.setImageURI(result);
        }
    }

    private void setupRegisterButton() {
        binding.btnSaveProduct.setOnClickListener(v -> {
            if (validateFields()) {
                addNewProduct();
            }
        });
    }

    private boolean validateFields() {
        EditText[] fields = {nameEditText, descriptionEditText, priceEditText, discountEditText};
        Spinner[] spinners = {categorySpinner, subcategorySpinner, eventTypeSpinner};

        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText().toString().trim())) {
                showToast("Please fill in all fields");
                return false;
            }
        }

        return true;
    }



    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void addNewProduct() {
        updateProduct();
        notEditedProduct.setChanged(true);
        productsOperations.update(notEditedProduct, new ProductsOperations.UpdateProductListener() {
            @Override
            public void onSuccess() {
                productToEdit.setChanged(false);
                productsOperations.save(productToEdit, new ProductsOperations.ProductSaveListener() {
                    @Override
                    public void onSuccess(String productId) {
                        Toast.makeText(getContext(), "Product: " + productToEdit.getName() + " is edited." , Toast.LENGTH_SHORT).show();
                        Navigation.findNavController(requireView()).navigateUp();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Toast.makeText(getContext(), "Product: " + productToEdit.getName() + " is not edited." , Toast.LENGTH_SHORT).show();

                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Product: " + productToEdit.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void updateProduct() {
        if (productToEdit == null)
            return;

        Product editedProduct = createProductFromInput();
        if (editedProduct != null) {
            // Update fields of the existing product
            productToEdit.setName(editedProduct.getName());
            productToEdit.setDescription(editedProduct.getDescription());
            productToEdit.setPrice(editedProduct.getPrice());
            productToEdit.setDiscount(editedProduct.getDiscount());
            productToEdit.setCategory(editedProduct.getCategory());
            productToEdit.setSubcategory(editedProduct.getSubcategory());
            productToEdit.setEvents(editedProduct.getEvents());
            productToEdit.setAvailable(editedProduct.isAvailable());
            productToEdit.setVisible(editedProduct.isVisible());
            productToEdit.setChanged(false);

        }
    }


    private Product createProductFromInput() {
        Product newProduct = new Product();
        newProduct.setId(productToEdit.getId());
        newProduct.setName(getTextFromEditText(nameEditText));
        newProduct.setDescription(getTextFromEditText(descriptionEditText));
        newProduct.setCompanyId(productToEdit.getCompanyId());

        String selectedCategoryName = categorySpinner.getSelectedItem().toString();
        Category selectedCategory = findCategoryByName(selectedCategoryName);
        newProduct.setCategory(selectedCategory);

        String selectedSubcategoryName = subcategorySpinner.getSelectedItem().toString();
        Subcategory selectedSubcategory = findSubcategoryByName(selectedSubcategoryName);
        newProduct.setSubcategory(selectedSubcategory);
        newProduct.setEvents(selectedEventTypes);

        String priceText = getTextFromEditText(priceEditText);
        double price = Double.parseDouble(priceText);
        newProduct.setPrice(price);

        String discountText = getTextFromEditText(discountEditText);
        int discount =  Integer.parseInt(discountText);
        newProduct.setDiscount(discount);
        newProduct.setVisible(visibleCheckBox.isChecked());
        newProduct.setAvailable(availableCheckBox.isChecked());
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.ic_product);
        newProduct.setImages(images);
        newProduct.setDeleted(false);

        return newProduct;
    }


    private Category findCategoryByName(String categoryName) {
        for (Category category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }
        return null; // Return null if not found
    }

    private Subcategory findSubcategoryByName(String subcategoryName) {
        for (Subcategory subcategory : subcategories) {
            if (subcategory.getName().equals(subcategoryName)) {
                return subcategory;
            }
        }
        return null; // Return null if not found
    }

    private EventType findEventTypeByName(String eventTypeName) {
        for (EventType eventType : eventTypes) {
            if (eventType.getTypeName().equals(eventTypeName)) {
                return eventType;
            }
        }
        return null; // Return null if not found
    }


    private String getTextFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}