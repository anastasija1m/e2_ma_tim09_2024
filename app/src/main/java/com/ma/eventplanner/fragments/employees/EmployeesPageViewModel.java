package com.ma.eventplanner.fragments.employees;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EmployeesPageViewModel extends ViewModel {

    private final MutableLiveData<String> searchText;

    public EmployeesPageViewModel() {
        searchText = new MutableLiveData<>();
        searchText.setValue("");
    }

    public LiveData<String> getText() {
        return searchText;
    }

    public void setSearchText(String query) {
        searchText.setValue(query);
    }
}