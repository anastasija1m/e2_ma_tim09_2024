package com.ma.eventplanner.fragments.pricelist;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class PricelistBundleFragment extends Fragment {

    private List<com.ma.eventplanner.model.Bundle> bundleList = new ArrayList<>();
    private BundlesOperations bundlesOperations;
    private RecyclerViewAdapter adapter;

    public PricelistBundleFragment() { }

    public PricelistBundleFragment(List<com.ma.eventplanner.model.Bundle> bundles) {
        this.bundleList = bundles;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pricelist_bundle, container, false);
        RecyclerView recyclerView = rootView.findViewById(R.id.recyclerView);

        bundlesOperations = new BundlesOperations();
        adapter = new RecyclerViewAdapter(bundleList);

        // Change the layout manager to LinearLayoutManager with vertical orientation
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(adapter);

        return rootView;
    }

    // RecyclerViewAdapter class to bind data to RecyclerView
    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

        private List<com.ma.eventplanner.model.Bundle> bundleList;

        RecyclerViewAdapter(List<com.ma.eventplanner.model.Bundle> bundleList) {
            this.bundleList = bundleList;
        }

        @NonNull
        @Override
        public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            com.ma.eventplanner.model.Bundle bundle = bundleList.get(position);
            holder.bind(bundle);
        }

        @Override
        public int getItemCount() {
            return bundleList.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView textViewSerial, textViewName, textViewPrice, textViewDiscount, textViewNewPrice;

            ViewHolder(@NonNull View itemView) {
                super(itemView);
                textViewSerial = itemView.findViewById(R.id.textViewSerial);
                textViewName = itemView.findViewById(R.id.textViewName);
                textViewPrice = itemView.findViewById(R.id.textViewPrice);
                textViewDiscount = itemView.findViewById(R.id.textViewDiscount);
                textViewNewPrice = itemView.findViewById(R.id.textViewNewPrice);

                // Set click listener
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                // Get the selected bundle
                com.ma.eventplanner.model.Bundle selectedBundle = bundleList.get(getAdapterPosition());

                NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
                Bundle bundle = new Bundle();
                bundle.putParcelable("bundle", selectedBundle);
                navController.navigate(R.id.nav_edit_bundle, bundle);
            }

            void bind(com.ma.eventplanner.model.Bundle bundle) {
                textViewSerial.setText(String.valueOf(getAdapterPosition() + 1)); // Serial number
                textViewName.setText(bundle.getName());
                textViewPrice.setText(String.valueOf(bundle.getPrice()));
                textViewDiscount.setText(String.valueOf(bundle.getDiscount()));
                textViewNewPrice.setText(String.valueOf(getNewPrice(bundle)));
            }

            private double getNewPrice(com.ma.eventplanner.model.Bundle bundle) {
                double newPrice = bundle.getPrice() * (1 - (double) bundle.getDiscount() / 100);
                DecimalFormat df = new DecimalFormat("#.##");
                return Double.parseDouble(df.format(newPrice));
            }
        }
    }
}
