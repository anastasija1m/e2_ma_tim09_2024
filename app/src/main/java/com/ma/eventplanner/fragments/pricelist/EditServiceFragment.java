package com.ma.eventplanner.fragments.pricelist;

import static com.ma.eventplanner.database.BundlesOperations.*;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.model.Service;

import java.util.ArrayList;
import java.util.List;

public class EditServiceFragment extends Fragment {

    private static Service service;
    private Service editedService, notEditedService;
    private ServicesOperations servicesOperations;
    private BundlesOperations bundlesOperations;
    private TextView textViewName;
    private EditText editTextPrice;
    private EditText editTextDiscount;
    private Button btnSave;

    public EditServiceFragment() {
        // Required empty public constructor
    }

    public static EditServiceFragment newInstance(Service service) {
        EditServiceFragment fragment = new EditServiceFragment();
        Bundle args = new Bundle();
        args.putParcelable("service", service);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        servicesOperations = new ServicesOperations();
        bundlesOperations = new BundlesOperations();
        return inflater.inflate(R.layout.fragment_edit_service, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Initialize views
        textViewName = view.findViewById(R.id.textViewName);
        editTextPrice = view.findViewById(R.id.editTextPrice);
        editTextDiscount = view.findViewById(R.id.editTextDiscount);
        btnSave = view.findViewById(R.id.btn_save);

        // Set onClickListener for the Save button
        btnSave.setOnClickListener(v -> saveChanges());

        // Check if service data is passed as argument
        if (getArguments() != null && getArguments().containsKey("service")) {
            service = getArguments().getParcelable("service");
            notEditedService = getArguments().getParcelable("service");
            editedService = getArguments().getParcelable("service");
            // Set initial values
            textViewName.setText(service.getName());
            editTextPrice.setText(String.valueOf(service.getPrice()));
            editTextDiscount.setText(String.valueOf(service.getDiscount()));
        }
    }

    private void saveChanges() {
        notEditedService.setChanged(true);
        servicesOperations.update(notEditedService, new ServicesOperations.UpdateServiceListener() {
            @Override
            public void onSuccess() {
                saveChangedService();
                editedService.setChanged(false);
                servicesOperations.save(editedService, new ServicesOperations.ServiceSaveListener() {
                    @Override
                    public void onSuccess(String serviceId) {
                        //updateBundle();
                        Toast.makeText(getContext(), "Service: " + editedService.getName() + " is edited." , Toast.LENGTH_SHORT).show();
                        Navigation.findNavController(requireView()).navigateUp();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        updateBundle();
                        Toast.makeText(getContext(), "Service: " + editedService.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Service: " + editedService.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateBundle() {
        bundlesOperations.getAllBundles(new BundlesOperations.GetAllBundlesListener<com.ma.eventplanner.model.Bundle>() {
            @Override
            public void onSuccess(ArrayList<com.ma.eventplanner.model.Bundle> result) {
                for (com.ma.eventplanner.model.Bundle bundle : result) {
                    for (Service service1 : bundle.getServices()) {
                        List<Service> newList = bundle.getServices();
                        if (service1.getId().equals(service.getId())) {
                            newList.remove(service);
                            newList.add(editedService);
                            bundle.setChanged(true);
                            bundlesOperations.update(bundle, new BundlesOperations.UpdateBundleListener() {
                                @Override
                                public void onSuccess() {
                                    bundle.setChanged(false);
                                    bundle.setServices(newList);
                                    bundlesOperations.save(bundle, new BundlesOperations.BundleSaveListener() {
                                        @Override
                                        public void onSuccess(String bundleId) {
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Toast.makeText(getContext(), "Service: " + editedService.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    Toast.makeText(getContext(), "Service: " + editedService.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
                                }
                            });

                            break;
                        }
                    }
                }
                Toast.makeText(getContext(), "Service: " + editedService.getName() + " is edited." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }

            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    private void saveChangedService(){
        double newPrice = Double.parseDouble(editTextPrice.getText().toString());
        int newDiscount = Integer.parseInt(editTextDiscount.getText().toString());
        service.setPrice(newPrice);
        service.setDiscount(newDiscount);
        updateService();
    }

    private void updateService() {
        if (service == null)
            return;

        Service editedService = new Service();
        editedService.setName(service.getName());
        editedService.setDescription(service.getDescription());
        editedService.setPrice(service.getPrice());
        editedService.setDiscount(service.getDiscount());
        editedService.setCategory(service.getCategory());
        editedService.setSubcategory(service.getSubcategory());
        editedService.setEvents(service.getEvents());
        editedService.setAvailable(service.isAvailable());
        editedService.setVisible(service.isVisible());
        editedService.setCompanyId(service.getCompanyId());
        editedService.setImages(service.getImages());
        editedService.setDeleted(false);
        editedService.setChanged(false);
    }
}
