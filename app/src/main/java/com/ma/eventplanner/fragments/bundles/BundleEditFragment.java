package com.ma.eventplanner.fragments.bundles;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.BundlesOperations;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentBundleEditBinding;
import com.ma.eventplanner.databinding.FragmentNewBundleBinding;
import com.ma.eventplanner.databinding.FragmentNewServiceBinding;
import com.ma.eventplanner.fragments.services.ServiceEditFragment;
import com.ma.eventplanner.model.AcceptanceType;
import com.ma.eventplanner.model.Address;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Item;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;
import com.ma.eventplanner.model.Bundle;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class BundleEditFragment extends Fragment {

    private FragmentBundleEditBinding binding;
    private EditText nameEditText, descriptionEditText, priceEditText, discountEditText, reservationDeadlineEditText, cancellationDeadlineEditText;
    private Spinner categorySpinner, productsSpinner, servicesSpinner, acceptanceCriteriaSpinner;
    private CheckBox availableCheckBox, visibleCheckBox;
    private RecyclerView selectedEmployeesRecyclerView, selectedEventsRecyclerView, selectedProductsRecyclerView, selectedServicesRecyclerView;
    private List<Category> categories;
    private List<Subcategory> subcategories;
    private List<EventType> eventTypes;
    private List<Employee> employees;
    private List<Product> products;
    private List<Service> services;
    private List<AcceptanceType> acceptanceCriteria;
    private List<Employee> selectedEmployees = new ArrayList<>();
    private List<Subcategory> selectedSubcategories = new ArrayList<>();
    private List<Product> selectedProducts = new ArrayList<>();
    private List<Service> selectedServices = new ArrayList<>();
    private Bundle bundleToEdit, notEditedBundle;
    private List<EventType> selectedEventTypes = new ArrayList<>();
    private BundlesOperations bundlesOperations;
    private String loggedUserId;
    private String loggedUserCompanyId;
    private UsersOperations usersOperations;

    public BundleEditFragment() {
    }

    public static BundleEditFragment newInstance(Bundle bundle) {
        BundleEditFragment fragment = new BundleEditFragment();
        android.os.Bundle args = new android.os.Bundle();
        args.putParcelable("bundle", bundle);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, android.os.Bundle savedInstanceState) {
        binding = FragmentBundleEditBinding.inflate(inflater, container, false);
        View root = binding.getRoot();
        bundlesOperations = new BundlesOperations();
        usersOperations = new UsersOperations();
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");
        usersOperations.getEmployeeById(loggedUserId, new UsersOperations.OnGetEmployeeByIdListener() {
            @Override
            public void onSuccess(Employee employee) {
                loggedUserCompanyId = employee.getCompanyId();
            }

            @Override
            public void onFailure(Exception e) {
                usersOperations.getOwnerById(loggedUserId, new UsersOperations.OnGetOwnerByIdListener() {
                    @Override
                    public void onSuccess(Owner owner) {
                        loggedUserCompanyId = owner.getCompanyId();
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
            }
        });

        setupListeners();
        setupProductsRecyclerView(root);
        setupServicesRecyclerView(root);

        // Check if product data is passed as argument
        if (getArguments() != null && getArguments().containsKey("bundle")) {
            bundleToEdit = getArguments().getParcelable("bundle");
            notEditedBundle = getArguments().getParcelable("bundle");
            if (bundleToEdit != null) {
                initializeViews(root);
                fillFieldsWithBundleData();

                categorySpinner.setEnabled(false);
            }
        }

        return root;
    }

    private void fillFieldsWithBundleData() {
        // Fill common fields
        nameEditText.setText(bundleToEdit.getName());
        descriptionEditText.setText(bundleToEdit.getDescription());
        priceEditText.setText(String.valueOf(bundleToEdit.getPrice()));
        discountEditText.setText(String.valueOf(bundleToEdit.getDiscount()));

        availableCheckBox.setChecked(bundleToEdit.isAvailable());
        visibleCheckBox.setChecked(bundleToEdit.isVisible());

        // Set reservation deadline if not null
        if (bundleToEdit.getReservationDeadline() != 0) {
            reservationDeadlineEditText.setText(String.valueOf(bundleToEdit.getReservationDeadline()));
        }

        // Set cancellation deadline if not null
        if (bundleToEdit.getCancellationDeadline() != 0) {
            cancellationDeadlineEditText.setText(String.valueOf(bundleToEdit.getCancellationDeadline()));
        }

        selectedServices.addAll(bundleToEdit.getServices());
        updateSelectedServicesRecyclerView();

        selectedProducts.addAll(bundleToEdit.getProducts());
        updateSelectedProductsRecyclerView();

    }

    private void updateSelectedEventsRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEventAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                EventType eventType = selectedEventTypes.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventType.getTypeName());
            }

            @Override
            public int getItemCount() {
                return selectedEventTypes.size();
            }
        };

        // Set the adapter to the RecyclerView
        selectedEventsRecyclerView.setAdapter(selectedEventAdapter);

    }


    private int getCategoryPositionByName(String categoryName) {
        for (int i = 0; i < categories.size(); i++) {
            if (categories.get(i).getName().equals(categoryName)) {
                return i;
            }
        }
        return -1;
    }

    private int getEventTypePositionByName(String eventTypeName) {
        for (int i = 0; i < eventTypes.size(); i++) {
            if (eventTypes.get(i).getTypeName().equals(eventTypeName)) {
                return i;
            }
        }
        return -1;
    }


    private void initializeViews(View root) {
        // Assuming you have predefined lists of categories, subcategories, and event types
        CategoriesOperations categoriesOperations = new CategoriesOperations();
        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categories = result;
                categories.add(0, new Category("-1L", "Select Category", "", ""));
                List<String> categoryNames = getFieldList(categories, "name");
                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categoryNames);
                categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categorySpinner.setAdapter(categoryAdapter);
                // Select the category in the spinner
                if (bundleToEdit.getCategory() != null) {
                    int categoryPosition = getCategoryPositionByName(bundleToEdit.getCategory().getName());
                    if (categoryPosition != -1) {
                        categorySpinner.setSelection(categoryPosition);
                    }
                }

            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        ProductsOperations productsOperations = new ProductsOperations();
        productsOperations.getAllByCategoryId(loggedUserCompanyId, bundleToEdit.getCategory(), new ProductsOperations.GetAllProductsListener<Product>() {
            @Override
            public void onSuccess(ArrayList<Product> result) {
                products = result;
                products.add(0, new Product("1L", "1L", null, "Select Product", "", 0.0, 0, new ArrayList<>(), null, false, false, false, null));
                List<String> productNames = extractProductNames(products);
                ArrayAdapter<String> productsAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, productNames);
                productsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                productsSpinner.setAdapter(productsAdapter);
            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        ServicesOperations servicesOperations = new ServicesOperations();
        servicesOperations.getAllByCategoryId(loggedUserCompanyId, bundleToEdit.getCategory(), new ServicesOperations.GetAllServicesListener<Service>() {
            @Override
            public void onSuccess(ArrayList<Service> result) {
                services = result;
                services.add(0, new Service("1L", "1L", null, "Select Service", "", 0.0, 0, new ArrayList<>(), null, false, false, false, null, "", new ArrayList<>(), 0, 0, 0, null));
                List<String> serviceNames = extractServiceNames(services);
                ArrayAdapter<String> servicesAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, serviceNames);
                servicesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                servicesSpinner.setAdapter(servicesAdapter);
            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        acceptanceCriteria = getAcceptanceCriteria();

        categorySpinner = root.findViewById(R.id.spinner_category);
        productsSpinner = root.findViewById(R.id.spinner_products);
        servicesSpinner = root.findViewById(R.id.spinner_services);
        acceptanceCriteriaSpinner = root.findViewById(R.id.spinner_acceptance_type);

        selectedProductsRecyclerView = root.findViewById(R.id.recycler_selected_products);
        selectedProductsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        selectedServicesRecyclerView = root.findViewById(R.id.recycler_selected_services);
        selectedServicesRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        List<String> acceptanceCriteriaNames = new ArrayList<>();
        for (AcceptanceType type : acceptanceCriteria) {
            acceptanceCriteriaNames.add(type.toString());
        }

        // Create adapters for spinners
        ArrayAdapter<String> acceptanceCriteriaAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, acceptanceCriteriaNames);

        acceptanceCriteriaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        acceptanceCriteriaSpinner.setAdapter(acceptanceCriteriaAdapter);

        nameEditText = binding.editTextName;
        descriptionEditText = binding.editTextDescription;
        priceEditText = binding.editTextPrice;
        discountEditText = binding.editTextDiscount;
        reservationDeadlineEditText = binding.editTextReservationDeadline;
        cancellationDeadlineEditText = binding.editTextCancellationDeadline;
        availableCheckBox = binding.checkboxAvailable;
        visibleCheckBox = binding.checkboxVisible;


        productsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    Product selectedProduct = products.get(position);

                    // Add the selected employee to the list
                    selectedProducts.add(selectedProduct);

                    // Update the RecyclerView to display the list of selected employees
                    updateSelectedProductsRecyclerView();

                    // Remove the selected employee from the Spinner's list of available employees
                    products.remove(selectedProduct);
                    updateProductsSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no employee is selected
            }
        });

        servicesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    Service selectedService = services.get(position);

                    // Add the selected employee to the list
                    selectedServices.add(selectedService);

                    // Update the RecyclerView to display the list of selected employees
                    updateSelectedServicesRecyclerView();

                    // Remove the selected employee from the Spinner's list of available employees
                    services.remove(selectedService);
                    updateServicesSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no employee is selected
            }
        });

    }

    public  List<String> extractProductNames(List<Product> products) {
        List<String> productNames = new ArrayList<>();
        for (Product product : products) {
            productNames.add(product.getName());
        }
        return productNames;
    }

    public  List<String> extractServiceNames(List<Service> products) {
        List<String> productNames = new ArrayList<>();
        for (Service product : products) {
            productNames.add(product.getName());
        }
        return productNames;
    }

    private void setupProductsRecyclerView(View root) {
        selectedProductsRecyclerView = root.findViewById(R.id.recycler_selected_products);
        selectedProductsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedProductsAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Product product = selectedProducts.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(product.getName());
            }

            @Override
            public int getItemCount() {
                return selectedProducts.size();
            }
        };

        selectedProductsRecyclerView.setAdapter(selectedProductsAdapter);
    }

    private void updateSelectedProductsRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedProductsAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Product product = selectedProducts.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(product.getName());
            }

            @Override
            public int getItemCount() {
                return selectedProducts.size();
            }
        };

        selectedProductsRecyclerView.setAdapter(selectedProductsAdapter);
    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateProductsSpinnerAdapter() {
        // Create a new list of strings to store employee names and surnames
        List<String> productsNames = getNameFieldList(products,"name");

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> productAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, productsNames);

        // Set the layout resource for the adapter
        productAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        productsSpinner.setAdapter(productAdapter);
    }
    private void setupServicesRecyclerView(View root) {
        selectedServicesRecyclerView = root.findViewById(R.id.recycler_selected_services);
        selectedServicesRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedServicesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Service service = selectedServices.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(service.getName());
            }

            @Override
            public int getItemCount() {
                return selectedEmployees.size();
            }
        };

        selectedServicesRecyclerView.setAdapter(selectedServicesAdapter);
    }

    private void updateSelectedServicesRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedServicesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                Service service = selectedServices.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(service.getName());
            }

            @Override
            public int getItemCount() {
                return selectedServices.size();
            }
        };

        selectedServicesRecyclerView.setAdapter(selectedServicesAdapter);
    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateServicesSpinnerAdapter() {
        // Create a new list of strings to store employee names and surnames
        List<String> serviceNames = getNameFieldList(services, "name");

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> serviceAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, serviceNames);

        // Set the layout resource for the adapter
        serviceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        servicesSpinner.setAdapter(serviceAdapter);
    }


    private List<String> getFieldList(List<?> list, String... fieldNames) {
        List<String> fieldList = new ArrayList<>();
        try {
            for (Object obj : list) {
                StringBuilder valueBuilder = new StringBuilder();
                for (String fieldName : fieldNames) {
                    Field field = obj.getClass().getDeclaredField(fieldName);
                    field.setAccessible(true);
                    String value = (String) field.get(obj);
                    valueBuilder.append(value).append(" ");
                }
                fieldList.add(valueBuilder.toString().trim());
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldList;
    }

    private List<String> getNameFieldList(List<?> list, String fieldName) {
        List<String> fieldList = new ArrayList<>();
        try {
            for (Object obj : list) {
                // Get the field from the superclass (Item) if it's not declared in the subclass
                Field field = Item.class.getDeclaredField(fieldName);
                field.setAccessible(true); // Ensure that the field is accessible
                Object value = field.get(obj); // Get the value of the field
                fieldList.add(value.toString());
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldList;
    }


    // Method to populate the list of acceptance criteria
    private List<AcceptanceType> getAcceptanceCriteria() {
        List<AcceptanceType> acceptanceCriteria = new ArrayList<>();

        // Add acceptance criteria options
        acceptanceCriteria.add(AcceptanceType.AUTOMATIC);
        acceptanceCriteria.add(AcceptanceType.MANUAL);

        return acceptanceCriteria;
    }

    private void setupListeners() {
        setupImagePicker();
        setupRegisterButton();
    }

    private void setupImagePicker() {
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                this::handleImagePickerResult);

        binding.btnUploadPicture.setOnClickListener(v -> mGetContent.launch("image/*"));
    }

    private void handleImagePickerResult(Uri result) {
        if (result != null) {
            binding.bundlePictureView.setImageURI(result);
        }
    }

    private void setupRegisterButton() {
        binding.btnSaveBundle.setOnClickListener(v -> {
            if (validateFields()) {
                addNewBundle();
            }
        });
    }

    private boolean validateFields() {
        EditText[] fields = {nameEditText, descriptionEditText, priceEditText, discountEditText, reservationDeadlineEditText, cancellationDeadlineEditText};
        Spinner[] spinners = {categorySpinner, acceptanceCriteriaSpinner};

        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText().toString().trim())) {
                showToast("Please fill in all fields");
                return false;
            }
        }

        for (Spinner spinner : spinners) {
            if (spinner.getSelectedItem() == null) {
                showToast("Please select a value from all spinners");
                return false;
            }
        }

        return true;
    }



    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void addNewBundle() {

        updateBundle();
        notEditedBundle.setChanged(true);
        bundlesOperations.update(notEditedBundle, new BundlesOperations.UpdateBundleListener() {
            @Override
            public void onSuccess() {
                bundleToEdit.setChanged(false);
                bundlesOperations.save(bundleToEdit, new BundlesOperations.BundleSaveListener() {
                    @Override
                    public void onSuccess(String productId) {
                        Toast.makeText(getContext(), "Bundle: " + bundleToEdit.getName() + " is edited." , Toast.LENGTH_SHORT).show();
                        Navigation.findNavController(requireView()).navigateUp();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Toast.makeText(getContext(), "Bundle: " + bundleToEdit.getName() + " is not edited." , Toast.LENGTH_SHORT).show();

                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Bundle: " + bundleToEdit.getName() + " is not edited." , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateBundle() {
        if (bundleToEdit == null)
            return;

        Bundle editedBundle = createBundleFromInput();
        if (editedBundle != null) {
            // Update fields of the existing product
            bundleToEdit.setName(editedBundle.getName());
            bundleToEdit.setDescription(editedBundle.getDescription());
            bundleToEdit.setPrice(editedBundle.getPrice());
            bundleToEdit.setDiscount(editedBundle.getDiscount());
            bundleToEdit.setCategory(editedBundle.getCategory());
            bundleToEdit.setSubcategories(editedBundle.getSubcategories());
            bundleToEdit.setProducts(editedBundle.getProducts());
            bundleToEdit.setServices(editedBundle.getServices());
            bundleToEdit.setEvents(editedBundle.getEvents());
            bundleToEdit.setEmployees(editedBundle.getEmployees());
            bundleToEdit.setReservationDeadline(editedBundle.getReservationDeadline());
            bundleToEdit.setCancellationDeadline(editedBundle.getCancellationDeadline());
            bundleToEdit.setAcceptanceType(editedBundle.getAcceptanceType());
            bundleToEdit.setAvailable(editedBundle.isAvailable());
            bundleToEdit.setVisible(editedBundle.isVisible());

        }
    }

    private Bundle createBundleFromInput() {
        Bundle newBundle = new Bundle();
        newBundle.setId(String.valueOf(System.currentTimeMillis()));
        newBundle.setName(getTextFromEditText(nameEditText));
        newBundle.setDescription(getTextFromEditText(descriptionEditText));

        String selectedCategoryName = categorySpinner.getSelectedItem().toString();
        Category selectedCategory = findCategoryByName(selectedCategoryName);
        newBundle.setCategory(selectedCategory);

        newBundle.setSubcategories(selectedSubcategories);

        newBundle.setEvents(selectedEventTypes);

        String priceText = getTextFromEditText(priceEditText);
        double price = Double.parseDouble(priceText);
        newBundle.setPrice(price);

        String discountText = getTextFromEditText(discountEditText);
        int discount = Integer.parseInt(discountText);
        newBundle.setDiscount(discount);

        // Retrieve additional fields
        String reservationDeadlineText = getTextFromEditText(reservationDeadlineEditText);
        int reservationDeadline = Integer.parseInt(reservationDeadlineText);
        newBundle.setReservationDeadline(reservationDeadline);

        String cancellationDeadlineText = getTextFromEditText(cancellationDeadlineEditText);
        int cancellationDeadline = Integer.parseInt(cancellationDeadlineText);
        newBundle.setCancellationDeadline(cancellationDeadline);

        newBundle.setEmployees(selectedEmployees);
        newBundle.setProducts(selectedProducts);
        newBundle.setServices(selectedServices);

        // Handle acceptance criteria selection if applicable
        AcceptanceType selectedAcceptanceType = (acceptanceCriteriaSpinner.getSelectedItem() instanceof AcceptanceType) ? (AcceptanceType) acceptanceCriteriaSpinner.getSelectedItem() : null;
        newBundle.setAcceptanceType(selectedAcceptanceType);

        newBundle.setVisible(visibleCheckBox.isChecked());
        newBundle.setAvailable(availableCheckBox.isChecked());
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.ic_bundle);
        newBundle.setImages(images);
        newBundle.setDeleted(false);

        return newBundle;
    }



    private Category findCategoryByName(String categoryName) {
        for (Category category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }
        return null; // Return null if not found
    }

    private Subcategory findSubcategoryByName(String subcategoryName) {
        for (Subcategory subcategory : subcategories) {
            if (subcategory.getName().equals(subcategoryName)) {
                return subcategory;
            }
        }
        return null; // Return null if not found
    }

    private EventType findEventTypeByName(String eventTypeName) {
        for (EventType eventType : eventTypes) {
            if (eventType.getTypeName().equals(eventTypeName)) {
                return eventType;
            }
        }
        return null; // Return null if not found
    }


    private String getTextFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}