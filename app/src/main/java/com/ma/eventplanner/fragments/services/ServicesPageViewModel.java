package com.ma.eventplanner.fragments.services;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ServicesPageViewModel extends ViewModel {

    private final MutableLiveData<String> searchText;

    public ServicesPageViewModel() {
        searchText = new MutableLiveData<>();
        searchText.setValue("");
    }

    public LiveData<String> getText() {
        return searchText;
    }

    public void setSearchText(String query) {
        searchText.setValue(query);
    }
}