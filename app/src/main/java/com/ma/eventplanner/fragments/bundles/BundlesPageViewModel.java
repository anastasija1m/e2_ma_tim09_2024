package com.ma.eventplanner.fragments.bundles;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BundlesPageViewModel extends ViewModel {

    private final MutableLiveData<String> searchText;

    public BundlesPageViewModel() {
        searchText = new MutableLiveData<>();
        searchText.setValue("");
    }

    public LiveData<String> getText() {
        return searchText;
    }

    public void setSearchText(String query) {
        searchText.setValue(query);
    }
}