package com.ma.eventplanner.fragments.products;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.database.ProductRequestsOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.databinding.FragmentNewProductBinding;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.ProductRequest;
import com.ma.eventplanner.model.SubcategoryType;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NewProductFragment extends Fragment {

    private FragmentNewProductBinding binding;
    private EditText nameEditText, descriptionEditText, priceEditText, discountEditText, requestSubcategoryEditText;
    private Spinner categorySpinner, subcategorySpinner, eventTypeSpinner;
    private CheckBox availableCheckBox, visibleCheckBox;
    private List<Category> categories;
    private List<Subcategory> subcategories;
    private List<EventType> eventTypes;
    private List<EventType> selectedEventTypes = new ArrayList<>();
    private RecyclerView  selectedEventsRecyclerView;
    private ProductsOperations productsOperations;
    private EventTypesOperations eventTypesOperations;
    String loggedUserId;


    public NewProductFragment() {
    }

    public static NewProductFragment newInstance() {
        return new NewProductFragment();
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentNewProductBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        productsOperations = new ProductsOperations();
        eventTypesOperations = new EventTypesOperations();

        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");


        initializeViews(root);
        setupListeners();
        setupEventRecyclerView(root);

        return root;
    }

    private void initializeViews(View root) {
        // Assuming you have predefined lists of categories, subcategories, and event types
        CategoriesOperations categoriesOperations = new CategoriesOperations();
        categoriesOperations.getAll(new CategoriesOperations.GetAllCategoriesListener<Category>() {
            @Override
            public void onSuccess(List<Category> result) {
                categories = result;
                categories.add(0, new Category("-1L", "Select Category", "", ""));
                List<String> categoryNames = getFieldList(categories, "name");
                ArrayAdapter<String> categoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, categoryNames);
                categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                categorySpinner.setAdapter(categoryAdapter);

            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        eventTypesOperations.getAll(new EventTypesOperations.GetAllEventTypesListener<EventType>() {
            @Override
            public void onSuccess(List<EventType> result) {
                eventTypes = result;
                eventTypes.add(0, new EventType("-1L", "Select Event Type", "", new ArrayList<>(), false));
                List<String> eventTypeNames = getFieldList(eventTypes, "typeName");
                ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeNames);
                eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                eventTypeSpinner.setAdapter(eventTypeAdapter);

            }
            @Override
            public void onFailure(Exception e) {

            }
        });

        categorySpinner = root.findViewById(R.id.spinner_category);
        subcategorySpinner = root.findViewById(R.id.spinner_subcategory);
        eventTypeSpinner = root.findViewById(R.id.spinner_event_type);

        selectedEventsRecyclerView = root.findViewById(R.id.recycler_selected_events);
        selectedEventsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        nameEditText = binding.editTextName;
        requestSubcategoryEditText = binding.requestSubcategory;
        descriptionEditText = binding.editTextDescription;
        priceEditText = binding.editTextPrice;
        discountEditText = binding.editTextDiscount;
        availableCheckBox = binding.checkboxAvailable;
        visibleCheckBox = binding.checkboxVisible;

        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid category is selected (excluding the placeholder)
                if (position > 0) {
                    String selectedCategoryName = categorySpinner.getSelectedItem().toString();
                    Category selectedCategory = findCategoryByName(selectedCategoryName);
                    getSubcategories(selectedCategory);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no category is selected
            }
        });

        eventTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // Check if a valid position is selected (excluding the placeholder)
                if (position > 0) {
                    EventType eventType = eventTypes.get(position);

                    // Add the selected employee to the list
                    selectedEventTypes.add(eventType);

                    // Update the RecyclerView to display the list of selected employees
                    updateSelectedEventsRecyclerView();

                    // Remove the selected employee from the Spinner's list of available employees
                    eventTypes.remove(eventType);
                    updateEventsSpinnerAdapter();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Handle the case where no employee is selected
            }
        });
    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateSelectedEventsRecyclerView() {
        // Create an adapter for the RecyclerView
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEventAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                EventType eventType = selectedEventTypes.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventType.getTypeName());
            }

            @Override
            public int getItemCount() {
                return selectedEventTypes.size();
            }
        };

        // Set the adapter to the RecyclerView
        selectedEventsRecyclerView.setAdapter(selectedEventAdapter);

    }


    // Method to update the Spinner's adapter after removing the selected employee
    private void updateEventsSpinnerAdapter() {

        List<String> eventTypeNames = new ArrayList<>();
        for (EventType event : eventTypes) {
            // Concatenate first name and last name
            String fullName = event.getTypeName();
            eventTypeNames.add(fullName);
        }
        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> eventTypeAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, eventTypeNames);

        // Set the layout resource for the adapter
        eventTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        eventTypeSpinner.setAdapter(eventTypeAdapter);
    }

    private void updateSubcategorySpinner(List<Subcategory> subcategories) {
        subcategories.add(0, new Subcategory("-1L", "Select Category", "", SubcategoryType.PRODUCT,""));

        // Extract names from the list of subcategories
        List<String> subcategoryNames = getFieldList(subcategories, "name");

        // Create a new adapter for the Spinner using the filtered list
        ArrayAdapter<String> subcategoryAdapter = new ArrayAdapter<>(requireContext(), android.R.layout.simple_spinner_item, subcategoryNames);

        // Set the layout resource for the adapter
        subcategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Set the adapter for the Spinner
        subcategorySpinner.setAdapter(subcategoryAdapter);
    }


    private List<String> getFieldList(List<?> list, String fieldName) {
        List<String> fieldList = new ArrayList<>();
        try {
            for (Object obj : list) {
                Field field = obj.getClass().getDeclaredField(fieldName);
                field.setAccessible(true);
                String value = (String) field.get(obj);
                fieldList.add(value);
            }
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return fieldList;
    }


    private void setupEventRecyclerView(View root) {
        selectedEventsRecyclerView = root.findViewById(R.id.recycler_selected_events);
        selectedEventsRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        // Create and set up RecyclerView adapter for selected employees
        RecyclerView.Adapter<RecyclerView.ViewHolder> selectedEmployeesAdapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                EventType eventType = selectedEventTypes.get(position);
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventType.getTypeName());
            }

            @Override
            public int getItemCount() {
                return selectedEventTypes.size();
            }
        };

        selectedEventsRecyclerView.setAdapter(selectedEmployeesAdapter);

    }

    private void getSubcategories(Category selectedCategory) {

        SubcategoriesOperations subcategoriesOperations = new SubcategoriesOperations();
        subcategoriesOperations.getAllByCategoryId(selectedCategory.getId(), new SubcategoriesOperations.GetAllSubcategoriesListener<Subcategory>() {
            @Override
            public void onSuccess(List<Subcategory> result) {
                subcategories = result;
                updateSubcategorySpinner(result);
            }
            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    private void setupListeners() {
        setupImagePicker();
        setupRegisterButton();
    }

    private void setupImagePicker() {
        ActivityResultLauncher<String> mGetContent = registerForActivityResult(new ActivityResultContracts.GetContent(),
                this::handleImagePickerResult);

        binding.btnUploadPicture.setOnClickListener(v -> mGetContent.launch("image/*"));
    }

    private void handleImagePickerResult(Uri result) {
        if (result != null) {
            binding.productPictureView.setImageURI(result);
        }
    }

    private void setupRegisterButton() {
        binding.btnSaveProduct.setOnClickListener(v -> {
            if (validateFields()) {
                addNewProduct();
            }
        });
    }

    private boolean validateFields() {
        EditText[] fields = {nameEditText, descriptionEditText, priceEditText, discountEditText};
        Spinner[] spinners = {categorySpinner};
        RecyclerView[] recyclerViews = {selectedEventsRecyclerView};
        for (EditText field : fields) {
            if (TextUtils.isEmpty(field.getText().toString().trim())) {
                showToast("Please fill in all fields");
                return false;
            }
        }

        for (Spinner spinner : spinners) {
            if (spinner.getSelectedItem() == null) {
                showToast("Please select a value from all spinners");
                return false;
            }
        }

        // Check if subcategory spinner is not selected and requestSubcategoryEditText is empty
        if (subcategorySpinner.getSelectedItemPosition() == 0 && TextUtils.isEmpty(requestSubcategoryEditText.getText().toString().trim())) {
            showToast("Please select a subcategory or enter a value in the 'Request Subcategory' field");
            return false;
        } else if (subcategorySpinner.getSelectedItemPosition() == 0 && !TextUtils.isEmpty(requestSubcategoryEditText.getText().toString().trim())) {
            // Subcategory spinner is not selected but requestSubcategoryEditText is filled
            makeProductRequest(requestSubcategoryEditText.getText().toString());
            return false; // Return false as the request is made
        }

        for (RecyclerView recyclerView : recyclerViews) {
            if (!Arrays.stream(recyclerViews).findAny().isPresent()) {
                showToast("Please select a value from all spinners");
                return false;
            }
        }

        return true;
    }

    private void makeProductRequest(String requestedSubcategory) {
        // Create a ProductRequest object with the requested subcategory
        ProductRequest productRequest = new ProductRequest();
        Product product = createProductFromInput();
        productRequest.setProduct(product);
        Subcategory request = new Subcategory();
        request.setName(requestedSubcategory);
        request.setType(SubcategoryType.PRODUCT);
        request.setDescription("opis");
        request.setCategoryId(product.getCategory().getId());
        productRequest.setRequestedSubcategory(request);
        productRequest.setOwnerId(loggedUserId);
        productRequest.setStatus(RequestStatus.ON_PENDING);

        ProductRequestsOperations productRequestsOperations = new ProductRequestsOperations();

        productRequestsOperations.save(productRequest, new ProductRequestsOperations.SaveProductRequestListener() {
            @Override
            public void onSuccess(String productRequestId) {
                Log.e("ProductSave", "Product request sent successful: " + productRequestId);

            }

            @Override
            public void onFailure(Exception e) {
                Log.e("ProductSave", "Error sending product request: " + e.getMessage());

            }
        });

        Navigation.findNavController(requireView()).navigateUp();

    }



    private void showToast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }

    private void addNewProduct() {
        Product newProduct = createProductFromInput();

        productsOperations.save(newProduct, new ProductsOperations.ProductSaveListener() {
            @Override
            public void onSuccess(String productId) {
                Log.e("ProductSave", "Product saved successful: " + productId);
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("ProductSave", "Error saving category to database: " + e.getMessage());
            }
        });

        Navigation.findNavController(requireView()).navigateUp();
    }

    private Product createProductFromInput() {
        Product newProduct = new Product();
        newProduct.setName(getTextFromEditText(nameEditText));
        newProduct.setDescription(getTextFromEditText(descriptionEditText));
        // get from user company id
        newProduct.setCompanyId("1L");

        String selectedCategoryName = categorySpinner.getSelectedItem().toString();
        Category selectedCategory = findCategoryByName(selectedCategoryName);
        newProduct.setCategory(selectedCategory);

        String selectedSubcategoryName = subcategorySpinner.getSelectedItem().toString();
        Subcategory selectedSubcategory = findSubcategoryByName(selectedSubcategoryName);
        newProduct.setSubcategory(selectedSubcategory);

        newProduct.setEvents(selectedEventTypes);

        String priceText = getTextFromEditText(priceEditText);
        double price = Double.parseDouble(priceText);
        newProduct.setPrice(price);

        String discountText = getTextFromEditText(discountEditText);
        int discount =  Integer.parseInt(discountText);
        newProduct.setDiscount(discount);
        newProduct.setVisible(visibleCheckBox.isChecked());
        newProduct.setAvailable(availableCheckBox.isChecked());
        List<Integer> images = new ArrayList<>();
        images.add(R.drawable.ic_product);
        newProduct.setImages(images);
        newProduct.setDeleted(false);

        return newProduct;
    }


    private Category findCategoryByName(String categoryName) {
        for (Category category : categories) {
            if (category.getName().equals(categoryName)) {
                return category;
            }
        }
        return null; // Return null if not found
    }

    private Subcategory findSubcategoryByName(String subcategoryName) {
        for (Subcategory subcategory : subcategories) {
            if (subcategory.getName().equals(subcategoryName)) {
                return subcategory;
            }
        }
        return null; // Return null if not found
    }

    private EventType findEventTypeByName(String eventTypeName) {
        for (EventType eventType : eventTypes) {
            if (eventType.getTypeName().equals(eventTypeName)) {
                return eventType;
            }
        }
        return null; // Return null if not found
    }


    private String getTextFromEditText(EditText editText) {
        return editText.getText().toString().trim();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}