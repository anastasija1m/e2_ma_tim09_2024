package com.ma.eventplanner.fragments.services;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkRequest;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.adapters.employee.EmployeeAvailabilityScheduleAdapter;
import com.ma.eventplanner.adapters.employee.EmployeeSpinnerAdapter;
import com.ma.eventplanner.adapters.EventSpinnerAdapter;
import com.ma.eventplanner.database.AuthenticationManager;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.database.NotificationWorker;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.ServiceReservationsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.FragmentServiceDetailsBinding;
import com.ma.eventplanner.model.AvailabilitySlot;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EmployeeEvent;
import com.ma.eventplanner.model.EmployeeWorkHours;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.EventStatus;
import com.ma.eventplanner.model.EventType;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.ServiceReservation;
import com.ma.eventplanner.model.ServiceReservationStatus;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class ServiceDetailsFragment extends Fragment {

    private FragmentServiceDetailsBinding binding;
    private Service service;
    private ServicesOperations servicesOperations;
    private EventsOperations eventsOperations;
    private ServiceReservationsOperations serviceReservationsOperations;
    private UsersOperations usersOperations;
    private NotificationsOperations notificationsOperations;
    private List<Event> events;
    private EventOrganizer loggedInEventOrganizer;
    private AuthenticationManager authenticationManager;

    public ServiceDetailsFragment() { }

    public static ServiceDetailsFragment newInstance() {
        return new ServiceDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        authenticationManager = new AuthenticationManager();
        loggedInEventOrganizer = new EventOrganizer();
        HomeActivity homeActivity = (HomeActivity) requireActivity();
        loggedInEventOrganizer = homeActivity.getLoggedInEventOrganizer();
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        servicesOperations = new ServicesOperations();
        eventsOperations = new EventsOperations();
        serviceReservationsOperations = new ServiceReservationsOperations();
        notificationsOperations = new NotificationsOperations();
        usersOperations = new UsersOperations();
        binding = FragmentServiceDetailsBinding.inflate(inflater, container, false);

        Activity activity = getActivity();

        isEventOrganizer(activity);

        return binding.getRoot();
    }

    private void isEventOrganizer(Activity activity) {
        if (!(activity instanceof HomeActivity)) {
            return;
        }

        HomeActivity homeActivity = (HomeActivity) activity;

        if (homeActivity.getLoggedInUser().getRole() != Role.EVENT_ORGANIZER) {
            binding.linLayReserveService.setVisibility(View.GONE);
        } else {
            binding.deleteServiceButton.setVisibility(View.GONE);
            binding.editServiceButton.setVisibility(View.GONE);
            events = new ArrayList<>();
            fetchEvents();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bindServiceDetails();
        setupReserveButton();
    }

    private void setupReserveButton() {
        Button reserveButton = requireView().findViewById(R.id.reserve_service_button);

        if (!service.isAvailable()) {
            binding.reserveServiceButton.setEnabled(false);
        }

        reserveButton.setOnClickListener(v -> showReserveDialog());
    }

    private void showReserveDialog() {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View dialogView = inflater.inflate(R.layout.dialog_reserve_service, null);

        Spinner eventSpinner = dialogView.findViewById(R.id.spinner_event);
        Spinner employeeSpinner = dialogView.findViewById(R.id.spinner_employee);
        RecyclerView scheduleRecyclerView = dialogView.findViewById(R.id.recycler_view_schedule);
        EditText startTimeEditText = dialogView.findViewById(R.id.edit_text_start_time);
        EditText endTimeEditText = dialogView.findViewById(R.id.edit_text_end_time);

        eventSpinner.setSelection(0);

        EventSpinnerAdapter eventAdapter = new EventSpinnerAdapter(requireContext(), events);
        eventSpinner.setAdapter(eventAdapter);

        List<Employee> employees = service.getEmployees();
        EmployeeSpinnerAdapter employeeAdapter = new EmployeeSpinnerAdapter(requireContext(), employees);
        employeeSpinner.setAdapter(employeeAdapter);

        employeeSpinner.setSelection(0);

        scheduleRecyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));

        final long[] eventDate = new long[1];
        final Event[] selectedEvent = new Event[1];

        eventSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedEvent[0] = (Event) parent.getItemAtPosition(position);
                eventDate[0] = selectedEvent[0].getDate();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        final List<AvailabilitySlot>[] availabilitySlotsHolder = new List[]{new ArrayList<>()};

        employeeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Employee selectedEmployee = (Employee) parent.getItemAtPosition(position);
                DayOfWeek dayOfWeek = getDayOfWeek(eventDate[0]);
                EmployeeWorkHours workHours = getWorkHoursForDay(selectedEmployee, dayOfWeek);

                availabilitySlotsHolder[0] = generateAvailabilitySlots(workHours, selectedEvent[0], selectedEmployee.getEvents());

                EmployeeAvailabilityScheduleAdapter scheduleAdapter = new EmployeeAvailabilityScheduleAdapter(availabilitySlotsHolder[0]);
                scheduleRecyclerView.setAdapter(scheduleAdapter);
            }



            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        if (service.getDuration() != 0) {
            startTimeEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void afterTextChanged(Editable s) {
                    updateEndTime(startTimeEditText, endTimeEditText);
                }
            });
        } else {
            startTimeEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {}

                @Override
                public void afterTextChanged(Editable s) {}
            });
        }



        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        builder.setView(dialogView)
                .setTitle("Reserve service")
                .setPositiveButton("Confirm", (dialog, which) -> {
                    String startTime = startTimeEditText.getText().toString();
                    String endTime = endTimeEditText.getText().toString();
                    Event event = (Event) eventSpinner.getSelectedItem();
                    Employee employee = (Employee) employeeSpinner.getSelectedItem();


                    if (isReservationValid(event.getDate()) && validateReservation(startTime, endTime, availabilitySlotsHolder[0])) {
                        saveReservation(event, employee, startTime, endTime);
                    } else {
                        Toast.makeText(requireContext(), "Reservation is invalid. Please check the selected time slot.", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancel", (dialog, which) -> {
                    dialog.dismiss();
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private boolean validateReservation(String startTime, String endTime, List<AvailabilitySlot> availabilitySlots) {
        if (service.getDuration() == 0) {
            int minEngagement = service.getMinEngagement();
            int maxEngagement = service.getMaxEngagement();

            LocalTime start = LocalTime.parse(startTime);
            LocalTime end = LocalTime.parse(endTime);

            long durationInHours = Duration.between(start, end).toHours();

            if (durationInHours < minEngagement || durationInHours > maxEngagement) {
                return false;
            }
        }

        for (AvailabilitySlot slot : availabilitySlots) {
            if (slot.isAvailable()) {
                continue;
            }

            String slotStartTime = slot.getStartTime();
            String slotEndTime = slot.getEndTime();

            if ((startTime.compareTo(slotEndTime) < 0 && endTime.compareTo(slotStartTime) > 0) ||
                    (slotStartTime.compareTo(endTime) < 0 && slotEndTime.compareTo(startTime) > 0)) {
                return false;
            }
        }
        return true;
    }

    private void updateEndTime(EditText startTimeEditText, EditText endTimeEditText) {
        String startTimeText = startTimeEditText.getText().toString().trim();
        if (!startTimeText.isEmpty()) {
            try {
                String[] startTimeParts = startTimeText.split(":");
                int startHour = Integer.parseInt(startTimeParts[0]);
                int startMinute = Integer.parseInt(startTimeParts[1]);


                int duration = service.getDuration();
                int endHour = (startHour + duration) % 24;

                String endTimeText = String.format("%02d:%02d", endHour, startMinute);
                endTimeEditText.setText(endTimeText);
            } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isReservationValid(long eventDate) {
        int reservationDeadline = service.getReservationDeadline();
        int cancellationDeadline = service.getCancellationDeadline();
        int maxDeadline = Math.max(reservationDeadline, cancellationDeadline);

        LocalDate eventLocalDate = Instant.ofEpochMilli(eventDate).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate deadlineDate = eventLocalDate.minusDays(maxDeadline);
        LocalDate currentDate = LocalDate.now();

        return deadlineDate.isAfter(currentDate) || deadlineDate.isEqual(currentDate);
    }

    private void saveReservation(Event event, Employee employee, String startTime, String endTime) {
        String eventId = event.getId();
        String serviceId = service.getId();
        String employeeEmail = employee.getEmail();
        Date date = new Date(event.getDate());

        ServiceReservation reservation = new ServiceReservation("", eventId, serviceId, employeeEmail, employee.getCompanyId(), loggedInEventOrganizer.getEmail(), date, "", startTime, endTime, ServiceReservationStatus.NEW);

        List<EmployeeEvent> employeeEvents = new ArrayList<>(employee.getEvents());

        EmployeeEvent newEmployeeEvent = new EmployeeEvent(event.getName(), EventStatus.RESERVED, new Date(event.getDate()), startTime, endTime);

        employeeEvents.add(newEmployeeEvent);

        employee.setEvents(employeeEvents);

        List<Employee> serviceEmployees = new ArrayList<>(service.getEmployees());

        service.setEmployees(serviceEmployees);

        serviceReservationsOperations.save(reservation, new ServiceReservationsOperations.SaveServiceReservationListener() {
            @Override
            public void onSuccess(String serviceReservationId) {
                Toast.makeText(getContext(), "Reservation saved successfully, ID:" + serviceReservationId, Toast.LENGTH_SHORT).show();

                usersOperations.getUid(employeeEmail, new UsersOperations.OnGetUidListener() {
                    @Override
                    public void onSuccess(String uid) {
                        servicesOperations.update(service, new ServicesOperations.UpdateServiceListener() {
                            @Override
                            public void onSuccess() {
                                Log.d("Service reservations", "Service updated successful");
                                Notification notification = new Notification("", "New service reservation", "You have new service reservation: " + service.getName(), uid);
                                notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                    @Override
                                    public void onSuccess() {
                                        Toast.makeText(getContext(), "Notification sent.", Toast.LENGTH_SHORT).show();
                                        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
                                        String organizerId = sharedPreferences.getString("userId", "");
                                        Notification notification = new Notification("", "Service reservation", "You have service reservation (in comming hour): " + service.getName(), organizerId);


                                        scheduleNotification(notification, startTime, event.getDate());

                                    }

                                    @Override
                                    public void onFailure(Exception e) {

                                    }
                                });
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.d("Service reservations", String.valueOf(e));
                            }
                        });
                    }

                    @Override
                    public void onFailure(Exception e) {

                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Failed to save reservation", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void scheduleNotification(Notification scheduledNotification, String sendingTime, long eventDateMillis) {
        try {
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            Date parsedTime = timeFormat.parse(sendingTime);

            // Combine the date and time
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(eventDateMillis);
            calendar.set(Calendar.HOUR_OF_DAY, parsedTime.getHours());
            calendar.set(Calendar.MINUTE, parsedTime.getMinutes());
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            // Subtract one hour
            calendar.add(Calendar.HOUR_OF_DAY, -1);

            // Create the Date object for sending the notification
            Date notificationDate = calendar.getTime();

            // Log the notification date for debugging purposes
            SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Log.d("Notification Date", dateTimeFormat.format(notificationDate));

            // Schedule the WorkManager task
            long delayInMillis = notificationDate.getTime() - System.currentTimeMillis();

            if (delayInMillis > 0) {
                Data inputData = new Data.Builder()
                        .putString("notificationId", scheduledNotification.getId())
                        .putString("title", scheduledNotification.getTitle())
                        .putString("content", scheduledNotification.getContent())
                        .putString("recipientId", scheduledNotification.getRecipientId())
                        .build();

                WorkRequest notificationWork = new OneTimeWorkRequest.Builder(NotificationWorker.class)
                        .setInitialDelay(delayInMillis, TimeUnit.MILLISECONDS)
                        .setInputData(inputData)
                        .build();

                WorkManager.getInstance(requireContext()).enqueue(notificationWork);
            } else {
                Log.e("Notification Scheduling", "The notification time is in the past.");
            }

        } catch (Exception e) {
            Log.e("Notification Scheduling", "Error scheduling notification", e);
        }
    }

    private DayOfWeek getDayOfWeek(long eventDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(eventDate);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        return DayOfWeek.of(dayOfWeek);
    }

    private EmployeeWorkHours getWorkHoursForDay(Employee employee, DayOfWeek dayOfWeek) {
        return employee.getWorkSchedule().getWorkHoursForDay(dayOfWeek.name());
    }

    private List<AvailabilitySlot> generateAvailabilitySlots(EmployeeWorkHours workHours, Event selectedEvent, List<EmployeeEvent> employeeEvents) {
        List<AvailabilitySlot> availabilitySlots = new ArrayList<>();

        String workStartTime = workHours.getStartTime();
        String workEndTime = workHours.getEndTime();

        if (employeeEvents == null) {
            employeeEvents = new ArrayList<>();
        }

        List<EmployeeEvent> eventsForTheDay = new ArrayList<>();
        Date selectedEventDate = new Date(selectedEvent.getDate());
        for (EmployeeEvent employeeEvent : employeeEvents) {
            if (isSameDay(employeeEvent.getDate(), selectedEventDate)) {
                eventsForTheDay.add(employeeEvent);
            }
        }

        eventsForTheDay.sort(Comparator.comparing(EmployeeEvent::getStartTime));

        String currentStart = workStartTime;

        for (EmployeeEvent employeeEvent : eventsForTheDay) {
            String eventStart = employeeEvent.getStartTime();
            String eventEnd = employeeEvent.getEndTime();

            if (currentStart.compareTo(eventStart) < 0) {
                availabilitySlots.add(new AvailabilitySlot(currentStart, eventStart, true));
            }

            availabilitySlots.add(new AvailabilitySlot(eventStart, eventEnd, false));

            currentStart = eventEnd;
        }

        if (currentStart.compareTo(workEndTime) < 0) {
            availabilitySlots.add(new AvailabilitySlot(currentStart, workEndTime, true));
        }

        return availabilitySlots;
    }

    private boolean isSameDay(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
    }

    private void bindServiceDetails() {
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey("service")) {
            service = bundle.getParcelable("service");
            if (service != null) {
                bindBasicDetails();
                setupDeleteButton();
                setupEditButton();
            }
        }
    }

    private void bindBasicDetails() {
        binding.serviceImage.setImageResource(service.getImages().get(0));
        binding.serviceName.setText(service.getName());
        binding.serviceDescription.setText(service.getDescription());
        binding.serviceSpecificities.setText(service.getSpecificities());
        binding.serviceCategory.setText(service.getCategory().getName());
        binding.serviceSubcategory.setText(service.getSubcategory().getName());

        List<Employee> employees = service.getEmployees();
        List<String> employeeNames = new ArrayList<>();

        for (Employee employee : employees) {
            String fullName = employee.getName() + " " + employee.getSurname();
            employeeNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> adapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(employeeNames.get(position));
            }

            @Override
            public int getItemCount() {
                return employeeNames.size();
            }
        };

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        binding.employeesRecyclerView.setLayoutManager(layoutManager);

        List<EventType> eventTypes = service.getEvents();
        List<String> eventTypeNames = new ArrayList<>();

        for (EventType eventType : eventTypes) {
            String fullName = eventType.getTypeName();
            eventTypeNames.add(fullName);
        }

        // Create a RecyclerView adapter
        RecyclerView.Adapter<RecyclerView.ViewHolder> eventadapter = new RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
                return new RecyclerView.ViewHolder(view) {};
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
                TextView textView = holder.itemView.findViewById(android.R.id.text1);
                textView.setText(eventTypeNames.get(position));
            }

            @Override
            public int getItemCount() {
                return eventTypeNames.size();
            }
        };

        LinearLayoutManager layoutEventManager = new LinearLayoutManager(getContext());
        binding.eventsRecyclerView.setLayoutManager(layoutEventManager);

        binding.employeesRecyclerView.setAdapter(adapter);
        binding.eventsRecyclerView.setAdapter(eventadapter);
        binding.servicePrice.setText(String.valueOf(service.getPrice()));
        binding.serviceDiscount.setText(String.valueOf(service.getDiscount()));
        binding.serviceReservationDeadline.setText(String.valueOf(service.getReservationDeadline()));
        binding.serviceCancellationDeadline.setText(String.valueOf(service.getCancellationDeadline()));
    }

    private void setupDeleteButton() {
        Button deleteButton = requireView().findViewById(R.id.delete_service_button);
        deleteButton.setOnClickListener(v -> showConfirmationDialog());
    }

    private void setupEditButton() {
        Button editButton = requireView().findViewById(R.id.edit_service_button);
        editButton.setOnClickListener(v -> {
            NavController navController = Navigation.findNavController(requireActivity(), R.id.fragment_nav_content_main);
            Bundle bundle = new Bundle();
            bundle.putParcelable("service", service);
            navController.navigate(R.id.nav_service_edit, bundle);
        });
    }


    private void showConfirmationDialog() {
        new AlertDialog.Builder(requireContext())
                .setMessage("Are you sure you want to delete this service?")
                .setTitle("Confirmation")
                .setPositiveButton("Yes", (dialog, which) -> toggleProductStatus())
                .setNegativeButton("No", null)
                .show();
    }

    private void toggleProductStatus() {
        service.setDeleted(true);
        servicesOperations.update(service, new ServicesOperations.UpdateServiceListener() {
            @Override
            public void onSuccess() {
                Toast.makeText(getContext(), "Service: " + service.getName() + " is deleted." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }

            @Override
            public void onFailure(Exception e) {
                Toast.makeText(getContext(), "Service: " + service.getName() + " is not deleted." , Toast.LENGTH_SHORT).show();
                Navigation.findNavController(requireView()).navigateUp();
            }
        });
    }

    private void fetchEvents() {
        SharedPreferences sharedPreferences = requireContext().getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String organizerId = sharedPreferences.getString("userId", "");
        eventsOperations.getAllByOrganizerId(organizerId, new EventsOperations.GetAllEventsListener<Event>() {
            @Override
            public void onSuccess(List<Event> result) {
                events = result;
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("ServiceDetailsFragment", String.valueOf(e));
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}