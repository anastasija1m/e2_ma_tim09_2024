package com.ma.eventplanner.activities;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ma.eventplanner.R;
import com.ma.eventplanner.database.AuthenticationManager;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.databinding.ActivityHomeBinding;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.User;

import java.util.HashSet;
import java.util.Set;

public class HomeActivity extends AppCompatActivity {

    private ActivityHomeBinding binding;
    private AppBarConfiguration mAppBarConfiguration;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private NavController navController;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private User loggedUser;
    private Owner loggedInOwner;
    private Employee loggedInEmployee;
    private EventOrganizer loggedInEventOrganizer;
    private UsersOperations usersOperations;
    private Set<Integer> topLevelDestinations = new HashSet<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        usersOperations = new UsersOperations();
        getLoggedUser();
        getLoggedOwner();
        getLoggedEmployee();
        getLoggedEventOrganizer();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(HomeActivity.this, Manifest.permission.POST_NOTIFICATIONS) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(HomeActivity.this, new String[]{Manifest.permission.POST_NOTIFICATIONS}, 101);
            }
        }

        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        drawer = binding.drawerLayout;
        navigationView = binding.navView;
        toolbar = binding.activityHomeBase.toolbar;

        setSupportActionBar(toolbar);

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_burger_menu);
            actionBar.setHomeButtonEnabled(true);
        }

        navController = Navigation.findNavController(this, R.id.fragment_nav_content_main);
        navController.addOnDestinationChangedListener((navController, navDestination, bundle) -> {
            int id = navDestination.getId();
            boolean isTopLevelDestination = topLevelDestinations.contains(id);
            if (!isTopLevelDestination) {
                if (id == R.id.nav_dummy) {
                    Toast.makeText(HomeActivity.this, "Dummy fragment", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_employees) {
                    Toast.makeText(HomeActivity.this, "Employees page", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_products) {
                    Toast.makeText(HomeActivity.this, "Products page", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_services) {
                    Toast.makeText(HomeActivity.this, "Services page", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_bundles) {
                    Toast.makeText(HomeActivity.this, "Bundles page", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_administration_event)  {
                    Toast.makeText(HomeActivity.this, "Administration page", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_search) {
                    Toast.makeText(HomeActivity.this, "Search page", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_events) {
                    Toast.makeText(HomeActivity.this, "Events page", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_registration_requests) {
                    Toast.makeText(HomeActivity.this, "Registration requests", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_employee_profile) {
                    Toast.makeText(HomeActivity.this, "My profile", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_product_requests) {
                    Toast.makeText(HomeActivity.this, "Product requests", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_service_requests) {
                    Toast.makeText(HomeActivity.this, "Service requests", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_employee_reservations) {
                    Toast.makeText(HomeActivity.this, "Reservations", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_owner_reservations_page) {
                    Toast.makeText(HomeActivity.this, "Reservations", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_organizer_reservations_page) {
                    Toast.makeText(HomeActivity.this, "Reservations", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_companies_page) {
                    Toast.makeText(HomeActivity.this, "Companies", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_owner_profile) {
                    Toast.makeText(HomeActivity.this, "My profile", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_rating_reports) {
                    Toast.makeText(HomeActivity.this, "Rating reports", Toast.LENGTH_SHORT).show();
                } else if (id == R.id.nav_pricelists) {
                    Toast.makeText(HomeActivity.this, "Price lists", Toast.LENGTH_SHORT).show();
            }
                drawer.closeDrawers();
            }
        });

        mAppBarConfiguration = new AppBarConfiguration
                .Builder(R.id.nav_dummy, R.id.nav_search, R.id.nav_events, R.id.nav_employees, R.id.nav_administration_event, R.id.nav_products, R.id.nav_services, R.id.nav_bundles, R.id.nav_registration_requests, R.id.nav_pricelists)
                .setOpenableLayout(drawer)
                .build();

        navigationView.setNavigationItemSelectedListener(null);

        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

        navigationView.setNavigationItemSelectedListener(item -> {
            item.setChecked(true);
            drawer.closeDrawers();

            handleNavigation(item.getItemId());

            return true;
        });

        ImageButton notificationButton = findViewById(R.id.notification_button);
        notificationButton.setOnClickListener(v -> openNotificationFragment());
        getFCMToken();
    }

    private void openNotificationFragment() {
        navController.navigate(R.id.nav_notifications);
    }

    private void handleNavigation(int itemId) {
        if (itemId == R.id.nav_dummy) {
            navController.navigate(R.id.nav_dummy);
        } else if (itemId == R.id.nav_employees) {
            navController.navigate(R.id.nav_employees);
        } else if (itemId == R.id.nav_administration_event) {
            navController.navigate(R.id.nav_administration_event);
        } else if (itemId == R.id.nav_services) {
            navController.navigate(R.id.nav_services);
        } else if (itemId == R.id.nav_products) {
            navController.navigate(R.id.nav_products);
        } else if (itemId == R.id.nav_bundles) {
            navController.navigate(R.id.nav_bundles);
        } else if (itemId == R.id.nav_search) {
            navController.navigate(R.id.nav_search);
        } else if (itemId == R.id.nav_events) {
            navController.navigate(R.id.nav_events);
        } else if (itemId == R.id.nav_registration_requests) {
            navController.navigate(R.id.nav_registration_requests);
        } else if(itemId == R.id.nav_employee_profile) {
            navController.navigate(R.id.nav_employee_profile);
        } else if (itemId == R.id.nav_logout) {
            AuthenticationManager.getInstance().signOut(HomeActivity.this);
        } else if (itemId == R.id.nav_product_requests) {
            navController.navigate(R.id.nav_product_requests);
        } else if (itemId == R.id.nav_service_requests) {
            navController.navigate(R.id.nav_service_requests);
        } else if (itemId == R.id.nav_employee_reservations) {
            navController.navigate(R.id.nav_employee_reservations);
        } else if (itemId == R.id.nav_owner_reservations_page) {
            navController.navigate(R.id.nav_owner_reservations_page);
        } else if (itemId == R.id.nav_organizer_reservations_page) {
            navController.navigate(R.id.nav_organizer_reservations_page);
        } else if (itemId == R.id.nav_companies_page) {
            navController.navigate(R.id.nav_companies_page);
        } else if (itemId == R.id.nav_owner_profile) {
            navController.navigate(R.id.nav_owner_profile);
        } else if (itemId == R.id.nav_rating_reports) {
            navController.navigate(R.id.nav_rating_reports);
        }  else if (itemId == R.id.nav_pricelists) {
            navController.navigate(R.id.nav_pricelists);
        } else if (itemId == R.id.nav_reports) {
            navController.navigate(R.id.nav_reports);
        }
    }

    private void setupNavigationMenu() {
        Menu navMenu = navigationView.getMenu();
        navMenu.clear();

        navMenu.add(Menu.NONE, R.id.nav_dummy, Menu.NONE, R.string.dummy).setIcon(R.drawable.ic_employee);
        navMenu.add(Menu.NONE, R.id.nav_search, Menu.NONE, R.string.search).setIcon(R.drawable.ic_search);

        //TODO set per Role and set icon

        switch (loggedUser.getRole()) {
            case ADMIN:
                navMenu.add(Menu.NONE, R.id.nav_administration_event, Menu.NONE, R.string.administration).setIcon(R.drawable.ic_employee);
                navMenu.add(Menu.NONE, R.id.nav_registration_requests, Menu.NONE, "Registration requests").setIcon(R.drawable.ic_request);
                navMenu.add(Menu.NONE, R.id.nav_product_requests, Menu.NONE, "Product requests").setIcon(R.drawable.ic_request);
                navMenu.add(Menu.NONE, R.id.nav_service_requests, Menu.NONE, "Service requests").setIcon(R.drawable.ic_request);
                navMenu.add(Menu.NONE, R.id.nav_rating_reports, Menu.NONE, "Rating reports").setIcon(R.drawable.ic_report);
                navMenu.add(Menu.NONE, R.id.nav_reports, Menu.NONE, "Reports").setIcon(R.drawable.ic_report);

                break;
            case OWNER:
                navMenu.add(Menu.NONE, R.id.nav_employees, Menu.NONE, R.string.employees).setIcon(R.drawable.ic_employee);
                navMenu.add(Menu.NONE, R.id.nav_products, Menu.NONE, R.string.products).setIcon(R.drawable.ic_product);
                navMenu.add(Menu.NONE, R.id.nav_services, Menu.NONE, R.string.services).setIcon(R.drawable.ic_service);
                navMenu.add(Menu.NONE, R.id.nav_bundles, Menu.NONE, R.string.bundles).setIcon(R.drawable.ic_bundle);
                navMenu.add(Menu.NONE, R.id.nav_owner_reservations_page, Menu.NONE, R.string.reservations).setIcon(R.drawable.ic_reservation);
                navMenu.add(Menu.NONE, R.id.nav_owner_profile, Menu.NONE, R.string.employee_profile).setIcon(R.drawable.ic_employee);
                navMenu.add(Menu.NONE, R.id.nav_pricelists, Menu.NONE, R.string.priceLists).setIcon(R.drawable.ic_pricelist);
                break;
            case EVENT_ORGANIZER:
                navMenu.add(Menu.NONE, R.id.nav_events, Menu.NONE, R.string.events).setIcon(R.drawable.ic_event);
                navMenu.add(Menu.NONE, R.id.nav_services, Menu.NONE, R.string.services).setIcon(R.drawable.ic_service);
                navMenu.add(Menu.NONE, R.id.nav_products, Menu.NONE, R.string.products).setIcon(R.drawable.ic_product);
                navMenu.add(Menu.NONE, R.id.nav_organizer_reservations_page, Menu.NONE, R.string.reservations).setIcon(R.drawable.ic_reservation);
                break;
            case EMPLOYEE:
                navMenu.add(Menu.NONE, R.id.nav_employee_profile, Menu.NONE, R.string.employee_profile).setIcon(R.drawable.ic_employee);
                navMenu.add(Menu.NONE, R.id.nav_products, Menu.NONE, R.string.products).setIcon(R.drawable.ic_product);
                navMenu.add(Menu.NONE, R.id.nav_services, Menu.NONE, R.string.services).setIcon(R.drawable.ic_service);
                navMenu.add(Menu.NONE, R.id.nav_bundles, Menu.NONE, R.string.bundles).setIcon(R.drawable.ic_bundle);
                navMenu.add(Menu.NONE, R.id.nav_employee_reservations, Menu.NONE, R.string.reservations).setIcon(R.drawable.ic_reservation);
                navMenu.add(Menu.NONE, R.id.nav_pricelists, Menu.NONE, R.string.priceLists).setIcon(R.drawable.ic_pricelist);

        }

        navMenu.add(Menu.NONE, R.id.nav_companies_page, Menu.NONE, R.string.companies).setIcon(R.drawable.ic_company);
        navMenu.add(Menu.NONE, R.id.nav_logout, Menu.NONE, R.string.log_out).setIcon(R.drawable.ic_log_out);

        navigationView.setNavigationItemSelectedListener(item -> {
            handleNavigation(item.getItemId());
            return false;
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, drawer) || super.onSupportNavigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                    || super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    private void getLoggedUser() {
        SharedPreferences sharedPreferences = getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "");

        usersOperations.getById(userId, new UsersOperations.OnGetByIdListener() {
            @Override
            public void onSuccess(User user) {
                loggedUser = user;
                setupNavigationMenu();
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void getLoggedOwner() {
        SharedPreferences sharedPreferences = getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "");

        usersOperations.getOwnerById(userId, new UsersOperations.OnGetOwnerByIdListener() {
            @Override
            public void onSuccess(Owner user) {
                loggedInOwner = user;
                setupNavigationMenu();
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void getLoggedEmployee() {
        SharedPreferences sharedPreferences = getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "");

        usersOperations.getEmployeeById(userId, new UsersOperations.OnGetEmployeeByIdListener() {
            @Override
            public void onSuccess(Employee employee) {
                loggedInEmployee = employee;
                setupNavigationMenu();
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    private void getLoggedEventOrganizer() {
        SharedPreferences sharedPreferences = getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        String userId = sharedPreferences.getString("userId", "");

        usersOperations.getEventOrganizerById(userId, new UsersOperations.OnGetEventOrganizerByIdListener() {
            @Override
            public void onSuccess(EventOrganizer eventOrganizer) {
                loggedInEventOrganizer = eventOrganizer;
                setupNavigationMenu();
            }

            @Override
            public void onFailure(Exception e) {
            }
        });
    }

    public User getLoggedInUser() {
        return loggedUser;
    }

    public Owner getLoggedInOwner() {
        return loggedInOwner;
    }

    public Employee getLoggedInEmployee() {
        return loggedInEmployee;
    }

    public EventOrganizer getLoggedInEventOrganizer() {
        return loggedInEventOrganizer;
    }


    void getFCMToken() {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(task -> {
            if(task.isSuccessful()) {
                String token = task.getResult();
                AuthenticationManager.currentUserDetails().update("fcmToken", token);
            }
        });
    }
}