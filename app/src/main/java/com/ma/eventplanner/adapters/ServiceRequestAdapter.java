package com.ma.eventplanner.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.ServiceRequest;

import java.util.List;

public class ServiceRequestAdapter extends BaseAdapter {
    private Context context;
    private List<ServiceRequest> serviceRequests;

    private final Activity activity;

    public ServiceRequestAdapter(Context context, List<ServiceRequest> serviceRequests, Activity activity) {
        this.context = context;
        this.serviceRequests = serviceRequests;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return serviceRequests.size();
    }

    @Override
    public Object getItem(int position) {
        return serviceRequests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.service_request_card, parent, false);
            holder = new ViewHolder();
            holder.serviceNameTextView = convertView.findViewById(R.id.service_name_request);
            holder.serviceCategoryTextView = convertView.findViewById(R.id.service_category_name_request);
            holder.servicePriceTextView = convertView.findViewById(R.id.service_price_request);
            holder.serviceSubcategoryTextView = convertView.findViewById(R.id.service_subcategory_name_request);
            holder.detailsButton = convertView.findViewById(R.id.service_request_details_button);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ServiceRequest serviceRequest = serviceRequests.get(position);

        holder.serviceNameTextView.setText(serviceRequest.getService().getName());
        holder.serviceCategoryTextView.setText(serviceRequest.getService().getCategory().getName());
        holder.servicePriceTextView.setText(String.valueOf(serviceRequest.getService().getPrice()));
        holder.serviceSubcategoryTextView.setText(serviceRequest.getRequestedSubcategory().getName());

        holder.detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("selected_request_id", serviceRequest.getId());
                NavController navController = Navigation.findNavController(activity, R.id.fragment_nav_content_main);
                navController.navigate(R.id.nav_service_request_details, bundle);
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView serviceNameTextView;
        TextView serviceCategoryTextView;
        TextView servicePriceTextView;
        TextView serviceSubcategoryTextView;
        Button detailsButton;
    }
}