package com.ma.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.model.Event;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventsListAdapter extends ArrayAdapter<Event> {
    private final List<Event> aEvents;

    public EventsListAdapter(Context context, ArrayList<Event> events) {
        super(context, R.layout.item_event_card, events);
        aEvents = events;
    }
    @Override
    public int getCount() {
        return aEvents.size();
    }

    @Nullable
    @Override
    public Event getItem(int position) {
        return aEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Event event = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_event_card,
                    parent, false);
        }
        LinearLayout eventCard = convertView.findViewById(R.id.event_card);
        TextView eventName = convertView.findViewById(R.id.eventt_name);
        TextView eventDesc = convertView.findViewById(R.id.eventt_type);
        TextView eventDate = convertView.findViewById(R.id.eventt_date);

        if(event != null){
            eventName.setText(event.getName());
            eventDesc.setText(event.getDescription());

            long timestamp = event.getDate();
            Date date = new Date(timestamp);
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy.");
            String formattedDate = dateFormat.format(date);

            eventDate.setText(formattedDate);
            eventCard.setOnClickListener(v -> {
                NavController navController = Navigation.findNavController((HomeActivity) getContext(), R.id.fragment_nav_content_main);

                Bundle bundle = new Bundle();
                bundle.putParcelable("event", event);
                navController.navigate(R.id.nav_event_management, bundle);
            });
        }

        return convertView;
    }
}
