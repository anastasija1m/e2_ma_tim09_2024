package com.ma.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.CategoriesOperations;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class CategoryListAdapter extends ArrayAdapter<Category> {

    private final List<Category> categoryList;
    private final Activity activity;
    private final CategoriesOperations categoriesOperations;
    private final UsersOperations usersOperations;
    private final NotificationsOperations notificationsOperations;
    private final ProductsOperations productsOperations;
    private final ServicesOperations servicesOperations;
    private List<Product> products;
    private List<Service> services;
    private String loggedUserId;

    public CategoryListAdapter(@NonNull Context context, int resource, @NonNull List<Category> categoryList, Activity activity, CategoriesOperations categoriesOperations) {
        super(context, resource, categoryList);
        SharedPreferences sharedPreferences = context.getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        loggedUserId = sharedPreferences.getString("userId", "");
        this.categoryList = categoryList;
        this.activity = activity;
        this.categoriesOperations = categoriesOperations;
        this.usersOperations = new UsersOperations();
        this.notificationsOperations = new NotificationsOperations();
        this.productsOperations = new ProductsOperations();
        this.servicesOperations = new ServicesOperations();

        productsOperations.getAllProducts(new ProductsOperations.GetAllProductsListener<Product>() {
            @Override
            public void onSuccess(ArrayList<Product> result) {
                products = result;
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        servicesOperations.getAllServices(new ServicesOperations.GetAllServicesListener<Service>() {
            @Override
            public void onSuccess(ArrayList<Service> result) {
                services = result;
            }

            @Override
            public void onFailure(Exception e) {

            }
        });
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.create_list_item_category, parent, false);
        }

        final Category currentCategory = categoryList.get(position);

        TextView categoryNameTextView = view.findViewById(R.id.category_name_text);
        categoryNameTextView.setText(currentCategory.getName());

        setupDeleteButton(view, position);
        setupShowSubcategoryButton(view, currentCategory);
        setupEditButton(view, currentCategory);

        return view;
    }

    private void setupDeleteButton(@NonNull View view, final int position) {
        ImageButton deleteButton = view.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteCategory(position);
            }
        });
    }

    private void setupShowSubcategoryButton(@NonNull View view, final Category currentCategory) {
        ImageButton showSubcategoryButton = view.findViewById(R.id.subcategory_button);
        showSubcategoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSubcategories(currentCategory);
            }
        });
    }

    private void setupEditButton(@NonNull View view, final Category currentCategory) {
        ImageButton editButton = view.findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleEditButtonClick(currentCategory);
            }
        });
    }

    private void deleteCategory(int position) {
        Category category = categoryList.get(position);
        String categoryId = category.getId();

        boolean hasProductWithCategoryId = products.stream()
                .anyMatch(product -> product.getCategory().getId().equals(categoryId));

        boolean hasServiceWithCategoryId = services.stream()
                .anyMatch(product -> product.getCategory().getId().equals(categoryId));

        if (hasProductWithCategoryId || hasServiceWithCategoryId) {
            Toast.makeText(activity, "Cannot delete category", Toast.LENGTH_SHORT).show();
            return;
        }

        categoriesOperations.delete(categoryId, new CategoriesOperations.DeleteCategoryListener() {
            @Override
            public void onSuccess() {
                Log.e("CategoryDelete", "Category deleted successful: " + categoryId);
                categoryList.remove(position);
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("CategoryDelete", "Error deleting category from database: " + e.getMessage());
            }
        });
    }

    private void showSubcategories(@NonNull Category currentCategory) {
        NavController navController = Navigation.findNavController(activity, R.id.fragment_nav_content_main);
        Bundle bundle = new Bundle();
        bundle.putString("selected_category_name", currentCategory.getName());
        bundle.putString("selected_category_id", currentCategory.getId());
        navController.navigate(R.id.nav_create_subcategory_category_event, bundle);
    }

    private void handleEditButtonClick(final Category currentCategory) {
        populateFieldsWithCategoryData(currentCategory);
        updateAddButtonForEdit();
        setAddButtonClickListenerForEdit(currentCategory);
    }

    private void populateFieldsWithCategoryData(@NonNull Category category) {
        TextInputEditText categoryNameEditText = activity.findViewById(R.id.category_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
        categoryNameEditText.setText(category.getName());
        descriptionEditText.setText(category.getDescription());
    }

    private void updateAddButtonForEdit() {
        Button addButton = activity.findViewById(R.id.add_button);
        addButton.setText(R.string.save);
    }

    private void setAddButtonClickListenerForEdit(final Category currentCategory) {
        Button addButton = activity.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText categoryNameEditText = activity.findViewById(R.id.category_name);
                TextInputEditText descriptionEditText = activity.findViewById(R.id.description);

                String oldName = currentCategory.getName();

                currentCategory.setName(Objects.requireNonNull(categoryNameEditText.getText()).toString());
                currentCategory.setDescription(Objects.requireNonNull(descriptionEditText.getText()).toString());

                categoriesOperations.update(currentCategory, new CategoriesOperations.UpdateCategoryListener() {
                    @Override
                    public void onSuccess() {
                        Log.e("CategorySave", "Category updated successful successful: ");
                        notifyDataSetChanged();

                        categoryNameEditText.setText("");
                        descriptionEditText.setText("");

                        usersOperations.getAllByRole(Role.OWNER, new UsersOperations.OnGetAllByRoleListener() {
                            @Override
                            public void onSuccess(HashMap<String, User> userMap) {
                                for (String adminId : userMap.keySet()) {
                                    Notification notification = createNotificationForOwner(adminId, currentCategory, oldName);
                                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d(TAG, "Notification sent to owner: " + adminId);
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e(TAG, "Error sending notification to admin: " + adminId, e);
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.e(TAG, "Error getting admins", e);
                            }
                        });
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.e("CategorySave", "Error updating category to database: " + e.getMessage());
                    }
                });

                notifyDataSetChanged();
                clearFieldsAndResetButton(addButton);
            }
        });
    }

    private Notification createNotificationForOwner(String ownerId, Category category, String oldName) {
        String title = "The category has been changed";
        String content = String.format("The category %s has been changed to %s.",
                oldName,
                category.getName());

        return new Notification("", title, content, ownerId);
    }

    private void clearFieldsAndResetButton(@NonNull Button addButton) {
        TextInputEditText categoryNameEditText = activity.findViewById(R.id.category_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);

        categoryNameEditText.setText("");
        descriptionEditText.setText("");
        addButton.setText(R.string.add);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewCategory();
            }
        });
    }

    private void addNewCategory() {
        TextInputEditText categoryNameEditText = activity.findViewById(R.id.category_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
        String categoryName = Objects.requireNonNull(categoryNameEditText.getText()).toString();
        String description = Objects.requireNonNull(descriptionEditText.getText()).toString();
        Category newCategory = new Category(null, categoryName, description, loggedUserId);

        categoriesOperations.save(newCategory, new CategoriesOperations.SaveCategoryListener() {
            @Override
            public void onSuccess(String categoryId) {
                Log.e("CategorySave", "Category saved successful: " + categoryId);
                categoryList.add(newCategory);
                notifyDataSetChanged();

                categoryNameEditText.setText("");
                descriptionEditText.setText("");
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("CategorySave", "Error saving category to database: " + e.getMessage());
            }
        });
    }
}
