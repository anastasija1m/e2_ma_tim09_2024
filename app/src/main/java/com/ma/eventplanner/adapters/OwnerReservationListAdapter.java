package com.ma.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.ServiceReservation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class OwnerReservationListAdapter extends ArrayAdapter<ServiceReservation> {

    private final ArrayList<ServiceReservation> aServiceReservations;

    public OwnerReservationListAdapter(Context context, ArrayList<ServiceReservation> serviceReservations) {
        super(context, R.layout.owner_reservation_card, serviceReservations);
        this.aServiceReservations = serviceReservations;
    }

    @Override
    public int getCount() {
        return aServiceReservations.size();
    }

    @Nullable
    @Override
    public ServiceReservation getItem(int position) {
        return aServiceReservations.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ServiceReservation serviceReservation = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.owner_reservation_card,
                    parent, false);
        }

        TextView textStartTime = convertView.findViewById(R.id.text_start_time);
        TextView textEndTime = convertView.findViewById(R.id.text_end_time);
        TextView textDate = convertView.findViewById(R.id.text_date);
        TextView textOrganizer = convertView.findViewById(R.id.text_organizer);
        TextView textService = convertView.findViewById(R.id.text_service);
        TextView textEvent = convertView.findViewById(R.id.text_event);
        TextView textReservationStatus = convertView.findViewById(R.id.text_reservation_status);
        TextView textEmployeeName = convertView.findViewById(R.id.text_employee_name);
        TextView textEmployeeSurname = convertView.findViewById(R.id.text_employee_surname);
        TextView textOrganizerName = convertView.findViewById(R.id.text_organizer_name);
        TextView textOrganizerSurname = convertView.findViewById(R.id.text_organizer_surname);

        if (serviceReservation != null) {
            textStartTime.setText(serviceReservation.getStartTime());
            textEndTime.setText(serviceReservation.getEndTime());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String formattedDate = sdf.format(serviceReservation.getDate());
            textDate.setText(formattedDate);
            textOrganizer.setText(serviceReservation.getOrganizerEmail());
            textReservationStatus.setText(serviceReservation.getServiceReservationStatus().toString());

            getService(serviceReservation.getServiceId(), textService);
            getEvent(serviceReservation.getEventId(), textEvent);
            getEmployee(serviceReservation.getEmployeeEmail(), textEmployeeName, textEmployeeSurname);
            getOrganizer(serviceReservation.getOrganizerEmail(), textOrganizerName, textOrganizerSurname);
        }

        return convertView;
    }

    private void getService(String serviceId, TextView textService) {
        ServicesOperations serviceOperations = new ServicesOperations();
        serviceOperations.getById(serviceId, new ServicesOperations.GetServiceByIdListener() {
            @Override
            public void onSuccess(Service service) {
                textService.setText(service.getName());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting service name: ", e);
            }
        });
    }

    private void getEvent(String eventId, TextView textEvent) {
        EventsOperations eventsOperations = new EventsOperations();
        eventsOperations.getById(eventId, new EventsOperations.GetEventByIdListener<Event>() {
            @Override
            public void onSuccess(Event result) {
                textEvent.setText(result.getName());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting event name: ", e);
            }
        });
    }

    private void getEmployee(String employeeEmail, TextView textEmployeeName, TextView textEmployeeSurname) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEmployeeByEmail(employeeEmail, new UsersOperations.OnGetEmployeeByEmailListener() {
            @Override
            public void onSuccess(Employee employee) {
                textEmployeeName.setText(employee.getName());
                textEmployeeSurname.setText(employee.getSurname());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching employee for reservation: ", e);
            }
        });
    }

    private void getOrganizer(String organizerEmail, TextView textOrganizerName, TextView textOrganizerSurname) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEventOrganizerByEmail(organizerEmail, new UsersOperations.OnGetEventOrganizerByEmailListener() {
            @Override
            public void onSuccess(EventOrganizer eventOrganizer) {
                textOrganizerName.setText(eventOrganizer.getFirstName());
                textOrganizerSurname.setText(eventOrganizer.getLastName());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching employee for reservation: ", e);
            }
        });
    }
}
