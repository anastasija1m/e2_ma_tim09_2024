package com.ma.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.fragments.events.EventGuestsFragment;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventGuest;

import java.util.ArrayList;
import java.util.List;

public class EventGuestListAdapter extends ArrayAdapter<EventGuest> {
    private final List<EventGuest> aGuests;
    private final EventGuestsFragment fragment;

    public EventGuestListAdapter(EventGuestsFragment fragment, Context context, ArrayList<EventGuest> guests) {
        super(context, R.layout.item_guest, guests);
        aGuests = guests;
        this.fragment = fragment;
    }
    @Override
    public int getCount() {
        return aGuests.size();
    }

    @Nullable
    @Override
    public EventGuest getItem(int position) {
        return aGuests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        EventGuest guest = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_guest,
                    parent, false);
        }
        LinearLayout guestCard = convertView.findViewById(R.id.item_guest_item);
        TextView guestName = convertView.findViewById(R.id.item_g1);
        TextView guestAge = convertView.findViewById(R.id.item_g2);
        TextView guestInvited = convertView.findViewById(R.id.item_g3);
        TextView guestAccepted = convertView.findViewById(R.id.item_g4);
        TextView guestNotes = convertView.findViewById(R.id.item_g5);

        if(guest != null){
            guestName.setText(guest.getName()+" "+ guest.getSurname());
            guestAge.setText(guest.getAgeRange());
            guestInvited.setText(guest.getInvited().toString());
            guestAccepted.setText(guest.getHasAccepted().toString());
            guestNotes.setText(guest.getAdditionalNotes());
            guestCard.setOnClickListener(v -> {
                fragment.openDialog2(guest);
            });
        }

        return convertView;
    }
}
