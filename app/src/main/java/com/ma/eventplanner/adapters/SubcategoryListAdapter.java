package com.ma.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.ProductsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.SubcategoriesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;
import com.ma.eventplanner.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class SubcategoryListAdapter extends ArrayAdapter<Subcategory> {

    private final List<Subcategory> subcategoryList;
    private final Activity activity;
    private final SubcategoriesOperations subcategoriesOperations;
    private final String categoryId;
    private Spinner typeSpinner;

    private final UsersOperations usersOperations;
    private final NotificationsOperations notificationsOperations;
    private final ProductsOperations productsOperations;
    private final ServicesOperations servicesOperations;

    private List<Product> products;
    private List<Service> services;

    public SubcategoryListAdapter(@NonNull Context context, int resource, @NonNull List<Subcategory> subcategoryList, Activity activity,
                                  SubcategoriesOperations subcategoriesOperations, String categoryId, Spinner typeSpinner) {
        super(context, resource, subcategoryList);
        this.subcategoryList = subcategoryList;
        this.activity = activity;
        this.subcategoriesOperations = subcategoriesOperations;
        this.categoryId = categoryId;

        this.usersOperations = new UsersOperations();
        this.notificationsOperations = new NotificationsOperations();

        this.productsOperations = new ProductsOperations();
        this.servicesOperations = new ServicesOperations();

        productsOperations.getAllProducts(new ProductsOperations.GetAllProductsListener<Product>() {
            @Override
            public void onSuccess(ArrayList<Product> result) {
                products = result;
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        servicesOperations.getAllServices(new ServicesOperations.GetAllServicesListener<Service>() {
            @Override
            public void onSuccess(ArrayList<Service> result) {
                services = result;
            }

            @Override
            public void onFailure(Exception e) {

            }
        });

        this.typeSpinner = typeSpinner;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.create_list_item_subcategory, parent, false);
        }

        final Subcategory currentSubcategory = subcategoryList.get(position);

        TextView subcategoryNameTextView = view.findViewById(R.id.subcategory_name_text);
        subcategoryNameTextView.setText(currentSubcategory.getName());

        ImageButton deleteButton = view.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteSubcategory(position);
            }
        });

        ImageButton editButton = view.findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateFieldsWithSubcategoryData(currentSubcategory);
                updateAddButtonForEdit();
                setAddButtonClickListenerForEdit(currentSubcategory);
            }
        });

        return view;
    }

    private void deleteSubcategory(int position) {
        Subcategory subcategory = subcategoryList.get(position);
        String subcategoryId = subcategory.getId();

        boolean hasProductWithSubcategoryId = products.stream()
                .anyMatch(product -> product.getSubcategory().getId().equals(subcategoryId));

        boolean hasServiceWithSubcategoryId = services.stream()
                .anyMatch(product -> product.getCategory().getId().equals(subcategoryId));

        if (hasProductWithSubcategoryId || hasServiceWithSubcategoryId) {
            Toast.makeText(activity, "Cannot delete subcategory", Toast.LENGTH_SHORT).show();
            return;
        }

        subcategoriesOperations.delete(subcategoryId, new SubcategoriesOperations.DeleteSubcategoryListener() {
            @Override
            public void onSuccess() {
                Log.e("CategoryDelete", "Subcategory deleted successful: " + categoryId);
                subcategoryList.remove(position);
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("CategoryDelete", "Error deleting subcategory from database: " + e.getMessage());
            }
        });
    }

    private void populateFieldsWithSubcategoryData(@NonNull Subcategory subcategory) {
        TextInputEditText subcategoryNameEditText = activity.findViewById(R.id.subcategory_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
        subcategoryNameEditText.setText(subcategory.getName());
        descriptionEditText.setText(subcategory.getDescription());
        if(subcategory.getType() == SubcategoryType.PRODUCT) {
            typeSpinner.setSelection(0);
        } else if (subcategory.getType() == SubcategoryType.SERVICE) {
            typeSpinner.setSelection(1);
        }

    }

    private void updateAddButtonForEdit() {
        Button addButton = activity.findViewById(R.id.add_button);
        addButton.setText(R.string.save);
    }

    private void setAddButtonClickListenerForEdit(final Subcategory currentSubcategory) {
        Button addButton = activity.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText subcategoryNameEditText = activity.findViewById(R.id.subcategory_name);
                TextInputEditText descriptionEditText = activity.findViewById(R.id.description);

                String oldName = currentSubcategory.getName();

                currentSubcategory.setName(Objects.requireNonNull(subcategoryNameEditText.getText()).toString());
                currentSubcategory.setDescription(Objects.requireNonNull(descriptionEditText.getText()).toString());
                currentSubcategory.setType(Objects.requireNonNull(SubcategoryType.valueOf(typeSpinner.getSelectedItem().toString())));

                subcategoriesOperations.update(currentSubcategory, new SubcategoriesOperations.UpdateSubcategoryListener() {
                    @Override
                    public void onSuccess() {
                        Log.e("CategorySave", "Subcategory updated successful successful: ");

                        notifyDataSetChanged();
                        clearFieldsAndResetButton(addButton);

                        subcategoryNameEditText.setText("");
                        descriptionEditText.setText("");

                        usersOperations.getAllByRole(Role.OWNER, new UsersOperations.OnGetAllByRoleListener() {
                            @Override
                            public void onSuccess(HashMap<String, User> userMap) {
                                for (String adminId : userMap.keySet()) {
                                    Notification notification = createNotificationForOwner(adminId, currentSubcategory, oldName);
                                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d(TAG, "Notification sent to owner: " + adminId);
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e(TAG, "Error sending notification to admin: " + adminId, e);
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.e(TAG, "Error getting admins", e);
                            }
                        });

                    }
                    @Override
                    public void onFailure(Exception e) {
                        Log.e("CategorySave", "Error updating subcategory to database: " + e.getMessage());
                    }
                });
            }
        });
    }

    private Notification createNotificationForOwner(String ownerId, Subcategory subcategory, String oldName) {
        String title = "The category has been changed";
        String content = String.format("The subcategory %s has been changed to %s.",
                oldName,
                subcategory.getName());

        return new Notification("", title, content, ownerId);
    }

    private void clearFieldsAndResetButton(@NonNull Button addButton) {
        TextInputEditText subcategoryNameEditText = activity.findViewById(R.id.subcategory_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);

        subcategoryNameEditText.setText("");
        descriptionEditText.setText("");
        addButton.setText(R.string.add);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewSubcategory();
            }
        });
    }

    private void addNewSubcategory() {
        TextInputEditText subcategoryNameEditText = activity.findViewById(R.id.subcategory_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
        String subcategoryName = Objects.requireNonNull(subcategoryNameEditText.getText()).toString();
        String description = Objects.requireNonNull(descriptionEditText.getText()).toString();
        String selectedType = typeSpinner.getSelectedItem().toString();

        Subcategory subcategory = new Subcategory(null, subcategoryName, description, SubcategoryType.valueOf(selectedType), categoryId);

        subcategoriesOperations.save(subcategory, new SubcategoriesOperations.SaveSubcategoryListener() {
            @Override
            public void onSuccess(String subcategoryId) {
                Log.e("SubcategorySave", "Subcategory saved successful: " + subcategoryId);
                subcategoryList.add(subcategory);
                notifyDataSetChanged();

                subcategoryNameEditText.setText("");
                descriptionEditText.setText("");
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("SubcategorySave", "Error saving subcategory to database: " + e.getMessage());
            }
        });


    }
}
