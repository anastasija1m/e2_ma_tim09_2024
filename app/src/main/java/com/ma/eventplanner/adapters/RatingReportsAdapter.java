package com.ma.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.CompanyRatingOperations;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.RatingReportOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.RatingReport;
import com.ma.eventplanner.model.RatingReportStatus;

import java.util.ArrayList;

public class RatingReportsAdapter extends ArrayAdapter<RatingReport> {

    private final ArrayList<RatingReport> aRatingReports;

    public RatingReportsAdapter(Context context, ArrayList<RatingReport> ratingReports) {
        super(context, R.layout.rating_report_card, ratingReports);
        this.aRatingReports = ratingReports;
    }

    @Override
    public int getCount() {
        return aRatingReports.size();
    }

    @Nullable
    @Override
    public RatingReport getItem(int position) {
        return aRatingReports.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        RatingReport ratingReport = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.rating_report_card,
                    parent, false);
        }

        TextView ownerEmail = convertView.findViewById(R.id.owner_email);
        TextView ratingDate = convertView.findViewById(R.id.rating_date);
        TextView ratingComment = convertView.findViewById(R.id.rating_comment);
        TextView ratingStatus = convertView.findViewById(R.id.rating_status);
        Button acceptButton = convertView.findViewById(R.id.accept_button);
        Button rejectButton = convertView.findViewById(R.id.reject_button);

        if (ratingReport != null) {
            ownerEmail.setText(ratingReport.getOwnerEmail());
            ratingDate.setText(ratingReport.getDate().toString());
            ratingComment.setText(ratingReport.getReportReason());
            ratingStatus.setText(ratingReport.getStatus().toString());
        }

        acceptButton.setOnClickListener(v -> {
            CompanyRatingOperations companyRatingOperations = new CompanyRatingOperations();
            companyRatingOperations.delete(ratingReport.getRatingId(), new CompanyRatingOperations.DeleteCompanyRatingListener() {
                @Override
                public void onSuccess() {
                    Log.d(TAG, "Company rating deleted successfully");
                    ratingReport.setStatus(RatingReportStatus.ACCEPTED);
                    RatingReportOperations ratingReportOperations = new RatingReportOperations();
                    ratingReportOperations.update(ratingReport, new RatingReportOperations.UpdateRatingReportListener() {
                        @Override
                        public void onSuccess() {
                            Log.d(TAG, "Rating report updated successfully");
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error updating rating report", e);
                        }
                    });
                }

                @Override
                public void onFailure(Exception e) {
                    Log.e(TAG, "Error deleting company rating", e);
                }
            });
        });

        rejectButton.setOnClickListener(v -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle("Reason for Rejection");

            final EditText input = new EditText(getContext());
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

            builder.setPositiveButton("OK", (dialog, which) -> {
                String rejectionReason = input.getText().toString();
                if (!TextUtils.isEmpty(rejectionReason)) {
                    // First, get the owner's ID by email
                    UsersOperations usersOperations = new UsersOperations();
                    usersOperations.getOwnerIdByEmail(ratingReport.getOwnerEmail(), new UsersOperations.OnGetOwnerByEmailListener() {
                        @Override
                        public void onSuccess(String ownerId) {
                            // Once you have the owner's ID, proceed to update the rating report and create the notification
                            ratingReport.setStatus(RatingReportStatus.REJECTED);
                            RatingReportOperations ratingReportOperations = new RatingReportOperations();
                            ratingReportOperations.update(ratingReport, new RatingReportOperations.UpdateRatingReportListener() {
                                @Override
                                public void onSuccess() {
                                    Log.d(TAG, "Rating report updated successfully");
                                    notifyDataSetChanged();
                                    // Now create the notification
                                    Notification notification = createNotification(ownerId, rejectionReason);
                                    NotificationsOperations notificationsOperations = new NotificationsOperations();
                                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d(TAG, "Notification sent");
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e(TAG, "Error sending notification", e);
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    Log.e(TAG, "Error updating rating report", e);
                                }
                            });
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error getting owner ID", e);
                        }
                    });
                }
            });
            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

            builder.create().show();
        });


        return convertView;
    }

    private Notification createNotification(String ownerId, String rejectionReason) {
        String title = "Admin has rejected your report!";
        String content = String.format("Admin has rejected your review report. Reason: %s.", rejectionReason);

        return new Notification("", title, content, ownerId);
    }
}
