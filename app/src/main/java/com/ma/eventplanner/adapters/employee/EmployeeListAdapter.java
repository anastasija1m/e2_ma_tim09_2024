package com.ma.eventplanner.adapters.employee;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.ImagesOperations;
import com.ma.eventplanner.model.Employee;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class EmployeeListAdapter extends ArrayAdapter<Employee> {

    private final HashMap<String, Employee> aEmployeeMap;
    private final List<Employee> aEmployees;
    private final ImagesOperations imagesOperations;

    public EmployeeListAdapter(Context context, HashMap<String, Employee> employeeMap) {
        super(context, R.layout.employee_card, new ArrayList<>(employeeMap.values()));
        aEmployeeMap = employeeMap;
        aEmployees = new ArrayList<>(employeeMap.values());
        imagesOperations = new ImagesOperations();
    }

    @Override
    public int getCount() {
        return aEmployees.size();
    }

    @Nullable
    @Override
    public Employee getItem(int position) {
        return aEmployees.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Employee employee = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.employee_card,
                    parent, false);
        }
        LinearLayout employeeCard = convertView.findViewById(R.id.employee_card_item);
        ImageView imageView = convertView.findViewById(R.id.employee_image);
        TextView employeeName = convertView.findViewById(R.id.employee_name);
        TextView employeeSurname = convertView.findViewById(R.id.employee_surname);
        TextView employeeEmail = convertView.findViewById(R.id.employee_email);
        TextView employeePhoneNumber = convertView.findViewById(R.id.employee_phone_number);

        if(employee != null){
            String imagePath = "profile-images/";
            String imageName = employee.getImageName();
            imagesOperations.get(imagePath, imageName)
                    .addOnSuccessListener(imageBytes -> {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                        imageView.setImageBitmap(bitmap);
                    })
                    .addOnFailureListener(exception -> Log.e(TAG, "Error loading image: " + exception.getMessage()));

            employeeName.setText(employee.getName());
            employeeSurname.setText(employee.getSurname());
            employeeEmail.setText(employee.getEmail());
            employeePhoneNumber.setText(employee.getPhoneNumber());
            employeeCard.setOnClickListener(v -> {
                Toast.makeText(getContext(), "Clicked: " + employee.getName() + " " + employee.getSurname()
                        , Toast.LENGTH_SHORT).show();

                NavController navController = Navigation.findNavController((HomeActivity) getContext(), R.id.fragment_nav_content_main);

                Bundle bundle = new Bundle();
                bundle.putParcelable("employee", employee);
                bundle.putString("employeeUid", getKeyByValue(aEmployeeMap, employee));
                navController.navigate(R.id.nav_employee_details, bundle);
            });
        }

        return convertView;
    }

    private String getKeyByValue(HashMap<String, Employee> map, Employee value) {
        for (String key : map.keySet()) {
            if (Objects.equals(map.get(key), value)) {
                return key;
            }
        }
        return null;
    }
}