package com.ma.eventplanner.adapters.employee;


import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.AvailabilitySlot;

import java.util.List;

public class EmployeeAvailabilityScheduleAdapter extends RecyclerView.Adapter<EmployeeAvailabilityScheduleAdapter.ViewHolder> {

    private final List<AvailabilitySlot> availabilitySlots;

    public EmployeeAvailabilityScheduleAdapter(List<AvailabilitySlot> availabilitySlots) {
        this.availabilitySlots = availabilitySlots;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        AvailabilitySlot slot = availabilitySlots.get(position);
        holder.startTimeTextView.setText(slot.getStartTime());
        holder.endTimeTextView.setText(slot.getEndTime());
        holder.statusTextView.setText(slot.isAvailable() ? "Available" : "Unavailable");
    }

    @Override
    public int getItemCount() {
        return availabilitySlots.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView startTimeTextView;
        TextView endTimeTextView;
        TextView statusTextView;

        ViewHolder(View itemView) {
            super(itemView);
            startTimeTextView = itemView.findViewById(R.id.start_time);
            endTimeTextView = itemView.findViewById(R.id.end_time);
            statusTextView = itemView.findViewById(R.id.status);
        }
    }
}

