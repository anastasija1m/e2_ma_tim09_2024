package com.ma.eventplanner.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.ProductRequest;

import java.util.List;

public class ProductRequestAdapter extends BaseAdapter {
    private Context context;
    private List<ProductRequest> productRequests;

    private final Activity activity;

    public ProductRequestAdapter(Context context, List<ProductRequest> productRequests, Activity activity) {
        this.context = context;
        this.productRequests = productRequests;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return productRequests.size();
    }

    @Override
    public Object getItem(int position) {
        return productRequests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.product_request_card, parent, false);
            holder = new ViewHolder();
            holder.productNameTextView = convertView.findViewById(R.id.product_name_request);
            holder.productCategoryTextView = convertView.findViewById(R.id.product_category_name_request);
            holder.productPriceTextView = convertView.findViewById(R.id.product_price_request);
            holder.productSubcategoryTextView = convertView.findViewById(R.id.product_subcategory_name_request);
            holder.detailsButton = convertView.findViewById(R.id.product_request_details_button);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ProductRequest productRequest = productRequests.get(position);

        holder.productNameTextView.setText(productRequest.getProduct().getName());
        //TODO get category by ID
        holder.productCategoryTextView.setText(productRequest.getProduct().getCategory().getName());
        holder.productPriceTextView.setText(String.valueOf(productRequest.getProduct().getPrice()));
        holder.productSubcategoryTextView.setText(productRequest.getRequestedSubcategory().getName());

        holder.detailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("selected_request_id", productRequest.getId());
                NavController navController = Navigation.findNavController(activity, R.id.fragment_nav_content_main);
                navController.navigate(R.id.nav_product_request_details, bundle);
            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView productNameTextView;
        TextView productCategoryTextView;
        TextView productPriceTextView;
        TextView productSubcategoryTextView;
        Button detailsButton;
    }
}