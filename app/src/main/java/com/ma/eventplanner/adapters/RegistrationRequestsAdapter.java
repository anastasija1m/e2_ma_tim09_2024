package com.ma.eventplanner.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.model.RegistrationRequest;
import com.ma.eventplanner.R;
import com.ma.eventplanner.model.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

public class RegistrationRequestsAdapter extends BaseAdapter {
    private Context context;
    private List<RegistrationRequest> requests;
    //private OnInfoClickListener onInfoClickListener;
    private final Activity activity;

    /*
    public void setOnInfoClickListener(OnInfoClickListener listener) {
        this.onInfoClickListener = listener;
    }
     */

    public RegistrationRequestsAdapter(Context context, List<RegistrationRequest> requests, Activity activity) {
        this.context = context;
        this.requests = new ArrayList<>(requests);
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return requests.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_reqistration_request_card, parent, false);
            holder = new ViewHolder();
            holder.ownerFirstNameTextView = convertView.findViewById(R.id.owner_first_name);
            holder.ownerLastNameTextView = convertView.findViewById(R.id.owner_last_name);
            holder.ownerEmailTextView = convertView.findViewById(R.id.owner_email);

            holder.companyNameTextView = convertView.findViewById(R.id.company_name);
            holder.companyEmailTextView = convertView.findViewById(R.id.company_email);

            holder.statusTextView = convertView.findViewById(R.id.reg_req_status);

            holder.infoButton = convertView.findViewById(R.id.info_button);


            RegistrationRequest registrationRequest = requests.get(position);

            holder.infoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("selected_request_id", registrationRequest.getId());
                    NavController navController = Navigation.findNavController(activity, R.id.fragment_nav_content_main);
                    navController.navigate(R.id.nav_registration_requests_details, bundle);
                }
            });


            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        RegistrationRequest request = requests.get(position);

        holder.ownerFirstNameTextView.setText(" " + request.getOwner().getFirstName());
        holder.ownerLastNameTextView.setText(" " + request.getOwner().getLastName());
        holder.ownerEmailTextView.setText(" " + request.getOwner().getEmail());

        holder.companyNameTextView.setText(" " + request.getCompany().getName());
        holder.companyEmailTextView.setText(" " + request.getCompany().getEmail());

        holder.statusTextView.setText(" " + request.getStatus().toString());


        return convertView;
    }

    public void refreshList(List<RegistrationRequest> updatedRequests) {
        this.requests = updatedRequests;
        notifyDataSetChanged();
    }

    public void updateData(List<RegistrationRequest> newRequests) {
        this.requests.clear();
        this.requests.addAll(newRequests);
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView ownerEmailTextView;
        TextView ownerFirstNameTextView;
        TextView ownerLastNameTextView;
        TextView companyNameTextView;
        TextView companyEmailTextView;
        TextView statusTextView;
        ImageButton infoButton;
    }

}