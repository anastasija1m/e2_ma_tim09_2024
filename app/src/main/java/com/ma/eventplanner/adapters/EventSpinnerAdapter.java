package com.ma.eventplanner.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ma.eventplanner.model.Event;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class EventSpinnerAdapter extends ArrayAdapter<Event> {

    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy.", Locale.getDefault());

    public EventSpinnerAdapter(Context context, List<Event> events) {
        super(context, android.R.layout.simple_spinner_item, events);
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView textView = view.findViewById(android.R.id.text1);
        Event event = getItem(position);
        if (event != null) {
            String formattedDate = DATE_FORMAT.format(new Date(event.getDate()));
            textView.setText(event.getName() + " - " + formattedDate);
        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        TextView textView = view.findViewById(android.R.id.text1);
        Event event = getItem(position);
        if (event != null) {
            String formattedDate = DATE_FORMAT.format(new Date(event.getDate()));
            textView.setText(event.getName() + " - " + formattedDate);
        }
        return view;
    }
}