package com.ma.eventplanner.adapters.employee;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.Employee;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class WeeklyCalendarAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<Date> mWeekDates;
    private final Employee mEmployee;
    private final String mEmployeeUid;

    public WeeklyCalendarAdapter(Context context, List<Date> weekDates, Employee employee, String employeeUid) {
        this.mContext = context;
        this.mWeekDates = weekDates;
        this.mEmployee = employee;
        this.mEmployeeUid = employeeUid;
    }

    @Override
    public int getCount() {
        return mWeekDates.size();
    }

    @Override
    public Object getItem(int position) {
        return mWeekDates.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            view = inflater.inflate(R.layout.item_day_card, parent, false);
        }

        TextView dayOfWeekTextView = view.findViewById(R.id.dayOfWeekTextView);
        TextView dayOfMonthTextView = view.findViewById(R.id.dayOfMonthTextView);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(mWeekDates.get(position));
        dayOfWeekTextView.setText(new SimpleDateFormat("EEE", Locale.getDefault()).format(calendar.getTime()));
        dayOfMonthTextView.setText(String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));

        view.setOnClickListener(v -> {
            Date selectedDate = mWeekDates.get(position);

            NavController navController = Navigation.findNavController(v);
            Bundle bundle = new Bundle();
            bundle.putSerializable("selectedDate", selectedDate);
            bundle.putParcelable("employee", mEmployee);
            bundle.putSerializable("employeeUid", mEmployeeUid);
            navController.navigate(R.id.nav_employee_event, bundle);
        });

        return view;
    }

    public void setWeekDates(List<Date> weekDates) {
        this.mWeekDates.clear();
        this.mWeekDates.addAll(weekDates);
        notifyDataSetChanged();
    }
}
