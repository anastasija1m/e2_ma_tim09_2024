package com.ma.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.model.Product;
import com.ma.eventplanner.model.Service;

import java.util.ArrayList;
import java.util.List;

public class ServiceListAdapter extends ArrayAdapter<Service> {

    private final List<Service> aServices;

    public ServiceListAdapter(Context context, ArrayList<Service> services) {
        super(context, R.layout.service_card, services);
        aServices = services;
    }

    @Override
    public int getCount() {
        return aServices.size();
    }

    @Nullable
    @Override
    public Service getItem(int position) {
        return aServices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Service service = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.service_card,
                    parent, false);
        }
        LinearLayout serviceCard = convertView.findViewById(R.id.service_card_item);
        ImageView imageView = convertView.findViewById(R.id.service_image);
        TextView productName = convertView.findViewById(R.id.service_name);
        TextView productCategory = convertView.findViewById(R.id.service_category);
        TextView productSubcategory = convertView.findViewById(R.id.service_subcategory);
        TextView productPrice = convertView.findViewById(R.id.service_price);
        if(service != null){
            imageView.setImageResource(service.getImages().get(0));
            productName.setText(service.getName());
            productCategory.setText(service.getCategory().getName());
            productSubcategory.setText(service.getSubcategory().getName());
            productPrice.setText(String.valueOf(service.getPrice()));
            serviceCard.setOnClickListener(v -> {
                Toast.makeText(getContext(), "Clicked: " + service.getName() , Toast.LENGTH_SHORT).show();

                NavController navController = Navigation.findNavController((HomeActivity) getContext(), R.id.fragment_nav_content_main);

                Bundle bundle = new Bundle();
                bundle.putParcelable("service", service);
                navController.navigate(R.id.nav_service_details, bundle);
            });
        }

        return convertView;
    }
}