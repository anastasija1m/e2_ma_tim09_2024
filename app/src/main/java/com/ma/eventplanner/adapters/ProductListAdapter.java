package com.ma.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductListAdapter extends ArrayAdapter<Product> {

    private final List<Product> aProducts;

    public ProductListAdapter(Context context, ArrayList<Product> products) {
        super(context, R.layout.product_card, products);
        aProducts = products;
    }

    @Override
    public int getCount() {
        return aProducts.size();
    }

    @Nullable
    @Override
    public Product getItem(int position) {
        return aProducts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Product product = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.product_card,
                    parent, false);
        }
        LinearLayout productCard = convertView.findViewById(R.id.product_card_item);
        ImageView imageView = convertView.findViewById(R.id.product_image);
        TextView productName = convertView.findViewById(R.id.product_name);
        TextView productCategory = convertView.findViewById(R.id.product_category);
        TextView productSubcategory = convertView.findViewById(R.id.product_subcategory);
        TextView productPrice = convertView.findViewById(R.id.product_price);
        if(product != null){
            imageView.setImageResource(product.getImages().get(0));
            productName.setText(product.getName());
            productCategory.setText(product.getCategory().getName());
            productSubcategory.setText(product.getSubcategory().getName());
            productPrice.setText(String.valueOf(product.getPrice()));
            productCard.setOnClickListener(v -> {
                Toast.makeText(getContext(), "Clicked: " + product.getName() , Toast.LENGTH_SHORT).show();

                NavController navController = Navigation.findNavController((HomeActivity) getContext(), R.id.fragment_nav_content_main);

                Bundle bundle = new Bundle();
                bundle.putParcelable("product", product);
                navController.navigate(R.id.nav_product_details, bundle);
            });
        }

        return convertView;
    }
}