package com.ma.eventplanner.adapters.employee;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.EmployeeEvent;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class EmployeeEventListAdapter extends ArrayAdapter<EmployeeEvent> {

    private final List<EmployeeEvent> aEmployeeEvents;
    private final Context mContext;

    public EmployeeEventListAdapter(Context context, ArrayList<EmployeeEvent> employeeEvents) {
        super(context, R.layout.item_event, employeeEvents);
        this.mContext = context;
        this.aEmployeeEvents = employeeEvents;
    }

    @Override
    public int getCount() {
        return aEmployeeEvents.size();
    }

    @Nullable
    @Override
    public EmployeeEvent getItem(int position) {
        return aEmployeeEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        EmployeeEvent employeeEvent = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_event,
                    parent, false);
        }

        TextView eventNameTextView = convertView.findViewById(R.id.event_name);
        TextView eventDateTextView = convertView.findViewById(R.id.event_date);
        TextView eventTimeTextView = convertView.findViewById(R.id.event_start_and_end_time);
        TextView eventStatusTextView = convertView.findViewById(R.id.event_status);

        eventNameTextView.setText(employeeEvent.getName());
        LocalDate localDate = employeeEvent.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String formattedDate = localDate.format(formatter);
        eventDateTextView.setText(formattedDate);
        eventTimeTextView.setText(employeeEvent.getStartTime() + " - " + employeeEvent.getEndTime());
        eventStatusTextView.setText(employeeEvent.getStatus().toString());

        return convertView;
    }
}

