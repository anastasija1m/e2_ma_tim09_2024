package com.ma.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.Subcategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PickSubcategoryAdapter extends ArrayAdapter<Subcategory> {
    private final Context context;
    private final List<Subcategory> subcategories;

    Map<String, Boolean> selectedSubcategoriesMap = new HashMap<>();
    List<String> selectedSubcategoriesIds = new ArrayList<>();

    public List<String> getSelectedSubcategoriesIds() {
        return selectedSubcategoriesIds;
    }

    public void deselectAllCheckboxes() {
        for (int i = 0; i < getCount(); i++) {
            View view = getView(i, null, null);
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            if (viewHolder != null) {
                viewHolder.checkBox.setChecked(false);
                Subcategory subcategory = subcategories.get(i);
                selectedSubcategoriesMap.remove(subcategory.getId());
                selectedSubcategoriesIds.remove(subcategory.getId());
            }
        }

        notifyDataSetChanged();
    }

    public void selectCheckboxesForCategories(List<String> categoryIds) {
        for (int i = 0; i < getCount(); i++) {
            View view = getView(i, null, null);
            ViewHolder viewHolder = (ViewHolder) view.getTag();
            if (viewHolder != null) {
                Subcategory subcategory = subcategories.get(i);
                if (categoryIds.contains(subcategory.getId())) {
                    viewHolder.checkBox.setChecked(true);
                    selectedSubcategoriesMap.put(subcategory.getId(), true);
                    selectedSubcategoriesIds.add(subcategory.getId());
                }
            }
        }
        notifyDataSetChanged();
    }

    public PickSubcategoryAdapter(Context context, List<Subcategory> subcategories) {
        super(context, R.layout.item_pick_subcategory_card, subcategories);
        this.context = context;
        this.subcategories = subcategories;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_pick_subcategory_card, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.subcategoryNameText = convertView.findViewById(R.id.subcategory_name_text);
            viewHolder.subcategoryTypeText = convertView.findViewById(R.id.subcategory_type_text);
            viewHolder.checkBox = convertView.findViewById(R.id.suggested_subcategory_checkbox);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Subcategory subcategory = subcategories.get(position);
        viewHolder.subcategoryNameText.setText(subcategory.getName());
        viewHolder.subcategoryTypeText.setText(subcategory.getType().toString());

        viewHolder.checkBox.setChecked(selectedSubcategoriesMap.containsKey(subcategory.getId()) && selectedSubcategoriesMap.get(subcategory.getId()));

        viewHolder.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {
            Subcategory chckedSubcategory = subcategories.get(position);
            if (isChecked) {
                selectedSubcategoriesIds.add(chckedSubcategory.getId());
            } else {
                selectedSubcategoriesIds.remove(chckedSubcategory.getId());
            }
        });


        return convertView;
    }

    static class ViewHolder {
        TextView subcategoryNameText;
        TextView subcategoryTypeText;
        CheckBox checkBox;
    }
}