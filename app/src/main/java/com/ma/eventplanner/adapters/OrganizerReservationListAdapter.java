package com.ma.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.ServiceReservationsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EmployeeEvent;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.ServiceReservation;
import com.ma.eventplanner.model.ServiceReservationStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;

public class OrganizerReservationListAdapter extends ArrayAdapter<ServiceReservation> {

    private final ArrayList<ServiceReservation> aServiceReservations;
    private final ServiceReservationsOperations reservationsOperations;
    private final EventOrganizer eventOrganizer;
    private Service mService = new Service();

    public OrganizerReservationListAdapter(Context context, ArrayList<ServiceReservation> serviceReservations, EventOrganizer eventOrganizer) {
        super(context, R.layout.organizer_reservation_card, serviceReservations);
        this.aServiceReservations = serviceReservations;
        this.eventOrganizer = eventOrganizer;
        reservationsOperations = new ServiceReservationsOperations();
    }

    @Override
    public int getCount() {
        return aServiceReservations.size();
    }

    @Nullable
    @Override
    public ServiceReservation getItem(int position) {
        return aServiceReservations.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ServiceReservation serviceReservation = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.organizer_reservation_card,
                    parent, false);
        }

        TextView textStartTime = convertView.findViewById(R.id.text_start_time);
        TextView textEndTime = convertView.findViewById(R.id.text_end_time);
        TextView textDate = convertView.findViewById(R.id.text_date);
        TextView textService = convertView.findViewById(R.id.text_service);
        TextView textEvent = convertView.findViewById(R.id.text_event);
        TextView textReservationStatus = convertView.findViewById(R.id.text_reservation_status);
        TextView textEmployeeName = convertView.findViewById(R.id.text_employee_name);
        TextView textEmployeeSurname = convertView.findViewById(R.id.text_employee_surname);
        Button cancelButton = convertView.findViewById(R.id.button_cancel);

        if (serviceReservation != null) {
            textStartTime.setText(serviceReservation.getStartTime());
            textEndTime.setText(serviceReservation.getEndTime());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String formattedDate = sdf.format(serviceReservation.getDate());
            textDate.setText(formattedDate);
            textReservationStatus.setText(serviceReservation.getServiceReservationStatus().toString());

            getService(serviceReservation.getServiceId(), textService);
            getEvent(serviceReservation.getEventId(), textEvent);
            getEmployee(serviceReservation.getEmployeeEmail(), textEmployeeName, textEmployeeSurname);

            cancelButton.setEnabled(serviceReservation.getServiceReservationStatus() == ServiceReservationStatus.NEW || serviceReservation.getServiceReservationStatus() == ServiceReservationStatus.ACCEPTED);
        }

        cancelButton.setOnClickListener(v -> {
            if (serviceReservation != null) {
                int cancellationDeadlineDays = mService.getCancellationDeadline();

                long reservationTimeMillis = serviceReservation.getDate().getTime();
                long cancellationDeadlineMillis = cancellationDeadlineDays * 24 * 60 * 60 * 1000L;
                long cancellationDeadlineDateMillis = reservationTimeMillis - cancellationDeadlineMillis;

                long currentTimeMillis = System.currentTimeMillis();

                if (currentTimeMillis <= cancellationDeadlineDateMillis) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Confirmation");
                    builder.setMessage("Are you sure you want to cancel this reservation?");
                    builder.setPositiveButton("Yes", (dialog, which) -> {
                        serviceReservation.setServiceReservationStatus(ServiceReservationStatus.CANCELLED_ORGANIZER);
                        reservationsOperations.update(serviceReservation, new ServiceReservationsOperations.UpdateServiceReservationListener() {
                            @Override
                            public void onSuccess() {
                                Log.e(TAG, "Successfully cancelled reservation.");
                                removeReservationFromEmployeeEvents(serviceReservation);
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.e(TAG, "Error cancelling reservation: ", e);
                            }
                        });
                        notifyDataSetChanged();
                    });
                    builder.setNegativeButton("No", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    Log.i(TAG, "Cancellation deadline has passed. Cannot cancel reservation.");
                    Toast.makeText(this.getContext(), "Cancellation deadline has passed. Cannot cancel reservation.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return convertView;
    }

    private void removeReservationFromEmployeeEvents(ServiceReservation serviceReservation) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEmployeeByEmail(serviceReservation.getEmployeeEmail(), new UsersOperations.OnGetEmployeeByEmailListener() {
            @Override
            public void onSuccess(Employee employee) {
                boolean eventRemoved = false;

                if (serviceReservation.getServiceReservationStatus() == ServiceReservationStatus.ACCEPTED) {
                    for (Iterator<EmployeeEvent> iterator = employee.getEvents().iterator(); iterator.hasNext(); ) {
                        EmployeeEvent event = iterator.next();
                        if (event.getDate().equals(serviceReservation.getDate())
                                && event.getStartTime().equals(serviceReservation.getStartTime())
                                && event.getEndTime().equals(serviceReservation.getEndTime())) {
                            iterator.remove();
                            eventRemoved = true;
                            break;
                        }
                    }
                }

                usersOperations.updateEmployee(employee.getEmail(), employee, new UsersOperations.OnUpdateEmployeeListener() {
                    @Override
                    public void onSuccess() {
                        Log.i(TAG, "Employee updated successfully.");
                        sendNotification(employee);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.e(TAG, "Error updating employee: ", e);
                    }
                });

                if (!eventRemoved && serviceReservation.getServiceReservationStatus() == ServiceReservationStatus.ACCEPTED) {
                    Log.i(TAG, "No matching event found for removal.");
                }
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching employee for reservation: ", e);
            }
        });
    }

    private void sendNotification(Employee employee) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEmployeeIdByOrganizerEmail(employee.getEmail(), new UsersOperations.OnGetUidListener() {
            @Override
            public void onSuccess(String employeeId) {
                Log.d(TAG, "Employee ID: " + employeeId);
                Notification notification = createNotification(employeeId);
                NotificationsOperations notificationsOperations = new NotificationsOperations();
                notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Notification sent");
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.e(TAG, "Error sending notification", e);
                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting organizer ID: ", e);
            }
        });
    }

    private Notification createNotification(String employeeId) {
        String title = "Service reservation has been rejected";
        String content = String.format("Reserved service for %s %s has been cancelled.", eventOrganizer.getFirstName(), eventOrganizer.getLastName());

        return new Notification("", title, content, employeeId);
    }


    private void getService(String serviceId, TextView textService) {
        ServicesOperations serviceOperations = new ServicesOperations();
        serviceOperations.getById(serviceId, new ServicesOperations.GetServiceByIdListener() {
            @Override
            public void onSuccess(Service service) {
                textService.setText(service.getName());
                mService = service;
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting service name: ", e);
            }
        });
    }

    private void getEvent(String eventId, TextView textEvent) {
        EventsOperations eventsOperations = new EventsOperations();
        eventsOperations.getById(eventId, new EventsOperations.GetEventByIdListener<Event>() {
            @Override
            public void onSuccess(Event result) {
                textEvent.setText(result.getName());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting event name: ", e);
            }
        });
    }

    private void getEmployee(String employeeEmail, TextView textEmployeeName, TextView textEmployeeSurname) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEmployeeByEmail(employeeEmail, new UsersOperations.OnGetEmployeeByEmailListener() {
            @Override
            public void onSuccess(Employee employee) {
                textEmployeeName.setText(employee.getName());
                textEmployeeSurname.setText(employee.getSurname());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching employee for reservation: ", e);
            }
        });
    }
}
