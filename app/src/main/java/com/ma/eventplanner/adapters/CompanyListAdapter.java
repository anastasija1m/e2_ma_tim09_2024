package com.ma.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.ImagesOperations;
import com.ma.eventplanner.model.Company;

import java.util.ArrayList;

public class CompanyListAdapter extends ArrayAdapter<Company> {

    private final ArrayList<Company> aCompanies;
    private final ImagesOperations imagesOperations;

    public CompanyListAdapter(Context context, ArrayList<Company> companies) {
        super(context, R.layout.company_card, companies);
        this.aCompanies = companies;
        imagesOperations = new ImagesOperations();
    }

    @Override
    public int getCount() {
        return aCompanies.size();
    }

    @Nullable
    @Override
    public Company getItem(int position) {
        return aCompanies.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Company company = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.company_card,
                    parent, false);
        }

        LinearLayout companyCard = convertView.findViewById(R.id.company_card_item);
        ImageView imageView = convertView.findViewById(R.id.company_image);
        TextView companyName = convertView.findViewById(R.id.company_name);
        TextView companyDescription = convertView.findViewById(R.id.company_description);
        TextView companyPhone = convertView.findViewById(R.id.company_phone_number);

        if (company != null) {
            String imagePath = "profile-images/";
            String imageName = company.getImageName();
            imagesOperations.get(imagePath, imageName)
                    .addOnSuccessListener(imageBytes -> {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                        imageView.setImageBitmap(bitmap);
                    })
                    .addOnFailureListener(exception -> Log.e(TAG, "Error loading image: " + exception.getMessage()));

            companyName.setText(company.getName());
            companyDescription.setText(company.getDescription());
            companyPhone.setText(company.getPhoneNumber());

            companyCard.setOnClickListener(v -> {
                Toast.makeText(getContext(), "Clicked: " + company.getName()
                        , Toast.LENGTH_SHORT).show();

                NavController navController = Navigation.findNavController((HomeActivity) getContext(), R.id.fragment_nav_content_main);

                Bundle bundle = new Bundle();
                bundle.putParcelable("company", company);
                navController.navigate(R.id.nav_company_details, bundle);
            });
        }

        return convertView;
    }
}
