package com.ma.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.model.Bundle;

import java.util.ArrayList;
import java.util.List;

public class BundleListAdapter extends ArrayAdapter<Bundle> {

    private final List<Bundle> aBundles;

    public BundleListAdapter(Context context, ArrayList<Bundle> bundles) {
        super(context, R.layout.bundle_card, bundles);
        aBundles = bundles;
    }

    @Override
    public int getCount() {
        return aBundles.size();
    }

    @Nullable
    @Override
    public Bundle getItem(int position) {
        return aBundles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Bundle bundle = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.bundle_card,
                    parent, false);
        }
        LinearLayout bundleCard = convertView.findViewById(R.id.bundle_card_item);
        ImageView imageView = convertView.findViewById(R.id.bundle_image);
        TextView productName = convertView.findViewById(R.id.bundle_name);
        TextView productCategory = convertView.findViewById(R.id.bundle_category);
        TextView productPrice = convertView.findViewById(R.id.bundle_price);
        if(bundle != null){
            imageView.setImageResource(bundle.getImages().get(0));
            productName.setText(bundle.getName());
            productCategory.setText(bundle.getCategory().getName());
            productPrice.setText(String.valueOf(bundle.getPrice()));
            bundleCard.setOnClickListener(v -> {
                Toast.makeText(getContext(), "Clicked: " + bundle.getName() , Toast.LENGTH_SHORT).show();

                NavController navController = Navigation.findNavController((HomeActivity) getContext(), R.id.fragment_nav_content_main);

                android.os.Bundle bundleOs = new android.os.Bundle();
                bundleOs.putParcelable("bundle", bundle);
                navController.navigate(R.id.nav_bundle_details, bundleOs);
            });
        }

        return convertView;
    }
}