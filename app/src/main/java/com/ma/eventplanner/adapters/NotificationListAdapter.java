package com.ma.eventplanner.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.Notification;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class NotificationListAdapter extends ArrayAdapter<Notification> {

    private final Context context;
    private final List<Notification> notifications;
    private View.OnClickListener markAsReadClickListener;


    public void setMarkAsReadClickListener(View.OnClickListener listener) {
        this.markAsReadClickListener = listener;
    }
    public NotificationListAdapter(@NonNull Context context, List<Notification> notifications) {
        super(context, R.layout.item_notification_card, notifications);
        this.context = context;
        this.notifications = notifications;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rowView = convertView;
        ViewHolder viewHolder;

        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);
            rowView = inflater.inflate(R.layout.item_notification_card, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.titleTextView = rowView.findViewById(R.id.notification_title);
            viewHolder.contentTextView = rowView.findViewById(R.id.notification_content);
            viewHolder.dateTextView = rowView.findViewById(R.id.notification_date);
            viewHolder.markAsReadButton = rowView.findViewById(R.id.mark_as_read_button);

            rowView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) rowView.getTag();
        }


        Notification notification = notifications.get(position);
        viewHolder.titleTextView.setText(notification.getTitle());
        viewHolder.contentTextView.setText(notification.getContent());

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
        String formattedDate = dateFormat.format(notification.getSent());
        viewHolder.dateTextView.setText(formattedDate);

        if (notification.getIsRead()) {
            viewHolder.markAsReadButton.setVisibility(View.GONE);
        } else {
            viewHolder.markAsReadButton.setVisibility(View.VISIBLE);

            viewHolder.markAsReadButton.setOnClickListener(markAsReadClickListener);
            viewHolder.markAsReadButton.setTag(notification);
        }


        return rowView;
    }

    static class ViewHolder {
        TextView titleTextView;
        TextView contentTextView;
        TextView dateTextView;
        ImageButton markAsReadButton;
    }


}