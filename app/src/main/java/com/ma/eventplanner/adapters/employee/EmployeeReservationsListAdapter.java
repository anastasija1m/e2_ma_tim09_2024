package com.ma.eventplanner.adapters.employee;

import static android.content.ContentValues.TAG;

import android.app.AlertDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventsOperations;
import com.ma.eventplanner.database.NotificationsOperations;
import com.ma.eventplanner.database.ServiceReservationsOperations;
import com.ma.eventplanner.database.ServicesOperations;
import com.ma.eventplanner.database.UsersOperations;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EmployeeEvent;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.EventStatus;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.Service;
import com.ma.eventplanner.model.ServiceReservation;
import com.ma.eventplanner.model.ServiceReservationStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class EmployeeReservationsListAdapter extends ArrayAdapter<ServiceReservation> {

    private final ArrayList<ServiceReservation> aServiceReservations;
    private final ServiceReservationsOperations reservationsOperations;
    private final Employee loggedInEmployee;

    public EmployeeReservationsListAdapter(Context context, ArrayList<ServiceReservation> serviceReservations, Employee loggedInEmployee) {
        super(context, R.layout.employee_reservation_card, serviceReservations);
        this.aServiceReservations = serviceReservations;
        this.loggedInEmployee = loggedInEmployee;
        reservationsOperations = new ServiceReservationsOperations();
    }

    @Override
    public int getCount() {
        return aServiceReservations.size();
    }

    @Nullable
    @Override
    public ServiceReservation getItem(int position) {
        return aServiceReservations.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ServiceReservation serviceReservation = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.employee_reservation_card,
                    parent, false);
        }
        TextView textStartTime = convertView.findViewById(R.id.text_start_time);
        TextView textEndTime = convertView.findViewById(R.id.text_end_time);
        TextView textDate = convertView.findViewById(R.id.text_date);
        TextView textOrganizer = convertView.findViewById(R.id.text_organizer);
        TextView textService = convertView.findViewById(R.id.text_service);
        TextView textEvent = convertView.findViewById(R.id.text_event);
        TextView textReservationStatus = convertView.findViewById(R.id.text_reservation_status);
        TextView textOrganizerName = convertView.findViewById(R.id.text_organizer_name);
        TextView textOrganizerSurname = convertView.findViewById(R.id.text_organizer_surname);
        Button acceptButton = convertView.findViewById(R.id.button_accept);
        Button rejectButton = convertView.findViewById(R.id.button_reject);

        if (serviceReservation != null) {
            textStartTime.setText(serviceReservation.getStartTime());
            textEndTime.setText(serviceReservation.getEndTime());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String formattedDate = sdf.format(serviceReservation.getDate());
            textDate.setText(formattedDate);
            textOrganizer.setText(serviceReservation.getOrganizerEmail());
            textReservationStatus.setText(serviceReservation.getServiceReservationStatus().toString());

            getService(serviceReservation.getServiceId(), textService);
            getEvent(serviceReservation.getEventId(), textEvent);
            getOrganizer(serviceReservation.getOrganizerEmail(), textOrganizerName, textOrganizerSurname);

            if (serviceReservation.getServiceReservationStatus() == ServiceReservationStatus.NEW) {
                acceptButton.setEnabled(true);
                rejectButton.setEnabled(true);
            } else {
                acceptButton.setEnabled(false);
                rejectButton.setEnabled(false);
            }
        }

        acceptButton.setOnClickListener(v -> {
            if (serviceReservation != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Confirmation");
                builder.setMessage("Are you sure you want to accept this reservation?");
                builder.setPositiveButton("Yes", (dialog, which) -> {
                    serviceReservation.setServiceReservationStatus(ServiceReservationStatus.ACCEPTED);
                    Date date = new Date();
                    serviceReservation.setStatusModifiedDate(date);
                    reservationsOperations.update(serviceReservation, new ServiceReservationsOperations.UpdateServiceReservationListener() {
                        @Override
                        public void onSuccess() {
                            Log.e(TAG, "Successfully accepted reservation.");
                            addReservationToEmployeeEvents(serviceReservation);
                            String organizerEmail = serviceReservation.getOrganizerEmail();
                            UsersOperations usersOperations = new UsersOperations();
                            usersOperations.getOrganizerIdByOrganizerEmail(organizerEmail, new UsersOperations.OnGetUidListener() {
                                @Override
                                public void onSuccess(String organizerId) {
                                    Log.d(TAG, "Organizer ID: " + organizerId);
                                    Notification notification = createNotification(organizerId);
                                    NotificationsOperations notificationsOperations = new NotificationsOperations();
                                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d(TAG, "Notification sent");
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e(TAG, "Error sending notification", e);
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    Log.e(TAG, "Error getting organizer ID: ", e);
                                }
                            });

                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error accepting reservation: ", e);
                        }
                    });
                    notifyDataSetChanged();
                });
                builder.setNegativeButton("No", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        rejectButton.setOnClickListener(v -> {
            if (serviceReservation != null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Confirmation");
                builder.setMessage("Are you sure you want to reject this reservation?");
                builder.setPositiveButton("Yes", (dialog, which) -> {
                    serviceReservation.setServiceReservationStatus(ServiceReservationStatus.CANCELLED_PUP);
                    Date date = new Date();
                    serviceReservation.setStatusModifiedDate(date);
                    reservationsOperations.update(serviceReservation, new ServiceReservationsOperations.UpdateServiceReservationListener() {
                        @Override
                        public void onSuccess() {
                            Log.e(TAG, "Successfully rejected reservation.");
                            String organizerEmail = serviceReservation.getOrganizerEmail();
                            UsersOperations usersOperations = new UsersOperations();
                            usersOperations.getOrganizerIdByOrganizerEmail(organizerEmail, new UsersOperations.OnGetUidListener() {
                                @Override
                                public void onSuccess(String organizerId) {
                                    Log.d(TAG, "Organizer ID: " + organizerId);
                                    Notification notification = createNotificationRejection(organizerId);
                                    NotificationsOperations notificationsOperations = new NotificationsOperations();
                                    notificationsOperations.save(notification, new NotificationsOperations.OnSaveSuccessListener() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d(TAG, "Notification sent");
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e(TAG, "Error sending notification", e);
                                        }
                                    });
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    Log.e(TAG, "Error getting organizer ID: ", e);
                                }
                            });
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error rejecting reservation: ", e);
                        }
                    });
                    notifyDataSetChanged();
                });
                builder.setNegativeButton("No", null);
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        return convertView;
    }

    private void getService(String serviceId, TextView textService) {
        ServicesOperations serviceOperations = new ServicesOperations();
        serviceOperations.getById(serviceId, new ServicesOperations.GetServiceByIdListener() {
            @Override
            public void onSuccess(Service service) {
                textService.setText(service.getName());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting service name: ", e);
            }
        });
    }

    private void getEvent(String eventId, TextView textEvent) {
        EventsOperations eventsOperations = new EventsOperations();
        eventsOperations.getById(eventId, new EventsOperations.GetEventByIdListener<Event>() {
            @Override
            public void onSuccess(Event result) {
                textEvent.setText(result.getName());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting event name: ", e);
            }
        });
    }

    private void getOrganizer(String organizerEmail, TextView textOrganizerName, TextView textOrganizerSurname) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getEventOrganizerByEmail(organizerEmail, new UsersOperations.OnGetEventOrganizerByEmailListener() {
            @Override
            public void onSuccess(EventOrganizer eventOrganizer) {
                textOrganizerName.setText(eventOrganizer.getFirstName());
                textOrganizerSurname.setText(eventOrganizer.getLastName());
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error fetching employee for reservation: ", e);
            }
        });
    }

    private void addReservationToEmployeeEvents(ServiceReservation serviceReservation) {
        ServicesOperations serviceOperations = new ServicesOperations();
        serviceOperations.getById(serviceReservation.getServiceId(), new ServicesOperations.GetServiceByIdListener() {
            @Override
            public void onSuccess(Service service) {
                EmployeeEvent event = new EmployeeEvent(service.getName(), EventStatus.RESERVED, serviceReservation.getDate(), serviceReservation.getStartTime(), serviceReservation.getEndTime());
                loggedInEmployee.addEvent(event);
                UsersOperations usersOperations = new UsersOperations();
                usersOperations.updateEmployee(loggedInEmployee.getEmail(), loggedInEmployee, new UsersOperations.OnUpdateEmployeeListener() {
                    @Override
                    public void onSuccess() {
                        Log.d(TAG, "Employee event list updated successfully");
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.e(TAG, "Failed to update employee event list: ", e);
                    }
                });
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Error getting service: ", e);
            }
        });
    }

    private Notification createNotificationRejection(String organizerId) {
        String title = "Service reservation has been rejected. Rate this company!";
        String content = String.format("Reserved service for %s %s has been rejected. You have an option to rate this company!", loggedInEmployee.getName(), loggedInEmployee.getSurname());

        return new Notification("", title, content, organizerId);
    }

    private Notification createNotification(String organizerId) {
        String title = "Service reservation has been accepted. Rate this company!";
        String content = String.format("Reserved service for %s %s has been accepted. You have an option to rate this company!", loggedInEmployee.getName(), loggedInEmployee.getSurname());

        return new Notification("", title, content, organizerId);
    }
}