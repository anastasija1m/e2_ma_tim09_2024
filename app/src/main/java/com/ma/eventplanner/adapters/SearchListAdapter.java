package com.ma.eventplanner.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ma.eventplanner.R;
import com.ma.eventplanner.model.Item;

import java.util.ArrayList;
import java.util.List;

public class SearchListAdapter extends ArrayAdapter<Item> {

    private final List<Item> aItems;

    public SearchListAdapter(Context context, ArrayList<Item> items) {
        super(context, R.layout.item_item, items);
        aItems = items;
    }

    @Override
    public int getCount() {
        return aItems.size();
    }

    @Nullable
    @Override
    public Item getItem(int position) {
        return aItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Item item = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_item,
                    parent, false);
        }
        RelativeLayout itemCard = convertView.findViewById(R.id.item_card_item);
        ImageView imageView = convertView.findViewById(R.id.item_image);
        TextView itemName = convertView.findViewById(R.id.item_name);
        TextView itemCategory = convertView.findViewById(R.id.item_category);
        TextView itemSubcategory = convertView.findViewById(R.id.item_subcategory);

        if(item != null){
            if(item.getImages() != null)
                imageView.setImageResource(item.getImages().get(0));
            itemName.setText(item.getName());
            itemCategory.setText(item.getDescription());
            itemSubcategory.setText(String.valueOf(item.getPrice()));
            itemCard.setOnClickListener(v -> {
                Log.i("App", "Clicked: " + item.getName() + ", id: " +
                        item.getId().toString());
                Toast.makeText(getContext(), "Clicked: " + item.getName() + ", id: " +
                        item.getId().toString(), Toast.LENGTH_SHORT).show();
            });
        }

        return convertView;
    }
}
