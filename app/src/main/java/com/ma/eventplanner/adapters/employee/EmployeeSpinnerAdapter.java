package com.ma.eventplanner.adapters.employee;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ma.eventplanner.model.Employee;

import java.util.List;

public class EmployeeSpinnerAdapter extends ArrayAdapter<Employee> {

    public EmployeeSpinnerAdapter(Context context, List<Employee> employees) {
        super(context, android.R.layout.simple_spinner_item, employees);
        setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);
        TextView textView = view.findViewById(android.R.id.text1);
        Employee employee = getItem(position);
        if (employee != null) {
            textView.setText(employee.getName() + " " + employee.getSurname());
        }
        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        TextView textView = view.findViewById(android.R.id.text1);
        Employee employee = getItem(position);
        if (employee != null) {
            textView.setText(employee.getName() + " " + employee.getSurname());
        }
        return view;
    }
}
