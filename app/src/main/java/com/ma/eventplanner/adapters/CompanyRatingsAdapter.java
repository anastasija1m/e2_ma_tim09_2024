package com.ma.eventplanner.adapters;

import static android.content.ContentValues.TAG;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.database.CompanyRatingOperations;
import com.ma.eventplanner.database.RatingReportOperations;
import com.ma.eventplanner.model.CompanyRating;
import com.ma.eventplanner.model.RatingReport;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CompanyRatingsAdapter extends ArrayAdapter<CompanyRating> {

    private final ArrayList<CompanyRating> aCompanyRatings;
    private User mLoggedInUser;

    public CompanyRatingsAdapter(Context context, ArrayList<CompanyRating> companyRatings, User loggedInUser) {
        super(context, R.layout.company_rating_card, companyRatings);
        this.aCompanyRatings = companyRatings;
        mLoggedInUser = loggedInUser;
    }

    @Override
    public int getCount() {
        return aCompanyRatings.size();
    }

    @Nullable
    @Override
    public CompanyRating getItem(int position) {
        return aCompanyRatings.get(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        CompanyRating companyRating = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.company_rating_card,
                    parent, false);
        }

        LinearLayout companyCard = convertView.findViewById(R.id.company_card_item);
        TextView textOrganizerEmail = convertView.findViewById(R.id.organizer_email);
        TextView textRating = convertView.findViewById(R.id.rating);
        TextView textComment = convertView.findViewById(R.id.comment);
        TextView textDate = convertView.findViewById(R.id.date);

        if (companyRating != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String formattedDate = sdf.format(companyRating.getRatingDate());
            textDate.setText(formattedDate);
            textOrganizerEmail.setText(companyRating.getOrganizerEmail());
            textRating.setText(companyRating.getRating().toString());
            textComment.setText(companyRating.getComment());

            if (mLoggedInUser.getRole().equals(Role.OWNER)) {
                companyCard.setOnClickListener(v -> {
                    NavController navController = Navigation.findNavController((HomeActivity) getContext(), R.id.fragment_nav_content_main);

                    CompanyRatingOperations companyRatingOperations = new CompanyRatingOperations();
                    companyRatingOperations.getCompanyRatingIdByCompanyRating(companyRating, new CompanyRatingOperations.GetCompanyRatingIdListener() {
                        @Override
                        public void onSuccess(String companyRatingId) {
                            RatingReportOperations ratingReportOperations = new RatingReportOperations();
                            ratingReportOperations.getAllByOwnerEmail(mLoggedInUser.getEmail(), new RatingReportOperations.GetAllRatingReportsListener<RatingReport>() {
                                @Override
                                public void onSuccess(List<RatingReport> ratingReports) {
                                    for (RatingReport report : ratingReports) {
                                        if (report.getRatingId().equals(companyRatingId)) {
                                            Log.d(TAG, "Owner has already submitted a report for this company rating");
                                            return;
                                        }
                                    }

                                    Bundle bundle = new Bundle();
                                    bundle.putParcelable("loggedInUser", mLoggedInUser);
                                    bundle.putString("ratingId", companyRatingId);
                                    navController.navigate(R.id.nav_report_rating, bundle);
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    Log.e(TAG, "Error getting rating reports by owner email", e);
                                }
                            });
                        }

                        @Override
                        public void onFailure(Exception e) {
                            Log.e(TAG, "Error getting company rating ID", e);
                        }
                    });
                });
            }
        }

        return convertView;
    }
}
