package com.ma.eventplanner.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.ma.eventplanner.R;
import com.ma.eventplanner.database.EventTypesOperations;
import com.ma.eventplanner.model.EventType;

import java.util.List;
import java.util.Objects;

public class EventTypeListAdapter extends ArrayAdapter<EventType> {

    private final List<EventType> eventTypeList;
    private final Activity activity;
    private EventTypesOperations eventTypesOperations;
    private PickSubcategoryAdapter subcategoryAdapter;

    public EventTypeListAdapter(@NonNull Context context, int resource, @NonNull List<EventType> eventTypeList,
                                Activity activity, EventTypesOperations eventTypesOperations, PickSubcategoryAdapter subcategoryAdapter) {
        super(context, resource, eventTypeList);
        this.eventTypeList = eventTypeList;
        this.activity = activity;
        this.eventTypesOperations = eventTypesOperations;
        this.subcategoryAdapter = subcategoryAdapter;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            view = inflater.inflate(R.layout.create_list_item_event_type, parent, false);
        }

        final EventType currentEventType = eventTypeList.get(position);

        TextView eventTypeNameTextView = view.findViewById(R.id.event_type_name_text);
        eventTypeNameTextView.setText(currentEventType.getTypeName());

        ImageButton editButton = view.findViewById(R.id.edit_button);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleEditButtonClick(currentEventType);
            }
        });

        return view;
    }

    private void handleEditButtonClick(final EventType currentEventType) {
        populateFieldsWithEventTypeData(currentEventType);
        subcategoryAdapter.selectCheckboxesForCategories(currentEventType.getSuggestedSubcategoryIds());
        updateAddButtonForEdit();
        setAddButtonClickListenerForEdit(currentEventType);
    }

    private void populateFieldsWithEventTypeData(EventType eventType) {
        TextInputEditText typeNameEditText = activity.findViewById(R.id.type_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
        Switch isActiveSwitch = activity.findViewById(R.id.is_event_type_active);
        typeNameEditText.setText(eventType.getTypeName());
        descriptionEditText.setText(eventType.getDescription());
        isActiveSwitch.setChecked(eventType.getIsActive());
    }

    private void updateAddButtonForEdit() {
        Button addButton = activity.findViewById(R.id.add_button);
        addButton.setText(R.string.save);
    }

    private void setAddButtonClickListenerForEdit(final EventType currentEventType) {
        Button addButton = activity.findViewById(R.id.add_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText typeNameEditText = activity.findViewById(R.id.type_name);
                TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
                Switch isActiveSwitch = activity.findViewById(R.id.is_event_type_active);

                currentEventType.setTypeName(Objects.requireNonNull(typeNameEditText.getText()).toString());
                currentEventType.setDescription(Objects.requireNonNull(descriptionEditText.getText()).toString());
                currentEventType.setIsActive(isActiveSwitch.isChecked());
                currentEventType.setSuggestedSubcategoryIds(subcategoryAdapter.getSelectedSubcategoriesIds());

                eventTypesOperations.update(currentEventType, new EventTypesOperations.UpdateEventTypeListener() {
                    @Override
                    public void onSuccess() {
                        Log.e("EventTypeSave", "Event type updated successful");
                        notifyDataSetChanged();
                        clearFieldsAndResetButton(addButton);
                    }

                    @Override
                    public void onFailure(Exception e) {
                        Log.e("EventTypeSave", "Error updating event type to database: " + e.getMessage());
                    }
                });
            }
        });
    }

    private void clearFieldsAndResetButton(@NonNull Button addButton) {
        TextInputEditText typeNameEditText = activity.findViewById(R.id.type_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
        Switch isActiveSwitch = activity.findViewById(R.id.is_event_type_active);

        typeNameEditText.setText("");
        descriptionEditText.setText("");
        isActiveSwitch.setChecked(false);
        addButton.setText(R.string.add);

        subcategoryAdapter.deselectAllCheckboxes();

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNewEventType();
            }
        });
    }

    private void addNewEventType() {
        TextInputEditText typeNameEditText = activity.findViewById(R.id.type_name);
        TextInputEditText descriptionEditText = activity.findViewById(R.id.description);
        Switch isActiveSwitch = activity.findViewById(R.id.is_event_type_active);
        String typeName = Objects.requireNonNull(typeNameEditText.getText()).toString();
        String description = Objects.requireNonNull(descriptionEditText.getText()).toString();
        boolean isActive = isActiveSwitch.isChecked();

        List<String> selectedSubcategoriesIds = subcategoryAdapter.getSelectedSubcategoriesIds();
        EventType eventType = new EventType(null, typeName, description, selectedSubcategoriesIds, isActive);


        eventTypesOperations.save(eventType, new EventTypesOperations.SaveEventTypeListener() {
            @Override
            public void onSuccess(String categoryId) {
                Log.e("EventTypeSave", "Event type saved successful: " + categoryId);
                eventTypeList.add(eventType);
                notifyDataSetChanged();

                typeNameEditText.setText("");
                descriptionEditText.setText("");
                isActiveSwitch.setChecked(false);

                subcategoryAdapter.deselectAllCheckboxes();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("EventTypeSave", "Error saving event type to database: " + e.getMessage());
            }
        });
    }
}
