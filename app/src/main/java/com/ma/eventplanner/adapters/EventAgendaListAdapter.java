package com.ma.eventplanner.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.ma.eventplanner.R;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventAgendaListAdapter extends ArrayAdapter<EventActivity> {
    private final List<EventActivity> aActivities;

    public EventAgendaListAdapter(Context context, ArrayList<EventActivity> activities) {
        super(context, R.layout.item_agenda, activities);
        aActivities = activities;
    }

    @Override
    public int getCount() {
        return aActivities.size();
    }

    @Nullable
    @Override
    public EventActivity getItem(int position) {
        return aActivities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        EventActivity activity = getItem(position);
        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_agenda,
                    parent, false);
        }
        LinearLayout activityCard = convertView.findViewById(R.id.item_agenda_item);
        TextView happeningName = convertView.findViewById(R.id.item_a1);
        TextView happeningDesc = convertView.findViewById(R.id.item_a2);
        TextView happeningDate = convertView.findViewById(R.id.item_a3);
        TextView happeningLoc = convertView.findViewById(R.id.item_a4);

        if(activity != null){
            happeningName.setText(activity.getName());
            happeningDesc.setText(activity.getDescription());

            long timestamp = activity.getStartTime();
            Date date = new Date(timestamp);
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            String formattedDate = dateFormat.format(date);

            long timestamp1 = activity.getEndTime();
            Date date1 = new Date(timestamp1);
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH:mm");
            String formattedDate1 = dateFormat1.format(date1);

            happeningDate.setText(formattedDate + " - " + formattedDate1);
            happeningLoc.setText(activity.getLocation());
            activityCard.setOnClickListener(v -> {
                //TODO: otvori dijalog za edit
            });
        }

        return convertView;
    }
}
