package com.ma.eventplanner.adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.ma.eventplanner.fragments.events.EventAgendaFragment;
import com.ma.eventplanner.fragments.events.EventBudgetFragment;
import com.ma.eventplanner.fragments.events.EventGuestsFragment;
import com.ma.eventplanner.fragments.events.EventManagementPageFragment;
import com.ma.eventplanner.model.Event;

public class EventManagementViewPagerAdapter extends FragmentStateAdapter {
    private final Event event;
    public EventManagementViewPagerAdapter(@NonNull FragmentActivity fragmentActivity, Event event) {
        super(fragmentActivity);
        this.event = event;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position){
            case 1:
                return new EventGuestsFragment(event);
            case 2:
                return new EventBudgetFragment(event);
            default:
                return new EventAgendaFragment(event);
        }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
