package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventActivity;

import java.util.ArrayList;
import java.util.List;

public class EventAgendaOperations {
    private static final String TAG = "EventAgendaOperations";

    private final CollectionReference agendasCollection;

    private final FirebaseFirestore db;

    public EventAgendaOperations() {
        db = FirebaseFirestore.getInstance();
        this.agendasCollection = db.collection("agendas");
    }

    public void getAllByEventId(String eventId, EventAgendaOperations.GetAllAgendaListener<EventActivity> listener) {
        agendasCollection
                .whereEqualTo("eventId", eventId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<EventActivity> events = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        EventActivity event = documentSnapshot.toObject(EventActivity.class);
                        events.add(event);
                    }
                    listener.onSuccess(events);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, EventAgendaOperations.GetAgendaByIdListener<EventActivity> listener) {
        agendasCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        EventActivity event = documentSnapshot.toObject(EventActivity.class);
                        listener.onSuccess(event);
                    } else {
                        listener.onFailure(new Exception("EventActivity not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(EventActivity event, final EventAgendaOperations.SaveAgendaListener listener) {
        DocumentReference docRef = db.collection("agendas").document();
        String documentId = docRef.getId();
        event.setId(documentId);

        docRef.set(event)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "EventActivity saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving eventActivity", e);
                    listener.onFailure(e);
                });
    }

    public void update(EventActivity event, EventAgendaOperations.UpdateAgendaListener listener) {
        String eventId = event.getId();
        if (eventId == null) {
            Log.e(TAG, "EventActivity ID is null. Cannot update event.");
            listener.onFailure(new IllegalArgumentException("EventActivity ID is null"));
            return;
        }

        agendasCollection.document(eventId)
                .set(event)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "EventActivity updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating eventActivity", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, EventAgendaOperations.DeleteAgendaListener listener) {
        agendasCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllAgendaListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetAgendaByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveAgendaListener {
        void onSuccess(String eventId);
        void onFailure(Exception e);
    }

    public interface UpdateAgendaListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteAgendaListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
