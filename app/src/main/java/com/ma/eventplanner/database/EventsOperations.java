package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Event;

import java.util.ArrayList;
import java.util.List;

public class EventsOperations {
    private static final String TAG = "EventsOperations";

    private final CollectionReference eventsCollection;

    private final FirebaseFirestore db;

    public EventsOperations() {
        db = FirebaseFirestore.getInstance();
        this.eventsCollection = db.collection("events");
    }

    public void getAllByOrganizerId(String organizerId, EventsOperations.GetAllEventsListener<Event> listener) {
        eventsCollection
                .whereEqualTo("organizerId", organizerId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Event> events = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Event event = documentSnapshot.toObject(Event.class);
                        events.add(event);
                    }
                    listener.onSuccess(events);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, EventsOperations.GetEventByIdListener<Event> listener) {
        eventsCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        Event event = documentSnapshot.toObject(Event.class);
                        listener.onSuccess(event);
                    } else {
                        listener.onFailure(new Exception("Event not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(Event event, final EventsOperations.SaveEventListener listener) {
        DocumentReference docRef = db.collection("events").document();
        String documentId = docRef.getId();
        event.setId(documentId);

        docRef.set(event)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Event saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving event", e);
                    listener.onFailure(e);
                });
    }

    public void update(Event event, EventsOperations.UpdateEventListener listener) {
        String eventId = event.getId();
        if (eventId == null) {
            Log.e(TAG, "Event ID is null. Cannot update event.");
            listener.onFailure(new IllegalArgumentException("Event ID is null"));
            return;
        }

        eventsCollection.document(eventId)
                .set(event)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Event updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating event", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, EventsOperations.DeleteEventListener listener) {
        eventsCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllEventsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetEventByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveEventListener {
        void onSuccess(String eventId);
        void onFailure(Exception e);
    }

    public interface UpdateEventListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteEventListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
