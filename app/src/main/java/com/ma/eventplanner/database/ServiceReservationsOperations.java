package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.ServiceReservation;
import com.ma.eventplanner.model.ServiceReservationStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ServiceReservationsOperations {
    private static final String TAG = "ServiceReservationsOps";
    private final CollectionReference serviceReservationsCollection;
    private final FirebaseFirestore db;

    public ServiceReservationsOperations() {
        db = FirebaseFirestore.getInstance();
        this.serviceReservationsCollection = db.collection("service-reservations");
    }

    public void getAll(GetAllServiceReservationsListener<ServiceReservation> listener) {
        serviceReservationsCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ServiceReservation> serviceReservations = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ServiceReservation serviceReservation = documentSnapshot.toObject(ServiceReservation.class);
                        serviceReservations.add(serviceReservation);
                    }
                    listener.onSuccess(serviceReservations);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, GetServiceReservationByIdListener<ServiceReservation> listener) {
        serviceReservationsCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        ServiceReservation serviceReservation = documentSnapshot.toObject(ServiceReservation.class);
                        listener.onSuccess(serviceReservation);
                    } else {
                        listener.onFailure(new Exception("ServiceReservation not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(ServiceReservation serviceReservation, final SaveServiceReservationListener listener) {
        DocumentReference docRef = db.collection("service-reservations").document();
        String documentId = docRef.getId();
        serviceReservation.setId(documentId);

        docRef.set(serviceReservation)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "ServiceReservation saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving service reservation", e);
                    listener.onFailure(e);
                });
    }

    public void update(ServiceReservation serviceReservation, UpdateServiceReservationListener listener) {
        String serviceReservationId = serviceReservation.getId();
        if (serviceReservationId == null) {
            Log.e(TAG, "ServiceReservation ID is null. Cannot update service reservation.");
            listener.onFailure(new IllegalArgumentException("ServiceReservation ID is null"));
            return;
        }

        serviceReservationsCollection.document(serviceReservationId)
                .set(serviceReservation)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "ServiceReservation updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating service reservation", e);
                    listener.onFailure(e);
                });
    }

    public void getAllByEmployeeEmail(String employeeEmail, GetAllServiceReservationsListener<ServiceReservation> listener) {
        serviceReservationsCollection
                .whereEqualTo("employeeEmail", employeeEmail)
                .whereIn("serviceReservationStatus", Arrays.asList(
                        ServiceReservationStatus.NEW.name(),
                        ServiceReservationStatus.ACCEPTED.name(),
                        ServiceReservationStatus.CANCELLED_PUP.name(),
                        ServiceReservationStatus.CANCELLED_ORGANIZER.name(),
                        ServiceReservationStatus.COMPLETED.name()))
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ServiceReservation> serviceReservations = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ServiceReservation serviceReservation = documentSnapshot.toObject(ServiceReservation.class);
                        serviceReservations.add(serviceReservation);
                    }
                    listener.onSuccess(serviceReservations);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByOrganizerEmail(String organizerEmail, GetAllServiceReservationsListener<ServiceReservation> listener) {
        serviceReservationsCollection
                .whereEqualTo("organizerEmail", organizerEmail)
                .whereIn("serviceReservationStatus", Arrays.asList(
                        ServiceReservationStatus.NEW.name(),
                        ServiceReservationStatus.ACCEPTED.name(),
                        ServiceReservationStatus.CANCELLED_PUP.name(),
                        ServiceReservationStatus.CANCELLED_ORGANIZER.name(),
                        ServiceReservationStatus.COMPLETED.name()))
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ServiceReservation> serviceReservations = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ServiceReservation serviceReservation = documentSnapshot.toObject(ServiceReservation.class);
                        serviceReservations.add(serviceReservation);
                    }
                    listener.onSuccess(serviceReservations);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void delete(String id, DeleteServiceReservationListener listener) {
        serviceReservationsCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByCompanyId(String companyId, GetAllServiceReservationsListener<ServiceReservation> listener) {
        serviceReservationsCollection
                .whereEqualTo("companyId", companyId)
                .whereIn("serviceReservationStatus", Arrays.asList(
                        ServiceReservationStatus.CANCELLED_PUP.name(),
                        ServiceReservationStatus.COMPLETED.name()))
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ServiceReservation> serviceReservations = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ServiceReservation serviceReservation = documentSnapshot.toObject(ServiceReservation.class);
                        serviceReservations.add(serviceReservation);
                    }
                    listener.onSuccess(serviceReservations);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllServiceReservationsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetServiceReservationByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveServiceReservationListener {
        void onSuccess(String serviceReservationId);
        void onFailure(Exception e);
    }

    public interface UpdateServiceReservationListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteServiceReservationListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
