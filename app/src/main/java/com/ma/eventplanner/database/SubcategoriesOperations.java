package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Subcategory;
import com.ma.eventplanner.model.SubcategoryType;

import java.util.ArrayList;
import java.util.List;

public class SubcategoriesOperations {
    private static final String TAG = "SubcategoriesOperations";

    private final CollectionReference subcategoriesCollection;

    private final FirebaseFirestore db;

    public SubcategoriesOperations() {
        db = FirebaseFirestore.getInstance();
        this.subcategoriesCollection = db.collection("subcategories");
    }

    public void getAll(GetAllSubcategoriesListener<Subcategory> listener) {
        subcategoriesCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Subcategory> subcategories = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Subcategory subcategory = documentSnapshot.toObject(Subcategory.class);
                        subcategories.add(subcategory);
                    }
                    listener.onSuccess(subcategories);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByType(SubcategoryType type, GetAllSubcategoriesListener<Subcategory> listener) {
        subcategoriesCollection
                .whereEqualTo("type", type.toString())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Subcategory> subcategories = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Subcategory subcategory = documentSnapshot.toObject(Subcategory.class);
                        subcategories.add(subcategory);
                    }
                    listener.onSuccess(subcategories);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, GetSubcategoryByIdListener<Subcategory> listener) {
        subcategoriesCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        Subcategory subcategory = documentSnapshot.toObject(Subcategory.class);
                        listener.onSuccess(subcategory);
                    } else {
                        listener.onFailure(new Exception("Subcategory not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(Subcategory subcategory, final SaveSubcategoryListener listener) {
        DocumentReference docRef = db.collection("subcategories").document();
        String documentId = docRef.getId();
        subcategory.setId(documentId);

        docRef.set(subcategory)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Subcategory saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving subcategory", e);
                    listener.onFailure(e);
                });
    }

    public void update(Subcategory subcategory, UpdateSubcategoryListener listener) {
        String subcategoryId = subcategory.getId();
        if (subcategoryId == null) {
            Log.e(TAG, "Subcategory ID is null. Cannot update subcategory.");
            listener.onFailure(new IllegalArgumentException("Subcategory ID is null"));
            return;
        }

        subcategoriesCollection.document(subcategoryId)
                .set(subcategory)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Subcategory updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating subcategory", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, DeleteSubcategoryListener listener) {
        subcategoriesCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByCategoryId(String categoryId, GetAllSubcategoriesListener<Subcategory> listener) {
        subcategoriesCollection
                .whereEqualTo("categoryId", categoryId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Subcategory> subcategories = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Subcategory subcategory = documentSnapshot.toObject(Subcategory.class);
                        subcategories.add(subcategory);
                    }
                    listener.onSuccess(subcategories);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllSubcategoriesListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetSubcategoryByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveSubcategoryListener {
        void onSuccess(String subcategoryId);
        void onFailure(Exception e);
    }

    public interface UpdateSubcategoryListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteSubcategoryListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
