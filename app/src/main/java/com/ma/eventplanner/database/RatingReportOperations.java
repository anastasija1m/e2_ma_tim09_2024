package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.RatingReport;
import com.ma.eventplanner.model.RatingReportStatus;

import java.util.ArrayList;
import java.util.List;

public class RatingReportOperations {

    private static final String TAG = "RatingReportOps";
    private final CollectionReference ratingReportsCollection;
    private final FirebaseFirestore db;

    public RatingReportOperations() {
        db = FirebaseFirestore.getInstance();
        this.ratingReportsCollection = db.collection("rating-reports");
    }

    public void save(RatingReport ratingReport, final RatingReportOperations.SaveRatingReportListener listener) {
        DocumentReference docRef = db.collection("rating-reports").document();
        String documentId = docRef.getId();

        docRef.set(ratingReport)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Rating report saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving rating report", e);
                    listener.onFailure(e);
                });
    }

    public void getAllByOwnerEmail(String ownerEmail, GetAllRatingReportsListener<RatingReport> listener) {
        ratingReportsCollection
                .whereEqualTo("ownerEmail", ownerEmail)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<RatingReport> ratingReports = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        RatingReport ratingReport = documentSnapshot.toObject(RatingReport.class);
                        ratingReports.add(ratingReport);
                    }
                    listener.onSuccess(ratingReports);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error getting rating reports by owner email", e);
                    listener.onFailure(e);
                });
    }

    public void getAll(RatingReportOperations.GetAllRatingReportsListener<RatingReport> listener) {
        ratingReportsCollection
                .whereEqualTo("status", RatingReportStatus.REPORTED)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<RatingReport> ratingReports = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        RatingReport ratingReport = documentSnapshot.toObject(RatingReport.class);
                        ratingReports.add(ratingReport);
                    }
                    listener.onSuccess(ratingReports);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void update(RatingReport ratingReport, UpdateRatingReportListener listener) {
        ratingReportsCollection
                .whereEqualTo("ownerEmail", ratingReport.getOwnerEmail())
                .whereEqualTo("date", ratingReport.getDate())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    if (!queryDocumentSnapshots.isEmpty()) {
                        for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                            String documentId = documentSnapshot.getId();
                            ratingReportsCollection.document(documentId)
                                    .set(ratingReport)
                                    .addOnSuccessListener(aVoid -> {
                                        Log.d(TAG, "Rating report updated successfully");
                                        listener.onSuccess();
                                    })
                                    .addOnFailureListener(e -> {
                                        Log.e(TAG, "Error updating rating report", e);
                                        listener.onFailure(e);
                                    });
                            return;  // Update the first matching document and exit
                        }
                    } else {
                        Log.e(TAG, "No matching rating report found for update");
                        listener.onFailure(new Exception("No matching rating report found"));
                    }
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error getting rating report for update", e);
                    listener.onFailure(e);
                });
    }

    public interface SaveRatingReportListener {
        void onSuccess(String ratingReportId);
        void onFailure(Exception e);
    }

    public interface GetAllRatingReportsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface UpdateRatingReportListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}