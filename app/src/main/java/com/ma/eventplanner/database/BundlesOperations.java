package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Bundle;
import com.ma.eventplanner.model.Product;

import java.util.ArrayList;

public class BundlesOperations {
    private static final String TAG = "BundlesOperations";
    private final FirebaseFirestore db;
    CollectionReference bundlesCollection;

    public BundlesOperations() {
        db = FirebaseFirestore.getInstance();
        bundlesCollection = db.collection("bundles");
    }

    public void save(Bundle bundle, final BundlesOperations.BundleSaveListener listener) {
        DocumentReference docRef = db.collection("bundles").document();
        String documentId = docRef.getId();
        bundle.setId(documentId);

        docRef.set(bundle)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Bundle saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving bundle", e);
                    listener.onFailure(e);
                });
    }


    public interface BundleSaveListener {
        void onSuccess(String bundleId);
        void onFailure(Exception e);
    }

    public void getAllByCompanyId(String companyId, BundlesOperations.GetAllBundlesListener<Bundle> listener) {
        bundlesCollection
                .whereEqualTo("companyId", companyId)
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Bundle> bundles = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Bundle bundle = documentSnapshot.toObject(Bundle.class);
                        bundles.add(bundle);
                    }
                    listener.onSuccess(bundles);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllBundles(BundlesOperations.GetAllBundlesListener<Bundle> listener) {
        bundlesCollection
                //.whereEqualTo("available", true)
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Bundle> bundles = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Bundle bundle = documentSnapshot.toObject(Bundle.class);
                        bundles.add(bundle);
                    }
                    listener.onSuccess(bundles);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllBundlesListener<T> {
        void onSuccess(ArrayList<T> result);
        void onFailure(Exception e);
    }

    public void delete(String id, BundlesOperations.DeleteBundleListener listener) {
        bundlesCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface DeleteBundleListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public void update(Bundle bundle, BundlesOperations.UpdateBundleListener listener) {
        String bundleId = bundle.getId();
        if (bundleId == null) {
            Log.e(TAG, "Bundle ID is null. Cannot update bundle.");
            listener.onFailure(new IllegalArgumentException("Bundle ID is null"));
            return;
        }

        bundlesCollection.document(bundleId)
                .set(bundle)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Bundle updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating bundle", e);
                    listener.onFailure(e);
                });
    }

    public interface UpdateBundleListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
