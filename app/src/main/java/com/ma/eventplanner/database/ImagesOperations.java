package com.ma.eventplanner.database;

import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ImagesOperations {
    private final StorageReference mStorageRef;

    public ImagesOperations() {
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    public Task<byte[]> get(String path, String imageName) {
        StorageReference imageRef = mStorageRef.child(path + "/" + imageName);

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Task<byte[]> task = imageRef.getBytes(Long.MAX_VALUE).addOnSuccessListener(bytes -> {
            try {
                outputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return task.continueWith(task1 -> outputStream.toByteArray());
    }

    public Task<String> save(String path, String imageName, byte[] imageData) {
        StorageReference imageRef = mStorageRef.child(path + "/" + imageName);

        ByteArrayInputStream inputStream = new ByteArrayInputStream(imageData);

        UploadTask uploadTask = imageRef.putStream(inputStream);

        return uploadTask.continueWith(task -> {
            if (!task.isSuccessful()) {
                throw task.getException();
            }

            return imageRef.getDownloadUrl().toString();
        });
    }
}
