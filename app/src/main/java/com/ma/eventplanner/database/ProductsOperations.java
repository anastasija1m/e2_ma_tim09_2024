package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductsOperations {
    private static final String TAG = "ProductsOperations";
    private final FirebaseFirestore db;
    CollectionReference productsCollection;

    public ProductsOperations() {
        db = FirebaseFirestore.getInstance();
        productsCollection = db.collection("products");
    }

    public void save(Product product, final ProductsOperations.ProductSaveListener listener) {
        DocumentReference docRef = db.collection("products").document();
        String documentId = docRef.getId();
        product.setId(documentId);

        docRef.set(product)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Category saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving notification", e);
                    listener.onFailure(e);
                });
    }


    public interface ProductSaveListener {
        void onSuccess(String productId);
        void onFailure(Exception e);
    }

    public void getAllByCompanyId(String companyId, ProductsOperations.GetAllProductsListener<Product> listener) {
        productsCollection
                .whereEqualTo("companyId", companyId)
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Product> products = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Product product = documentSnapshot.toObject(Product.class);
                        products.add(product);
                    }
                    listener.onSuccess(products);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByCategoryId(String companyId, Category category, ProductsOperations.GetAllProductsListener<Product> listener) {
        productsCollection
                .whereEqualTo("companyId", companyId)
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                .whereEqualTo("category", category)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Product> products = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Product product = documentSnapshot.toObject(Product.class);
                        products.add(product);
                    }
                    listener.onSuccess(products);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllProducts(ProductsOperations.GetAllProductsListener<Product> listener) {
        productsCollection
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                //.whereEqualTo("available", true)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Product> products = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Product product = documentSnapshot.toObject(Product.class);
                        products.add(product);
                    }
                    listener.onSuccess(products);
                })
                .addOnFailureListener(listener::onFailure);
    }
    public interface GetAllProductsListener<T> {
        void onSuccess(ArrayList<T> result);
        void onFailure(Exception e);
    }

    public void delete(String id, ProductsOperations.DeleteProductListener listener) {
        productsCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface DeleteProductListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public void update(Product product, ProductsOperations.UpdateProductListener listener) {
        String productId = product.getId();
        if (productId == null) {
            Log.e(TAG, "Product ID is null. Cannot update product.");
            listener.onFailure(new IllegalArgumentException("Product ID is null"));
            return;
        }

        productsCollection.document(productId)
                .set(product)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Product updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating product", e);
                    listener.onFailure(e);
                });
    }

    public interface UpdateProductListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
