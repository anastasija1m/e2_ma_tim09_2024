package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Company;

import java.util.ArrayList;
import java.util.List;

public class CategoriesOperations {
    private static final String TAG = "CategoriesOperations";

    private final CollectionReference categoriesCollection;

    private final FirebaseFirestore db;

    public CategoriesOperations() {
        db = FirebaseFirestore.getInstance();
        this.categoriesCollection = db.collection("categories");
    }

    public void getAll(GetAllCategoriesListener<Category> listener) {
        categoriesCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Category> categories = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Category category = documentSnapshot.toObject(Category.class);
                        categories.add(category);
                    }
                    listener.onSuccess(categories);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByIds(List<String> ids, GetAllCategoriesListener<Category> listener) {
        categoriesCollection.whereIn("id", ids)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Category> categories = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Category category = documentSnapshot.toObject(Category.class);
                        categories.add(category);
                    }
                    listener.onSuccess(categories);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByOwnerId(String ownerId, GetAllCategoriesListener<Category> listener) {
        categoriesCollection
                .whereEqualTo("ownerId", ownerId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Category> categories = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Category category = documentSnapshot.toObject(Category.class);
                        categories.add(category);
                    }
                    listener.onSuccess(categories);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, GetCategoryByIdListener<Category> listener) {
        categoriesCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        Category category = documentSnapshot.toObject(Category.class);
                        listener.onSuccess(category);
                    } else {
                        listener.onFailure(new Exception("Category not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(Category category, final SaveCategoryListener listener) {
        DocumentReference docRef = db.collection("categories").document();
        String documentId = docRef.getId();
        category.setId(documentId);

        docRef.set(category)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Category saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving notification", e);
                    listener.onFailure(e);
                });
    }

    public void update(Category category, UpdateCategoryListener listener) {
        String categoryId = category.getId();
        if (categoryId == null) {
            Log.e(TAG, "Category ID is null. Cannot update category.");
            listener.onFailure(new IllegalArgumentException("Category ID is null"));
            return;
        }

        categoriesCollection.document(categoryId)
                .set(category)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Category updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating category", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, DeleteCategoryListener listener) {
        categoriesCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllCategoriesListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetCategoryByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveCategoryListener {
        void onSuccess(String categoryId);
        void onFailure(Exception e);
    }

    public interface UpdateCategoryListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteCategoryListener {
        void onSuccess();
        void onFailure(Exception e);
    }

}
