package com.ma.eventplanner.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.google.firebase.firestore.FirebaseFirestore;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.User;

import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NotificationWorker extends Worker {
    private static final String TAG = "NotificationWorker";

    public NotificationWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String notificationId = getInputData().getString("notificationId");
        if (notificationId == null) {
            return Result.failure();
        }

        FirebaseFirestore db = FirebaseFirestore.getInstance();
        db.collection("notifications").document(notificationId).get()
                .addOnSuccessListener(documentSnapshot -> {
                    Notification notification = documentSnapshot.toObject(Notification.class);
                    if (notification != null) {
                        sendPushNotification(notification);
                    }
                })
                .addOnFailureListener(e -> Log.e(TAG, "Failed to fetch notification", e));

        return Result.success();
    }

    private void sendPushNotification(Notification notification) {
        UsersOperations usersOperations = new UsersOperations();
        usersOperations.getById(notification.getRecipientId(), new UsersOperations.OnGetByIdListener() {
            @Override
            public void onSuccess(User user) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    JSONObject notificationObject = new JSONObject();
                    notificationObject.put("title", notification.getTitle());
                    notificationObject.put("body", notification.getContent());

                    jsonObject.put("notification", notificationObject);
                    jsonObject.put("to", user.getFcmToken());

                    callApi(jsonObject);
                } catch (Exception e) {
                    Log.e(TAG, "An unexpected error occurred", e);
                }
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Failed to get user by id", e);
            }
        });
    }

    void callApi(JSONObject jsonObject) {
        MediaType JSON = MediaType.get("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        String url = "https://fcm.googleapis.com/fcm/send";
        RequestBody body = RequestBody.create(jsonObject.toString(), JSON);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Authorization", "Bearer AAAAEWa7UUk:APA91bGMAbuHwlmB4zbpqMeSgWwpA9vE34sG6WvAokO1UwoZLNxfXODq8kMjprBuww5Br4Aj2mj5WDJ_mcFXvQWCIH5qNo7ENXr4_f99Ob2JSeXcMzlPdTuqJNdTSHrmTo5gGqEcNUql")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "Failed", e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
                // Handle response if needed
            }
        });
    }
}
