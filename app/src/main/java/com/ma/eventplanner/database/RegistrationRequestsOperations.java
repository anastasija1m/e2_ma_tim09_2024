package com.ma.eventplanner.database;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.Transaction;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ma.eventplanner.model.RegistrationRequest;
import com.ma.eventplanner.model.ServiceRequest;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RegistrationRequestsOperations {
    private static final String TAG = "RegistrationRequestsOperations";

    private final FirebaseFirestore db;
    private final StorageReference mStorageRef;

    public RegistrationRequestsOperations() {
        db = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    public void save(RegistrationRequest registrationRequest, Intent profileImage, Intent companyImage, OnSaveSuccessListener listener) {
        db.runTransaction((Transaction.Function<Void>) transaction -> {
            String documentId = db.collection("registration-requests").document().getId();
            //registrationRequest.setId(documentId);

            registrationRequest.setId(documentId);
            String profileImageName = uploadImage(documentId, profileImage, "requests/profile-images/");
            String companyImageName = uploadImage(documentId, companyImage, "requests/company-images/");

            registrationRequest.getOwner().setImageName(profileImageName);
            registrationRequest.getCompany().setImageName(companyImageName);

            db.collection("registration-requests").document(documentId).set(registrationRequest);

            return null;
        }).addOnSuccessListener(result -> {
            Log.d(TAG, "Transaction success!");
            listener.onSuccess();
        }).addOnFailureListener(e -> {
            Log.e(TAG, "Transaction failure.", e);
            listener.onFailure(e);
        });
    }

    public void getAll(OnGetAllSuccessListener listener) {
        db.collection("registration-requests")
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<RegistrationRequest> registrationRequestsList = new ArrayList<>(); // Koristimo listu umesto mape
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        RegistrationRequest request = documentSnapshot.toObject(RegistrationRequest.class);
                        registrationRequestsList.add(request); // Dodajemo zahtev u listu
                    }
                    listener.onSuccess(registrationRequestsList); // Prenosimo listu kao rezultat
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void update(RegistrationRequest registrationRequest, OnSaveSuccessListener listener) {
        String documentId = registrationRequest.getId();

        db.collection("registration-requests").document(documentId)
                .set(registrationRequest)
                .addOnSuccessListener(result -> {
                    Log.d(TAG, "Update success!");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Update failure.", e);
                    listener.onFailure(e);
                });
    }

    public void getById(String id, RegistrationRequestsOperations.GetRegistrationRequestByIdListener<RegistrationRequest> listener) {
        db.collection("registration-requests").document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        RegistrationRequest registrationRequest = documentSnapshot.toObject(RegistrationRequest.class);
                        listener.onSuccess(registrationRequest);
                    } else {
                        listener.onFailure(new Exception("Registration request not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }


    private String uploadImage(String requestId, Intent imageIntent, String path) {
        if (imageIntent == null) {
            return "default.jpg";
        }

        Uri imageUri = imageIntent.getData();
        if (imageUri != null) {
            return uploadUri(requestId, imageUri, path);
        }

        Bitmap bitmap = (Bitmap) Objects.requireNonNull(imageIntent.getExtras()).get("data");
        if (bitmap != null) {
            return uploadBitmap(requestId, bitmap, path);
        }

        return "default.jpg";
    }

    @NonNull
    private String uploadUri(String requestId, Uri imageUri, String path) {
        StorageReference userImageRef = mStorageRef.child(path + requestId + ".jpg");

        userImageRef.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> {
                    Log.d(TAG, "Image upload successful");
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Image upload failed", e);
                });

        return userImageRef.getName();
    }

    @NonNull
    private String uploadBitmap(String requestId, @NonNull Bitmap bitmap, String path) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] data = baos.toByteArray();

        StorageReference userImageRef = mStorageRef.child(path + requestId + ".jpg");

        UploadTask uploadTask = userImageRef.putBytes(data);
        uploadTask.addOnSuccessListener(taskSnapshot -> {
            Log.d(TAG, "Image upload successful");
        }).addOnFailureListener(e -> {
            Log.e(TAG, "Image upload failed", e);
        });

        return userImageRef.getName();
    }



    public interface OnSaveSuccessListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface OnGetAllSuccessListener {
        void onSuccess(List<RegistrationRequest> registrationRequests);
        void onFailure(Exception e);
    }

    public interface GetRegistrationRequestByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }
}
