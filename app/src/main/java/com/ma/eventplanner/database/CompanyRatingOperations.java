package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.CompanyRating;

import java.util.ArrayList;
import java.util.List;

public class CompanyRatingOperations {

    private static final String TAG = "CompanyRatingOps";
    private final CollectionReference companyRatingsCollection;
    private final FirebaseFirestore db;

    public CompanyRatingOperations() {
        db = FirebaseFirestore.getInstance();
        this.companyRatingsCollection = db.collection("company-ratings");
    }

    public void save(CompanyRating companyRating, final SaveCompanyRatingListener listener) {
        DocumentReference docRef = db.collection("company-ratings").document();
        String documentId = docRef.getId();

        docRef.set(companyRating)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Company rating saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving company rating", e);
                    listener.onFailure(e);
                });
    }

    public void getAll(GetAllCompanyRatingsListener<CompanyRating> listener) {
        companyRatingsCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<CompanyRating> companyRatings = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        CompanyRating companyRating = documentSnapshot.toObject(CompanyRating.class);
                        companyRatings.add(companyRating);
                    }
                    listener.onSuccess(companyRatings);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByCompanyId(String companyId, GetAllCompanyRatingsListener<CompanyRating> listener) {
        companyRatingsCollection
                .whereEqualTo("companyId", companyId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<CompanyRating> companyRatings = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        CompanyRating companyRating = documentSnapshot.toObject(CompanyRating.class);
                        companyRatings.add(companyRating);
                    }
                    listener.onSuccess(companyRatings);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error getting company ratings by company ID", e);
                    listener.onFailure(e);
                });
    }

    public void getCompanyRatingIdByCompanyRating(CompanyRating companyRating, GetCompanyRatingIdListener listener) {
        companyRatingsCollection
                .whereEqualTo("companyId", companyRating.getCompanyId())
                .whereEqualTo("organizerEmail", companyRating.getOrganizerEmail())
                .whereEqualTo("rating", companyRating.getRating())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        String companyRatingId = documentSnapshot.getId();
                        listener.onSuccess(companyRatingId);
                        return;
                    }
                    listener.onSuccess(null);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error getting company rating ID by company rating", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String companyRatingId, CompanyRatingOperations.DeleteCompanyRatingListener listener) {
        companyRatingsCollection.document(companyRatingId)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetCompanyRatingIdListener {
        void onSuccess(String companyRatingId);
        void onFailure(Exception e);
    }

    public interface SaveCompanyRatingListener {
        void onSuccess(String companyRatingId);
        void onFailure(Exception e);
    }

    public interface GetAllCompanyRatingsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface DeleteCompanyRatingListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
