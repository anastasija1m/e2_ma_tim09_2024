package com.ma.eventplanner.database;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ma.eventplanner.model.Notification;
import com.ma.eventplanner.model.User;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NotificationsOperations {
    private static final String TAG = "NotificationsOperations";
    private final FirebaseFirestore db;
    private final CollectionReference notificationsRef;
    private final UsersOperations usersOperations;

    public NotificationsOperations() {
        db = FirebaseFirestore.getInstance();
        notificationsRef = db.collection("notifications");
        usersOperations = new UsersOperations();
    }

    public void save(@NonNull Notification notification, OnSaveSuccessListener listener) {
        DocumentReference docRef = db.collection("notifications").document();
        String documentId = docRef.getId();
        notification.setId(documentId);
        docRef.set(notification)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Notification saved successfully");
                    sendPushNotification(notification);
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving notification", e);
                    listener.onFailure(e);
                });
    }

    private void sendPushNotification(Notification notification) {
            usersOperations.getById(notification.getRecipientId(), new UsersOperations.OnGetByIdListener() {
                @Override
                public void onSuccess(User user) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        JSONObject notificationObject = new JSONObject();
                        notificationObject.put("title", notification.getTitle());
                        notificationObject.put("body", notification.getContent());

                        jsonObject.put("notification", notificationObject);
                        jsonObject.put("to", user.getFcmToken());

                        callApi(jsonObject);
                    } catch (Exception e) {
                        Log.e(TAG, "An unexpected error occurred", e);
                    }
                }

                @Override
                public void onFailure(Exception e) {
                    Log.e(TAG, "Failed to get user by id", e);
                }
            });
    }

    void callApi(JSONObject jsonObject) {
        MediaType JSON = MediaType.get("application/json; charset=utf-8");
        OkHttpClient client = new OkHttpClient();
        String url = "https://fcm.googleapis.com/fcm/send";
        RequestBody body = RequestBody.create(jsonObject.toString(), JSON);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Authorization", "Bearer AAAAEWa7UUk:APA91bGMAbuHwlmB4zbpqMeSgWwpA9vE34sG6WvAokO1UwoZLNxfXODq8kMjprBuww5Br4Aj2mj5WDJ_mcFXvQWCIH5qNo7ENXr4_f99Ob2JSeXcMzlPdTuqJNdTSHrmTo5gGqEcNUql")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(@NonNull Call call, @NonNull IOException e) {
                Log.e(TAG, "Failed", e);
            }

            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {

            }
        });

    }

    public void update(Notification notification, OnUpdateSuccessListener listener) {
        String notificationId = notification.getId();
        if (notificationId == null) {
            Log.e(TAG, "Notification ID is null. Cannot update notification.");
            listener.onFailure(new IllegalArgumentException("Notification ID is null"));
            return;
        }

        DocumentReference docRef = notificationsRef.document(notificationId);
        docRef.set(notification)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Notification updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating notification", e);
                    listener.onFailure(e);
                });
    }

    public void getAllByRecipientId(String recipientId, OnQueryCompleteListener listener) {
        notificationsRef.whereEqualTo("recipientId", recipientId)
                .whereEqualTo("isRead", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Notification> notifications = new ArrayList<>();
                    for (DocumentSnapshot document : queryDocumentSnapshots) {
                        Notification notification = document.toObject(Notification.class);
                        notifications.add(notification);
                    }
                    listener.onSuccess(notifications);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface OnSaveSuccessListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface OnQueryCompleteListener {
        void onSuccess(List<Notification> notifications);
        void onFailure(Exception e);
    }

    public interface OnUpdateSuccessListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
