package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.CompanyReport;

import java.util.ArrayList;
import java.util.List;

public class CompanyReportOperations {
    private static final String TAG = "CompanyReportOperations";
    private final CollectionReference reportsCollection;
    private final FirebaseFirestore db;

    public CompanyReportOperations() {
        db = FirebaseFirestore.getInstance();
        this.reportsCollection = db.collection("company_reports");
    }

    public void getAll(GetAllCompanyReportsListener<CompanyReport> listener) {
        reportsCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<CompanyReport> reports = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        CompanyReport report = documentSnapshot.toObject(CompanyReport.class);
                        reports.add(report);
                    }
                    listener.onSuccess(reports);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(CompanyReport report, final CompanyReportSaveListener listener) {
        DocumentReference docRef = db.collection("company_reports").document();
        String documentId = docRef.getId();
        report.setId(documentId);

        docRef.set(report)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Company report saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving company report", e);
                    listener.onFailure(e);
                });
    }

    public void getById(String id, GetCompanyReportByIdListener<CompanyReport> listener) {
        reportsCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        CompanyReport report = documentSnapshot.toObject(CompanyReport.class);
                        listener.onSuccess(report);
                    } else {
                        listener.onFailure(new Exception("Company report not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllCompanyReportsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetCompanyReportByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface CompanyReportSaveListener {
        void onSuccess(String reportId);
        void onFailure(Exception e);
    }
}
