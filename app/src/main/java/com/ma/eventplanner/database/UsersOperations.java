package com.ma.eventplanner.database;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ma.eventplanner.model.Employee;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Owner;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.User;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class UsersOperations {
    private static final String TAG = "UsersOperations";
    private final FirebaseFirestore db;
    private final StorageReference mStorageRef;

    public UsersOperations() {
        db = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();
    }

    public void save(String userId, User user, Intent image, OnSaveSuccessListener listener) {
        db.runTransaction(transaction -> {
            String imageName = uploadImage(userId, image);

            if (user.getRole() == Role.EVENT_ORGANIZER) {
                EventOrganizer eventOrganizer = (EventOrganizer) user;
                eventOrganizer.setImageName(imageName);
                DocumentReference userDocRef = db.collection("users").document(userId);
                transaction.set(userDocRef, eventOrganizer);
            }
            else if (user.getRole() == Role.OWNER) {
                Owner owner = (Owner) user;
                owner.setImageName(imageName);
                DocumentReference userDocRef = db.collection("users").document(userId);
                transaction.set(userDocRef, owner);
            }
            else if(user.getRole().equals(Role.EMPLOYEE)) {
                Employee employee = (Employee) user;
                employee.setImageName(imageName);
                DocumentReference userDocRef = db.collection("users").document(userId);
                transaction.set(userDocRef, employee);
            }

            return null;
        }).addOnSuccessListener(aVoid -> {
            Log.d(TAG, "Transaction successfully completed");
            listener.onSuccess();
        }).addOnFailureListener(e -> {
            Log.e(TAG, "Transaction failed: ", e);
            listener.onFailure(e);
        });
    }

    public void getById(String userId, OnGetByIdListener listener) {
        DocumentReference userDocRef = db.collection("users").document(userId);

        userDocRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    User user = document.toObject(User.class);
                    listener.onSuccess(user);
                } else {
                    Log.d(TAG, "No such document");
                    listener.onFailure(new Exception("No such document"));
                }
            } else {
                Log.d(TAG, "getById failed with ", task.getException());
                listener.onFailure(task.getException());
            }
        });
    }

    public void getAllByRole(@NonNull Role role, OnGetAllByRoleListener listener) {
        db.collection("users")
                .whereEqualTo("role", role.toString())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    HashMap<String, User> userMap = new HashMap<>();
                    for (DocumentSnapshot document : queryDocumentSnapshots) {
                        String userId = document.getId();
                        User user = document.toObject(User.class);
                        userMap.put(userId, user);
                    }
                    listener.onSuccess(userMap);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error getting documents: ", e);
                    listener.onFailure(e);
                });
    }

    public void getOwnerById(String userId, OnGetOwnerByIdListener listener) {
        DocumentReference userDocRef = db.collection("users").document(userId);

        userDocRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Owner owner = document.toObject(Owner.class);
                    listener.onSuccess(owner);
                } else {
                    Log.d(TAG, "No such document");
                    listener.onFailure(new Exception("No such document"));
                }
            } else {
                Log.d(TAG, "getOwnerById failed with ", task.getException());
                listener.onFailure(task.getException());
            }
        });
    }

    public void getEmployeeById(String userId, OnGetEmployeeByIdListener listener) {
        DocumentReference userDocRef = db.collection("users").document(userId);

        userDocRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    Employee employee = document.toObject(Employee.class);
                    listener.onSuccess(employee);
                } else {
                    Log.d(TAG, "No such document");
                    listener.onFailure(new Exception("No such document"));
                }
            } else {
                Log.d(TAG, "getEmployeeById failed with ", task.getException());
                listener.onFailure(task.getException());
            }
        });
    }

    public void getEventOrganizerById(String userId, OnGetEventOrganizerByIdListener listener) {
        DocumentReference userDocRef = db.collection("users").document(userId);

        userDocRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot document = task.getResult();
                if (document.exists()) {
                    EventOrganizer eventOrganizer = document.toObject(EventOrganizer.class);
                    listener.onSuccess(eventOrganizer);
                } else {
                    Log.d(TAG, "No such document");
                    listener.onFailure(new Exception("No such document"));
                }
            } else {
                Log.d(TAG, "getEmployeeById failed with ", task.getException());
                listener.onFailure(task.getException());
            }
        });
    }

    public void getUid(String email, OnGetUidListener listener) {
        db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (!querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            String uid = document.getId();
                            listener.onSuccess(uid);
                        } else {
                            Log.d(TAG, "No document found with the provided email");
                            listener.onFailure(new Exception("No document found with the provided email"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    private String uploadImage(String userId, Intent imageIntent) {
        if (imageIntent == null) {
            return "default.jpg";
        }

        Uri imageUri = imageIntent.getData();
        if (imageUri != null) {
            return uploadUri(userId, imageUri);
        }

        Bitmap bitmap = (Bitmap) Objects.requireNonNull(imageIntent.getExtras()).get("data");
        if (bitmap != null) {
            return uploadBitmap(userId, bitmap);
        }

        byte[] imageBytes = imageIntent.getByteArrayExtra("user_image");
        if (imageBytes != null) {
            return uploadBytes(userId, imageBytes);
        }

        return "default.jpg";
    }

    @NonNull
    private String uploadUri(String userId, Uri imageUri) {
        StorageReference userImageRef = mStorageRef.child("profile-images/" + userId + ".jpg");

        userImageRef.putFile(imageUri)
                .addOnSuccessListener(taskSnapshot -> Log.d(TAG, "Image upload successful"))
                .addOnFailureListener(e -> Log.e(TAG, "Image upload failed", e));

        return userImageRef.getName();
    }

    @NonNull
    private String uploadBitmap(String userId, @NonNull Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, baos);
        byte[] data = baos.toByteArray();

        StorageReference userImageRef = mStorageRef.child("profile-images/" + userId + ".jpg");

        UploadTask uploadTask = userImageRef.putBytes(data);
        uploadTask.addOnSuccessListener(taskSnapshot ->
                Log.d(TAG, "Image upload successful")).addOnFailureListener(e ->
                Log.e(TAG, "Image upload failed", e));

        return userImageRef.getName();
    }

    @NonNull
    private String uploadBytes(String userId, byte[] imageBytes) {
        StorageReference userImageRef = mStorageRef.child("profile-images/" + userId + ".jpg");

        UploadTask uploadTask = userImageRef.putBytes(imageBytes);
        uploadTask.addOnSuccessListener(taskSnapshot -> Log.d(TAG, "Image upload successful")).addOnFailureListener(e -> Log.e(TAG, "Image upload failed", e));

        return userImageRef.getName();
    }

    public void getAllEmployeesByCompany(String companyId, OnGetAllEmployeesListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.EMPLOYEE.name())
                .whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null) {
                            HashMap<String, Employee> employeeMap = new HashMap<>();
                            for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                                String employeeId = document.getId();
                                Employee employee = document.toObject(Employee.class);
                                employeeMap.put(employeeId, employee);
                            }
                            listener.onSuccess(employeeMap);
                        } else {
                            Log.d(TAG, "No documents found");
                            listener.onFailure(new Exception("No documents found"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void updateEmployee(String email, Employee updatedEmployee, OnUpdateEmployeeListener listener) {
        db.collection("users")
                .whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            document.getReference().set(updatedEmployee)
                                    .addOnSuccessListener(aVoid -> {
                                        Log.d(TAG, "Employee updated successfully");
                                        listener.onSuccess();
                                    })
                                    .addOnFailureListener(e -> {
                                        Log.e(TAG, "Failed to update employee: ", e);
                                        listener.onFailure(e);
                                    });
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getOwnerIdByCompanyId(String companyId, OnGetOwnerIdListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.OWNER.toString())
                .whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (DocumentSnapshot document : task.getResult()) {
                            Owner owner = document.toObject(Owner.class);
                            if (owner != null) {
                                String ownerId = document.getId();
                                listener.onSuccess(ownerId);
                                return;
                            }
                        }
                        listener.onFailure(new Exception("No owner found for company ID: " + companyId));
                    } else {
                        Log.e(TAG, "Error getting owner document: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getAllEmployeesByCompanyId(String companyId, OnGetAllEmployeessListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.EMPLOYEE.name())
                .whereEqualTo("companyId", companyId)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null) {
                            List<Employee> employees = new ArrayList<>();
                            for (DocumentSnapshot document : querySnapshot.getDocuments()) {
                                Employee employee = document.toObject(Employee.class);
                                employees.add(employee);
                            }
                            listener.onSuccess(employees);
                        } else {
                            Log.d(TAG, "No documents found");
                            listener.onFailure(new Exception("No documents found"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getEventOrganizerByEmail(String email, OnGetEventOrganizerByEmailListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.EVENT_ORGANIZER.toString())
                .whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            EventOrganizer eventOrganizer = document.toObject(EventOrganizer.class);
                            listener.onSuccess(eventOrganizer);
                        } else {
                            Log.d(TAG, "No event organizer found with the provided email");
                            listener.onFailure(new Exception("No event organizer found with the provided email"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getOrganizerIdByOrganizerEmail(String organizerEmail, OnGetUidListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.EVENT_ORGANIZER.toString())
                .whereEqualTo("email", organizerEmail)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            String organizerId = document.getId();
                            listener.onSuccess(organizerId);
                        } else {
                            Log.d(TAG, "No event organizer found with the provided email");
                            listener.onFailure(new Exception("No event organizer found with the provided email"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getEmployeeByEmail(String email, OnGetEmployeeByEmailListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.EMPLOYEE.toString())
                .whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            Employee employee = document.toObject(Employee.class);
                            listener.onSuccess(employee);
                        } else {
                            Log.d(TAG, "No employee found with the provided email");
                            listener.onFailure(new Exception("No employee found with the provided email"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getEmployeeIdByOrganizerEmail(String employeeEmail, OnGetUidListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.EMPLOYEE.toString())
                .whereEqualTo("email", employeeEmail)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            String organizerId = document.getId();
                            listener.onSuccess(organizerId);
                        } else {
                            Log.d(TAG, "No employee found with the provided email");
                            listener.onFailure(new Exception("No employee found with the provided email"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void getOwnerIdByEmail(String email, OnGetOwnerByEmailListener listener) {
        db.collection("users")
                .whereEqualTo("role", Role.OWNER.toString())
                .whereEqualTo("email", email)
                .get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        QuerySnapshot querySnapshot = task.getResult();
                        if (querySnapshot != null && !querySnapshot.isEmpty()) {
                            DocumentSnapshot document = querySnapshot.getDocuments().get(0);
                            listener.onSuccess(document.getId());
                        } else {
                            Log.d(TAG, "No owner found with the provided email");
                            listener.onFailure(new Exception("No owner found with the provided email"));
                        }
                    } else {
                        Log.e(TAG, "Error getting documents: ", task.getException());
                        listener.onFailure(task.getException());
                    }
                });
    }

    public interface OnGetAllEmployeessListener {
        void onSuccess(List<Employee> employees);
        void onFailure(Exception e);
    }

    public interface OnUpdateEmployeeListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface OnSaveSuccessListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface OnGetByIdListener {
        void onSuccess(User user);
        void onFailure(Exception e);
    }

    public interface OnGetAllByRoleListener {
        void onSuccess(HashMap<String, User> userMap);
        void onFailure(Exception e);
    }

    public interface OnGetOwnerByIdListener {
        void onSuccess(Owner owner);
        void onFailure(Exception e);
    }

    public interface OnGetEmployeeByIdListener {
        void onSuccess(Employee employee);
        void onFailure(Exception e);
    }

    public interface OnGetAllEmployeesListener {
        void onSuccess(HashMap<String, Employee> employeeMap);
        void onFailure(Exception e);
    }

    public interface OnGetOwnerIdListener {
        void onSuccess(String ownerId);
        void onFailure(Exception e);
    }

    public interface OnGetEventOrganizerByIdListener {
        void onSuccess(EventOrganizer eventOrganizer);
        void onFailure(Exception e);
    }

    public interface OnGetUidListener {
        void onSuccess(String uid);
        void onFailure(Exception e);
    }

    public interface OnGetEventOrganizerByEmailListener {
        void onSuccess(EventOrganizer eventOrganizer);
        void onFailure(Exception e);
    }

    public interface OnGetOwnerByEmailListener {
        void onSuccess(String ownerId);
        void onFailure(Exception e);
    }

    public interface OnGetEmployeeByEmailListener {
        void onSuccess(Employee employee);
        void onFailure(Exception e);
    }
}
