package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.ProductRequest;
import com.ma.eventplanner.model.RequestStatus;

import java.util.ArrayList;
import java.util.List;

public class ProductRequestsOperations {
    private static final String TAG = "ProductRequestsOperations";
    private final CollectionReference productRequestsCollection;
    private final FirebaseFirestore db;

    public ProductRequestsOperations() {
        db = FirebaseFirestore.getInstance();
        this.productRequestsCollection = db.collection("product-requests");
    }

    public void getAll(GetAllProductRequestsListener<ProductRequest> listener) {
        productRequestsCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ProductRequest> productRequests = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ProductRequest productRequest = documentSnapshot.toObject(ProductRequest.class);
                        productRequests.add(productRequest);
                    }
                    listener.onSuccess(productRequests);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByStatus(RequestStatus status, GetAllProductRequestsListener<ProductRequest> listener) {
        productRequestsCollection
                .whereEqualTo("status", status.toString())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ProductRequest> productRequests = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ProductRequest productRequest = documentSnapshot.toObject(ProductRequest.class);
                        productRequests.add(productRequest);
                    }
                    listener.onSuccess(productRequests);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, GetProductRequestByIdListener<ProductRequest> listener) {
        productRequestsCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        ProductRequest productRequest = documentSnapshot.toObject(ProductRequest.class);
                        listener.onSuccess(productRequest);
                    } else {
                        listener.onFailure(new Exception("ProductRequest not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(ProductRequest productRequest, final SaveProductRequestListener listener) {
        DocumentReference docRef = db.collection("product-requests").document();
        String documentId = docRef.getId();
        productRequest.setId(documentId);

        docRef.set(productRequest)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "ProductRequest saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving product request", e);
                    listener.onFailure(e);
                });
    }

    public void update(ProductRequest productRequest, UpdateProductRequestListener listener) {
        String productRequestId = productRequest.getId();
        if (productRequestId == null) {
            Log.e(TAG, "ProductRequest ID is null. Cannot update product request.");
            listener.onFailure(new IllegalArgumentException("ProductRequest ID is null"));
            return;
        }

        productRequestsCollection.document(productRequestId)
                .set(productRequest)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "ProductRequest updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating product request", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, DeleteProductRequestListener listener) {
        productRequestsCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllProductRequestsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetProductRequestByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveProductRequestListener {
        void onSuccess(String productRequestId);
        void onFailure(Exception e);
    }

    public interface UpdateProductRequestListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteProductRequestListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
