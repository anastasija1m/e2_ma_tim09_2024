package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.EventType;

import java.util.ArrayList;
import java.util.List;

public class EventTypesOperations {
    private static final String TAG = "EventTypesOperations";
    private final CollectionReference eventTypesCollection;
    private final FirebaseFirestore db;

    public EventTypesOperations() {
        db = FirebaseFirestore.getInstance();
        this.eventTypesCollection = db.collection("event-types");
    }

    public void getAll(GetAllEventTypesListener<EventType> listener) {
        eventTypesCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<EventType> eventTypes = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        EventType eventType = documentSnapshot.toObject(EventType.class);
                        eventTypes.add(eventType);
                    }
                    listener.onSuccess(eventTypes);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByIds(List<String> ids, GetAllEventTypesListener<EventType> listener) {
        eventTypesCollection.whereIn("id", ids)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<EventType> eventTypes = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        EventType eventType = documentSnapshot.toObject(EventType.class);
                        eventTypes.add(eventType);
                    }
                    listener.onSuccess(eventTypes);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, GetEventTypeByIdListener<EventType> listener) {
        eventTypesCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        EventType eventType = documentSnapshot.toObject(EventType.class);
                        listener.onSuccess(eventType);
                    } else {
                        listener.onFailure(new Exception("EventType not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(EventType eventType, final SaveEventTypeListener listener) {
        DocumentReference docRef = db.collection("event-types").document();
        String documentId = docRef.getId();
        eventType.setId(documentId);

        docRef.set(eventType)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "EventType saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving event type", e);
                    listener.onFailure(e);
                });
    }

    public void update(EventType eventType, UpdateEventTypeListener listener) {
        String eventTypeId = eventType.getId();
        if (eventTypeId == null) {
            Log.e(TAG, "EventType ID is null. Cannot update event type.");
            listener.onFailure(new IllegalArgumentException("EventType ID is null"));
            return;
        }

        eventTypesCollection.document(eventTypeId)
                .set(eventType)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "EventType updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating event type", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, DeleteEventTypeListener listener) {
        eventTypesCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllEventTypesListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetEventTypeByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveEventTypeListener {
        void onSuccess(String eventTypeId);
        void onFailure(Exception e);
    }

    public interface UpdateEventTypeListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteEventTypeListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
