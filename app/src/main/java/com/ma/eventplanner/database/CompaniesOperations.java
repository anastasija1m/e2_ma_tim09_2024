package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Company;

import java.util.ArrayList;
import java.util.List;

public class CompaniesOperations {
    private static final String TAG = "CompaniesOperations";
    private final CollectionReference companiesCollection;
    private final FirebaseFirestore db;

    public CompaniesOperations() {
        db = FirebaseFirestore.getInstance();
        this.companiesCollection = db.collection("companies");
    }

    public void getAll(CompaniesOperations.GetAllCompaniesListener<Company> listener) {
        companiesCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Company> companies = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Company company = documentSnapshot.toObject(Company.class);
                        companies.add(company);
                    }
                    listener.onSuccess(companies);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(Company company, final CompanySaveListener listener) {
        DocumentReference docRef = db.collection("companies").document();
        String documentId = docRef.getId();
        company.setId(documentId);

        docRef.set(company)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Company saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving event", e);
                    listener.onFailure(e);
                });
    }

    public void getById(String id, CompaniesOperations.GetCompanyByIdListener<Company> listener) {
        companiesCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        Company company = documentSnapshot.toObject(Company.class);
                        listener.onSuccess(company);
                    } else {
                        listener.onFailure(new Exception("Company not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllCompaniesListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetCompanyByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface CompanySaveListener {
        void onSuccess(String companyId);
        void onFailure(Exception e);
    }

}
