package com.ma.eventplanner.database;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.messaging.FirebaseMessaging;
import com.ma.eventplanner.activities.HomeActivity;
import com.ma.eventplanner.activities.LoginActivity;
import com.ma.eventplanner.model.EventOrganizer;
import com.ma.eventplanner.model.Role;
import com.ma.eventplanner.model.User;

import java.util.Objects;

public class AuthenticationManager {
    private static final String TAG = "AuthenticationManager";
    private final FirebaseAuth mAuth;

    private static AuthenticationManager instance;
    private final UsersOperations usersOperations;

    public AuthenticationManager() {
        mAuth = FirebaseAuth.getInstance();
        usersOperations = new UsersOperations();
    }

    public void createUserWithEmailAndPasswordLuka(final String email, final String password, final Activity activity, final User eventOrganizer, Intent image) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser firebaseUser = mAuth.getCurrentUser();
                            assert firebaseUser != null;
                            String userId = firebaseUser.getUid();

                            usersOperations.save(userId, eventOrganizer, image, new UsersOperations.OnSaveSuccessListener() {
                                @Override
                                public void onSuccess() {
                                    sendEmailVerification(activity, firebaseUser);
                                }

                                @Override
                                public void onFailure(Exception e) {
                                    Log.e(TAG, "Failed to save user data: ", e);
                                }
                            });
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(activity, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }



    public void createUserWithEmailAndPassword(final String email, final String password, final Activity activity, final User user, Intent image, final OnUserCreationListener listener) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createUserWithEmail:success");
                        FirebaseUser firebaseUser = mAuth.getCurrentUser();
                        assert firebaseUser != null;
                        String userId = firebaseUser.getUid();

                        usersOperations.save(userId, user, image, new UsersOperations.OnSaveSuccessListener() {
                            @Override
                            public void onSuccess() {
                                sendEmailVerification(activity, firebaseUser);
                                listener.onSuccess();
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.e(TAG, "Failed to save user data: ", e);
                                listener.onFailure(e);
                            }
                        });
                    } else {
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(activity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                        listener.onFailure(task.getException());
                    }
                });
    }

    private void sendEmailVerification(final Activity activity, @NonNull FirebaseUser user) {
        user.sendEmailVerification()
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "Email verification sent.");
                        Toast.makeText(activity, "Verification email sent.",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        Log.e(TAG, "sendEmailVerification", task.getException());
                        Toast.makeText(activity, "Failed to send verification email.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void signInWithEmailAndPassword(final String email, final String password, final Activity activity) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        FirebaseUser firebaseUser = mAuth.getCurrentUser();
                        if (firebaseUser != null && firebaseUser.isEmailVerified()) {
                            Log.d(TAG, "signInWithEmail:success");
                            Toast.makeText(activity, "Login successful", Toast.LENGTH_SHORT).show();

                            firebaseUser.getIdToken(true)
                                    .addOnCompleteListener(task1 -> {
                                        if (task1.isSuccessful()) {
                                            String token = task1.getResult().getToken();
                                            saveLoggedInStatus(activity, firebaseUser.getUid(), token);
                                        } else {
                                            Log.e(TAG, "Failed to get token", task1.getException());
                                        }
                                    });
                        } else {
                            Log.d(TAG, "signInWithEmail: email not verified");
                            Toast.makeText(activity, "Please verify your email address", Toast.LENGTH_SHORT).show();
                            mAuth.signOut();
                        }
                    } else {
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Toast.makeText(activity, "Authentication failed. Please check your credentials.", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void signOut(Activity activity) {
        FirebaseMessaging.getInstance().deleteToken().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                mAuth.signOut();

                SharedPreferences sharedPreferences = activity.getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("isLoggedIn", false);
                editor.putString("userId", "");
                editor.apply();


                Intent intent = new Intent(activity, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
            }
        });
    }

    public static synchronized AuthenticationManager getInstance() {
        if (instance == null) {
            instance = new AuthenticationManager();
        }
        return instance;
    }

    private void saveLoggedInStatus(Activity activity, String userId, String token) {
        SharedPreferences sharedPreferences = activity.getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("userId", userId);
        editor.putString("token", token);
        editor.putBoolean("isLoggedIn", true);
        editor.apply();

        Intent intent = new Intent(activity, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public void refreshTokenAndSession(final String userId, final Activity activity, final OnRefreshTokenAndSessionListener listener) {
        FirebaseUser user = mAuth.getCurrentUser();
        if (user != null) {
            user.getIdToken(true)
                    .addOnCompleteListener(task -> {
                        if (task.isSuccessful()) {
                            String refreshedToken = task.getResult().getToken();

                            SharedPreferences sharedPreferences = activity.getSharedPreferences("user_prefs", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("token", refreshedToken);
                            editor.apply();

                            listener.onSuccess();
                        } else {
                            Exception exception = task.getException();
                            listener.onFailure(exception);
                        }
                    });
        } else {
            Intent intent = new Intent(activity, LoginActivity.class);
            activity.startActivity(intent);
            activity.finish();
        }
    }

    public void enableEmployeeAccount(final String employeeUid, final String email, final String password, final Activity activity, final OnUserCreationListener listener) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity, task -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createUserWithEmail:success");
                        FirebaseUser firebaseUser = mAuth.getCurrentUser();
                        assert firebaseUser != null;
                        sendEmailVerification(activity, firebaseUser);

                        changeDocumentId(employeeUid, firebaseUser.getUid(), new OnDocumentIdChangeListener() {
                            @Override
                            public void onSuccess() {
                                Log.d(TAG, "Document ID changed successfully.");
                                listener.onSuccess();
                            }

                            @Override
                            public void onFailure(Exception e) {
                                Log.e(TAG, "Failed to change document ID.", e);
                                listener.onFailure(e);
                            }
                        });
                    } else {
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(activity, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                        listener.onFailure(task.getException());
                    }
                });
    }

    public void changeDocumentId(String userId, String newDocumentId, OnDocumentIdChangeListener listener) {
        DocumentReference oldDocRef = FirebaseFirestore.getInstance().collection("users").document(userId);

        oldDocRef.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot documentSnapshot = task.getResult();
                if (documentSnapshot.exists()) {
                    DocumentReference newDocRef = FirebaseFirestore.getInstance().collection("users").document(newDocumentId);

                    newDocRef.set(Objects.requireNonNull(documentSnapshot.getData()))
                            .addOnSuccessListener(aVoid -> {
                                oldDocRef.delete()
                                        .addOnSuccessListener(aVoid1 -> {
                                            Log.d(TAG, "Document ID changed successfully.");
                                            listener.onSuccess();
                                        })
                                        .addOnFailureListener(e -> {
                                            Log.e(TAG, "Failed to delete old document.", e);
                                            listener.onFailure(e);
                                        });
                            })
                            .addOnFailureListener(e -> {
                                Log.e(TAG, "Failed to create new document with new ID.", e);
                                listener.onFailure(e);
                            });
                } else {
                    Log.d(TAG, "Old document doesn't exist.");
                    listener.onFailure(new Exception("Old document doesn't exist."));
                }
            } else {
                Log.e(TAG, "Failed to get old document.", task.getException());
                listener.onFailure(task.getException());
            }
        });
    }

    public void disableEmployeeAccount(final String employeeUid, final OnUserDeletionListener listener) {
        usersOperations.getById(employeeUid, new UsersOperations.OnGetByIdListener() {
            @Override
            public void onSuccess(User user) {
                if (user.getRole().equals(Role.EMPLOYEE)) {
                    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                    if (firebaseUser != null && firebaseUser.getUid().equals(employeeUid)) {
                        firebaseUser.delete()
                                .addOnSuccessListener(aVoid -> {
                                    Log.d(TAG, "User account deleted successfully.");
                                    listener.onSuccess();
                                })
                                .addOnFailureListener(e -> {
                                    Log.w(TAG, "Failed to delete user account.", e);
                                    listener.onFailure(e);
                                });
                    } else {
                        Log.w(TAG, "User is not authenticated or provided user ID does not match the authenticated user.");
                        listener.onFailure(new Exception("User is not authenticated or provided user ID does not match the authenticated user."));
                    }
                } else {
                    Log.w(TAG, "The user with provided ID is not an Employee.");
                    listener.onFailure(new Exception("The user with provided ID is not an Employee."));
                }
            }

            @Override
            public void onFailure(Exception e) {
                Log.e(TAG, "Failed to get user by ID: " + employeeUid, e);
                listener.onFailure(e);
            }
        });
    }

    public static DocumentReference currentUserDetails(){
        return FirebaseFirestore.getInstance().collection("users").document(currentUserId());
    }

    public static String currentUserId(){
        return FirebaseAuth.getInstance().getUid();
    }

    public interface OnRefreshTokenAndSessionListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface OnUserCreationListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface OnUserDeletionListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface OnDocumentIdChangeListener  {
        void onSuccess();
        void onFailure(Exception e);
    }
}

