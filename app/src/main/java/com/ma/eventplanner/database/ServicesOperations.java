package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Category;
import com.ma.eventplanner.model.Service;

import java.util.ArrayList;

public class ServicesOperations {
    private static final String TAG = "ServicesOperations";
    private final FirebaseFirestore db;
    CollectionReference servicesCollection;

    public ServicesOperations() {
        db = FirebaseFirestore.getInstance();
        servicesCollection = db.collection("services");
    }

    public void save(Service service, final ServicesOperations.ServiceSaveListener listener) {
        DocumentReference docRef = db.collection("services").document();
        String documentId = docRef.getId();
        service.setId(documentId);

        docRef.set(service)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Service saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving service", e);
                    listener.onFailure(e);
                });
    }


    public interface ServiceSaveListener {
        void onSuccess(String productId);
        void onFailure(Exception e);
    }

    public void getAllByCompanyId(String companyId, ServicesOperations.GetAllServicesListener<Service> listener) {
        servicesCollection
                .whereEqualTo("companyId", companyId)
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Service> services = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Service service = documentSnapshot.toObject(Service.class);
                        services.add(service);
                    }
                    listener.onSuccess(services);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByCategoryId(String companyId, Category category, ServicesOperations.GetAllServicesListener<Service> listener) {
        servicesCollection
                .whereEqualTo("companyId", companyId)
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                .whereEqualTo("category", category)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Service> products = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Service product = documentSnapshot.toObject(Service.class);
                        products.add(product);
                    }
                    listener.onSuccess(products);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllServices(ServicesOperations.GetAllServicesListener<Service> listener) {
        servicesCollection
                .whereEqualTo("deleted", false)
                .whereEqualTo("changed", false)
                //.whereEqualTo("available", true)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    ArrayList<Service> services = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        Service service = documentSnapshot.toObject(Service.class);
                        services.add(service);
                    }
                    listener.onSuccess(services);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllServicesListener<T> {
        void onSuccess(ArrayList<T> result);
        void onFailure(Exception e);
    }

    public void delete(String id, ServicesOperations.DeleteServiceListener listener) {
        servicesCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface DeleteServiceListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public void update(Service service, ServicesOperations.UpdateServiceListener listener) {
        String serviceId = service.getId();
        if (serviceId == null) {
            Log.e(TAG, "Service ID is null. Cannot update product.");
            listener.onFailure(new IllegalArgumentException("Service ID is null"));
            return;
        }

        servicesCollection.document(serviceId)
                .set(service)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "Service updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating service", e);
                    listener.onFailure(e);
                });
    }

    public interface UpdateServiceListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public void getById(String id, final GetServiceByIdListener listener) {
        servicesCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        Service service = documentSnapshot.toObject(Service.class);
                        listener.onSuccess(service);
                    } else {
                        listener.onFailure(new Exception("Service not found"));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetServiceByIdListener {
        void onSuccess(Service service);
        void onFailure(Exception e);
    }
}
