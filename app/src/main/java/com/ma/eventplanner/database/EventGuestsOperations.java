package com.ma.eventplanner.database;

import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.Event;
import com.ma.eventplanner.model.EventGuest;

import java.util.ArrayList;
import java.util.List;

public class EventGuestsOperations {
    private static final String TAG = "EventGuestsOperations";

    private final CollectionReference eventsCollection;

    private final FirebaseFirestore db;

    public EventGuestsOperations() {
        db = FirebaseFirestore.getInstance();
        this.eventsCollection = db.collection("guests");
    }

    public void getAllByEventId(String eventId, EventGuestsOperations.GetAllEventGuestsListener<EventGuest> listener) {
        eventsCollection
                .whereEqualTo("eventId", eventId)
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<EventGuest> events = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        EventGuest event = documentSnapshot.toObject(EventGuest.class);
                        events.add(event);
                    }
                    listener.onSuccess(events);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, EventGuestsOperations.GetEventGuestByIdListener<EventGuest> listener) {
        eventsCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        EventGuest event = documentSnapshot.toObject(EventGuest.class);
                        listener.onSuccess(event);
                    } else {
                        listener.onFailure(new Exception("EventGuest not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(EventGuest event, final EventGuestsOperations.SaveEventGuestListener listener) {
        DocumentReference docRef = db.collection("guests").document();
        String documentId = docRef.getId();
        event.setId(documentId);

        docRef.set(event)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "EventGuest saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving eventGuest", e);
                    listener.onFailure(e);
                });
    }

    public void update(EventGuest event, EventGuestsOperations.UpdateEventGuestListener listener) {
        String eventId = event.getId();
        if (eventId == null) {
            Log.e(TAG, "EventGuest ID is null. Cannot update event.");
            listener.onFailure(new IllegalArgumentException("EventGuest ID is null"));
            return;
        }

        eventsCollection.document(eventId)
                .set(event)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "EventGuest updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating eventGuest", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, EventGuestsOperations.DeleteEventGuestListener listener) {
        eventsCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllEventGuestsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetEventGuestByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveEventGuestListener {
        void onSuccess(String eventId);
        void onFailure(Exception e);
    }

    public interface UpdateEventGuestListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteEventGuestListener {
        void onSuccess();
        void onFailure(Exception e);
    }
}
