package com.ma.eventplanner.database;
import android.util.Log;

import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.ma.eventplanner.model.RequestStatus;
import com.ma.eventplanner.model.ServiceRequest;

import java.util.ArrayList;
import java.util.List;

public class ServiceRequestsOperations {
    private static final String TAG = "ServiceRequestsOperations";
    private final CollectionReference serviceRequestsCollection;
    private final FirebaseFirestore db;

    public ServiceRequestsOperations() {
        db = FirebaseFirestore.getInstance();
        this.serviceRequestsCollection = db.collection("service-requests");
    }

    public void getAll(GetAllServiceRequestsListener<ServiceRequest> listener) {
        serviceRequestsCollection
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ServiceRequest> serviceRequests = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ServiceRequest serviceRequest = documentSnapshot.toObject(ServiceRequest.class);
                        serviceRequests.add(serviceRequest);
                    }
                    listener.onSuccess(serviceRequests);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getAllByStatus(RequestStatus status, GetAllServiceRequestsListener<ServiceRequest> listener) {
        serviceRequestsCollection
                .whereEqualTo("status", status.toString())
                .get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<ServiceRequest> serviceRequests = new ArrayList<>();
                    for (QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots) {
                        ServiceRequest serviceRequest = documentSnapshot.toObject(ServiceRequest.class);
                        serviceRequests.add(serviceRequest);
                    }
                    listener.onSuccess(serviceRequests);
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void getById(String id, GetServiceRequestByIdListener<ServiceRequest> listener) {
        serviceRequestsCollection.document(id)
                .get()
                .addOnSuccessListener(documentSnapshot -> {
                    if (documentSnapshot.exists()) {
                        ServiceRequest serviceRequest = documentSnapshot.toObject(ServiceRequest.class);
                        listener.onSuccess(serviceRequest);
                    } else {
                        listener.onFailure(new Exception("ServiceRequest not found with id: " + id));
                    }
                })
                .addOnFailureListener(listener::onFailure);
    }

    public void save(ServiceRequest serviceRequest, final SaveServiceRequestListener listener) {
        DocumentReference docRef = db.collection("service-requests").document();
        String documentId = docRef.getId();
        serviceRequest.setId(documentId);

        docRef.set(serviceRequest)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "ServiceRequest saved successfully");
                    listener.onSuccess(documentId);
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error saving service request", e);
                    listener.onFailure(e);
                });
    }

    public void update(ServiceRequest serviceRequest, UpdateServiceRequestListener listener) {
        String serviceRequestId = serviceRequest.getId();
        if (serviceRequestId == null) {
            Log.e(TAG, "ServiceRequest ID is null. Cannot update service request.");
            listener.onFailure(new IllegalArgumentException("ServiceRequest ID is null"));
            return;
        }

        serviceRequestsCollection.document(serviceRequestId)
                .set(serviceRequest)
                .addOnSuccessListener(aVoid -> {
                    Log.d(TAG, "ServiceRequest updated successfully");
                    listener.onSuccess();
                })
                .addOnFailureListener(e -> {
                    Log.e(TAG, "Error updating service request", e);
                    listener.onFailure(e);
                });
    }

    public void delete(String id, DeleteServiceRequestListener listener) {
        serviceRequestsCollection.document(id)
                .delete()
                .addOnSuccessListener(aVoid -> listener.onSuccess())
                .addOnFailureListener(listener::onFailure);
    }

    public interface GetAllServiceRequestsListener<T> {
        void onSuccess(List<T> result);
        void onFailure(Exception e);
    }

    public interface GetServiceRequestByIdListener<T> {
        void onSuccess(T result);
        void onFailure(Exception e);
    }

    public interface SaveServiceRequestListener {
        void onSuccess(String serviceRequestId);
        void onFailure(Exception e);
    }

    public interface UpdateServiceRequestListener {
        void onSuccess();
        void onFailure(Exception e);
    }

    public interface DeleteServiceRequestListener {
        void onSuccess();
        void onFailure(Exception e);
    }

}
