package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;

public class Service extends Item {
    private Subcategory subcategory;
    private String specificities;
    private List<Employee> employees;
    private int reservationDeadline;
    private int cancellationDeadline;
    private int duration;
    private int minEngagement;
    private int maxEngagement;
    private AcceptanceType acceptanceType;

    public Service() { }

    public Service(String id, String companyId, Category category, String name, String description, double price, int discount, List<Integer> images, List<EventType> events, boolean isVisible, boolean isAvailable, boolean isDeleted, Subcategory subcategory, String specificities, List<Employee> employees, int reservationDeadline, int cancellationDeadline, int duration, AcceptanceType acceptanceType) {
        super(id, companyId, category, name, description, price, discount, images, events, isVisible, isAvailable, isDeleted);
        this.subcategory = subcategory;
        this.specificities = specificities;
        this.employees = employees;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.acceptanceType = acceptanceType;
        this.duration = duration;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public String getSpecificities() {
        return specificities;
    }

    public void setSpecificities(String specificities) {
        this.specificities = specificities;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public int getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(int reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public int getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(int cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public AcceptanceType getAcceptanceType() {
        return acceptanceType;
    }

    public void setAcceptanceType(AcceptanceType acceptanceType) {
        this.acceptanceType = acceptanceType;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getMinEngagement() {
        return minEngagement;
    }

    public void setMinEngagement(int minEngagement) {
        this.minEngagement = minEngagement;
    }

    public int getMaxEngagement() {
        return maxEngagement;
    }

    public void setMaxEngagement(int maxEngagement) {
        this.maxEngagement = maxEngagement;
    }

    @NonNull
    @Override
    public String toString() {
        return "Service{" +
                "subcategory=" + subcategory +
                ", specificities='" + specificities + '\'' +
                ", employees=" + employees +
                ", reservationDeadline=" + reservationDeadline +
                ", cancellationDeadline=" + cancellationDeadline +
                ", acceptanceType=" + acceptanceType +
                ", duration=" + duration +
                "} " + super.toString();
    }

    protected Service(Parcel in) {
        super(in);
        subcategory = in.readParcelable(Subcategory.class.getClassLoader());
        specificities = in.readString();
        employees = in.createTypedArrayList(Employee.CREATOR);
        reservationDeadline = in.readInt();
        cancellationDeadline = in.readInt();
        acceptanceType = AcceptanceType.valueOf(in.readString());
        duration = in.readInt();
    }

    public static final Creator<Service> CREATOR = new Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(subcategory, flags);
        dest.writeString(specificities);
        dest.writeTypedList(employees);
        dest.writeInt(reservationDeadline);
        dest.writeInt(cancellationDeadline);
        dest.writeString(acceptanceType.name());
        dest.writeInt(duration);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
