package com.ma.eventplanner.model;

public class ProductRequest {
    private String id;
    private String ownerId;
    private Product product;
    private Subcategory requestedSubcategory;
    private RequestStatus status;

    public ProductRequest() {
    }

    public ProductRequest(String id, String ownerId, Product product, Subcategory requestedSubcategory, RequestStatus status) {
        this.id = id;
        this.ownerId = ownerId;
        this.product = product;
        this.requestedSubcategory = requestedSubcategory;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Subcategory getRequestedSubcategory() {
        return requestedSubcategory;
    }

    public void setRequestedSubcategory(Subcategory requestedSubcategory) {
        this.requestedSubcategory = requestedSubcategory;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }
}
