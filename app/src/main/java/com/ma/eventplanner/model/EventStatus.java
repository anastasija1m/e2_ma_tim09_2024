package com.ma.eventplanner.model;

public enum EventStatus {

    RESERVED,
    OCCUPIED
}
