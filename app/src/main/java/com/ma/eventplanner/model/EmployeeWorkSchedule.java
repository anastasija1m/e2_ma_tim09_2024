package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.ServerTimestamp;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class EmployeeWorkSchedule implements Parcelable {

    private Map<String, EmployeeWorkHours> workHoursMap;
    @ServerTimestamp
    private Date scheduleStartDate;
    @ServerTimestamp
    private Date scheduleEndDate;

    public EmployeeWorkSchedule() {
        workHoursMap = new HashMap<>();
        initializeWorkHours();
    }

    public void initializeWorkHours() {
        workHoursMap.put(DayOfWeek.MONDAY.name(), new EmployeeWorkHours(LocalTime.of(9, 0).toString(), LocalTime.of(17, 0).toString()));
        workHoursMap.put(DayOfWeek.TUESDAY.name(), new EmployeeWorkHours(LocalTime.of(9, 0).toString(), LocalTime.of(17, 0).toString()));
        workHoursMap.put(DayOfWeek.WEDNESDAY.name(), new EmployeeWorkHours(LocalTime.of(10, 0).toString(), LocalTime.of(18, 0).toString()));
        workHoursMap.put(DayOfWeek.THURSDAY.name(), new EmployeeWorkHours(LocalTime.of(10, 0).toString(), LocalTime.of(18, 0).toString()));
        workHoursMap.put(DayOfWeek.FRIDAY.name(), new EmployeeWorkHours(LocalTime.of(9, 0).toString(), LocalTime.of(17, 0).toString()));

        scheduleStartDate = java.sql.Date.valueOf(String.valueOf(LocalDate.now().with(DayOfWeek.MONDAY)));
        scheduleEndDate = java.sql.Date.valueOf(String.valueOf(LocalDate.now().with(DayOfWeek.FRIDAY)));
    }

    protected EmployeeWorkSchedule(Parcel in) {
        int size = in.readInt();
        workHoursMap = new HashMap<>(size);
        for (int i = 0; i < size; i++) {
            String day = in.readString();
            EmployeeWorkHours hours = in.readParcelable(EmployeeWorkHours.class.getClassLoader());
            workHoursMap.put(day, hours);
        }
    }

    public Map<String, EmployeeWorkHours> getWorkHoursMap() {
        return workHoursMap;
    }

    public void setWorkHoursMap(Map<String, EmployeeWorkHours> workHoursMap) {
        this.workHoursMap = workHoursMap;
    }

    public void addWorkHoursForDay(DayOfWeek day, EmployeeWorkHours hours) {
        workHoursMap.put(day.name(), hours);
    }

    public void removeWorkHoursForDay(String day) {
        workHoursMap.remove(day);
    }

    public EmployeeWorkHours getWorkHoursForDay(String day) {
        return workHoursMap.get(day);
    }

    public Date getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(Date scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public Date getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(Date scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(workHoursMap.size());
        for (Map.Entry<String, EmployeeWorkHours> entry : workHoursMap.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeParcelable(entry.getValue(), flags);
        }
    }

    public static final Creator<EmployeeWorkSchedule> CREATOR = new Creator<EmployeeWorkSchedule>() {

        @Override
        public EmployeeWorkSchedule createFromParcel(Parcel in) {
            return new EmployeeWorkSchedule(in);
        }

        @Override
        public EmployeeWorkSchedule[] newArray(int size) {
            return new EmployeeWorkSchedule[size];
        }
    };
}
