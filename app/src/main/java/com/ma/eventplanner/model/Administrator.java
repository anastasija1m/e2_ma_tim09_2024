package com.ma.eventplanner.model;

import android.telephony.PhoneNumberUtils;

public class Administrator extends User{
    private String firstName;
    private String lastName;
    private String phoneNumber;

    public Administrator() {
    }

    public Administrator(String email, String password, String passwordConfirm, Role role, String firstName, String lastName, String phoneNumber) {
        super(email, password, passwordConfirm, role);
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;

        validate();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private void validate() {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("First name cannot be empty");
        }

        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("Last name cannot be empty");
        }

        if (phoneNumber == null || phoneNumber.isEmpty() ||  !PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            throw new IllegalArgumentException("Phone number is invalid");
        }
    }
}
