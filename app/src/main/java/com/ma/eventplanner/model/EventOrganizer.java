package com.ma.eventplanner.model;

import android.telephony.PhoneNumberUtils;

public class EventOrganizer extends User {
    private String firstName;
    private String lastName;
    private String imageName;
    private Address address;
    private String phoneNumber;

    public EventOrganizer() {

    }

    public EventOrganizer(String email, String password, String passwordConfirm, Role role, String firstName, String lastName, String imageName, Address address, String phoneNumber) {
        super(email, password, passwordConfirm, role);
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageName = imageName;
        this.address = address;
        this.phoneNumber = phoneNumber;

        //validate();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private void validate() {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("First name cannot be empty");
        }

        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("Last name cannot be empty");
        }

        if (imageName == null) {
            throw new IllegalArgumentException("Image name cannot be empty");
        }

        if (phoneNumber == null || phoneNumber.isEmpty() ||  !PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            throw new IllegalArgumentException("Phone number is invalid");
        }
    }
}
