package com.ma.eventplanner.model;

public enum Role {
    EVENT_ORGANIZER,
    OWNER,
    EMPLOYEE,
    ADMIN
}
