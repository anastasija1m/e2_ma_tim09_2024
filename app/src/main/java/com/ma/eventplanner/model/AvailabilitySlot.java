package com.ma.eventplanner.model;

public class AvailabilitySlot {
    private String startTime;
    private String endTime;
    private boolean isAvailable;

    public AvailabilitySlot(String startTime, String endTime, boolean isAvailable) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.isAvailable = isAvailable;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public boolean isAvailable() {
        return isAvailable;
    }
}

