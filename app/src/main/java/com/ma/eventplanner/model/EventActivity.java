package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.LocalTime;

public class EventActivity implements Parcelable {
    private String id;
    private String eventId;
    private String name;
    private String description;
    private Long startTime;
    private Long endTime;
    private String location;

    public EventActivity() {}

    public EventActivity(String id, String eventId, String name, String description, Long startTime, Long endTime, String location) {
        this.id = id;
        this.eventId = eventId;
        this.name = name;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
        this.location = location;
    }

    protected EventActivity(Parcel in) {
        id = in.readString();
        eventId = in.readString();
        name = in.readString();
        description = in.readString();
        startTime = in.readLong();
        endTime = in.readLong();
        location = in.readString();
    }

    public static final Creator<EventActivity> CREATOR = new Creator<EventActivity>() {
        @Override
        public EventActivity createFromParcel(Parcel in) {
            return new EventActivity(in);
        }

        @Override
        public EventActivity[] newArray(int size) {
            return new EventActivity[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @NonNull
    @Override
    public String toString() {
        return "EventActivity{" +
                "id=" + id +
                ", eventId=" + eventId +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", location='" + location + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(eventId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeLong(startTime);
        dest.writeLong(endTime);
        dest.writeString(location);
    }
}
