package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import java.util.Date;

public class CompanyReport implements Parcelable {
    private String id;
    private String reporterId;   // ID of the person who reported
    private String reportedCompanyId;  // ID of the company being reported
    private Date reportDate;     // Date of the report
    private String reportReason; // Reason for the report
    private ReportStatus status;       // Status of the report (reported, accepted, rejected)


    public CompanyReport() {
    }

    public CompanyReport(String id, String reporterId, String reportedCompanyId, Date reportDate, String reportReason, ReportStatus status) {
        this.id = id;
        this.reporterId = reporterId;
        this.reportedCompanyId = reportedCompanyId;
        this.reportDate = reportDate;
        this.reportReason = reportReason;
        this.status = status;

        validate();
    }

    protected CompanyReport(Parcel in) {
        this.id = in.readString();
        this.reporterId = in.readString();
        this.reportedCompanyId = in.readString();
        this.reportDate = new Date(in.readLong());
        this.reportReason = in.readString();
        this.status = ReportStatus.fromString(in.readString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReporterId() {
        return reporterId;
    }

    public void setReporterId(String reporterId) {
        this.reporterId = reporterId;
    }

    public String getReportedCompanyId() {
        return reportedCompanyId;
    }

    public void setReportedCompanyId(String reportedCompanyId) {
        this.reportedCompanyId = reportedCompanyId;
    }

    public Date getReportDate() {
        return reportDate;
    }

    public void setReportDate(Date reportDate) {
        this.reportDate = reportDate;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    private void validate() {
        if (reporterId == null || reporterId.isEmpty()) {
            throw new IllegalArgumentException("Reporter ID cannot be empty");
        }

        if (reportedCompanyId == null || reportedCompanyId.isEmpty()) {
            throw new IllegalArgumentException("Reported company ID cannot be empty");
        }

        if (reportDate == null) {
            throw new IllegalArgumentException("Report date cannot be null");
        }

        if (reportReason == null || reportReason.isEmpty()) {
            throw new IllegalArgumentException("Report reason cannot be empty");
        }

        if (status == null) {
            throw new IllegalArgumentException("Status cannot be null");
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(reporterId);
        dest.writeString(reportedCompanyId);
        dest.writeLong(reportDate.getTime());
        dest.writeString(reportReason);
        dest.writeString(status.toString());
    }

    public static final Creator<CompanyReport> CREATOR = new Creator<CompanyReport>() {
        @Override
        public CompanyReport createFromParcel(Parcel in) {
            return new CompanyReport(in);
        }

        @Override
        public CompanyReport[] newArray(int size) {
            return new CompanyReport[size];
        }
    };
}
