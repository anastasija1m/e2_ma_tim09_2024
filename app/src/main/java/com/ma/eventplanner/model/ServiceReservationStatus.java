package com.ma.eventplanner.model;

public enum ServiceReservationStatus {

    NEW,
    CANCELLED_PUP,
    CANCELLED_ORGANIZER,
    CANCELLED_ADMIN,
    ACCEPTED,
    COMPLETED
}
