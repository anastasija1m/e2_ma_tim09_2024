package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class EventType implements Parcelable {
    private String id;
    private String typeName;
    private String description;
    private boolean isActive;
    private List<String> suggestedSubcategoryIds;
    //private List<Subcategory> suggestedSubcategories;


    public EventType() { }

    public EventType(String id, String typeName, String description, /*List<Subcategory> suggestedSubcategories*/ List<String> suggestedSubcategoryIds, boolean isActive) {
        this.id = id;
        this.typeName = typeName;
        this.description = description;
        /*this.suggestedSubcategories = suggestedSubcategories;*/
        this.suggestedSubcategoryIds = suggestedSubcategoryIds;
        this.isActive = isActive;
    }

    protected EventType(Parcel in) {
        id = in.readString();
        typeName = in.readString();
        description = in.readString();
        isActive = in.readByte() != 0;
        //suggestedSubcategories = new ArrayList<>();
        //in.readList(suggestedSubcategories, Subcategory.class.getClassLoader());
        in.readList(suggestedSubcategoryIds, String.class.getClassLoader());
    }

    public static final Creator<EventType> CREATOR = new Creator<EventType>() {
        @Override
        public EventType createFromParcel(Parcel in) {
            return new EventType(in);
        }

        @Override
        public EventType[] newArray(int size) {
            return new EventType[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /*
    public List<Subcategory> getSuggestedSubcategories() {
        return suggestedSubcategories;
    }

    public void setSuggestedSubcategories(List<Subcategory> suggestedSubcategories) {
        this.suggestedSubcategories = suggestedSubcategories;
    }
     */

    public boolean getIsActive() {
        return isActive;
    }

    public List<String> getSuggestedSubcategoryIds() {
        return suggestedSubcategoryIds;
    }

    public void setSuggestedSubcategoryIds(List<String> suggestedSubcategoryIds) {
        this.suggestedSubcategoryIds = suggestedSubcategoryIds;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(typeName);
        dest.writeString(description);
        dest.writeByte((byte) (isActive ? 1 : 0));
        //dest.writeList(suggestedSubcategories);
        dest.writeList(suggestedSubcategoryIds);
    }

    @NonNull
    @Override
    public String toString() {
        return "EventType{" +
                "id=" + id +
                ", typeName='" + typeName + '\'' +
                ", description='" + description + '\'' +
                ", suggestedSubcategories=" /*+ suggestedSubcategories */ +
                ", isActive=" + isActive +
                '}';
    }
}