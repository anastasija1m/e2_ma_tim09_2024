package com.ma.eventplanner.model;

import java.time.DayOfWeek;

public class WorkDays {
    private DayOfWeek day;
    private WorkHours workHours;

    public WorkDays() {
    }

    public WorkDays(DayOfWeek day, WorkHours workHours) {
        this.day = day;
        this.workHours = workHours;
    }

    public DayOfWeek getDay() {
        return day;
    }

    public void setDay(DayOfWeek day) {
        this.day = day;
    }

    public WorkHours getWorkHours() {
        return workHours;
    }

    public void setWorkHours(WorkHours workHours) {
        this.workHours = workHours;
    }
}
