package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.time.LocalTime;


public class EmployeeWorkHours implements Parcelable {

    private String startTime;
    private String endTime;

    public EmployeeWorkHours(String startTime, String endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public EmployeeWorkHours() {
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected EmployeeWorkHours(Parcel in) {
        startTime = in.readString();
        endTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(startTime);
        dest.writeString(endTime);
    }

    public String validate() {
        LocalTime start = LocalTime.parse(startTime);
        LocalTime end = LocalTime.parse(endTime);

        if (start.isAfter(end)) {
            return "Start time cannot be after end time";
        }
        return null;
    }

    public static final Creator<EmployeeWorkHours> CREATOR = new Creator<EmployeeWorkHours>() {

        @Override
        public EmployeeWorkHours createFromParcel(Parcel in) {
            return new EmployeeWorkHours(in);
        }

        @Override
        public EmployeeWorkHours[] newArray(int size) {
            return new EmployeeWorkHours[size];
        }
    };
}
