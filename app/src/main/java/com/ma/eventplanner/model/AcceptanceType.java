package com.ma.eventplanner.model;

public enum AcceptanceType {
    AUTOMATIC("Automatic"),
    MANUAL("Manual");

    private final String displayName;

    AcceptanceType(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}
