package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Event implements Parcelable {
    private String id;
    private String name;
    private String description;
    private int maxParticipants;
    private boolean isPrivate;
    private String location;
    private int maxDistanceKm;
    private Long date;
    private String typeId;
    private String organizerId;
    private List<BudgetSubcategory> budgetSubcategories;

    public Event() {
        budgetSubcategories = new ArrayList<>();
    }

    public Event(String id, String name, String description, int maxParticipants, boolean isPrivate, String location, int maxDistanceKm, Long date, String typeId, String organizerId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.maxParticipants = maxParticipants;
        this.isPrivate = isPrivate;
        this.location = location;
        this.maxDistanceKm = maxDistanceKm;
        this.date = date;
        this.typeId = typeId;
        this.organizerId = organizerId;
        budgetSubcategories = new ArrayList<>();
    }

    protected Event(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        maxParticipants = in.readInt();
        isPrivate = in.readBoolean();
        location = in.readString();
        maxDistanceKm = in.readInt();
        date = in.readLong();
        typeId = in.readString();
        organizerId = in.readString();
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getMaxParticipants() {
        return maxParticipants;
    }

    public void setMaxParticipants(int maxParticipants) {
        this.maxParticipants = maxParticipants;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMaxDistanceKm() {
        return maxDistanceKm;
    }

    public void setMaxDistanceKm(int maxDistanceKm) {
        this.maxDistanceKm = maxDistanceKm;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getOrganizerId() {
        return organizerId;
    }

    public void setOrganizerId(String organizerId) {
        this.organizerId = organizerId;
    }

    public List<BudgetSubcategory> getBudgetSubcategories() {
        return budgetSubcategories;
    }

    public void setBudgetSubcategories(List<BudgetSubcategory> budgetSubcategories) {
        this.budgetSubcategories = budgetSubcategories;
    }

    @NonNull
    @Override
    public String toString() {
        return "EventTemp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", maxParticipants=" + maxParticipants +
                ", isPrivate=" + isPrivate +
                ", location='" + location + '\'' +
                ", maxDistanceKm=" + maxDistanceKm +
                ", date=" + date +
                ", typeId=" + typeId +
                ", organizerId=" + organizerId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeInt(maxParticipants);
        dest.writeBoolean(isPrivate);
        dest.writeString(location);
        dest.writeInt(maxDistanceKm);
        dest.writeLong(date);
        dest.writeString(typeId);
        dest.writeString(organizerId);
    }
}
