package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class CompanyRating implements Parcelable {

    public String comment;
    public Long rating;
    @ServerTimestamp
    private Date ratingDate;
    public String organizerEmail;
    public String companyId;

    public CompanyRating() {

    }

    public CompanyRating(String comment, Long rating, Date ratingDate, String organizerEmail, String companyId) {
        this.comment = comment;
        this.rating = rating;
        this.ratingDate = ratingDate;
        this.organizerEmail = organizerEmail;
        this.companyId = companyId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getRating() {
        return rating;
    }

    public void setRating(Long rating) {
        this.rating = rating;
    }

    public Date getRatingDate() {
        return ratingDate;
    }

    public void setRatingDate(Date ratingDate) {
        this.ratingDate = ratingDate;
    }

    public String getOrganizerEmail() {
        return organizerEmail;
    }

    public void setOrganizerEmail(String organizerEmail) {
        this.organizerEmail = organizerEmail;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(comment);
        dest.writeLong(rating);
        dest.writeSerializable(ratingDate);
        dest.writeString(organizerEmail);
        dest.writeString(companyId);
    }

    protected CompanyRating(Parcel in) {
        comment = in.readString();
        rating = in.readLong();
        ratingDate = (Date) in.readSerializable();
        organizerEmail = in.readString();
        companyId = in.readString();
    }

    public static final Creator<CompanyRating> CREATOR = new Creator<CompanyRating>() {

        @Override
        public CompanyRating createFromParcel(Parcel in) {
            return new CompanyRating(in);
        }

        @Override
        public CompanyRating[] newArray(int size) {
            return new CompanyRating[size];
        }
    };
}