package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.regex.Pattern;

public class User implements Parcelable {
    private String email;
    private String password;
    private Role role;
    private String FcmToken;

    public User() {
    }

    public User(String email, String password, String passwordConfirm, Role role) {
        this.email = email;
        this.password = password;
        this.role = role;

        //validate(passwordConfirm);
    }

    protected User(Parcel in) {
        email = in.readString();
        password = in.readString();
        role = Role.valueOf(in.readString());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getFcmToken() {
        return FcmToken;
    }

    public void setFcmToken(String FcmToken) {
        this.FcmToken = FcmToken;
    }

    private void validate(String passwordConfirm) {
        if (email == null || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            throw new IllegalArgumentException("Invalid email format");
        }

        validatePassword(passwordConfirm);

        if (role == null) {
            throw new IllegalArgumentException("Role cannot be null");
        }
    }

    private void validatePassword(String passwordConfirm) {
        if (password.length() < 8) {
            throw new IllegalArgumentException("Password must be at least 8 characters long");
        }

        if (!Pattern.compile("[A-Z]").matcher(password).find()) {
            throw new IllegalArgumentException("Password must contain at least one uppercase letter");
        }

        if (!Pattern.compile("[0-9]").matcher(password).find()) {
            throw new IllegalArgumentException("Password must contain at least one digit");
        }

        if (!Pattern.compile("[!@#$%^&*()\\-_=+\\\\|\\[{\\]};:'\",<.>/?]").matcher(password).find()) {
            throw new IllegalArgumentException("Password must contain at least one special character");
        }

        if (!password.equals(passwordConfirm)) {
            throw new IllegalArgumentException("Password and password confirmation do not match");
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(email);
        dest.writeString(password);
        dest.writeString(role.toString());
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
