package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.PhoneNumberUtils;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class Company implements Parcelable {
    private String id;
    private String email;
    private String name;
    private Address address;
    private String phoneNumber;
    private String description;
    private String imageName;
    private List<String> categoryIds;
    private List<String> eventTypeIds;
    private WeeklyWorkingSchedule weeklyWorkingSchedule;

    public Company() {
    }

    public Company(String id, String email, String name, Address address, String phoneNumber, String description, String imageName) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.description = description;
        this.imageName = imageName;

        this.categoryIds = new ArrayList<>();
        this.eventTypeIds = new ArrayList<>();
        this.weeklyWorkingSchedule = new WeeklyWorkingSchedule();

        //validate();
    }

    protected Company(Parcel in) {
        this.id = in.readString();
        this.email = in.readString();
        this.name = in.readString();
        address = in.readParcelable(Address.class.getClassLoader());
        this.phoneNumber = in.readString();
        this.description = in.readString();
        this.imageName = in.readString();
        this.categoryIds = in.createStringArrayList();
        this.eventTypeIds = in.createStringArrayList();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<String> getEventTypeIds() {
        return eventTypeIds;
    }

    public void setEventTypeIds(List<String> eventTypeIds) {
        this.eventTypeIds = eventTypeIds;
    }

    public WeeklyWorkingSchedule getWeeklyWorkingSchedule() {
        return weeklyWorkingSchedule;
    }

    public void setWeeklyWorkingSchedule(WeeklyWorkingSchedule weeklyWorkingSchedule) {
        this.weeklyWorkingSchedule = weeklyWorkingSchedule;
    }

    private void validate() {
        if (email == null || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            throw new IllegalArgumentException("Invalid email format");
        }

        if (name == null || name.isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty");
        }

        if (phoneNumber == null || phoneNumber.isEmpty() ||  !PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            throw new IllegalArgumentException("Phone number is invalid");
        }

        if (description == null || description.isEmpty()) {
            throw new IllegalArgumentException("Description cannot be empty");
        }

        if (imageName == null) {
            throw new IllegalArgumentException("Image name cannot be empty");
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(email);
        dest.writeString(name);
        dest.writeParcelable(address, flags);
        dest.writeString(phoneNumber);
        dest.writeString(description);
        dest.writeString(imageName);
        dest.writeStringList(categoryIds);
        dest.writeStringList(eventTypeIds);
    }

    public static final Creator<Company> CREATOR = new Creator<Company>() {
        @Override
        public Company createFromParcel(Parcel in) {
            return new Company(in);
        }

        @Override
        public Company[] newArray(int size) {
            return new Company[size];
        }
    };
}
