package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WorkSchedule implements Parcelable {

    Long id;
    private Map<DayOfWeek, WorkHours> workHoursMap;
    private LocalDate scheduleStartDate;
    private LocalDate scheduleEndDate;

    public WorkSchedule() {
        workHoursMap = new HashMap<>();
        initializeWorkHours();
    }

    public void initializeWorkHours() {
        // Kreiranje instanci Date objekata za radno vreme
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 9);
        cal.set(Calendar.MINUTE, 0);
        Date startTime = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 17);
        Date endTime = cal.getTime();

        // Postavljanje radnog vremena za svaki dan u nedelji
        workHoursMap.put(DayOfWeek.MONDAY, new WorkHours(startTime, endTime));
        workHoursMap.put(DayOfWeek.TUESDAY, new WorkHours(startTime, endTime));

        cal.set(Calendar.HOUR_OF_DAY, 10);
        startTime = cal.getTime();

        cal.set(Calendar.HOUR_OF_DAY, 18);
        endTime = cal.getTime();

        workHoursMap.put(DayOfWeek.WEDNESDAY, new WorkHours(startTime, endTime));
        workHoursMap.put(DayOfWeek.THURSDAY, new WorkHours(startTime, endTime));
        workHoursMap.put(DayOfWeek.FRIDAY, new WorkHours(startTime, endTime));

        // Postavljanje početnog i krajnjeg datuma rasporeda na trenutni ponedeljak i petak
        cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        scheduleStartDate = getStartOfWeek(cal);

        cal.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
        scheduleEndDate = getEndOfWeek(cal);
    }

    private LocalDate getStartOfWeek(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());
        return LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

    private LocalDate getEndOfWeek(Calendar calendar) {
        calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() + 4);
        return LocalDate.of(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
    }

    protected WorkSchedule(Parcel in) {
        int size = in.readInt();
        workHoursMap = new HashMap<>(size);
        for (int i = 0; i < size; i++) {
            DayOfWeek day = DayOfWeek.of(in.readInt());
            WorkHours hours = in.readParcelable(WorkHours.class.getClassLoader());
            workHoursMap.put(day, hours);
        }
    }

    public Map<DayOfWeek, WorkHours> getWorkHoursMap() {
        return workHoursMap;
    }

    public void setWorkHoursMap(Map<DayOfWeek, WorkHours> workHoursMap) {
        this.workHoursMap = workHoursMap;
    }

    public void addWorkHoursForDay(DayOfWeek day, WorkHours hours) {
        workHoursMap.put(day, hours);
    }

    public void removeWorkHoursForDay(DayOfWeek day) {
        workHoursMap.remove(day);
    }

    public WorkHours getWorkHoursForDay(DayOfWeek day) {
        return workHoursMap.get(day);
    }

    public Long getId() {
        return id;
    }


    public LocalDate getScheduleStartDate() {
        return scheduleStartDate;
    }

    public void setScheduleStartDate(LocalDate scheduleStartDate) {
        this.scheduleStartDate = scheduleStartDate;
    }

    public LocalDate getScheduleEndDate() {
        return scheduleEndDate;
    }

    public void setScheduleEndDate(LocalDate scheduleEndDate) {
        this.scheduleEndDate = scheduleEndDate;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(workHoursMap.size());
        for (Map.Entry<DayOfWeek, WorkHours> entry : workHoursMap.entrySet()) {
            dest.writeInt(entry.getKey().getValue()); // Writing ordinal value of DayOfWeek
            dest.writeParcelable(entry.getValue(), flags);
        }
    }

    public static final Creator<WorkSchedule> CREATOR = new Creator<WorkSchedule>() {

        @Override
        public WorkSchedule createFromParcel(Parcel in) {
            return new WorkSchedule(in);
        }

        @Override
        public WorkSchedule[] newArray(int size) {
            return new WorkSchedule[size];
        }
    };
}
