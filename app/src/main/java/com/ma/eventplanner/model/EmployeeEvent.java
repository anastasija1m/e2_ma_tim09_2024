package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class EmployeeEvent implements Parcelable {

    private String name;
    private EventStatus status;
    @ServerTimestamp
    private Date date;
    private String startTime;
    private String endTime;

    public EmployeeEvent() { }

    public EmployeeEvent(String name, EventStatus status, Date date, String startTime, String endTime) {
        this.name = name;
        this.status = status;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String getName() {
        return name;
    }

    public EventStatus getStatus() {
        return status;
    }

    public Date getDate() {
        return date;
    }

    public String getStartTime() { // Change return type to String
        return startTime;
    }

    public String getEndTime() {   // Change return type to String
        return endTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(EventStatus status) {
        this.status = status;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setStartTime(String startTime) { // Change parameter type to String
        this.startTime = startTime;
    }

    public void setEndTime(String endTime) {     // Change parameter type to String
        this.endTime = endTime;
    }

    protected EmployeeEvent(Parcel in) {
        name = in.readString();
        status = EventStatus.valueOf(in.readString());
        date = new Date(in.readLong());
        startTime = in.readString();
        endTime = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(status.name());
        dest.writeLong(date.getTime());
        dest.writeString(startTime);
        dest.writeString(endTime);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EmployeeEvent> CREATOR = new Creator<EmployeeEvent>() {
        @Override
        public EmployeeEvent createFromParcel(Parcel in) {
            return new EmployeeEvent(in);
        }

        @Override
        public EmployeeEvent[] newArray(int size) {
            return new EmployeeEvent[size];
        }
    };
}
