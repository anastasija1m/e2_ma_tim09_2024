package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.PhoneNumberUtils;

import androidx.annotation.NonNull;

public class Owner extends User implements Parcelable {
    private String firstName;
    private String lastName;
    private String imageName;
    private Address address;
    private String phoneNumber;
    private String companyId;

    public Owner() {
    }

    public Owner(String email, String password, String passwordConfirm, Role role, String firstName, String lastName, String imageName, Address address, String phoneNumber, String companyId) {
        super(email, password, passwordConfirm, role);
        this.firstName = firstName;
        this.lastName = lastName;
        this.imageName = imageName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.companyId = companyId;
        //validate();
    }

    protected Owner(Parcel in) {
        super(in);
        firstName = in.readString();
        lastName = in.readString();
        imageName = in.readString();
        address = in.readParcelable(Address.class.getClassLoader());
        phoneNumber = in.readString();
        companyId = in.readString();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    private void validate() {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("First name cannot be empty");
        }

        if (lastName == null || lastName.isEmpty()) {
            throw new IllegalArgumentException("Last name cannot be empty");
        }

        if (imageName == null) {
            throw new IllegalArgumentException("Image name cannot be empty");
        }

        if (phoneNumber == null || phoneNumber.isEmpty() ||  !PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber)) {
            throw new IllegalArgumentException("Phone number is invalid");
        }
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(imageName);
        dest.writeParcelable(address, flags);
        dest.writeString(phoneNumber);
        dest.writeString(companyId);
    }

    public static final Creator<Owner> CREATOR = new Creator<Owner>() {
        @Override
        public Owner createFromParcel(Parcel in) {
            return new Owner(in);
        }

        @Override
        public Owner[] newArray(int size) {
            return new Owner[size];
        }
    };
}
