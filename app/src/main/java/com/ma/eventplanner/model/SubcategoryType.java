package com.ma.eventplanner.model;

public enum SubcategoryType {

    SERVICE,
    PRODUCT
}
