package com.ma.eventplanner.model;

public class ServiceRequest {
    private String id;
    private String ownerId;
    private Service service;
    private Subcategory requestedSubcategory;
    private RequestStatus status;

    public ServiceRequest() {
    }

    public ServiceRequest(String id, String ownerId, Service service, Subcategory requestedSubcategory, RequestStatus status) {
        this.id = id;
        this.ownerId = ownerId;
        this.service = service;
        this.requestedSubcategory = requestedSubcategory;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Subcategory getRequestedSubcategory() {
        return requestedSubcategory;
    }

    public void setRequestedSubcategory(Subcategory requestedSubcategory) {
        this.requestedSubcategory = requestedSubcategory;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }
}
