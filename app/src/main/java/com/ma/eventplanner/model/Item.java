package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class Item implements Parcelable {
    
    private String id;
    private String companyId;
    private Category category;
    private String name;
    private String description;
    private double price;
    private int discount;
    private List<Integer> images;
    private List<EventType> events;
    private boolean isVisible;
    private boolean isAvailable;
    private boolean isDeleted;
    private boolean isChanged;

    public Item() {
    }

    public Item(String id, String companyId, Category category, String name, String description, double price, int discount, List<Integer> images, List<EventType> events, boolean isVisible, boolean isAvailable, boolean isDeleted) {
        this.id = id;
        this.companyId = companyId;
        this.category = category;
        this.name = name;
        this.description = description;
        this.price = price;
        this.discount = discount;
        this.images = images;
        this.events = events;
        this.isVisible = isVisible;
        this.isAvailable = isAvailable;
        this.isDeleted = isDeleted;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public List<Integer> getImages() {
        return images;
    }

    public void setImages(List<Integer> images) {
        this.images = images;
    }

    public List<EventType> getEvents() {
        return events;
    }

    public void setEvents(List<EventType> events) {
        this.events = events;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean visible) {
        isVisible = visible;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isChanged() {
        return isChanged;
    }

    public void setChanged(boolean changed) {
        isChanged = changed;
    }

    @NonNull
    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                "companyId=" + companyId +
                ", category=" + category +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", discount=" + discount +
                ", images=" + images +
                ", events=" + events +
                ", isVisible=" + isVisible +
                ", isAvailable=" + isAvailable +
                ", isDeleted=" + isDeleted +
                '}';
    }

    protected Item(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readString();
        }
        if (in.readByte() == 0) {
            companyId = null;
        } else {
            companyId = in.readString();
        }
        category = in.readParcelable(Category.class.getClassLoader());
        name = in.readString();
        description = in.readString();
        price = in.readDouble();
        discount = in.readInt();

        // Read the size of the ArrayList
        int imageSize = in.readInt();
        images = new ArrayList<>(imageSize);

        // Read each integer item and add it to the ArrayList
        for (int i = 0; i < imageSize; i++) {
            images.add(in.readInt());
        }

        events = in.createTypedArrayList(EventType.CREATOR);
        isVisible = in.readByte() != 0;
        isAvailable = in.readByte() != 0;
        isDeleted = in.readByte() != 0;
    }


    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(id);
        }
        if (companyId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeString(companyId);
        }
        dest.writeParcelable(category, flags);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(price);
        dest.writeInt(discount);

        // Write the size of the ArrayList
        dest.writeInt(images.size());

        // Write each integer item to the Parcel
        for (Integer value : images) {
            dest.writeInt(value);
        }

        dest.writeTypedList(events);
        dest.writeByte((byte) (isVisible ? 1 : 0));
        dest.writeByte((byte) (isAvailable ? 1 : 0));
        dest.writeByte((byte) (isDeleted ? 1 : 0));
    }

}
