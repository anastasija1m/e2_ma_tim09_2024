package com.ma.eventplanner.model;

public enum RatingReportStatus {

    REPORTED,
    REJECTED,
    ACCEPTED
}
