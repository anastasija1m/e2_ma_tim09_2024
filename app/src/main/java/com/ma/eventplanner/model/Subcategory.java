package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Subcategory implements Parcelable {

    private String id;
    private String name;
    private String description;
    private SubcategoryType type;
    //private Category category;
    private String categoryId;

    public Subcategory() {
    }

    public Subcategory(String id, String name, String description, SubcategoryType type, /*Category category*/ String categoryId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        //this.category = category;
        this.categoryId = categoryId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubcategoryType getType() {
        return type;
    }

    public void setType(SubcategoryType type) {
        this.type = type;
    }

    /*
    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    */

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    @NonNull
    @Override
    public String toString() {
        return "Subcategory{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", type=" + type +
                 /*", category=" + category + */
                '}';
    }

    protected Subcategory(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        type = SubcategoryType.valueOf(in.readString());
        //category = in.readParcelable(Category.class.getClassLoader());
        categoryId = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(type.name());
        //dest.writeParcelable(category, flags);
        dest.writeString(categoryId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Subcategory> CREATOR = new Creator<Subcategory>() {
        @Override
        public Subcategory createFromParcel(Parcel in) {
            return new Subcategory(in);
        }

        @Override
        public Subcategory[] newArray(int size) {
            return new Subcategory[size];
        }
    };
}
