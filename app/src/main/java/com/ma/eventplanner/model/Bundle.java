package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.List;

public class Bundle extends Item {
    private List<Subcategory> subcategories;
    private List<Employee> employees;
    private List<Product> products;
    private List<Service> services;
    private int reservationDeadline;
    private int cancellationDeadline;
    private AcceptanceType acceptanceType;

    public Bundle() { }

    public Bundle(String id, String companyId, Category category, String name, String description, double price, int discount, List<Integer> images, List<EventType> events, boolean isVisible, boolean isAvailable, boolean isDeleted, List<Subcategory> subcategories, List<Employee> employees, List<Product> products, List<Service> services, int reservationDeadline, int cancellationDeadline, AcceptanceType acceptanceType) {
        super(id, companyId, category, name, description, price, discount, images, events, isVisible, isAvailable, isDeleted);
        this.subcategories = subcategories;
        this.employees = employees;
        this.products = products;
        this.services = services;
        this.reservationDeadline = reservationDeadline;
        this.cancellationDeadline = cancellationDeadline;
        this.acceptanceType = acceptanceType;
    }

    public List<Subcategory> getSubcategories() {
        return subcategories;
    }

    public void setSubcategories(List<Subcategory> subcategories) {
        this.subcategories = subcategories;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public int getReservationDeadline() {
        return reservationDeadline;
    }

    public void setReservationDeadline(int reservationDeadline) {
        this.reservationDeadline = reservationDeadline;
    }

    public int getCancellationDeadline() {
        return cancellationDeadline;
    }

    public void setCancellationDeadline(int cancellationDeadline) {
        this.cancellationDeadline = cancellationDeadline;
    }

    public AcceptanceType getAcceptanceType() {
        return acceptanceType;
    }

    public void setAcceptanceType(AcceptanceType acceptanceType) {
        this.acceptanceType = acceptanceType;
    }

    @NonNull
    @Override
    public String toString() {
        return "Service{" +
                "subcategory=" + subcategories +
                ", employees=" + employees +
                ", products=" + products +
                ", services=" + services +
                ", reservationDeadline=" + reservationDeadline +
                ", cancellationDeadline=" + cancellationDeadline +
                ", acceptanceType=" + acceptanceType +
                "} " + super.toString();
    }

    protected Bundle(Parcel in) {
        super(in);
        subcategories = in.createTypedArrayList(Subcategory.CREATOR);
        employees = in.createTypedArrayList(Employee.CREATOR);
        products = in.createTypedArrayList(Product.CREATOR);
        services = in.createTypedArrayList(Service.CREATOR);
        reservationDeadline = in.readInt();
        cancellationDeadline = in.readInt();
        acceptanceType = AcceptanceType.valueOf(in.readString());
    }

    public static final Creator<Bundle> CREATOR = new Creator<Bundle>() {
        @Override
        public Bundle createFromParcel(Parcel in) {
            return new Bundle(in);
        }

        @Override
        public Bundle[] newArray(int size) {
            return new Bundle[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(subcategories);
        dest.writeTypedList(employees);
        dest.writeTypedList(products);
        dest.writeTypedList(services);
        dest.writeInt(reservationDeadline);
        dest.writeInt(cancellationDeadline);
        dest.writeString(acceptanceType.name());
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
