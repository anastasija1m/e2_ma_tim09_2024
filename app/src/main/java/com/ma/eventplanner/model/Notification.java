package com.ma.eventplanner.model;

import java.util.Date;

public class Notification {
    private String id;
    private String title;
    private String content;
    private boolean isRead;
    private Date sent;
    private Date read;
    private String recipientId;

    public Notification() {
    }

    public Notification(String id, String title, String content, String recipientId) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.isRead = false;
        this.sent = new Date();
        this.read = null;
        this.recipientId = recipientId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }

    public Date getRead() {
        return read;
    }

    public void setRead(Date read) {
        this.read = read;
    }

    public String getRecipientId() {
        return recipientId;
    }

    public void setRecipientId(String recipientId) {
        this.recipientId = recipientId;
    }

    public boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(boolean read) {
        isRead = read;
    }

    public void markAsRead() {
        this.isRead = true;
        this.read = new Date();
    }
}
