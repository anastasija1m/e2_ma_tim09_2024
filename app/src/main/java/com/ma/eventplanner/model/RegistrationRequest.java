package com.ma.eventplanner.model;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.Date;

public class RegistrationRequest {
    String id;
    Owner owner;
    Company company;
    RequestStatus status;
    Date sent;

    public RegistrationRequest() {

    }

    public RegistrationRequest(String id, Owner owner, Company company, RequestStatus status, Date sent) {
        this.id = id;
        this.owner = owner;
        this.company = company;
        this.status = status;
        this.sent = sent;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getSent() {
        return sent;
    }

    public void setSent(Date sent) {
        this.sent = sent;
    }

    public void accept() {
        this.status = RequestStatus.ACCEPTED;
    }

    public void reject() {
        this.status = RequestStatus.REJECTED;
    }
}
