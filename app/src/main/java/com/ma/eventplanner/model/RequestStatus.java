package com.ma.eventplanner.model;

public enum RequestStatus {
    ON_PENDING,
    ACCEPTED,
    REJECTED
}
