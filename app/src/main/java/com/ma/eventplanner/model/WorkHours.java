package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class WorkHours implements Parcelable {

    private Date startTime;
    private Date endTime;

    public WorkHours() {
        this.startTime = null;
        this.endTime = null;
    }

    public WorkHours(Date startTime, Date endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String validate() {
        if (startTime.after(endTime)) {
            return "Start time cannot be after end time";
        }
        return null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    protected WorkHours(Parcel in) {
        startTime = new Date(in.readLong());
        endTime = new Date(in.readLong());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(startTime.getTime());
        dest.writeLong(endTime.getTime());
    }

    public static final Creator<WorkHours> CREATOR = new Creator<WorkHours>() {

        @Override
        public WorkHours createFromParcel(Parcel in) {
            return new WorkHours(in);
        }

        @Override
        public WorkHours[] newArray(int size) {
            return new WorkHours[size];
        }
    };
}