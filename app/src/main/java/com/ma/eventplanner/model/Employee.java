package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Employee extends User implements Parcelable {

    private String name;
    private String surname;
    private Address address;
    private String phoneNumber;
    private String imageName;
    private boolean isAccountActive;
    private String companyId;
    private EmployeeWorkSchedule workSchedule;
    private List<EmployeeEvent> events;

    public Employee(String email, String password, String passwordConfirm, Role role, String name, String surname, String imageName, Address address, String phoneNumber, boolean isAccountActive, String companyId) {
        super(email, password, passwordConfirm, role);
        this.name = name;
        this.surname = surname;
        this.imageName = imageName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.isAccountActive = isAccountActive;
        this.companyId = companyId;
        this.workSchedule = new EmployeeWorkSchedule();
        events = new ArrayList<>();
    }

    public Employee() {
        workSchedule = new EmployeeWorkSchedule();
    }

    public EmployeeWorkSchedule getWorkSchedule() {
        return workSchedule;
    }

    public void setWorkSchedule(EmployeeWorkSchedule workSchedule) {
        this.workSchedule = workSchedule;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public Address getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getImageName() {
        return imageName;
    }

    public boolean getIsAccountActive() {
        return isAccountActive;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public void setIsAccountActive(boolean isAccountActive) {
        this.isAccountActive = isAccountActive;
    }

    public List<EmployeeEvent> getEvents() {
        return events;
    }

    public void setEvents(List<EmployeeEvent> events) {
        this.events = events;
    }

    public void addEvent(EmployeeEvent event) {
        this.events.add(event);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(imageName);
        dest.writeParcelable(address, flags);
        dest.writeString(phoneNumber);
        dest.writeBoolean(isAccountActive);
        dest.writeString(companyId);
        dest.writeParcelable(workSchedule, flags);
    }

    protected Employee(Parcel in) {
        super(in);
        name = in.readString();
        surname = in.readString();
        imageName = in.readString();
        address = in.readParcelable(Address.class.getClassLoader());
        phoneNumber = in.readString();
        isAccountActive = in.readBoolean();
        companyId = in.readString();
        workSchedule = in.readParcelable(WorkSchedule.class.getClassLoader());
    }

    public static final Creator<Employee> CREATOR = new Creator<Employee>() {

        @Override
        public Employee createFromParcel(Parcel in) {
            return new Employee(in);
        }

        @Override
        public Employee[] newArray(int size) {
            return new Employee[size];
        }
    };
}