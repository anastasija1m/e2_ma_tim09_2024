package com.ma.eventplanner.model;

public enum ReportStatus {
    REPORTED, ACCEPTED, REJECTED;

    public static ReportStatus fromString(String status) {
        switch (status.toUpperCase()) {
            case "REPORTED":
                return REPORTED;
            case "ACCEPTED":
                return ACCEPTED;
            case "REJECTED":
                return REJECTED;
            default:
                throw new IllegalArgumentException("Unknown status: " + status);
        }
    }

    public String toString() {
        return name();
    }
}