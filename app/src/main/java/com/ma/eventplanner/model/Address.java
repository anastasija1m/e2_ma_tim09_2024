package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class Address implements Parcelable {
    private String country;
    private String city;
    private String postalCode;
    private String street;
    private String number;

    public Address() {
    }

    public Address(String country, String city, String postalCode, String street, String number) {
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.number = number;

        //validate();
    }

    public Address(Parcel in) {
        country = in.readString();
        city = in.readString();
        postalCode = in.readString();
        street = in.readString();
        number = in.readString();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    private void validate() {
        if (country == null || country.isEmpty()) {
            throw new IllegalArgumentException("Country cannot be empty");
        }

        if (city == null || city.isEmpty()){
            throw new IllegalArgumentException("City cannot be empty");
        }

        if (postalCode == null || postalCode.isEmpty()){
            throw new IllegalArgumentException("Postal code cannot be empty");
        }
        if (street == null || street.isEmpty()){
            throw new IllegalArgumentException("Street cannot be empty");
        }
        if (number == null || number.isEmpty()){
            throw new IllegalArgumentException("Number cannot be empty");
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(country);
        dest.writeString(city);
        dest.writeString(postalCode);
        dest.writeString(street);
        dest.writeString(number);
    }

    public static final Creator<Address> CREATOR = new Creator<Address>() {
        @Override
        public Address createFromParcel(Parcel in) {
            return new Address(in);
        }

        @Override
        public Address[] newArray(int size) {
            return new Address[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return street + " " + number + ", " + postalCode + " " + city + ", " + country;
    }
}
