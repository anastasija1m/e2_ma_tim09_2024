package com.ma.eventplanner.model;

import java.util.List;

public class BudgetSubcategory {
    private Category category;
    private Subcategory subcategory;
    private Integer price;
    private List<Item> items;
    public BudgetSubcategory() {}
    public BudgetSubcategory(Category category, Subcategory subcategory, Integer price, List<Item> items) {
        this.category = category;
        this.subcategory = subcategory;
        this.price = price;
        this.items = items;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
