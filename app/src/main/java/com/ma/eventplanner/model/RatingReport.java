package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Date;

public class RatingReport implements Parcelable {

    private String ownerEmail;
    private Date date;
    private String reportReason;
    private RatingReportStatus status;
    private String ratingId;

    public RatingReport() {

    }

    public RatingReport(String ownerEmail, Date date, String reportReason, RatingReportStatus status, String ratingId) {
        this.ownerEmail = ownerEmail;
        this.date = date;
        this.reportReason = reportReason;
        this.status = status;
        this.ratingId = ratingId;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public RatingReportStatus getStatus() {
        return status;
    }

    public void setStatus(RatingReportStatus status) {
        this.status = status;
    }

    public String getRatingId() {
        return ratingId;
    }

    public void setRatingId(String ratingId) {
        this.ratingId = ratingId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(ownerEmail);
        dest.writeString(reportReason);
        dest.writeString(status.toString());
        dest.writeString(ratingId);
        dest.writeSerializable(date);
    }

    protected RatingReport(Parcel in) {
        ownerEmail = in.readString();
        reportReason = in.readString();
        status = RatingReportStatus.valueOf(in.readString());
        ratingId = in.readString();
        date = (Date) in.readSerializable();
    }

    public static final Creator<RatingReport> CREATOR = new Creator<RatingReport>() {
        @Override
        public RatingReport createFromParcel(Parcel in) {
            return new RatingReport(in);
        }

        @Override
        public RatingReport[] newArray(int size) {
            return new RatingReport[size];
        }
    };
}
