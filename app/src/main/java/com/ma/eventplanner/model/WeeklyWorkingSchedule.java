package com.ma.eventplanner.model;

import java.time.DayOfWeek;

public class WeeklyWorkingSchedule {
    private WorkDays monday;
    private WorkDays tuesday;
    private WorkDays wednesday;
    private WorkDays thursday;
    private WorkDays friday;
    private WorkDays saturday;
    private WorkDays sunday;

    public WeeklyWorkingSchedule() {
        this.monday = new WorkDays(DayOfWeek.MONDAY, new WorkHours());
        this.tuesday = new WorkDays(DayOfWeek.TUESDAY, new WorkHours());
        this.wednesday = new WorkDays(DayOfWeek.WEDNESDAY, new WorkHours());
        this.thursday = new WorkDays(DayOfWeek.THURSDAY, new WorkHours());
        this.friday = new WorkDays(DayOfWeek.FRIDAY, new WorkHours());
        this.saturday = new WorkDays(DayOfWeek.SATURDAY, new WorkHours());
        this.sunday = new WorkDays(DayOfWeek.SUNDAY, new WorkHours());
    }

    public WorkDays getMonday() {
        return monday;
    }

    public void setMonday(WorkDays monday) {
        this.monday = monday;
    }

    public WorkDays getTuesday() {
        return tuesday;
    }

    public void setTuesday(WorkDays tuesday) {
        this.tuesday = tuesday;
    }

    public WorkDays getWednesday() {
        return wednesday;
    }

    public void setWednesday(WorkDays wednesday) {
        this.wednesday = wednesday;
    }

    public WorkDays getThursday() {
        return thursday;
    }

    public void setThursday(WorkDays thursday) {
        this.thursday = thursday;
    }

    public WorkDays getFriday() {
        return friday;
    }

    public void setFriday(WorkDays friday) {
        this.friday = friday;
    }

    public WorkDays getSaturday() {
        return saturday;
    }

    public void setSaturday(WorkDays saturday) {
        this.saturday = saturday;
    }

    public WorkDays getSunday() {
        return sunday;
    }

    public void setSunday(WorkDays sunday) {
        this.sunday = sunday;
    }
}
