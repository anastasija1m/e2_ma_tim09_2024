package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

public class ServiceReservation implements Parcelable {

    private String id;
    private String eventId;
    private String serviceId;
    private String employeeEmail;
    private String organizerEmail;
    private Date date;
    private String packageId;
    private String startTime;
    private String endTime;
    private ServiceReservationStatus serviceReservationStatus;
    private String companyId;
    private Date statusModifiedDate;

    public ServiceReservation() {
    }

    public ServiceReservation(String id, String eventId, String serviceId, String employeeEmail, String companyId, String organizerEmail, Date date, String packageId, String startTime, String endTime, ServiceReservationStatus serviceReservationStatus) {
        this.id = id;
        this.eventId = eventId;
        this.serviceId = serviceId;
        this.employeeEmail = employeeEmail;
        this.companyId = companyId;
        this.organizerEmail = organizerEmail;
        this.date = date;
        this.packageId = packageId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.serviceReservationStatus = serviceReservationStatus;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getEmployeeEmail() {
        return employeeEmail;
    }

    public void setEmployeeEmail(String employeeEmail) {
        this.employeeEmail = employeeEmail;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public ServiceReservationStatus getServiceReservationStatus() {
        return serviceReservationStatus;
    }

    public void setServiceReservationStatus(ServiceReservationStatus serviceReservationStatus) {
        this.serviceReservationStatus = serviceReservationStatus;
    }

    public String getOrganizerEmail() {
        return organizerEmail;
    }

    public void setOrganizerEmail(String organizerEmail) {
        this.organizerEmail = organizerEmail;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public Date getStatusModifiedDate() {
        return statusModifiedDate;
    }

    public void setStatusModifiedDate(Date statusModifiedDate) {
        this.statusModifiedDate = statusModifiedDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(eventId);
        dest.writeString(serviceId);
        dest.writeString(employeeEmail);
        dest.writeString(companyId);
        dest.writeString(organizerEmail);
        dest.writeSerializable(date);
        dest.writeString(packageId);
        dest.writeString(startTime);
        dest.writeString(endTime);
        dest.writeSerializable(statusModifiedDate);
    }

    protected ServiceReservation(Parcel in) {
        id = in.readString();
        eventId = in.readString();
        serviceId = in.readString();
        employeeEmail = in.readString();
        companyId = in.readString();
        organizerEmail = in.readString();
        date = (Date) in.readSerializable();
        packageId = in.readString();
        startTime = in.readString();
        endTime = in.readString();
        statusModifiedDate = (Date) in.readSerializable();
    }

    public static final Creator<ServiceReservation> CREATOR = new Creator<ServiceReservation>() {

        @Override
        public ServiceReservation createFromParcel(Parcel in) {
            return new ServiceReservation(in);
        }

        @Override
        public ServiceReservation[] newArray(int size) {
            return new ServiceReservation[size];
        }
    };
}
