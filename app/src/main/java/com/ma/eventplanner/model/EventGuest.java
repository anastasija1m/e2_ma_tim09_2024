package com.ma.eventplanner.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

public class EventGuest implements Parcelable {
    private String id;
    private String eventId;
    private String name;
    private String surname;
    private String ageRange;
    private Boolean isInvited;
    private Boolean hasAccepted;
    private String additionalNotes;

    public EventGuest() {}
    public EventGuest(String id, String eventId, String name, String surname, String ageRange, Boolean isInvited, Boolean hasAccepted, String additionalNotes) {
        this.id = id;
        this.eventId = eventId;
        this.name = name;
        this.surname = surname;
        this.ageRange = ageRange;
        this.isInvited = isInvited;
        this.hasAccepted = hasAccepted;
        this.additionalNotes = additionalNotes;
    }

    protected EventGuest(Parcel in) {
        id = in.readString();
        eventId = in.readString();
        name = in.readString();
        surname = in.readString();
        ageRange = in.readString();
        byte tmpIsInvited = in.readByte();
        isInvited = tmpIsInvited == 0 ? null : tmpIsInvited == 1;
        byte tmpHasAccepted = in.readByte();
        hasAccepted = tmpHasAccepted == 0 ? null : tmpHasAccepted == 1;
        additionalNotes = in.readString();
    }

    public static final Creator<EventGuest> CREATOR = new Creator<EventGuest>() {
        @Override
        public EventGuest createFromParcel(Parcel in) {
            return new EventGuest(in);
        }

        @Override
        public EventGuest[] newArray(int size) {
            return new EventGuest[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAgeRange() {
        return ageRange;
    }

    public void setAgeRange(String ageRange) {
        this.ageRange = ageRange;
    }

    public Boolean getInvited() {
        return isInvited;
    }

    public void setInvited(Boolean invited) {
        isInvited = invited;
    }

    public Boolean getHasAccepted() {
        return hasAccepted;
    }

    public void setHasAccepted(Boolean hasAccepted) {
        this.hasAccepted = hasAccepted;
    }

    public String getAdditionalNotes() {
        return additionalNotes;
    }

    public void setAdditionalNotes(String additionalNotes) {
        this.additionalNotes = additionalNotes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(@NonNull Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(eventId);
        dest.writeString(name);
        dest.writeString(surname);
        dest.writeString(ageRange);
        dest.writeBoolean(isInvited);
        dest.writeBoolean(hasAccepted);
        dest.writeString(additionalNotes);
    }

    @NonNull
    @Override
    public String toString() {
        return "EventGuest{" +
                "id=" + id +
                ", eventId=" + eventId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", ageRange='" + ageRange + '\'' +
                ", isInvited=" + isInvited +
                ", hasAccepted=" + hasAccepted +
                ", additionalNotes='" + additionalNotes + '\'' +
                '}';
    }
}
