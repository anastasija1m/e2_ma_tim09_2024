package com.ma.eventplanner.model;

import android.os.Parcel;

import androidx.annotation.NonNull;

import java.util.List;

public class Product extends Item {
    private Subcategory subcategory;

    public Product() { }

    public Product(String id, String companyId, Category category, String name, String description, double price, int discount, List<Integer> images, List<EventType> events, boolean isVisible, boolean isAvailable, boolean isDeleted, Subcategory subcategory) {
        super(id, companyId, category, name, description, price, discount, images, events, isVisible, isAvailable, isDeleted);
        this.subcategory = subcategory;
    }

    public Subcategory getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(Subcategory subcategory) {
        this.subcategory = subcategory;
    }

    @NonNull
    @Override
    public String toString() {
        return "Product{" +
                "subcategory=" + subcategory +
                "} " + super.toString();
    }

    protected Product(Parcel in) {
        super(in);
        subcategory = in.readParcelable(Subcategory.class.getClassLoader());
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeParcelable(subcategory, flags);
    }

}
