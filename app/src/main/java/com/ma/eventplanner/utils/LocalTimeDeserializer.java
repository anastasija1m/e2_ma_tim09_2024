package com.ma.eventplanner.utils;

import com.google.firebase.firestore.ServerTimestamp;
import com.google.firebase.Timestamp;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeDeserializer implements JsonDeserializer<LocalTime> {

    @Override
    public LocalTime deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String timeString = json.getAsString();
        return LocalTime.parse(timeString, DateTimeFormatter.ISO_LOCAL_TIME);
    }
}
