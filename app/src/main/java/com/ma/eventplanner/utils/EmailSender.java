package com.ma.eventplanner.utils;

import android.content.Context;
import android.content.Intent;

public class EmailSender {

    public static void sendEmail(Context context, String emailAddress, String subject, String body, String type) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailAddress});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.setType(type);
        context.startActivity(Intent.createChooser(intent, "Send email..."));
    }
}
